﻿using System.Collections.Generic;
using System.Data.Entity;
using InternetProvider.BusinessObjects;
using InternetProvider.DbContext;
using InternetProvider.RepositoryInterfaces;

namespace InternetProvider.RepositoryImplementation
{
    public class PaymentRepository : IRepository<Payment>
    {
        private readonly InternetProviderContext _context;

        public PaymentRepository(InternetProviderContext dbContext)
        {
            _context = dbContext;
        }

        public IEnumerable<Payment> GetAll()
        {
            return _context.Payments;
        }

        public Payment Get(int id)
        {
            return _context.Payments.Find(id);
        }

        public void Create(Payment item)
        {
            _context.Payments.Add(item);
        }

        public void Update(Payment item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var order = _context.Payments.Find(id);
            if (order != null)
                _context.Payments.Remove(order);
        }
    }
}