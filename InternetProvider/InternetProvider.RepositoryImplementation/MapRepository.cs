﻿using System.Collections.Generic;
using System.Data.Entity;
using InternetProvider.BusinessObjects;
using InternetProvider.DbContext;
using InternetProvider.RepositoryInterfaces;

namespace InternetProvider.RepositoryImplementation
{
    public class MapRepository : IRepository<Map>
    {
        private readonly InternetProviderContext _context;

        public MapRepository(InternetProviderContext dbContext)
        {
            _context = dbContext;
        }

        public IEnumerable<Map> GetAll()
        {
            return _context.Maps;
        }

        public Map Get(int id)
        {
            return _context.Maps.Find(id);
        }

        public void Create(Map item)
        {
            _context.Maps.Add(item);
        }

        public void Update(Map item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var map = _context.Maps.Find(id);
            if (map != null)
                _context.Maps.Remove(map);
        }
    }
}