﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using InternetProvider.BusinessObjects;
using InternetProvider.DbContext;
using InternetProvider.RepositoryInterfaces;

namespace InternetProvider.RepositoryImplementation
{
    public class CommentRepository : IRepository<Comment>
    {
        private readonly InternetProviderContext _context;

        public CommentRepository(InternetProviderContext dbContext)
        {
            _context = dbContext;
        }

        public IEnumerable<Comment> GetAll()
        {
            return _context.Comments;
        }

        public Comment Get(int id)
        {
            return _context.Comments.Find(id);
        }

        public void Create(Comment item)
        {
            _context.Comments.Add(item);
        }

        public void Update(Comment item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var comment = _context.Comments.Find(id);
            if (comment != null)
                _context.Comments.Remove(comment);
        }

        public IEnumerable<Comment> CommentsByNews(int id)
        {
            return _context.Comments.ToList().Where(x => x.NewsId == id).OrderByDescending(y => y.Time);
        }

        public IEnumerable<Comment> GetTodayComments()
        {
            return _context.Comments.ToList().Where(x => x.Time.ToShortDateString() == DateTime.Now.ToShortDateString());
        }
    }
}