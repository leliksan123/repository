﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using InternetProvider.BusinessObjects;
using InternetProvider.DbContext;
using InternetProvider.RepositoryInterfaces;

namespace InternetProvider.RepositoryImplementation
{
    public class ApplicationRepository : IRepository<Application>
    {
        private readonly InternetProviderContext _context;

        public ApplicationRepository(InternetProviderContext dbContext)
        {
            _context = dbContext;
        }

        public IEnumerable<Application> GetAll()
        {
            return _context.Applications;
        }

        public Application Get(int id)
        {
            return _context.Applications.Find(id);
        }

        public void Create(Application item)
        {
            _context.Applications.Add(item);
        }

        public void Update(Application item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var application = _context.Applications.Find(id);
            if (application != null)
                _context.Applications.Remove(application);
        }

        public IEnumerable<Application> GetTodayApplications()
        {
            return
                _context.Applications.ToList()
                    .Where(x => x.ApplicationTime.ToShortDateString() == DateTime.Now.ToShortDateString());
        }

        public IEnumerable<Application> GetApplicationsSortByTime()
        {
            return _context.Applications.ToList().OrderBy(x => x.ApplicationTime);
        }

        public IEnumerable<Application> GetApplicationsSortByTimeOld()
        {
            return _context.Applications.ToList().OrderByDescending(x => x.ApplicationTime);
        }
    }
}