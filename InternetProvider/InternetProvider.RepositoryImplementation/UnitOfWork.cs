﻿using System;
using InternetProvider.DbContext;

namespace InternetProvider.RepositoryImplementation
{
    public class UnitOfWork : IDisposable
    {
        private readonly InternetProviderContext _context = new InternetProviderContext();
        private AccountRepository _accountRepository;
        private AdditionalFeatureRepository _additionalFeatureRepository;
        private AdvantageRepository _advantageRepository;
        private ApplicationRepository _applicationRepository;
        private CommentRepository _commentRepository;
        private bool _disposed;
        private FileRepository _fileRepository;
        private MapRepository _mapRepository;
        private NewsRepository _newsRepository;
        private OrderRepository _orderRepository;
        private PaymentRepository _paymentRepository;
        private PictureRepository _pictureRepository;
        private RateRepository _rateRepository;
        private ServiceRepository _serviceRepository;
        private UsersRepository _usersRepository;

        public NewsRepository News
        {
            get
            {
                if (_newsRepository == null)
                    _newsRepository = new NewsRepository(_context);
                return _newsRepository;
            }
        }

        public RateRepository Rate
        {
            get
            {
                if (_rateRepository == null)
                    _rateRepository = new RateRepository(_context);
                return _rateRepository;
            }
        }

        public UsersRepository User
        {
            get
            {
                if (_usersRepository == null)
                    _usersRepository = new UsersRepository(_context);
                return _usersRepository;
            }
        }

        public AdditionalFeatureRepository AdditionalFeature
        {
            get
            {
                if (_additionalFeatureRepository == null)
                    _additionalFeatureRepository = new AdditionalFeatureRepository(_context);
                return _additionalFeatureRepository;
            }
        }

        public ServiceRepository Service
        {
            get
            {
                if (_serviceRepository == null)
                    _serviceRepository = new ServiceRepository(_context);
                return _serviceRepository;
            }
        }

        public ApplicationRepository Application
        {
            get
            {
                if (_applicationRepository == null)
                    _applicationRepository = new ApplicationRepository(_context);
                return _applicationRepository;
            }
        }

        public AccountRepository Account
        {
            get
            {
                if (_accountRepository == null)
                    _accountRepository = new AccountRepository(_context);
                return _accountRepository;
            }
        }

        public MapRepository Map
        {
            get
            {
                if (_mapRepository == null)
                    _mapRepository = new MapRepository(_context);
                return _mapRepository;
            }
        }

        public PaymentRepository Payment
        {
            get
            {
                if (_paymentRepository == null)
                    _paymentRepository = new PaymentRepository(_context);
                return _paymentRepository;
            }
        }

        public CommentRepository Comment
        {
            get
            {
                if (_commentRepository == null)
                    _commentRepository = new CommentRepository(_context);
                return _commentRepository;
            }
        }

        public AdvantageRepository Advantage
        {
            get
            {
                if (_advantageRepository == null)
                    _advantageRepository = new AdvantageRepository(_context);
                return _advantageRepository;
            }
        }

        public FileRepository File
        {
            get
            {
                if (_fileRepository == null)
                    _fileRepository = new FileRepository(_context);
                return _fileRepository;
            }
        }

        public PictureRepository Picture
        {
            get
            {
                if (_pictureRepository == null)
                    _pictureRepository = new PictureRepository(_context);
                return _pictureRepository;
            }
        }

        public OrderRepository Order
        {
            get
            {
                if (_orderRepository == null)
                    _orderRepository = new OrderRepository(_context);
                return _orderRepository;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                    _context.Dispose();
                _disposed = true;
            }
        }
    }
}