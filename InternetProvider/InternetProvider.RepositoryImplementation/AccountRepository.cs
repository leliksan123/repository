﻿using System.Collections.Generic;
using System.Data.Entity;
using InternetProvider.BusinessObjects;
using InternetProvider.DbContext;
using InternetProvider.RepositoryInterfaces;

namespace InternetProvider.RepositoryImplementation
{
    public class AccountRepository : IRepository<Account>
    {
        private readonly InternetProviderContext _context;

        public AccountRepository(InternetProviderContext dbContext)
        {
            _context = dbContext;
        }

        public IEnumerable<Account> GetAll()
        {
            return _context.Accounts;
        }

        public Account Get(int id)
        {
            return _context.Accounts.Find(id);
        }

        public void Create(Account item)
        {
            _context.Accounts.Add(item);
        }

        public void Update(Account item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var account = _context.Accounts.Find(id);
            if (account != null)
                _context.Accounts.Remove(account);
        }
    }
}