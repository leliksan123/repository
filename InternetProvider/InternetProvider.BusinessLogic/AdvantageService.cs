﻿using System.Collections.Generic;
using System.Linq;
using InternetProvider.BusinessObjects;

namespace InternetProvider.BusinessLogic
{
    public class AdvantageService
    {
        public static List<Advantage> CutText(List<Advantage> advantagesList)
        {
            foreach (var advantage in advantagesList.Where(advantage => advantage.Text.Length > 200))
                advantage.Text = advantage.Text.Substring(0, 200);
            return advantagesList;
        }
    }
}