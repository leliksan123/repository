﻿using System;
using System.Linq;
using System.Threading;
using System.Web;
using InternetProvider.DbContext;

namespace InternetProvider.Modules
{
    public class OrdersInSystemModule : IHttpModule
    {
        private const long Interval = 85000000; //24 hours 
        public static Timer Timer;
        private static readonly object Synclock = new object();
        private static bool _sent;
        private readonly InternetProviderContext _context = new InternetProviderContext();

        public void Init(HttpApplication app)
        {
            Timer = new Timer(CheckDate, null, 0, Interval);
        }

        public void Dispose()
        {
        }

        private void CheckDate(object obj)
        {
            lock (Synclock)
            {
                var orders = _context.Orders.ToList();
                foreach (var order in orders)
                {
                    var addMonthsToOrder = order.ApplicationTime.AddMonths(1);
                    if ((DateTime.Now.ToShortDateString() == addMonthsToOrder.ToShortDateString()) && (_sent == false))
                    {
                        var account = _context.Accounts.Find(order.AccountId);
                        var rate = _context.Rates.Find(order.RateId);

                        if (account.Money >= rate.Price)
                        {
                            account.Money -= rate.Price;
                            account.Applications.First(x => x.RateId == rate.Id).Status = "Done";
                        }
                        else
                        {
                            account.Debt = account.Money - rate.Price;
                        }
                        order.ApplicationTime = DateTime.Now;
                        _context.SaveChanges();
                        _sent = true;
                    }
                    else if (DateTime.Now.ToShortDateString() != order.ApplicationTime.AddMonths(1).ToShortDateString())
                    {
                        _sent = false;
                    }
                }
            }
        }
    }
}