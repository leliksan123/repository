﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Web.Mvc;
using InternetProvider.BusinessObjects;
using InternetProvider.RepositoryImplementation;
using InternetProvider.ViewModels;
using InternetProvider.ViewModels.Payment;
using NLog;

namespace InternetProvider.Controllers
{
    [AllowAnonymous]
    public class PaymentController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly UnitOfWork _unitOfWork;

        public PaymentController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ViewResult Index()
        {
            Logger.Trace("Starting the PaymentController Index method. Previous page {0}", Request.UrlReferrer);
            var model = new PaymentViewModel {Date = DateTime.Now};

            var types = new List<Currency>();
            types.Add(new Currency { Value = 1, Text = "GRN" });
            types.Add(new Currency { Value = 25, Text = "Dollar" });
            types.Add(new Currency { Value = 28, Text = "Evro" });
            ViewBag.PartialTypes = types;

            if (User.Identity.IsAuthenticated)
            {
                var user = _unitOfWork.User.Find(User.Identity.Name);
                model.AccountId = user.Id;
                Logger.Trace("User authentication {0}", User.Identity.Name);
            }
            ViewBag.Message = TempData["message"] as string;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Success(PaymentViewModel model)
        {
            Logger.Trace("Starting the PaymentController Success Method. Previous page {0}", Request.UrlReferrer);
            if (ModelState.IsValid)
            {
                var payment = new Payment
                {
                    AccountId = model.AccountId,
                    Date = model.Date,
                    Sum = model.Sum
                };
                Logger.Info("Creating new payment AccountNumber ={0} , Money ={1}", model.AccountId, model.Sum);
                try
                {
                    _unitOfWork.Payment.Create(payment);
                    _unitOfWork.Save();
                }
                catch (DbUpdateException ex)
                {
                    Logger.Error("DbUpdateException {0}", ex.Message);
                    TempData["message"] = "This account does not exist";
                    return RedirectToAction("Index");
                }

                var user = _unitOfWork.User.Get(model.AccountId);
                user.Account.Money += model.Sum;

                _unitOfWork.Save();
                Logger.Info("User successfully added money to Account {0}", model.AccountId);
                TempData["message"] = "The account is successfully recharged";
                return RedirectToAction("Index");
            }
            Logger.Info("Recharge accountNumber {0} has failed", model.AccountId);
            return View("Index", model);
        }
    }
}