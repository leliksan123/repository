﻿using System.Data.Common;
using System.Linq;
using System.Web.Mvc;
using InternetProvider.BusinessObjects;
using InternetProvider.RepositoryImplementation;
using NLog;

namespace InternetProvider.Controllers
{
    [AllowAnonymous]
    public class AdvantageController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly UnitOfWork _unitOfWork;

        public AdvantageController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public ActionResult MoreDetails(int id)
        {
            Logger.Trace("Starting AdvantageController MoreDetails method with parameter id = {0}. Previous page {1}",
                id, Request.UrlReferrer);
            Advantage advantage;
            try
            {
                advantage = _unitOfWork.Advantage.GetAll().First(x => x.Id == id);
                Logger.Trace("Search Advantage to display with the search criteria id={0}", id);
            }
            catch (DbException ex)
            {
                Logger.Error("Advantage {0} Not found", id);
                ModelState.AddModelError("User", ex.Message);
                return View("~/Views/Error/ErrorPage.cshtml",
                    new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                        ControllerContext.RouteData.Values["action"].ToString()));
            }

            return PartialView("_FullTextAdvantage", advantage);
        }
    }
}