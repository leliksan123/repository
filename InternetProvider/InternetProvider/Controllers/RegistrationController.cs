﻿using System;
using System.Data.Entity.Infrastructure;
using System.Web.Mvc;
using System.Web.Security;
using InternetProvider.BusinessObjects;
using InternetProvider.Filters;
using InternetProvider.RepositoryImplementation;
using InternetProvider.ViewModels;
using NLog;
using WebMatrix.WebData;

namespace InternetProvider.Controllers
{
    [InitializeSimpleMembership]
    [AllowAnonymous]
    public class RegistrationController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly UnitOfWork _unitOfWork;

        public RegistrationController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [AllowAnonymous]
        public ViewResult Index()
        {
            Logger.Trace("Starting RegistrationController Index method. Previous page {0}", Request.UrlReferrer);
            ViewBag.Error = TempData["error"] as string;
            var model = new RegistrationViewModel {RegistrationTime = DateTime.Now};
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult RegisterComplited(RegistrationViewModel model)
        {
            Logger.Trace("Starting RegistrationController RegisterComplited method. Previous page {0}",
                Request.UrlReferrer);
            if (ModelState.IsValid)
                try
                {
                    WebSecurity.CreateUserAndAccount(model.Email, model.Password,
                        new
                        {
                            model.FirstName,
                            model.DateOfBirthday,
                            model.LastName,
                            model.Adress,
                            model.Phone,
                            model.RegistrationTime
                        });
                    WebSecurity.Login(model.Email, model.Password);
                    Logger.Info("User {0} successfully registered", model.Email);
                    Roles.AddUsersToRole(new[] {model.Email}, "User");
                    Logger.Info("The user is added to the role {0}", "User");
                    var account = new Account
                    {
                        Id = _unitOfWork.User.GetMaxId(),
                        Money = 0,
                        Debt = 0
                    };
                    try
                    {
                        _unitOfWork.Account.Create(account);
                        _unitOfWork.Save();
                    }
                    catch (DbUpdateException ex)
                    {
                        Logger.Error("DbUpdateException {0}", ex.Message);
                        return View("~/Views/Error/ErrorPage.cshtml",
                            new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                                ControllerContext.RouteData.Values["action"].ToString()));
                    }

                    Logger.Info("User Account {0} successfully created", model.Email);
                    return RedirectToAction("Index", "Home");
                }
                catch (MembershipCreateUserException e)
                {
                    Logger.Error("Error creating user in Membership {0}", e.Message);
                    ModelState.AddModelError("Membership", e.StatusCode.ToString());
                    TempData["error"] = "The user with the same name is already registered in the system";
                    return RedirectToAction("Index");
                }
            Logger.Info("User {0} is not created", model.Email);
            return View("Index", model);
        }
    }
}