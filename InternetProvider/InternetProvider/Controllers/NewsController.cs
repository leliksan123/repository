﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web.Mvc;
using InternetProvider.BusinessLogic;
using InternetProvider.BusinessObjects;
using InternetProvider.Models;
using InternetProvider.RepositoryImplementation;
using InternetProvider.ViewModels;
using NLog;

namespace InternetProvider.Controllers
{
    public class NewsController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly List<News> _newsList;
        private readonly UnitOfWork _unitOfWork;

        public NewsController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _newsList = _unitOfWork.News.OrderByDescendingPublicationDateGetAll().ToList();
        }

        [HttpGet]
        public ViewResult MoreDetails(int id)
        {
            Logger.Trace("Starting NewsController MoreDetails method with parameter id = {0}. Previous page {1}", id,
                Request.UrlReferrer);
            var model = new NewsCommentsViewModel();
            Logger.Trace("Searching news with parameter id={0}", id);
            var news = _unitOfWork.News.GetAll().First(x => x.Id == id);
            model.Author = news.Author;
            model.PublicationDate = news.PublicationDate;
            model.Text = news.Text;
            model.Title = news.Title;
            model.Picture = news.Picture.ToList();
            model.Time = DateTime.Now;
            model.Comments = _unitOfWork.Comment.CommentsByNews(id).ToList();
            model.Id = news.Id;

            if (User.Identity.IsAuthenticated)
                model.AuthorName = User.Identity.Name;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PostComment(NewsCommentsViewModel model)
        {
            Logger.Trace("Starting NewsController PostComment method. Previous page {0}", Request.UrlReferrer);

            if (ModelState.IsValid)
            {
                var comment = new Comment
                {
                    AuthorName = model.AuthorName,
                    Time = model.Time,
                    UserComm = model.UserComm,
                    NewsId = model.Id
                };
                try
                {
                    Logger.Info("Comment with id={0} entry in the database", comment.Id);
                    _unitOfWork.Comment.Create(comment);
                    _unitOfWork.Save();
                }
                catch (DbUpdateException ex)
                {
                    Logger.Error("DbUpdateException {0} ", ex.Message);
                    return View("~/Views/Error/ErrorPage.cshtml",
                        new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                            ControllerContext.RouteData.Values["action"].ToString()));
                }
                Logger.Info("User {0} has created a new comment", model.AuthorName);
                return RedirectToAction("MoreDetails", new {id = model.Id});
            }
            Logger.Info("New comment by author {0} was not created", model.AuthorName);
            return RedirectToAction("MoreDetails", new {id = model.Id});
        }

        public ViewResult GetAllNews(int page = 1)
        {
            Logger.Trace("Starting NewsController GetAllNews method with parameter page={0}. Previous page {1}", page,
                Request.UrlReferrer);
            var pageSize = 3;
            var newsService = new NewsService();
            var newslist = newsService.CutText(_newsList.Skip((page - 1)*pageSize).Take(pageSize).ToList());

            var pageInfo = new PageInfo {PageNumber = page, PageSize = pageSize, TotalItems = _newsList.Count};
            var model = new NewsPagesViewModel
            {
                Newses = newslist,
                PageInfo = pageInfo
            };
            return View(model);
        }
    }
}