﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using InternetProvider.Attributes;
using InternetProvider.BusinessObjects;

namespace InternetProvider.ViewModels
{
    public class ConnectViewModel
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "FirstName is required.")]
        [StringLength(100, ErrorMessage = "The FirstName must be at least 3 characters long.", MinimumLength = 3)]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Phone is required.")]
        public string Phone { get; set; }

        public string Wishes { get; set; }
        public int SelectedService { get; set; }
        public int SelectedRate { get; set; }
        public DateTime ApplicationTime { get; set; }

        [Mandatory(ErrorMessage = "You must agree to rules.")]
        public bool Agree { get; set; }

        public List<int> SelectedAdditionalFeatures { get; set; }
        public int AccountId { get; set; }
        public Account Account { get; set; }
        public List<Service> Services { get; set; }
        public List<Rate> Rates { get; set; }
        public List<AdditionalFeature> AdditionalFeatures { get; set; }
    }
}