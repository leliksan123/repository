﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using InternetProvider.BusinessObjects;

namespace InternetProvider.ViewModels
{
    public class NewsCommentsViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public DateTime PublicationDate { get; set; }
        public string Author { get; set; }

        [Required]
        public string AuthorName { get; set; }

        public DateTime Time { get; set; }

        [Required]
        public string UserComm { get; set; }

        public virtual List<Picture> Picture { get; set; }
        public virtual List<Comment> Comments { get; set; }
    }
}