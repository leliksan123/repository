﻿using System.Collections.Generic;
using InternetProvider.BusinessObjects;
using InternetProvider.Models;

namespace InternetProvider.ViewModels
{
    public class NewsPagesViewModel
    {
        public IEnumerable<News> Newses { get; set; }
        public PageInfo PageInfo { get; set; }
    }
}