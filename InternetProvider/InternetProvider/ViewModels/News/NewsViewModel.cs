﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using InternetProvider.BusinessObjects;

namespace InternetProvider.ViewModels
{
    public class NewsViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Title is required.")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Text is required.")]
        public string Text { get; set; }

        public DateTime PublicationDate { get; set; }

        [Required(ErrorMessage = "Author is required.")]
        public string Author { get; set; }

        public List<int> SelectedTags { get; set; }
        public virtual List<Picture> Picture { get; set; }
        public virtual List<Comment> Comments { get; set; }
    }
}