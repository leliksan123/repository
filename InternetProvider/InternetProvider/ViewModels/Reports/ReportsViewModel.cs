﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using InternetProvider.BusinessObjects;

namespace InternetProvider.ViewModels.Reports
{
    public class ReportsViewModel
    {
        public List<Application> Applications { get; set; }
        public List<User> Users { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
    }
}