﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InternetProvider.ViewModels.Payment
{
    public class Currency
    {
        public string Text { get; set; }
        public int Value { get; set; }
    }
}