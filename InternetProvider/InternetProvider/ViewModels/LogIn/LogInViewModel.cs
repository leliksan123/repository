﻿using System.ComponentModel.DataAnnotations;

namespace InternetProvider.ViewModels
{
    public class LogInViewModel
    {
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "Email is required.")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Invalid Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "The password can not be less than 6 and more than 50 characters",
             MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }
}