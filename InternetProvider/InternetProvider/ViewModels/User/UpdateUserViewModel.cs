﻿using System;
using System.Collections.Generic;
using InternetProvider.BusinessObjects;

namespace InternetProvider.ViewModels
{
    public class UpdateUserViewModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Adress { get; set; }
        public string Phone { get; set; }
        public List<Rate> Rates { get; set; }
        public DateTime DateOfBirthday { get; set; }
        public DateTime RegistrationTime { get; set; }
        public List<Rate> AllRates { get; set; }
        public int SelectedRate { get; set; }
        public int AccountId { get; set; }
    }
}