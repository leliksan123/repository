﻿using System.Collections.Generic;
using InternetProvider.BusinessObjects;

namespace InternetProvider.ViewModels
{
    public class RateListViewModel
    {
        public List<Rate> Rates { get; set; }
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
    }
}