﻿using System.Linq;
using System.Web.Mvc;
using InternetProvider.RepositoryImplementation;
using InternetProvider.ViewModels;
using NLog;
using WebMatrix.WebData;

namespace InternetProvider.Areas.Admin.Controllers
{
    public class AdminPanelController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly UnitOfWork _unitOfWork;

        public AdminPanelController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        [Authorize(Roles = "Admin,Moderator")]
        public ViewResult Index()
        {
            Logger.Trace("Starting AdminPanelController Index method. Previous page {0}", Request.UrlReferrer);
            var model = new AdminPanelHomeViewModel
            {
                Users = _unitOfWork.User.GetUsersRegistrationToday().ToList(),
                Comments = _unitOfWork.Comment.GetTodayComments().ToList(),
                Applications = _unitOfWork.Application.GetTodayApplications().ToList()
            };
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Index(LogInViewModel model)
        {
            Logger.Trace("Starting AdminPanelController Index method. Previous page {0}", Request.UrlReferrer);
            Logger.Trace("User Authorization");
            if (ModelState.IsValid && WebSecurity.Login(model.Email, model.Password))
            {
                Logger.Info("User {0} successfully authorized", model.Email);
                return RedirectToAction("Index", "AdminPanel");
            }
            Logger.Info("User {0} has entered an incorrect username or password", model.Email);
            TempData["error"] = "Invalid password or login";
            return RedirectToAction("Index", "Auth");
        }
    }
}