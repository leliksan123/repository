﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InternetProvider.BusinessLogic;
using InternetProvider.BusinessObjects;
using InternetProvider.RepositoryImplementation;
using InternetProvider.ViewModels;
using NLog;

namespace InternetProvider.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin,Moderator")]
    public class NewsController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly UnitOfWork _unitOfWork;

        public NewsController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ViewResult Index()
        {
            Logger.Trace("Starting NewsController Index method. Previous page {0}", Request.UrlReferrer);
            return View();
        }

        public PartialViewResult Create()
        {
            Logger.Trace("Starting NewsController Create method. Previous page {0}", Request.UrlReferrer);
            ViewBag.Message = TempData["message"] as string;
            var model = new NewsViewModel();
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(NewsViewModel model, HttpPostedFileBase upload)
        {
            Logger.Trace("Starting NewsController Save method. Previous page {0}", Request.UrlReferrer);
            var newsService = new NewsService();

            if (ModelState.IsValid)
            {
                Logger.Info("Checking the file extension {0}", upload.FileName);
                if (NewsService.Checkextension(upload) == 0)
                {
                    Logger.Info("Uploaded file with an incorrect extension {0}", upload.FileName);
                    TempData["message"] = "Upload file .jpeg , .jpg , .png";
                    return RedirectToAction("Create", "News");
                }

                var picture = new Picture
                {
                    PicturePath = NewsService.CreatePicture(upload) + upload.FileName
                };

                var news = new News
                {
                    Id = model.Id,
                    Title = model.Title,
                    Author = model.Author,
                    PublicationDate = DateTime.Now,
                    Picture = new List<Picture> {picture},
                    Text = model.Text
                };
                try
                {
                    _unitOfWork.News.Create(news);
                    _unitOfWork.Save();
                }
                catch (DbException ex)
                {
                    Logger.Error("DbException {0}", ex.Message);
                    return View("~/Views/Error/ErrorPage.cshtml",
                        new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                            ControllerContext.RouteData.Values["action"].ToString()));
                }

                Logger.Info("News {0} created successfully", model.Title);
                TempData["message"] = "News created successfully";
                return RedirectToAction("Create", "News");
            }
            Logger.Info("News {0} not created", model.Title);
            return RedirectToAction("Create", "News");
        }

        public PartialViewResult NewsList()
        {
            Logger.Trace("Starting NewsController NewsList method. Previous page {0}", Request.UrlReferrer);
            var newsService = new NewsService();
            ViewBag.Message = TempData["message"] as string;
            var model = new NewsListViewModel {Newses = NewsService.CutTextPreview(_unitOfWork.News.GetAll().ToList())};
            return PartialView(model);
        }

        public PartialViewResult NewsListDelete()
        {
            Logger.Trace("Starting NewsController NewsListDelete method. Previous page {0}", Request.UrlReferrer);
            var newsService = new NewsService();
            ViewBag.Message = TempData["message"] as string;
            var model = new NewsListViewModel {Newses = NewsService.CutTextPreview(_unitOfWork.News.GetAll().ToList())};
            return PartialView(model);
        }

        public PartialViewResult Update(int id)
        {
            Logger.Trace("Starting NewsController Update method with the parameter id = {0}. Previous page {1}", id,
                Request.UrlReferrer);
            Logger.Trace("Searching News with Id={0}", id);
            var news = _unitOfWork.News.Get(id);
            var model = new NewsViewModel
            {
                Id = news.Id,
                Author = news.Author,
                Picture = news.Picture.ToList(),
                Text = news.Text,
                Title = news.Title,
                PublicationDate = news.PublicationDate
            };
            return PartialView(model);
        }

        public ActionResult Delete(int id)
        {
            Logger.Trace("Starting NewsController Delete method with the parameter id = {0}. Previous page {1}", id,
                Request.UrlReferrer);
            Logger.Trace("Searching News with Id={0}", id);
            var news = _unitOfWork.News.Get(id);
            _unitOfWork.News.Delete(id);

            var picture = _unitOfWork.Picture.GetAll().FirstOrDefault(x => x.NewsId == id);
            var comments = _unitOfWork.Comment.GetAll().Where(x => x.NewsId == id).ToList();
            for (var i = 0; i < comments.Count(); i++)
                _unitOfWork.Comment.Delete(i);
            if (picture != null) _unitOfWork.Picture.Delete(picture.Id);

            try
            {
                _unitOfWork.Save();
            }
            catch (DbException ex)
            {
                Logger.Error("DbException {0}", ex.Message);
                return View("~/Views/Error/ErrorPage.cshtml",
                    new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                        ControllerContext.RouteData.Values["action"].ToString()));
            }
            Logger.Info("News {0}, as well as comments to the news and the picture removed", news.Title);
            TempData["message"] = news.Title + "successfully deleted";
            return RedirectToAction("NewsListDelete", "News");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(NewsViewModel model)
        {
            Logger.Trace("Starting NewsController Create method. Previous page {0}", Request.UrlReferrer);
            var pictures = model.Picture.Select(a => _unitOfWork.Picture.Get(a.Id)).ToList();
            if (ModelState.IsValid)
            {
                var news = new News
                {
                    Id = model.Id,
                    Title = model.Title,
                    Author = model.Author,
                    PublicationDate = model.PublicationDate,
                    Picture = pictures,
                    Text = model.Text
                };

                try
                {
                    _unitOfWork.News.Update(news);
                    _unitOfWork.Save();
                }
                catch (DbUpdateException ex)
                {
                    Logger.Error("DbUpdateException {0}", ex.Message);
                    return View("~/Views/Error/ErrorPage.cshtml",
                        new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                            ControllerContext.RouteData.Values["action"].ToString()));
                }

                Logger.Info("News {0} successfully edited", model.Title);
                TempData["message"] = news.Title + "successfully edited";
                return RedirectToAction("NewsList", "News");
            }
            Logger.Info("News {0} not edited", model.Title);
            TempData["message"] = "Element not edited";
            return RedirectToAction("NewsList", "News");
        }
    }
}