﻿using System.Data.Common;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InternetProvider.BusinessLogic;
using InternetProvider.BusinessObjects;
using InternetProvider.RepositoryImplementation;
using InternetProvider.ViewModels;
using NLog;

namespace InternetProvider.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class RatesController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly UnitOfWork _unitOfWork;

        public RatesController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ViewResult Index()
        {
            Logger.Trace("Starting RateController Index method. Previous page {0}", Request.UrlReferrer);
            return View();
        }

        public PartialViewResult Create()
        {
            Logger.Trace("Starting RateController Create method. Previous page {0}", Request.UrlReferrer);
            ViewBag.Message = TempData["message"] as string;
            var model = new RateViewModel {Services = _unitOfWork.Service.GetAll().ToList()};
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(RateViewModel model, HttpPostedFileBase uploadFile)
        {
            Logger.Trace("Starting RateController Save method. Previous page {0}", Request.UrlReferrer);
            var rateService = new RateService();
            if (ModelState.IsValid)
            {
                Logger.Info("Checking the file extension {0}", uploadFile.FileName);
                if (rateService.UploadFile(uploadFile) == false)
                {
                    Logger.Info("Uploaded file with an incorrect extension {0}", uploadFile.FileName);
                    TempData["message"] = "Upload file .pdf";
                    return RedirectToAction("Create", "Rates");
                }

                var file = new File
                {
                    Name = uploadFile.FileName
                };

                var rate = new Rate
                {
                    Description = model.Description,
                    Name = model.Name,
                    Price = model.Price,
                    ServiceId = model.SelectedService,
                    File = file
                };
                try
                {
                    _unitOfWork.Rate.Create(rate);
                    _unitOfWork.Save();
                }
                catch (DbUpdateException ex)
                {
                    Logger.Error("DbUpdateException {0}", ex.Message);
                    return View("~/Views/Error/ErrorPage.cshtml",
                        new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                            ControllerContext.RouteData.Values["action"].ToString()));
                }

                Logger.Info("Rate {0} created successfully", model.Name);

                TempData["message"] = "Rate created successfully";

                return RedirectToAction("Create", "Rates");
            }
            Logger.Info("Rate {0} not created", model.Name);
            return RedirectToAction("Create", "Rates");
        }

        public PartialViewResult RateList()
        {
            Logger.Trace("Starting RateController RateList method. Previous page {0}", Request.UrlReferrer);
            ViewBag.Message = TempData["message"] as string;
            var model = new RateListViewModel {Rates = _unitOfWork.Rate.GetAll().ToList()};
            return PartialView(model);
        }

        public PartialViewResult RateListDelete()
        {
            Logger.Trace("Starting RateController RateListDelete method. Previous page {0}", Request.UrlReferrer);
            ViewBag.Message = TempData["message"] as string;
            var model = new RateListViewModel {Rates = _unitOfWork.Rate.GetAll().ToList()};
            return PartialView(model);
        }

        public PartialViewResult Update(int id)
        {
            Logger.Trace("Starting RateController Update method with the parameter id = {0}. Previous {1}", id,
                Request.UrlReferrer);
            var rate = _unitOfWork.Rate.Get(id);
            Logger.Trace("Searching rate with Id={0}", id);
            var model = new RateViewModel
            {
                Description = rate.Description,
                Name = rate.Name,
                Price = rate.Price,
                Id = rate.Id,
                File = rate.File,
                Services = _unitOfWork.Service.GetAll().ToList()
            };
            return PartialView(model);
        }

        public ActionResult Delete(int id)
        {
            Logger.Trace("Starting RateController Delete method with the parameter id = {0}. Previous page {1}", id,
                Request.UrlReferrer);
            var rate = _unitOfWork.Rate.Get(id);
            Logger.Trace("Searching rate with Id={0}", id);
            try
            {
                _unitOfWork.Rate.Delete(id);
                _unitOfWork.File.Delete(id);
                _unitOfWork.Save();
            }
            catch (DbException ex)
            {
                Logger.Error("DbException {0}", ex.Message);
                return View("~/Views/Error/ErrorPage.cshtml",
                    new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                        ControllerContext.RouteData.Values["action"].ToString()));
            }

            Logger.Info("Rate {0} successfully deleted", rate.Name);
            TempData["message"] = rate.Name + "successfully deleted";
            return RedirectToAction("RateListDelete", "Rates");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RateViewModel model)
        {
            Logger.Trace("Starting RateController Create method. Previous page {0}", Request.UrlReferrer);
            var file = _unitOfWork.File.Get(model.File.Id);
            if (ModelState.IsValid)
            {
                var rate = new Rate
                {
                    Id = model.Id,
                    Description = model.Description,
                    Name = model.Name,
                    Price = model.Price,
                    ServiceId = model.SelectedService,
                    File = file
                };
                try
                {
                    _unitOfWork.Rate.Update(rate);
                    _unitOfWork.Save();
                }
                catch (DbUpdateException ex)
                {
                    Logger.Error("DbUpdateException {0}", ex.Message);
                    return View("~/Views/Error/ErrorPage.cshtml",
                        new HandleErrorInfo(ex, ControllerContext.RouteData.Values["controller"].ToString(),
                            ControllerContext.RouteData.Values["action"].ToString()));
                }

                Logger.Info("Rate {0} successfully edited", rate.Name);
                TempData["message"] = rate.Name + "successfully edited";
                return RedirectToAction("RateList", "Rates");
            }
            Logger.Info("Rate {0} not edited", model.Name);
            TempData["message"] = "Element not edited";
            return RedirectToAction("RateList", "Rates");
        }
    }
}