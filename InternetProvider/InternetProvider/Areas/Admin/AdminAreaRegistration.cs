﻿using System.Web.Mvc;

namespace InternetProvider.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration
    {
        public override string AreaName => "Admin";

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Admin",
                "Admin/{controller}/{action}/{id}",
                new {area = "Admin", controller = "Auth", action = "Index", id = UrlParameter.Optional},
                new[] {"InternetProvider.Areas.Admin.Controllers"}
            );
        }
    }
}