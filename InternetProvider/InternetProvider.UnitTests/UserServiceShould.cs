﻿using FluentAssertions;
using InternetProvider.BusinessLogic;
using NUnit.Framework;

namespace InternetProvider.UnitTests
{
    [TestFixture]
    public class UserServiceShould
    {
        public void GetPassword()
        {
            UserService.GetPassword().Should().BeOfType(typeof(string));

            UserService.GetPassword().Should().HaveLength(8);

            UserService.GetPassword().Should().NotBeEmpty();

            UserService.GetPassword().Should().NotContain("$%#*&@");
        }
    }
}