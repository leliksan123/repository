﻿using System.Collections.Generic;
using FluentAssertions;
using InternetProvider.BusinessLogic;
using InternetProvider.BusinessObjects;
using NUnit.Framework;

namespace InternetProvider.UnitTests
{
    [TestFixture]
    public class AdvantageServiceShould
    {
        public void CutTextList()
        {
            var list = new List<Advantage>
            {
                new Advantage {Id = 1, Name = "Free Phone", Text = "abcd"},
                new Advantage {Id = 2, Name = "Free IPTV", Text = "TDD"},
                new Advantage
                {
                    Id = 3,
                    Name = "Free IPTV",
                    Text = "The class in Listing 2 includes a test method named TestDetailsView(). " +
                           "This method contains three lines of code. The first line of code creates a " +
                           "new instance of the ProductController class. The second line of code invokes the " +
                           "controller’s Details() action method. Finally, the last line of code checks whether or " +
                           "not the view returned by the Details() action is the Details view." +
                           "not the view returned by the Details() action is the Details view."
                }
            };

            AdvantageService.CutText(list).Should().NotBeEmpty();

            AdvantageService.CutText(list).Should().NotContain(x => x.Text.Length > 200);
        }
    }
}