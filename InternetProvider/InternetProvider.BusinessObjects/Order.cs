﻿using System;

namespace InternetProvider.BusinessObjects
{
    public class Order
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public DateTime ApplicationTime { get; set; }
        public int RateId { get; set; }
    }
}