﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace InternetProvider.BusinessObjects
{
    public class User
    {
        public int Id { get; set; }
        public string Email { get; set; }

        [Column(TypeName = "date")]
        public DateTime RegistrationTime { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        [Column(TypeName = "date")]
        public DateTime DateOfBirthday { get; set; }

        public string Adress { get; set; }
        public string Phone { get; set; }
        public virtual Account Account { get; set; }
    }
}