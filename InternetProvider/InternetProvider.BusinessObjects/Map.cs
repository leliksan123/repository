﻿namespace InternetProvider.BusinessObjects
{
    public class Map
    {
        public int Id { get; set; }
        public double GeoLong { get; set; }
        public double GeoLat { get; set; }
    }
}