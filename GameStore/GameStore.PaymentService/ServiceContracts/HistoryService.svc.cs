﻿using System;
using System.IO;
using System.Web.Script.Serialization;
using GameStore.PaymentService.Interfaces;

namespace GameStore.PaymentService.ServiceContracts
{
    public class HistoryService : IHistoryService
    {
        private const string FileName = "History.txt";

        public void AddToHistory<T>(T transfer)
        {
            var path = AppDomain.CurrentDomain.BaseDirectory;
            var json = new JavaScriptSerializer().Serialize(transfer);
            File.WriteAllText(path + FileName, json);
        }
    }
}