﻿using System;
using System.Linq;
using GameStore.Extensions.Enums;
using GameStore.PaymentService.DataContracts;
using GameStore.PaymentService.Interfaces;
using GameStore.Tools.EmailSender;

namespace GameStore.PaymentService.ServiceContracts
{
    public class PaymentService : IPaymentService
    {
        private readonly HistoryService _history = new HistoryService();
        private readonly EmailSender _emailSender = new EmailSender();

        public string GetData(int value)
        {
            return $"You entered: {value}";
        }

        public PaymentStatus Pay(PaymentInfo model)
        {
            PaymentStatus result;

            if (!string.IsNullOrEmpty(model.Phone))
            {
                var code = GenerateCode();
                SendSms(code);
            }

            if (!string.IsNullOrEmpty(model.Email))
            {
                _emailSender.SendEmail(model.Email , "PaymentService").GetAwaiter();
            }

            if (!IfCardExists(model.CardNumber))
            {
                _history.AddToHistory($"Card with number {model.CardNumber} not exist");
                return PaymentStatus.NotExists;
            }

            switch (model.CardTypes)
            {
                case CardTypes.Visa:
                    result = VisaPayment(model.CardNumber, model.AmounthOfPayment);
                    break;
                case CardTypes.MasterCard:
                    result = MasterCardPayment(model.CardNumber, model.AmounthOfPayment);
                    break;
                default:
                    throw new NotSupportedException();
            }

            _history.AddToHistory($"User with cardNumber {model.CardNumber} created new transaction : Date {DateTime.UtcNow} Money: {model.AmounthOfPayment} Status: {result}");
            return result;
        }

        public bool IfCardExists(string cardNumber)
        {
            var cards = StubData.GetAccounts();
            var result = cards.Any(card => cardNumber.Replace(" ", string.Empty) == card.CardNumber);

            return result;
        }

        private PaymentStatus VisaPayment(string cardNumber, decimal amountOfPayment)
        {
            var money = StubData.GetAccounts().First(x => x.CardNumber == cardNumber);
            var status = amountOfPayment > money.AmounthOfMoney ? PaymentStatus.NotEnough : PaymentStatus.Successful;

            return status;
        }

        private PaymentStatus MasterCardPayment(string cardNumber, decimal amountOfPayment)
        {
            var money = StubData.GetAccounts().First(x => x.CardNumber == cardNumber);
            var status = amountOfPayment > money.AmounthOfMoney ? PaymentStatus.NotEnough : PaymentStatus.Successful;

            return status;
        }

        private void SendSms(int secureCode)
        {
            var sms = $"SMS has been sent. Your secureCode: {secureCode}";
        }

        private int GenerateCode()
        {
            var rand = new Random(DateTime.Now.Millisecond);
            return rand.Next();
        }
    }
}