﻿using System.Runtime.Serialization;

namespace GameStore.PaymentService.DataContracts
{
    [DataContract]
    public class User
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string SurName { get; set; }

        [DataMember]
        public int AccountNumber { get; set; }

        [DataMember]
        public string Email { get; set; }
    }
}