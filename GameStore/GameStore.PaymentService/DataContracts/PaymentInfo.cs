﻿using System.Runtime.Serialization;
using GameStore.Extensions.Enums;

namespace GameStore.PaymentService.DataContracts
{
    [DataContract]
    public class PaymentInfo
    {
        [DataMember]
        public int Month { get; set; }

        [DataMember]
        public int Year { get; set; }

        [DataMember]
        public int Cvv2 { get; set; }

        [DataMember]
        public string CardNumber { get; set; }

        [DataMember]
        public string FullName { get; set; }

        [DataMember]
        public CardTypes CardTypes { get; set; }

        [DataMember]
        public decimal AmounthOfPayment { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string Purpose { get; set; }
    }
}