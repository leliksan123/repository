﻿using System.Runtime.Serialization;

namespace GameStore.PaymentService.DataContracts
{
    [DataContract]
    public class Account
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int AccountOwner { get; set; }

        [DataMember]
        public decimal AmounthOfMoney { get; set; }

        [DataMember]
        public string CardNumber { get; set; }
    }
}