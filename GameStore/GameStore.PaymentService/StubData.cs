﻿using System.Collections.Generic;
using GameStore.PaymentService.DataContracts;

namespace GameStore.PaymentService
{
    public static class StubData
    {
        public static IEnumerable<User> GetUsers()
        {
            var users = new List<User>
            {
                new User {Id = 1, AccountNumber = 1, Email = "aaa@mail.ru", Name = "Vasia", SurName = "Yes"},
                new User {Id = 2, AccountNumber = 2, Email = "bbbb@mail.ru", Name = "Oleg", SurName = "Yes"},
                new User {Id = 3, AccountNumber = 3, Email = "ccc@mail.ru", Name = "Dima", SurName = "Yes"},
                new User {Id = 4, AccountNumber = 4, Email = "dddd@mail.ru", Name = "Bob", SurName = "No"} ,
                new User {Id = 5, AccountNumber = 5, Email = "acxz@mail.ru", Name = "Dima", SurName = "Yes"},
                new User {Id = 6, AccountNumber = 6, Email = "qwert@mail.ru", Name = "Bob", SurName = "No"}
            };
            return users;
        }

        public static IEnumerable<Account> GetAccounts()
        {
            var accounts = new List<Account>
            {
                new Account {Id = 1, AccountOwner = 1, AmounthOfMoney = 50 , CardNumber = "4556945911798219"},
                new Account {Id = 2, AccountOwner = 2, AmounthOfMoney = 59 , CardNumber = "4485786146094263"},
                new Account {Id = 3, AccountOwner = 3, AmounthOfMoney = 700 , CardNumber = "4735717413850374"},
                new Account {Id = 4, AccountOwner = 4, AmounthOfMoney = 100 , CardNumber = "4539855429894552"},
                new Account {Id = 5, AccountOwner = 5, AmounthOfMoney = 500 , CardNumber = "4485958238817089"},
                new Account {Id = 6, AccountOwner = 6, AmounthOfMoney = 250 , CardNumber = "4485823174192642"},
            };
            return accounts;
        }
    }
}