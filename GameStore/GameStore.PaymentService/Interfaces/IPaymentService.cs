﻿using System.ServiceModel;
using GameStore.Extensions.Enums;
using GameStore.PaymentService.DataContracts;

namespace GameStore.PaymentService.Interfaces
{
    [ServiceContract]
    public interface IPaymentService
    {

        [OperationContract]
        string GetData(int value);

        [OperationContract]
        PaymentStatus Pay(PaymentInfo model);

        [OperationContract]
        bool IfCardExists(string cardNumber);
    }
}