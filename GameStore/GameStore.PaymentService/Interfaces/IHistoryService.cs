﻿using System.ServiceModel;

namespace GameStore.PaymentService.Interfaces
{
    [ServiceContract]
    public interface IHistoryService
    {
        [OperationContract]
        void AddToHistory<T>(T transfer);
    }
}