using System.Web;
using System.Web.Routing;
using Moq;
using NUnit.Framework;

namespace GameStore.UnitTests.Routes
{
    [TestFixture]
    public class RoutesTests
    {

        private RouteCollection _routeCollection;
        private Mock<HttpRequestBase> _httpRequestBase;
        private Mock<HttpContextBase> _httpContextBase;

        [SetUp]
        public void SetUp()
        {
            _httpRequestBase = new Mock<HttpRequestBase>();
            _httpContextBase = new Mock<HttpContextBase>();
            _httpContextBase.Setup(c => c.Request).Returns(_httpRequestBase.Object);
            _routeCollection = new RouteCollection();
            RouteConfig.RegisterRoutes(_routeCollection);
        }

        [Test]
        [TestCase("~/en/basket")]
        public void RouteBusket_CheckRouteData(string url)
        {
            // Arrange
            _httpRequestBase.Setup(req => req.AppRelativeCurrentExecutionFilePath).Returns(url);

            // Act
            var routeData = _routeCollection.GetRouteData(_httpContextBase.Object);

            // Assert
            Assert.IsNotNull(routeData);
            Assert.AreEqual("Basket", routeData.Values["controller"]);
            Assert.AreEqual("Index", routeData.Values["action"]);
        }

        [Test]
        [TestCase("~/en/basket/Delete")]
        public void RouteBusketDelete_CheckRouteData(string url)
        {
            // Arrange
            _httpRequestBase.Setup(req => req.AppRelativeCurrentExecutionFilePath).Returns(url);

            // Act
            var routeData = _routeCollection.GetRouteData(_httpContextBase.Object);

            // Assert
            Assert.IsNotNull(routeData);
            Assert.AreEqual("Basket", routeData.Values["controller"]);
            Assert.AreEqual("Delete", routeData.Values["action"]);
        }

        [Test]
        [TestCase("~/en/orders/CreateOrder")]
        public void RouteCreateOrder_CheckRouteData(string url)
        {
            // Arrange
            _httpRequestBase.Setup(req => req.AppRelativeCurrentExecutionFilePath).Returns(url);

            // Act
            var routeData = _routeCollection.GetRouteData(_httpContextBase.Object);

            // Assert
            Assert.IsNotNull(routeData);
            Assert.AreEqual("Order", routeData.Values["controller"]);
            Assert.AreEqual("CreateOrder", routeData.Values["action"]);
        }

        [Test]
        [TestCase("~/en/game/123/newcomment")]
        [TestCase("~/en/game/Azep45/comments")]
        public void RouteGameComments_CheckRouteData(string url)
        {
            // Arrange
            _httpRequestBase.Setup(req => req.AppRelativeCurrentExecutionFilePath).Returns(url);

            // Act
            var routeData = _routeCollection.GetRouteData(_httpContextBase.Object);

            // Assert
            Assert.IsNotNull(routeData);
            Assert.AreEqual("Comment", routeData.Values["controller"]);
            Assert.IsNotNull(routeData.Values["key"]);
        }

        [Test]
        [TestCase("~/en/game/123")]
        [TestCase("~/en/game/A2")]
        public void RouteGameDetails_CheckRouteDate(string url)
        {
            // Arrange
            _httpRequestBase.Setup(req => req.AppRelativeCurrentExecutionFilePath).Returns(url);

            // Act
            var routeData = _routeCollection.GetRouteData(_httpContextBase.Object);

            // Assert
            Assert.IsNotNull(routeData);
            Assert.AreEqual("Game", routeData.Values["controller"]);
            Assert.AreEqual("Details", routeData.Values["action"]);
            Assert.IsNotNull(routeData.Values["key"]);
        }

        [Test]
        [TestCase("~/en/game/123/download")]
        [TestCase("~/en/game/Azep45/download")]
        public void RouteGameDownload_CheckRouteData(string url)
        {
            // Arrange
            _httpRequestBase.Setup(req => req.AppRelativeCurrentExecutionFilePath).Returns(url);

            // Act
            var routeData = _routeCollection.GetRouteData(_httpContextBase.Object);

            // Assert
            Assert.IsNotNull(routeData);
            Assert.AreEqual("Game", routeData.Values["controller"]);
            Assert.AreEqual("Download", routeData.Values["action"]);
            Assert.IsNotNull(routeData.Values["key"]);
        }

        [Test]
        [TestCase("~/en/game/123/Buy")]
        public void RouteGamesBuy_CheckRouteData(string url)
        {
            // Arrange
            _httpRequestBase.Setup(req => req.AppRelativeCurrentExecutionFilePath).Returns(url);

            // Act
            var routeData = _routeCollection.GetRouteData(_httpContextBase.Object);

            // Assert
            Assert.IsNotNull(routeData);
            Assert.AreEqual("Game", routeData.Values["controller"]);
            Assert.AreEqual("Buy", routeData.Values["action"]);
            Assert.IsNotNull(routeData.Values["key"]);
        }

        [Test]
        [TestCase("~/en/Games/DownloadFile")]
        public void RouteGamesDownloadFile_CheckRouteData(string url)
        {
            // Arrange
            _httpRequestBase.Setup(req => req.AppRelativeCurrentExecutionFilePath).Returns(url);

            // Act
            var routeData = _routeCollection.GetRouteData(_httpContextBase.Object);

            // Assert
            Assert.IsNotNull(routeData);
            Assert.AreEqual("Game", routeData.Values["controller"]);
            Assert.AreEqual("DownloadFile", routeData.Values["action"]);
        }

        [Test]
        [TestCase("~/en/Games")]
        public void RouteGamesList_CheckRouteData(string url)
        {
            // Arrange
            _httpRequestBase.Setup(req => req.AppRelativeCurrentExecutionFilePath).Returns(url);

            // Act
            var routeData = _routeCollection.GetRouteData(_httpContextBase.Object);

            // Assert
            Assert.IsNotNull(routeData);
            Assert.AreEqual("Game", routeData.Values["controller"]);
            Assert.AreEqual("Index", routeData.Values["action"]);
        }

        [Test]
        [TestCase("~/en/Games/New")]
        public void RouteGamesNew_CheckRouteData(string url)
        {
            // Arrange
            _httpRequestBase.Setup(req => req.AppRelativeCurrentExecutionFilePath).Returns(url);

            // Act
            var routeData = _routeCollection.GetRouteData(_httpContextBase.Object);

            // Assert
            Assert.IsNotNull(routeData);
            Assert.AreEqual("Game", routeData.Values["controller"]);
            Assert.AreEqual("New", routeData.Values["action"]);
        }


        [Test]
        [TestCase("~/en/Games/Remove")]
        public void RouteGamesRemove_CheckRouteData(string url)
        {
            // Arrange
            _httpRequestBase.Setup(req => req.AppRelativeCurrentExecutionFilePath).Returns(url);

            // Act
            var routeData = _routeCollection.GetRouteData(_httpContextBase.Object);

            // Assert
            Assert.IsNotNull(routeData);
            Assert.AreEqual("Game", routeData.Values["controller"]);
            Assert.AreEqual("Remove", routeData.Values["action"]);
        }

        [Test]
        [TestCase("~/en/Games/Update")]
        public void RouteGamesUpdate_CheckRouteData(string url)
        {
            // Arrange
            _httpRequestBase.Setup(req => req.AppRelativeCurrentExecutionFilePath).Returns(url);

            // Act
            var routeData = _routeCollection.GetRouteData(_httpContextBase.Object);

            // Assert
            Assert.IsNotNull(routeData);
            Assert.AreEqual("Game", routeData.Values["controller"]);
            Assert.AreEqual("Update", routeData.Values["action"]);
        }

        [Test]
        [TestCase("~/en/orders/MakeOrder")]
        public void RouteMakeOrder_CheckRouteData(string url)
        {
            // Arrange
            _httpRequestBase.Setup(req => req.AppRelativeCurrentExecutionFilePath).Returns(url);

            // Act
            var routeData = _routeCollection.GetRouteData(_httpContextBase.Object);

            // Assert
            Assert.IsNotNull(routeData);
            Assert.AreEqual("Order", routeData.Values["controller"]);
            Assert.AreEqual("MakeOrder", routeData.Values["action"]);
        }

        [Test]
        [TestCase("~/en/orders/PaymentTable")]
        public void RouteOrderPaymentTable_CheckRouteData(string url)
        {
            // Arrange
            _httpRequestBase.Setup(req => req.AppRelativeCurrentExecutionFilePath).Returns(url);

            // Act
            var routeData = _routeCollection.GetRouteData(_httpContextBase.Object);

            // Assert
            Assert.IsNotNull(routeData);
            Assert.AreEqual("Order", routeData.Values["controller"]);
            Assert.AreEqual("PaymentTable", routeData.Values["action"]);
        }

        [Test]
        [TestCase("~/en/publisher/Valve")]
        public void RoutePublisherDetails_CheckRouteData(string url)
        {
            // Arrange
            _httpRequestBase.Setup(req => req.AppRelativeCurrentExecutionFilePath).Returns(url);

            // Act
            var routeData = _routeCollection.GetRouteData(_httpContextBase.Object);

            // Assert
            Assert.IsNotNull(routeData);
            Assert.AreEqual("Publisher", routeData.Values["controller"]);
            Assert.AreEqual("Details", routeData.Values["action"]);
        }

        [Test]
        [TestCase("~/en/orders/UserInformation")]
        public void RouteUserInformation_CheckRouteData(string url)
        {
            // Arrange
            _httpRequestBase.Setup(req => req.AppRelativeCurrentExecutionFilePath).Returns(url);

            // Act
            var routeData = _routeCollection.GetRouteData(_httpContextBase.Object);

            // Assert
            Assert.IsNotNull(routeData);
            Assert.AreEqual("Order", routeData.Values["controller"]);
            Assert.AreEqual("UserInformation", routeData.Values["action"]);
        }

        [Test]
        [TestCase("~/en/orders/VisaPage")]
        public void RouteVisaPage_CheckRouteData(string url)
        {
            // Arrange
            _httpRequestBase.Setup(req => req.AppRelativeCurrentExecutionFilePath).Returns(url);

            // Act
            var routeData = _routeCollection.GetRouteData(_httpContextBase.Object);

            // Assert
            Assert.IsNotNull(routeData);
            Assert.AreEqual("Order", routeData.Values["controller"]);
            Assert.AreEqual("VisaPage", routeData.Values["action"]);
        }
    }
}