﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using GameStore.Areas.Admin;
using Moq;
using NUnit.Framework;

namespace GameStore.UnitTests.Routes
{
    [TestFixture]
    public class AdminPanelRoutesTests
    {
        private RouteCollection _routeCollection;
        private Mock<HttpContextBase> _httpContextBase;

        [SetUp]
        public void SetUp()
        {
            _routeCollection = new RouteCollection();
            var areaRegistration = new AdminAreaRegistration();
            var areaRegistrationContext = new AreaRegistrationContext(areaRegistration.AreaName, _routeCollection);
            areaRegistration.RegisterArea(areaRegistrationContext);
            _httpContextBase = new Mock<HttpContextBase>();
        }

        [Test]
        [TestCase("~/admin/en/publisher/New")]
        public void RouteCreateNewPublisher_CheckRouteData(string url)
        {
            // Arrange
            _httpContextBase.Setup(c => c.Request.AppRelativeCurrentExecutionFilePath).Returns(url);

            // Act
            var routeData = _routeCollection.GetRouteData(_httpContextBase.Object);

            // Assert
            Assert.IsNotNull(routeData);
            Assert.AreEqual("Admin", routeData.DataTokens["area"]);
            Assert.AreEqual("Publisher", routeData.Values["controller"]);
            Assert.AreEqual("New", routeData.Values["action"]);
        }

        [Test]
        [TestCase("~/admin/en/User/BanUser/1")]
        public void RouteBanUser_CheckRouteData(string url)
        {
            // Arrange
            _httpContextBase.Setup(c => c.Request.AppRelativeCurrentExecutionFilePath).Returns(url);

            // Act
            var routeData = _routeCollection.GetRouteData(_httpContextBase.Object);

            // Assert
            Assert.IsNotNull(routeData);
            Assert.AreEqual("Admin", routeData.DataTokens["area"]);
            Assert.AreEqual("User", routeData.Values["controller"]);
            Assert.AreEqual("BanUser", routeData.Values["action"]);
        }
    }
}
