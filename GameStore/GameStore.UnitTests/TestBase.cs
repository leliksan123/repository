﻿using System.Linq;
using AutoMapper;
using GameStore.MapperConfiguration.Mapping;
using Ploeh.AutoFixture;

namespace GameStore.UnitTests
{
    public class TestBase
    {
        protected Fixture Fixture { get; private set; }

        protected IMapper Mapper { get; private set; }

        public virtual void SetUp()
        {
            Fixture = new Fixture();

            Fixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList().ForEach(b => Fixture.Behaviors.Remove(b));
            Fixture.Behaviors.Add(new OmitOnRecursionBehavior());

            Mapper = new Config().Configure().CreateMapper();
        }
    }
}