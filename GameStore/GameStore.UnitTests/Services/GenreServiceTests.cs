﻿using System.Collections.Generic;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Mongo;
using GameStore.DAL.Interfaces;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.Services.Services;
using GameStore.Tools.Logger;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace GameStore.UnitTests.Services
{
    [TestFixture]
    public class GenreServicesTests : TestBase
    {
        private Mock<ILoggerFileService> _logger;
        private Mock<IUnitOfWork> _unitOfWork;
        private Mock<IValidationService> _validationService;

        private GenreService _genreService;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            _unitOfWork = new Mock<IUnitOfWork>();
            _validationService = new Mock<IValidationService>();
            _logger = new Mock<ILoggerFileService>();
            _genreService = new GenreService(_unitOfWork.Object, _logger.Object, Mapper, _validationService.Object);
        }

        [Test]
        public void Create_ShouldCreateNewGenre_WhenModelIsValid()
        {
            //Arrange
            var genre = Fixture.Create<GenreDto>();
            var item = Fixture.Create<Genre>();

            _unitOfWork.Setup(x => x.Genres.Insert(item));

            // Act
            _genreService.Create(genre);

            // Assert
            _unitOfWork.Verify(foo => foo.Genres.Insert(It.IsAny<Genre>()), Times.Once);
            _unitOfWork.Verify(foo => foo.Save(), Times.Once);
        }

        [Test]
        public void Delete_ShouldDeleteGenre_WhenNameIsValid()
        {
            //Arrange
            var genre = Fixture.Create<Genre>();
            var name = Fixture.Create<string>();
            _unitOfWork.Setup(x => x.Genres.Get(It.IsAny<string>())).Returns(genre);

            // Act
            _genreService.Delete(name);

            // Assert
            _unitOfWork.Verify(foo => foo.Genres.Update(It.IsAny<Genre>()), Times.Once);
            _unitOfWork.Verify(foo => foo.Save(), Times.Once);
        }

        [Test]
        public void Delete_ShouldDeleteGenreFromMongo_WhenGameIsNull()
        {
            //Arrange
            var genre = Fixture.Create<Category>();
            var name = Fixture.Create<string>();
            _unitOfWork.Setup(x => x.Genres.Get(It.IsAny<string>())).Returns(() => null);
            _unitOfWork.Setup(x => x.Categories.Get(It.IsAny<string>())).Returns(genre);

            // Act
            _genreService.Delete(name);

            // Assert
            _unitOfWork.Verify(foo => foo.Genres.Insert(It.IsAny<Genre>()), Times.Once);
            _unitOfWork.Verify(foo => foo.Save(), Times.Once);
        }

        [Test]
        public void GetAllGenres_ShouldReturnListGenres()
        {
            // Arrange
            var sut = Fixture.Create<List<Genre>>();
            var genres = Fixture.Create<List<Category>>();

            _unitOfWork.Setup(a => a.Genres.GetAll()).Returns(sut);
            _unitOfWork.Setup(x => x.Categories.GetAll()).Returns(genres);

            // Act
            var result = _genreService.GetAllGenres();

            // Assert
            _unitOfWork.Verify(foo => foo.Genres.GetAll(), Times.Once);
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof (List<GenreDto>), result);
        }

        [Test]
        public void GetGenresByNameWhenCreatingNewGame_ShouldReturnListGenresDTO_WhenPassGenresIds()
        {
            // Arrange
            var sut = Fixture.Create<List<Genre>>();
            var genresIds = Fixture.Create<List<string>>();
            var genres = Fixture.Create<List<Category>>();

            _unitOfWork.Setup(a => a.Genres.GetAll()).Returns(sut);
            _unitOfWork.Setup(x => x.Categories.GetAll()).Returns(genres);

            // Act
            var result = _genreService.GetGenresByName(genresIds);

            // Assert
            _unitOfWork.Verify(foo => foo.Genres.GetAll(), Times.Once);
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof (List<GenreDto>), result);
        }

        [Test]
        public void GetByName_ShouldReturnGenre_WhenNameISValid()
        {
            // Arrange
            _unitOfWork.Setup(a => a.Genres.Get(It.IsAny<string>())).Returns(new Genre());

            // Act
            var result = _genreService.GetByName(It.IsAny<string>());

            // Assert
            _unitOfWork.Verify(foo => foo.Genres.Get(It.IsAny<string>()), Times.Once);
            Assert.IsNotNull(result);
        }

        [Test]
        public void GetGenresOfGame_ShouldRetunListGenres_WhenKeyIsValid()
        {
            // Arrange
            var game = Fixture.Create<Game>();
            _unitOfWork.Setup(a => a.Games.Get(It.IsAny<string>())).Returns(game);

            // Act
            var result = _genreService.GetGenresOfGame(It.IsAny<string>());

            // Assert
            _unitOfWork.Verify(foo => foo.Games.Get(It.IsAny<string>()), Times.Once);
            Assert.IsNotNull(result);
        }

        [Test]
        public void Update_ShouldUpdateGenre_WhenGenreExists()
        {
            // Arrange
            var genreDto = Fixture.Create<GenreDto>();
            _unitOfWork.Setup(a => a.Genres.Get(It.IsAny<string>())).Returns(new Genre());

            // Act
             _genreService.Update(genreDto);

            // Assert
            _unitOfWork.Verify(foo => foo.Genres.Get(It.IsAny<string>()), Times.Once);
            _unitOfWork.Verify(x=>x.Genres.Update(It.IsAny<Genre>()),Times.Once);
        }

        [Test]
        public void Update_ShouldUpdateGenre_WhenGenreNotExists()
        {
            // Arrange
            var genreDto = Fixture.Create<GenreDto>();
            _unitOfWork.Setup(a => a.Genres.Get(It.IsAny<string>())).Returns(()=>null);

            // Act
            _genreService.Update(genreDto);

            // Assert
            _unitOfWork.Verify(foo => foo.Genres.Get(It.IsAny<string>()), Times.Once);
            _unitOfWork.Verify(x => x.Genres.Insert(It.IsAny<Genre>()), Times.Once);
        }
    }
}