﻿using System;
using System.Collections.Generic;
using GameStore.DAL.Entities;
using GameStore.DAL.Interfaces;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.Services.Services;
using GameStore.Tools.Logger;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace GameStore.UnitTests.Services
{
    [TestFixture]
    public class OrderDetailsServiceTests : TestBase
    {
        private Mock<ILoggerFileService> _logger;
        private Mock<IUnitOfWork> _unitOfWork;
        private Mock<IValidationService> _validationService;
        private Mock<IGameService> _gameService;
        private Mock<IOrderService> _orderService;
        private Mock<IUserService> _userService;

        private OrderDetailsService _orderDetailsService;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            _unitOfWork = new Mock<IUnitOfWork>();
            _validationService = new Mock<IValidationService>();
            _logger = new Mock<ILoggerFileService>();
            _gameService = new Mock<IGameService>();
            _orderService = new Mock<IOrderService>();
            _userService = new Mock<IUserService>();
            _orderDetailsService = new OrderDetailsService(_unitOfWork.Object, _logger.Object, Mapper,
                _validationService.Object, _gameService.Object, _orderService.Object);
        }

        [Test]
        public void CreateOrderDetailsWithQuantity_ShouldSuccessCreateOrderDetails_WhenModelIsValid()
        {
            //Arrange
            var sut = Fixture.Create<Dictionary<string, short>>();
            var user = Fixture.Create<List<User>>();
            var gameDto = Fixture.Create<GameDto>();
            var orderDto = Fixture.Create<OrderDto>();
            _gameService.Setup(x => x.GetGameByKey(It.IsAny<string>())).Returns(gameDto);
            _orderService.Setup(x => x.LastCustomersOrder(It.IsAny<int>())).Returns(orderDto);
            _unitOfWork.Setup(x => x.OrderDetailses.Insert(It.IsAny<OrderDetails>()));
            _unitOfWork.Setup(x => x.Users.Get(It.IsAny<string>())).Returns(user[0]);

            //Act
            _orderDetailsService.CreateOrderDetailsWithQuantity(sut,"");

            //Assert
            _unitOfWork.Verify(x => x.OrderDetailses.Insert(It.IsAny<OrderDetails>()), Times.AtLeastOnce);
            _unitOfWork.Verify(foo => foo.Save(), Times.AtLeastOnce);
        }

        [Test]
        public void GetOrderDetailsForOrder_ShouldReturnListOrderDetailsDtos_WhenModelIsValid()
        {
            //Arrange
            var sut = Fixture.Create<List<OrderDetails>>();
            var orderdto = Fixture.Create<OrderDto>();

            _unitOfWork.Setup(a => a.OrderDetailses.Get(It.IsAny<Func<OrderDetails, bool>>())).Returns(sut);

            //Act
            var result = _orderDetailsService.GetOrderDetailsForOrder(orderdto);

            //Assert
            _unitOfWork.Verify(foo => foo.OrderDetailses.Get(It.IsAny<Func<OrderDetails, bool>>()), Times.AtLeastOnce);
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof (IEnumerable<OrderDetailsDto>), result);
        }

        [Test]
        public void OrderSum_ShouldReturnOrderSum_WhenModelIsValid()
        {
            //Arrange
            var sut = Fixture.Create<List<Game>>();
            var orderDetailsDto = new List<OrderDetailsDto>
            {
                new OrderDetailsDto {ProductId = sut[0].Id}
            };
            _unitOfWork.Setup(x => x.Games.Get(It.IsAny<Func<Game,bool>>())).Returns(sut);

            //Act
            var result = _orderDetailsService.OrderSum(orderDetailsDto);

            //Assert
            _unitOfWork.Verify(foo => foo.Games.Get(It.IsAny<Func<Game,bool>>()), Times.AtLeastOnce);
            Assert.IsNotNull(result);
        }
    }
}