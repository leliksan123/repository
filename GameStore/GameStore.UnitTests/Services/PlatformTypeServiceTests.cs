﻿using System.Collections.Generic;
using GameStore.DAL.Entities;
using GameStore.DAL.Interfaces;
using GameStore.Services.Interfaces;
using GameStore.Services.Services;
using GameStore.Tools.Logger;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace GameStore.UnitTests.Services
{
    public class PlatformTypeServiceTests : TestBase
    {
        private Mock<ILoggerFileService> _logger;

        private PlatformTypeService _platformTypeService;
        private Mock<IUnitOfWork> _unitOfWork;
        private Mock<IValidationService> _validationService;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            _unitOfWork = new Mock<IUnitOfWork>();
            _validationService = new Mock<IValidationService>();
            _logger = new Mock<ILoggerFileService> ();
            _platformTypeService = new PlatformTypeService(_unitOfWork.Object, _logger.Object, Mapper,
                _validationService.Object);
        }

        [Test]
        public void GetAllPlatformTypes_ShouldReturnPlatformTypesList()
        {
            // Arrange
            var sut = Fixture.Create<List<PlatformType>>();
            _unitOfWork.Setup(x => x.PlatformTypes.GetAll()).Returns(sut);

            // Act
            var result = _platformTypeService.GetAllPlatformTypes();

            // Assert
            _unitOfWork.Verify(x => x.PlatformTypes.GetAll(), Times.Once);
            Assert.IsNotNull(result);
        }

        [Test]
        public void GetPlatformTypesByIdWhenCreatingNewGame_ShouldReturnPlatformTypesDtos_WhenParameterIsValid()
        {
            // Arrange
            var sut = Fixture.Create<List<PlatformType>>();
            var list = Fixture.Create<List<int>>();
            _unitOfWork.Setup(x => x.PlatformTypes.GetAll()).Returns(sut);

            // Act
            var result = _platformTypeService.GetPlatformTypesById(list);

            // Assert
            _unitOfWork.Verify(x => x.PlatformTypes.GetAll(), Times.Once);
            Assert.IsNotNull(result);
        }
    }
}