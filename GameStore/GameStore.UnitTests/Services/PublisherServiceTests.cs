﻿using System.Collections.Generic;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Mongo;
using GameStore.DAL.Interfaces;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.Services.Services;
using GameStore.Tools.Logger;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace GameStore.UnitTests.Services
{
    public class PublisherServiceTests : TestBase
    {
        private Mock<ILoggerFileService> _logger;

        private PublisherService _publisherService;
        private Mock<IUnitOfWork> _unitOfWork;
        private Mock<IValidationService> _validationService;
        private Mock<IPublisherTranslation> _publisherTranslateService;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            _unitOfWork = new Mock<IUnitOfWork>();
            _validationService = new Mock<IValidationService>();
            _publisherTranslateService = new Mock<IPublisherTranslation>();
            _logger = new Mock<ILoggerFileService>();
            _publisherService = new PublisherService(_logger.Object, Mapper, _unitOfWork.Object,
                _validationService.Object , _publisherTranslateService.Object);
        }

        [Test]
        public void GetAllPublishers_ShouldReturnPublisherList()
        {
            // Arrange
            var sut = Fixture.Create<List<Publisher>>();
            var suppliers = Fixture.Create<List<Supplier>>();

            _unitOfWork.Setup(x => x.Publishers.GetAll()).Returns(sut);
            _unitOfWork.Setup(x => x.Suppliers.GetAll()).Returns(suppliers);

            // Act
            var result = _publisherService.GetAllPublishers();

            // Assert
            _unitOfWork.Verify(x => x.Publishers.GetAll(), Times.Once);
            Assert.IsNotNull(result);
        }

        [Test]
        public void GetPublisherByCompanyName_ShouldReturnPublisherDto_WhenCompanyNameIsValid()
        {
            // Arrange
            var sut = Fixture.Create<List<Publisher>>();
            _unitOfWork.Setup(x => x.Publishers.GetAll()).Returns(sut);
            _unitOfWork.Setup(x => x.Publishers.Get(It.IsAny<string>())).Returns(sut[0]);

            // Act
            var result = _publisherService.GetPublisherByCompanyName(sut[0].CompanyName);

            // Assert
            _unitOfWork.Verify(x => x.Publishers.Get(It.IsAny<string>()), Times.Once);
            Assert.IsNotNull(result);
        }

        [Test]
        public void Create_ShouldSuccessCreatePublisher_WhenModelIsValid()
        {
            // Arrange
            var publisher = Fixture.Create<List<Publisher>>();

            var sut = Fixture.Create<PublisherDto>();
            _unitOfWork.Setup(x => x.Publishers.Insert(It.IsAny<Publisher>()));
            _unitOfWork.Setup(x=>x.Publishers.Get(It.IsAny<string>())).Returns(publisher[0]);
            _unitOfWork.Setup(x => x.Publishers.GetAll()).Returns(publisher);
            _unitOfWork.Setup(x => x.Languages.Get(It.IsAny<string>())).Returns(new Language());

            // Act
            _publisherService.Create(sut);

            // Assert
            _unitOfWork.Verify(x => x.Publishers.Insert(It.IsAny<Publisher>()), Times.Once);
            _unitOfWork.Verify(x => x.Publishers.GetAll(), Times.Once);
            _unitOfWork.Verify(x => x.Save(), Times.AtLeastOnce);
        }

        [Test]
        public void Delete_ShouldChangeStatus_WhenPublisherExists()
        {
            // Arrange
            var sut = Fixture.Create<List<Publisher>>();
            _unitOfWork.Setup(x => x.Publishers.Get(It.IsAny<string>())).Returns(sut[0]);

            // Act
             _publisherService.Delete(It.IsAny<string>());

            // Assert
            _unitOfWork.Verify(x => x.Publishers.Get(It.IsAny<string>()), Times.Once);
            _unitOfWork.Verify(x=>x.Publishers.Update(It.IsAny<Publisher>()) , Times.Once);
            _unitOfWork.Verify(x=>x.Save() , Times.Once);
        }

        [Test]
        public void Delete_ShouldCreatingAndChangeStatus_WhenPublisherNotExists()
        {
            // Arrange
            var sut = Fixture.Create<Supplier>();
            _unitOfWork.Setup(x => x.Publishers.Get(It.IsAny<string>())).Returns(()=>null);
            _unitOfWork.Setup(x => x.Suppliers.Get(It.IsAny<string>())).Returns(sut);

            // Act
            _publisherService.Delete(It.IsAny<string>());

            // Assert
            _unitOfWork.Verify(x => x.Publishers.Get(It.IsAny<string>()), Times.Once);
            _unitOfWork.Verify(x => x.Publishers.Insert(It.IsAny<Publisher>()), Times.Once);
            _unitOfWork.Verify(x => x.Save(), Times.Once);
        }
    }
}