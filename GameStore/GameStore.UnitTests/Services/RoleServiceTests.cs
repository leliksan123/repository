﻿using System;
using System.Collections.Generic;
using GameStore.DAL.Entities;
using GameStore.DAL.Interfaces;
using GameStore.Services.DTO;
using GameStore.Services.Services;
using GameStore.Tools.Logger;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace GameStore.UnitTests.Services
{
    public class RoleServiceTests : TestBase
    {
        private Mock<ILoggerFileService> _logger;

        private RoleService _roleService;
        private Mock<IUnitOfWork> _unitOfWork;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            _unitOfWork = new Mock<IUnitOfWork>();
            _logger = new Mock<ILoggerFileService>();
            _roleService = new RoleService(_unitOfWork.Object, Mapper, _logger.Object);
        }

        [Test]
        public void GetAll_ShouldReturnRolesList()
        {
            // Arrange
            var sut = Fixture.Create<IEnumerable<Role>>();
            _unitOfWork.Setup(x => x.Roles.GetAll()).Returns(sut);

            // Act
            var result = _roleService.GetAll();

            // Assert
            _unitOfWork.Verify(x => x.Roles.GetAll(), Times.Once);
            Assert.IsNotNull(result);
        }

        [Test]
        public void GetRoleByName_ShouldReturnRole_IfExist()
        {
            // Arrange
            var sut = Fixture.Create<IEnumerable<Role>>();
            _unitOfWork.Setup(x => x.Roles.Get(It.IsAny<Func<Role,bool>>())).Returns(sut);

            // Act
            var result = _roleService.GetRoleByName(It.IsAny<string>());

            // Assert
            _unitOfWork.Verify(x => x.Roles.Get(It.IsAny<Func<Role, bool>>()) , Times.Once);
            Assert.IsNotNull(result);
        }

        [Test]
        public void Create_ShouldAddRoleInDb_IfRoleNotExist()
        {
            // Arrange
            var role = Fixture.Create<RoleDTo>();
            var sut = Fixture.Create<IEnumerable<Role>>();
            _unitOfWork.Setup(x => x.Roles.GetAll()).Returns(sut);

            // Act
             _roleService.Create(role);

            // Assert
            _unitOfWork.Verify(x => x.Roles.GetAll(), Times.Once);
            _unitOfWork.Verify(x=>x.Save());
        }

        [Test]
        public void Delete_ShouldAddIsDeletedTrue_IfRoleExist()
        {
            // Arrange
            var role = Fixture.Create<Role>();
            _unitOfWork.Setup(x => x.Roles.Get(It.IsAny<string>())).Returns(role);

            // Act
            _roleService.Delete(It.IsAny<string>());

            // Assert
            _unitOfWork.Verify(x => x.Roles.Get(It.IsAny<string>()), Times.Once);
            _unitOfWork.Verify(x => x.Save());
        }
    }
}