﻿using System;
using System.Collections.Generic;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Mongo;
using GameStore.DAL.Interfaces;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.Services.Services;
using GameStore.Tools.Logger;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace GameStore.UnitTests.Services
{
    [TestFixture]
    public class CommentServicesTest : TestBase
    {
        private Mock<ILoggerFileService> _logger;
        private Mock<IUnitOfWork> _unitOfWork;
        private Mock<IValidationService> _validationService;

        private CommentService _commentServices;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            _unitOfWork = new Mock<IUnitOfWork>();
            _validationService = new Mock<IValidationService>();
            _logger = new Mock<ILoggerFileService>();
            _commentServices = new CommentService(_unitOfWork.Object, _logger.Object, Mapper, _validationService.Object);
        }

        [Test]
        public void Create_ShouldSuccessfullCreatedComment_WhenModelIsValid()
        {
            // Arrange
            var sut = Fixture.Create<CommentDto>();
            _unitOfWork.Setup(x => x.Comments.Insert(It.IsAny<Comment>()));

            // Act
            _commentServices.Create(sut);

            // Assert
            _unitOfWork.Verify(foo => foo.Comments.Insert(It.IsAny<Comment>()), Times.Once);
            _unitOfWork.Verify(e => e.Save(), Times.Once);
        }

        [Test]
        public void DeleteComment_ShouldSuccessDeleteComment_WhenIdIsValid()
        {
            // Arrange
            var sut = Fixture.Create<List<Comment>>();
            _unitOfWork.Setup(e => e.Comments.Get(It.IsAny<Func<Comment, bool>>())).Returns(sut);

            // Act
            _commentServices.DeleteComment(It.IsAny<Guid>());

            // Assert
            _unitOfWork.Verify(e => e.Comments.Get(It.IsAny<Func<Comment, bool>>()), Times.Once);
            _unitOfWork.Verify(e => e.Save(), Times.Once);
        }

        [Test]
        public void GetAllComments_ShouldReturnCommentsList()
        {
            // Arrange
            var sut = Fixture.Create<List<Comment>>();
            _unitOfWork.Setup(e => e.Comments.GetAll()).Returns(sut);

            // Act
            var result = _commentServices.GetAllComments();

            // Assert
            _unitOfWork.Verify(e => e.Comments.GetAll(), Times.Once);
            Assert.IsNotNull(result);
        }

        [Test]
        public void GetAllCommentsForGame_ShouldReturnListComments_WhenValidGameKey()
        {
            // Arrange
            var sut = Fixture.Create<List<Comment>>();
            _unitOfWork.Setup(x => x.Games.Get(It.IsAny<string>())).Returns(new Game {Id = 1});
            _unitOfWork.Setup(a => a.Comments.GetAll()).Returns(sut);

            // Act
            var result = _commentServices.GetAllCommentsForGame(It.IsAny<string>());

            // Assert
            _unitOfWork.Verify(x => x.Games.Get(It.IsAny<string>()), Times.Once);
            Assert.IsNotNull(result);
        }

        [Test]
        public void GetAllCommentsForGame_ShouldCreateGameFromMongo_WhenGameIsNull()
        {
            // Arrange
            var sut = Fixture.Create<List<Comment>>();
            _unitOfWork.Setup(x => x.Games.Get(It.IsAny<string>())).Returns(()=>null);
            _unitOfWork.Setup(x => x.Products.Get(It.IsAny<string>())).Returns(new Product());
            _unitOfWork.Setup(x => x.Comments.Get(It.IsAny<Func<Comment, bool>>())).Returns(sut);

            // Act
            var result = _commentServices.GetAllCommentsForGame(It.IsAny<string>());

            // Assert
            _unitOfWork.Verify(x => x.Games.Get(It.IsAny<string>()), Times.Once);
            _unitOfWork.Verify(x => x.Products.Get(It.IsAny<string>()), Times.Once);
            _unitOfWork.Verify(x=>x.Save() , Times.Once);
            Assert.IsNotNull(result);
        }

        [Test]
        public void GetAllCommentsForGame_ShouldThrowException_WhenGameKeyInvalid()
        {
            // Arrange
            var sut = Fixture.Create<List<Game>>();
            _unitOfWork.Setup(x => x.Games.Get(It.IsAny<string>())).Returns(new Game {Id = 1});
            _validationService.Setup(a => a.ValidateIfItemExist(It.IsAny<Game>()))
                .Throws(new ArgumentException("Comment not found"));

            // Act
            var ex = Assert.Throws<ArgumentException>(() => _commentServices.GetAllCommentsForGame(It.IsAny<string>()));

            // Assert
            _unitOfWork.Verify(x => x.Games.Get(It.IsAny<string>()), Times.Once);
            Assert.That(ex.Message, Is.EqualTo("Comment not found"));
        }

        [Test]
        public void Update_ShouldSuccesfullUpdateComment_WhenModelIsValid()
        {
            // Arrange
            var sut = Fixture.Create<List<Comment>>();
            var commentDto = Fixture.Create<CommentDto>();
            _unitOfWork.Setup(a => a.Comments.GetById(It.IsAny<Guid>())).Returns(() => sut[0]);
            _unitOfWork.Setup(a => a.Comments.Update(It.IsAny<Comment>()));

            // Act
            _commentServices.Update(commentDto);

            // Assert
            _unitOfWork.Verify(foo => foo.Comments.GetById(It.IsAny<Guid>()), Times.AtLeastOnce);
            _unitOfWork.Verify(foo => foo.Comments.Update(It.IsAny<Comment>()), Times.Once);
            _unitOfWork.Verify(e => e.Save(), Times.Once);
        }

        [Test]
        public void Update_ShouldThrowException_WhenCommentNotFoundInDatabase()
        {
            // Arrange
            var sut = Fixture.Create<CommentDto>();
            _unitOfWork.Setup(a => a.Comments.GetById(It.IsAny<Guid>())).Returns(() => null);
            _validationService.Setup(a => a.ValidateIfItemExist(It.IsAny<Comment>()))
                .Throws(new ArgumentException("Comment not found"));

            // Act
            var ex = Assert.Throws<ArgumentException>(() => _commentServices.Update(sut));

            // Assert
            _unitOfWork.Verify(x => x.Comments.GetById(It.IsAny<Guid>()), Times.Once);
            Assert.That(ex.Message, Is.EqualTo("Comment not found"));
        }

        [Test]
        public void GetCommentById_ShouldReturnComment_WhenIdIsValid()
        {
            // Arrange
            var sut = Fixture.Create<List<Comment>>();
            _unitOfWork.Setup(a => a.Comments.GetById(It.IsAny<Guid>())).Returns(() => sut[0]);

            // Act
           var result =  _commentServices.GetCommentById(It.IsAny<Guid>());

            // Assert
            _unitOfWork.Verify(foo => foo.Comments.GetById(It.IsAny<Guid>()), Times.Once);
            Assert.IsNotNull(result);
        }
    }
}