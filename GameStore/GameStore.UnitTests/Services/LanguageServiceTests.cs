﻿using System.Collections.Generic;
using GameStore.DAL.Entities;
using GameStore.DAL.Interfaces;
using GameStore.Services.Services;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace GameStore.UnitTests.Services
{
    public class LanguageServiceTests : TestBase
    {

        private LanguageService _languageService;
        private Mock<IUnitOfWork> _unitOfWork;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            _unitOfWork = new Mock<IUnitOfWork>();
            _languageService = new LanguageService(Mapper,_unitOfWork.Object);
        }

        [Test]
        public void GetLanguageByName_ShouldReturnLanguage()
        {
            // Arrange
            var sut = Fixture.Create<Language>();
            _unitOfWork.Setup(x => x.Languages.Get(It.IsAny<string>())).Returns(sut);

            // Act
            var result = _languageService.GetLanguageByName(It.IsAny<string>());

            // Assert
            _unitOfWork.Verify(x => x.Languages.Get(It.IsAny<string>()), Times.Once);
            Assert.IsNotNull(result);
        }

        [Test]
        public void GetAll_ShouldReturnLanguagesList()
        {
            // Arrange
            var sut = Fixture.Create<IEnumerable<Language>>();
            _unitOfWork.Setup(x => x.Languages.GetAll()).Returns(sut);

            // Act
            var result = _languageService.GetAllLanguage();

            // Assert
            _unitOfWork.Verify(x => x.Languages.GetAll(), Times.Once);
            Assert.IsNotNull(result);
        }
    }
}