﻿using System;
using System.Collections.Generic;
using GameStore.DAL.Entities;
using GameStore.DAL.Interfaces;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.Services.Services;
using GameStore.Tools.Logger;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace GameStore.UnitTests.Services
{
    public class UserServiceTests : TestBase
    {
        private Mock<IUnitOfWork> _unitOfWork;
        private Mock<ILoggerFileService> _loggerService;
        private Mock<IValidationService> _validationService;

        private UserService _userService;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();
            _unitOfWork = new Mock<IUnitOfWork>();
            _validationService = new Mock<IValidationService>();
            _loggerService = new Mock<ILoggerFileService>();
            _userService = new UserService(_loggerService.Object , Mapper , _unitOfWork.Object , _validationService.Object);
        }

        [Test]
        public void BanUser_ShouldSuccessBanUser_WhenIdIsValid()
        {
            //Arrange

            //Act
            var ex = Assert.Throws<NotImplementedException>(() => _userService.BanUser(It.IsAny<string>()));

            //Assert
            Assert.IsInstanceOf(typeof (NotImplementedException), ex);
        }

        [Test]
        public void Delete_ShouldSuccessDeleteUser_WhenEmailIsValid()
        {
            // Arrange
            var email = Fixture.Create<string>();
            _unitOfWork.Setup(x => x.Users.Delete(It.IsAny<string>()));

            // Act
            _userService.Delete(email);

            // Assert
            _unitOfWork.Verify(x => x.Users.Delete(It.IsAny<string>()), Times.Once);
            _unitOfWork.Verify(x => x.Save(), Times.Once);
        }

        [Test]
        public void GetAll_ShouldReturnListUsers()
        {
            //Arrange
            var sut = Fixture.Create<List<User>>();
            _unitOfWork.Setup(a => a.Users.GetAll()).Returns(sut);

            //Act
            var result = _userService.GetAll();

            //Assert
            _unitOfWork.Verify(foo => foo.Users.GetAll(), Times.Once);
            Assert.IsNotNull(result);
        }

        [Test]
        public void GetUserByEmail_ShouldReturnUser_WhenEmailIsValid()
        {
            //Arrange
            var sut = Fixture.Create<List<User>>();

            _unitOfWork.Setup(a => a.Users.Get(It.IsAny<Func<User,bool>>())).Returns(sut);

            //Act
            var result = _userService.GetUserByEmail(It.IsAny<string>());

            //Assert
            _unitOfWork.Verify(foo => foo.Users.Get(It.IsAny<Func<User, bool>>()), Times.Once);
            Assert.IsNotNull(result);
        }

        [Test]
        public void GetUsersregisterToday_ShouldReturnListUsers_WhenDataIsValid()
        {
            //Arrange
            var sut = Fixture.Create<List<User>>();

            _unitOfWork.Setup(a => a.Users.Get(It.IsAny<Func<User, bool>>())).Returns(sut);

            //Act
            var result = _userService.GetUsersregisterToday();

            //Assert
            _unitOfWork.Verify(foo => foo.Users.Get(It.IsAny<Func<User, bool>>()), Times.Once);
            Assert.IsNotNull(result);
        }

        [Test]
        public void Update_ShouldSuccessfullUpdateUser_WhenModelIsValid()
        {
            //Arrange
            var sut = Fixture.Create<User>();
            var userDto = Fixture.Create<UserDTo>();
            var role = Fixture.Create<Role>();
            _unitOfWork.Setup(a => a.Users.Get(It.IsAny<string>())).Returns(sut);
            _unitOfWork.Setup(x => x.Roles.Get(It.IsAny<string>())).Returns(role);

            //Act
             _userService.Update(userDto);

            //Assert
            _unitOfWork.Verify(foo => foo.Users.Get(It.IsAny<string>()), Times.AtLeastOnce);
            _unitOfWork.Verify(foo => foo.Roles.Get(It.IsAny<string>()), Times.AtLeastOnce);
            _unitOfWork.Verify(foo => foo.Save(), Times.Once);
        }
    }
}