using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Mongo;
using GameStore.DAL.Interfaces;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.Services.Services;
using GameStore.Tools.Logger;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace GameStore.UnitTests.Services
{
    [TestFixture]
    public class GameServicesTests : TestBase
    {
        private Mock<ILoggerFileService> _logger;
        private Mock<IUnitOfWork> _unitOfWork;
        private Mock<IGameTranslation> _gameTranslateService;
        private Mock<IValidationService> _validation;

        private GameService _gameServices;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            _unitOfWork = new Mock<IUnitOfWork>();
            _logger = new Mock<ILoggerFileService>();
            _gameTranslateService = new Mock<IGameTranslation>();
            _validation = new Mock<IValidationService>();
            _gameServices = new GameService(_unitOfWork.Object, _logger.Object, Mapper, _gameTranslateService.Object , _validation.Object);
        }

        [Test]
        public void Create_ShouldThrowArgumentException_WhenFoundDuplicateKey()
        {
            //Arrange
            var sut = Fixture.Create<List<Game>>();
            var products = Fixture.Create<List<Product>>();
            _unitOfWork.Setup(a => a.Games.GetAll()).Returns(sut);
            _unitOfWork.Setup(x => x.Products.GetAll()).Returns(products);

            var fakedto = new GameDto
            {
                Id = sut[0].Id,
                Key = sut[0].Key,
                Name = sut[0].Name
            };

            //Act
            var ex = Assert.Throws<ArgumentException>(() => _gameServices.Create(fakedto));

            //Assert
            Assert.That(ex.Message, Is.EqualTo("Game with this Key already exists in database"));
        }

        [Test]
        public void Delete_ShouldSuccesfullDeleteGame_WhenIdIsValid()
        {
            //Arrange
            var sut = Fixture.Create<List<Game>>();
            _unitOfWork.Setup(a => a.Games.Get(It.IsAny<string>())).Returns(sut[0]);

            //Act
            _gameServices.Delete(sut[0].Key);

            //Assert
            _unitOfWork.Verify(e => e.Games.Get(It.IsAny<string>()), Times.Once);
            _unitOfWork.Verify(foo => foo.Games.Update(It.IsAny<Game>()), Times.Once);
            _unitOfWork.Verify(e => e.Save(), Times.Once);
        }

        [Test]
        public void Delete_ShouldInsertGameAndAdded_WhengameNotExist()
        {
            //Arrange
            var sut = Fixture.Create<List<Game>>();
            _unitOfWork.Setup(a => a.Games.Get(It.IsAny<string>())).Returns(()=>null);
            _unitOfWork.Setup(x => x.Products.Get(It.IsAny<string>())).Returns(new Product());

            //Act
            _gameServices.Delete(sut[0].Key);

            //Assert
            _unitOfWork.Verify(e => e.Games.Get(It.IsAny<string>()), Times.Once);
            _unitOfWork.Verify(foo => foo.Products.Get(It.IsAny<string>()), Times.Once);
            _unitOfWork.Verify(e => e.Save(), Times.Once);
        }

        [Test]
        public void GetAllGames_ShouldReturnListGames()
        {
            //Arrange
            var sut = Fixture.Create<List<Game>>();
            var products = Fixture.Create<List<Product>>();

            _unitOfWork.Setup(a => a.Games.Get(It.IsAny<Func<Game, bool>>())).Returns(sut);
            _unitOfWork.Setup(x => x.Products.Get(It.IsAny<Expression<Func<Product, bool>>>())).Returns(products);

            //Act
            var result = _gameServices.GetAllGames();

            //Assert
            _unitOfWork.Verify(foo => foo.Games.Get(It.IsAny<Func<Game,bool>>()), Times.Once);
            _unitOfWork.Verify(foo => foo.Products.Get(It.IsAny<Expression<Func<Product, bool>>>()), Times.Once);
            Assert.IsNotNull(result);
        }

        [Test]
        public void GetAllGamesByPlatformType_ShouldReturnListGames_WhenIdIsValid()
        {
            //Arrange
            var sut = Fixture.Create<List<Game>>();
            _unitOfWork.Setup(a => a.Games.Get(It.IsAny<Func<Game, bool>>())).Returns(sut);

            //Act
            var result = _gameServices.GetAllGamesForPlatformType(It.IsAny<string>());

            //Assert
            _unitOfWork.Verify(e => e.Games.Get(It.IsAny<Func<Game, bool>>()), Times.Once);
            Assert.IsNotNull(result);
        }

        [Test]
        public void GetAllGamesForGenre_ShouldReturnListGames_WhenIdIsValid()
        {
            //Arrange
            var sut = Fixture.Create<List<Game>>();
            var product = Fixture.Create<List<Product>>();
            _unitOfWork.Setup(a => a.Games.Get(It.IsAny<Func<Game, bool>>())).Returns(sut);
            _unitOfWork.Setup(a => a.Products.Get(It.IsAny<Expression<Func<Product, bool>>>())).Returns(product);
            _unitOfWork.Setup(x => x.Categories.Get(It.IsAny<string>())).Returns(new Category());

            //Act
            var result = _gameServices.GetAllGamesForGenre(It.IsAny<string>());

            //Assert
            _unitOfWork.Verify(e => e.Games.Get(It.IsAny<Func<Game, bool>>()), Times.Once);
            Assert.IsNotNull(result);
        }

        [Test]
        public void GetAllGamesByPublisher_ShouldReturnListGames_WhenIdIsValid()
        {
            //Arrange
            var sut = Fixture.Create<List<Game>>();
            var product = Fixture.Create<List<Product>>();
            _unitOfWork.Setup(a => a.Games.Get(It.IsAny<Func<Game, bool>>())).Returns(sut);
            _unitOfWork.Setup(a => a.Products.Get(It.IsAny<Expression<Func<Product, bool>>>())).Returns(product);
            _unitOfWork.Setup(x => x.Suppliers.Get(It.IsAny<string>())).Returns(new Supplier());

            //Act
            var result = _gameServices.GetAllGamesForPublisher(It.IsAny<string>());

            //Assert
            _unitOfWork.Verify(e => e.Games.Get(It.IsAny<Func<Game, bool>>()), Times.Once);
            _unitOfWork.Verify(e => e.Products.Get(It.IsAny<Expression<Func<Product, bool>>>()), Times.Once);
            Assert.IsNotNull(result);
        }

        [Test]
        public void GetGameByKey_ShouldReturnGame_WhenValidGameKey()
        {
            //Arrange
            var sut = Fixture.Create<List<Game>>();
            _unitOfWork.Setup(x => x.Games.Get(It.IsAny<string>())).Returns(new Game {Id = 1});

            //Act
            var result = _gameServices.GetGameByKey(sut[0].Key);

            //Assert
            _unitOfWork.Verify(foo => foo.Games.Get(It.IsAny<string>()), Times.Once);
            Assert.IsNotNull(result);
        }

        [Test]
        public void Update_ShouldSuccesfullUpdateComment_WhenModelIsValid()
        {
            //Arrange
            var gameDto = Fixture.Create<GameDto>();
            var sut = Fixture.Create<List<Game>>();
            _unitOfWork.Setup(a => a.Games.Get(It.IsAny<string>())).Returns(() => sut[0]);
            _unitOfWork.Setup(a => a.Games.Update(It.IsAny<Game>()));

            _unitOfWork.Setup(x => x.Genres.Get(It.IsAny<string>())).Returns(new Genre {Id = 1});
            _unitOfWork.Setup(x => x.PlatformTypes.Get(It.IsAny<string>())).Returns(new PlatformType {Id = 1});
            _unitOfWork.Setup(x => x.Publishers.Get(It.IsAny<string>())).Returns(new Publisher {Id = 1});
            _unitOfWork.Setup(x => x.Languages.Get(It.IsAny<string>())).Returns(new Language());

            //Act
            _gameServices.Update(gameDto);

            //Assert
            _unitOfWork.Verify(foo => foo.Games.Get(It.IsAny<string>()), Times.Once);
            _unitOfWork.Verify(foo => foo.Games.Update(It.IsAny<Game>()), Times.Once);
            _unitOfWork.Verify(x => x.Genres.Get(It.IsAny<string>()), Times.AtLeastOnce);
            _unitOfWork.Verify(x => x.PlatformTypes.Get(It.IsAny<string>()), Times.AtLeastOnce);
            _unitOfWork.Verify(x => x.Publishers.Get(It.IsAny<string>()), Times.AtLeastOnce);
            _unitOfWork.Verify(e => e.Save(), Times.AtLeastOnce);
        }

        [Test]
        public void Create_ShouldSuccesfullCreatedGame_WhenModelIsValid()
        {
            //Arrange
            var gameDto = Fixture.Create<GameDto>();

            _unitOfWork.Setup(a => a.Games.Insert(It.IsAny<Game>()));
            _unitOfWork.Setup(a => a.Games.GetAll()).Returns(new List<Game>());
            _unitOfWork.Setup(a => a.Products.GetAll()).Returns(new List<Product>());
            _unitOfWork.Setup(x => x.Genres.Get(It.IsAny<string>())).Returns(new Genre { Id = 1 });
            _unitOfWork.Setup(x => x.PlatformTypes.Get(It.IsAny<string>())).Returns(new PlatformType { Id = 1 });
            _unitOfWork.Setup(x => x.Publishers.Get(It.IsAny<string>())).Returns(new Publisher { Id = 1 });
            _unitOfWork.Setup(x => x.Games.Get(It.IsAny<Func<Game, bool>>())).Returns(new List<Game>());
            _unitOfWork.Setup(x => x.Languages.Get(It.IsAny<string>())).Returns(new Language());

            //Act
            _gameServices.Create(gameDto);

            //Assert
            _unitOfWork.Verify(foo => foo.Games.Insert(It.IsAny<Game>()), Times.Once);
            _unitOfWork.Verify(a => a.Games.GetAll(), Times.Once);
            _unitOfWork.Verify(x => x.Genres.Get(It.IsAny<string>()), Times.AtLeastOnce);
            _unitOfWork.Verify(x => x.PlatformTypes.Get(It.IsAny<string>()), Times.AtLeastOnce);
            _unitOfWork.Verify(x => x.Publishers.Get(It.IsAny<string>()), Times.AtLeastOnce);
            _unitOfWork.Verify(e => e.Save(), Times.AtLeastOnce);
        }
    }
}