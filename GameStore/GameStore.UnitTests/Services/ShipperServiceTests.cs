﻿using System.Collections.Generic;
using GameStore.DAL.Entities.Mongo;
using GameStore.DAL.Interfaces;
using GameStore.Services.Services;
using GameStore.Tools.Logger;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace GameStore.UnitTests.Services
{
    public class ShipperServiceTests : TestBase
    {

        private ShipperService _shipperService;
        private Mock<IUnitOfWork> _unitOfWork;
        private Mock<ILoggerFileService> _logger;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            _unitOfWork = new Mock<IUnitOfWork>();
            _logger = new Mock<ILoggerFileService>();
            _shipperService = new ShipperService(_unitOfWork.Object,_logger.Object, Mapper);
        }

        [Test]
        public void GetAllShippers_ShouldReturnListShippers()
        {
            // Arrange
            var sut = Fixture.Create<List<Shipper>>();
            _unitOfWork.Setup(x => x.Shippers.GetAll()).Returns(sut);

            // Act
            var result = _shipperService.GetAllShippers();

            // Assert
            _unitOfWork.Verify(x => x.Shippers.GetAll(), Times.Once);
            Assert.IsNotNull(result);
        }
    }
}