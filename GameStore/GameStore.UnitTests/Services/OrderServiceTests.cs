﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Mongo;
using GameStore.DAL.Interfaces;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.Services.Services;
using GameStore.Tools.Logger;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace GameStore.UnitTests.Services
{
    public class OrderServiceTests : TestBase
    {
        private Mock<ILoggerFileService> _logger;

        private OrderService _orderService;
        private Mock<IUnitOfWork> _unitOfWork;
        private Mock<IValidationService> _validationService;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            _unitOfWork = new Mock<IUnitOfWork>();
            _validationService = new Mock<IValidationService>();
            _logger = new Mock<ILoggerFileService>();
            _orderService = new OrderService(_unitOfWork.Object, _logger.Object, Mapper, _validationService.Object);
        }

        [Test]
        public void Create_ShouldSuccessCreateOrder_WhenModelIsValid()
        {
            // Arrange
            var sut = Fixture.Create<OrderDto>();
            _unitOfWork.Setup(x => x.Orders.Insert(It.IsAny<Order>()));

            // Act
            _orderService.Create(sut);

            // Assert
            _unitOfWork.Verify(x => x.Orders.Insert(It.IsAny<Order>()), Times.Once);
            _unitOfWork.Verify(x => x.Save(), Times.Once);
        }

        [Test]
        public void LastCustomersOrder_ShouldReturnOrderDto_WhenIdIsValid()
        {
            // Arrange
            var sut = Fixture.Create<List<Order>>();
            _unitOfWork.Setup(x => x.Orders.Get(It.IsAny<Func<Order, bool>>())).Returns(sut);

            // Act
            var result = _orderService.LastCustomersOrder(sut[0].CustomerId);

            // Assert
            _unitOfWork.Verify(x => x.Orders.Get(It.IsAny<Func<Order,bool>>()), Times.Once);
            Assert.IsNotNull(result);
        }

        [Test]
        public void LastCustomersOrder_ShouldCreateOrderAndReturnOrderDto_WhenOrderNotExist()
        {
            // Arrange
            var sut = Fixture.Create<List<Order>>();
            _unitOfWork.Setup(x => x.Orders.Get(It.IsAny<Func<Order, bool>>())).Returns(new List<Order>());
            _unitOfWork.Setup(x => x.OrdersMongo.Get(It.IsAny<Expression<Func<OrderMongo, bool>>>())).Returns(new List<OrderMongo>());

            // Act
            _orderService.LastCustomersOrder(sut[0].CustomerId);

            // Assert
            _unitOfWork.Verify(x => x.Orders.Get(It.IsAny<Func<Order, bool>>()), Times.Once);
            _unitOfWork.Verify(x => x.OrdersMongo.Get(It.IsAny<Expression<Func<OrderMongo, bool>>>()), Times.Once);
        }

        [Test]
        public void GetAllOrders_ShouldReturnListOrders()
        {
            //Arrange
            var sut = Fixture.Create<List<Order>>();
            var products = new List<OrderMongo>();

            _unitOfWork.Setup(a => a.Orders.GetAll()).Returns(sut);
            _unitOfWork.Setup(x => x.OrdersMongo.GetAll()).Returns(products);

            //Act
            var result = _orderService.GetAllOrders();

            //Assert
            _unitOfWork.Verify(foo => foo.Orders.GetAll(), Times.Once);
            _unitOfWork.Verify(foo => foo.OrdersMongo.GetAll(), Times.Once);
            Assert.IsNotNull(result);
        }

        [Test]
        public void Update_ShoulSuccesUpdateOrder_WhenorderDtoIsValid()
        {
            // Arrange
            var sut = Fixture.Create<Order>();
            var orderDto = Fixture.Create<OrderDto>();
            _unitOfWork.Setup(x => x.Orders.GetById(It.IsAny<Guid>())).Returns(sut);

            // Act
            _orderService.Update(orderDto);

            // Assert
            _unitOfWork.Verify(x => x.Orders.GetById(It.IsAny<Guid>()), Times.Once);
            _unitOfWork.Verify(x => x.Orders.Update(It.IsAny<Order>()), Times.Once);
            _unitOfWork.Verify(x => x.Save(), Times.Once);
        }

        [Test]
        public void GetOrderById_ShouldReturnOrder_WhenIdIsValid()
        {
            // Arrange
            var sut = Fixture.Create<Order>();
            _unitOfWork.Setup(x => x.Orders.GetById(It.IsAny<Guid>())).Returns(sut);

            // Act
            var result = _orderService.GetOrderById(It.IsAny<Guid>());

            // Assert
            _unitOfWork.Verify(x => x.Orders.GetById(It.IsAny<Guid>()), Times.Once);
            Assert.IsNotNull(result);
        }

        [Test]
        public void Delete_ShouldDeleteOrder_WhenOrderFromDBIsNotNull()
        {
            // Arrange
            var sut = Fixture.Create<Order>();
            _unitOfWork.Setup(x => x.Orders.GetById(It.IsAny<Guid>())).Returns(sut);

            // Act
             _orderService.Delete(It.IsAny<Guid>());

            // Assert
            _unitOfWork.Verify(x => x.Orders.GetById(It.IsAny<Guid>()), Times.Once);
            _unitOfWork.Verify(x => x.Orders.Delete(It.IsAny<Guid>()), Times.Once);
            _unitOfWork.Verify(x => x.Save(), Times.Once);
        }

        [Test]
        public void Delete_ShouldCreateAndDeleteOrder_WhenOrderFromDBIsNull()
        {
            // Arrange
            var sut = new OrderMongo();
            _unitOfWork.Setup(x => x.Orders.GetById(It.IsAny<Guid>())).Returns(()=>null);
            _unitOfWork.Setup(x => x.OrdersMongo.Get(It.IsAny<string>())).Returns(sut);

            // Act
            _orderService.Delete(It.IsAny<Guid>());

            // Assert
            _unitOfWork.Verify(x => x.Orders.GetById(It.IsAny<Guid>()), Times.Once);
            _unitOfWork.Verify(x => x.OrdersMongo.Delete(It.IsAny<string>()), Times.Once);
            _unitOfWork.Verify(x => x.Save(), Times.Once);
        }

        [Test]
        public void EditStatus_ShouldSuccesfullUpdateOrder_WhenModelIsValid()
        {
            // Arrange
            var order = Fixture.Create<Order>();
            var orderDto = Fixture.Create<OrderDto>();
            _unitOfWork.Setup(x => x.Orders.GetById(It.IsAny<Guid>())).Returns(order);

            // Act
            _orderService.EditStatus(orderDto);

            // Assert
            _unitOfWork.Verify(x => x.Orders.GetById(It.IsAny<Guid>()), Times.Once);
            _unitOfWork.Verify(x => x.Save(), Times.Once);
        }

        [Test]
        public void GetOrdersBetweenDates_ShouldReturnListOrders_WhenDatesIsValid()
        {
            // Arrange
            var order = Fixture.Create<List<Order>>();

            var ordersMongo = new List<OrderMongo>();
            _unitOfWork.Setup(x => x.Orders.Get(It.IsAny<Func<Order,bool>>())).Returns(order);
            _unitOfWork.Setup(x => x.OrdersMongo.Get(It.IsAny<Expression<Func<OrderMongo, bool>>>())).Returns(ordersMongo);

            // Act
            var result = _orderService.GetOrdersBetweenDates(It.IsAny<DateTime>(), It.IsAny<DateTime>());

            // Assert
            _unitOfWork.Verify(x => x.Orders.Get(It.IsAny<Func<Order, bool>>()), Times.Once);
            _unitOfWork.Verify(x => x.OrdersMongo.Get(It.IsAny<Expression<Func<OrderMongo, bool>>>()), Times.Once);
           Assert.IsNotNull(result);
        }
    }
}