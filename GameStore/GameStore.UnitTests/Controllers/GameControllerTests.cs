﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using GameStore.Authorization.Infrastructure;
using GameStore.Controllers;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace GameStore.UnitTests.Controllers
{
    [TestFixture]
    public class GameControllerTests : TestBase
    {

        private Mock<IGameService> _gameService;
        private Mock<IGenreService> _genreService;
        private Mock<IPlatformTypeService> _platformService;
        private Mock<IPublisherService> _publisherService;
        private Mock<IFilterService> _filterService;
        private Mock<IAuthentication> _authService;

        private GameController _gamesController;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            _gameService = new Mock<IGameService>();
            _publisherService = new Mock<IPublisherService>();
            _genreService = new Mock<IGenreService>();
            _platformService = new Mock<IPlatformTypeService>();
            _filterService = new Mock<IFilterService>();
            _authService = new Mock<IAuthentication>();

            _gamesController = new GameController(_gameService.Object, Mapper, _genreService.Object,
                _platformService.Object, _publisherService.Object, _filterService.Object , _authService.Object);
        }

        [Test]
        public void Details_ShouldReturnNotNullResult_WhenKeyIsValid()
        {
            // Arrange
            var key = Fixture.Create<string>();
            var sut = Fixture.Create<List<GameDto>>();
            _gameService.Setup(a => a.GetGameByKey(It.IsAny<string>())).Returns(sut[0]);

            // Act
            var result = _gamesController.Details(key);

            // Assert
            _gameService.Verify(foo => foo.GetGameByKey(It.IsAny<string>()), Times.Once);
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof (ViewResult), result);
        }

        [Test]
        public void Download_ShouldReturnViewResult_WhenKeyValid()
        {
            // Arrange
            var key = Fixture.Create<string>();
            var gameDto = Fixture.Create<GameDto>();
            _gameService.Setup(x => x.GetGameByKey(key)).Returns(gameDto);

            var httpContext = new Mock<HttpContextBase>();
            httpContext.Setup(x => x.Server.MapPath("~/Content/File"))
                .Returns(
                    @"C:\Users\Oleksii_Parshyn\Documents\Visual Studio 2015\Projects\oleksii_parshyn\GameStore\GameStore\Content\File");

            _gamesController.ControllerContext = new ControllerContext(httpContext.Object, new RouteData(),
                _gamesController);

            // Act
            var result = _gamesController.Download(key);

            // Assert
            _gameService.Verify(foo => foo.GetGameByKey(It.IsAny<string>()), Times.Once);
            _gameService.Verify(x => x.WriteToFile(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            Assert.IsInstanceOf(typeof (FilePathResult), result);
        }

        [Test]
        public void GamesNumber_ShouldReturnContentType()
        {
            // Arrange
            var sut = Fixture.Create<List<GameDto>>();
            _gameService.Setup(a => a.GetAllGames()).Returns(sut);

            // Act
            var result = _gamesController.GamesNumber();

            // Assert
            _gameService.Verify(foo => foo.GetAllGames(), Times.Once);
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof (ContentResult), result);
        }

        [Test]
        public void GetGamesByGenre_ShouldReturnListGames_WhenIdIsValid()
        {
            // Arrange
            var sut = Fixture.Create<List<GameDto>>();
            var genreId = Fixture.Create<string>();
            _gameService.Setup(a => a.GetAllGamesForGenre(It.IsAny<string>())).Returns(sut);

            // Act
            var result = _gamesController.GetGamesByGenre(genreId);

            // Assert
            _gameService.Verify(foo => foo.GetAllGamesForGenre(It.IsAny<string>()), Times.Once);
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof (JsonResult), result);
        }

        [Test]
        public void GetGamesByPlatformType_ShouldReturnListGames_WhenIdIsValid()
        {
            // Arrange
            var sut = Fixture.Create<List<GameDto>>();
            var platformTypeId = Fixture.Create<string>();
            _gameService.Setup(a => a.GetAllGamesForPlatformType(It.IsAny<string>())).Returns(sut);

            // Act
            var result = _gamesController.GetGamesByPlatformType(platformTypeId);

            // Assert
            _gameService.Verify(foo => foo.GetAllGamesForPlatformType(It.IsAny<string>()), Times.Once);
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof (JsonResult), result);
        }

        [Test]
        public void ReturnFile_ShoulreturnFileSync()
        {
            //Arrange
            var sut = Fixture.Create<byte[]>();
            _gameService.Setup(x => x.GetImage(It.IsAny<string>())).Returns(sut);

            //Act
            var result = _gamesController.ReturnFile(It.IsAny<string>());

            //Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(FileResult), result);
        }
    }
}