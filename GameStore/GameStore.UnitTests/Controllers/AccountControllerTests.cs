﻿using System.Web.Mvc;
using GameStore.Authorization.Infrastructure;
using GameStore.Authorization.Models;
using GameStore.Controllers;
using GameStore.Services.Interfaces;
using Moq;
using NUnit.Framework;

namespace GameStore.UnitTests.Controllers
{
    [TestFixture]
    public class AccountControllerTests : TestBase
    {

        private Mock<IAuthentication> _authenticationService;
        private Mock<IUserService> _userService;

        private AccountController _accountController;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            _userService = new Mock<IUserService>();
            _authenticationService = new Mock<IAuthentication>();
            _accountController = new AccountController(Mapper , _userService.Object , _authenticationService.Object);
        }

        [Test]
        public void Login_ShouldReturnPartialView()
        {
            // Arrange
            var sut = new LoginModel();

            // Act
            var result = _accountController.Login();

            // Assert
            Assert.IsInstanceOf(typeof(PartialViewResult), result);
        }

    }
}
