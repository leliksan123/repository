﻿using System.Collections.Generic;
using System.Web.Mvc;
using GameStore.Areas.Admin.Controllers;
using GameStore.Authorization.Infrastructure;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.ViewModels.ViewModels.Game;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace GameStore.UnitTests.Controllers
{
    [TestFixture]
    public class AdminGameControllerTests : TestBase
    {

        private Mock<IGameService> _gameService;
        private Mock<IGenreService> _genreService;
        private Mock<IPlatformTypeService> _platformService;
        private Mock<IPublisherService> _publisherService;
        private Mock<IAuthentication> _authService;
        private Mock<IGameTranslation> _gameTranslationService;
        private Mock<IFilterService> _filterService;
        private Mock<ILanguageService> _languageService;

        private GameController _gamesController;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            _gameService = new Mock<IGameService>();
            _publisherService = new Mock<IPublisherService>();
            _filterService = new Mock<IFilterService>();
            _genreService = new Mock<IGenreService>();
            _platformService = new Mock<IPlatformTypeService>();
            _authService = new Mock<IAuthentication>();
            _languageService = new Mock<ILanguageService>();
            _gameTranslationService = new Mock<IGameTranslation>();
            _gamesController = new GameController(_authService.Object , Mapper , _gameService.Object, _genreService.Object , _platformService.Object, _publisherService.Object , _gameTranslationService.Object , _filterService.Object , _languageService.Object);

        }

        [Test]
        public void New_ShouldReturnRedirectResultIndex_WhenModelStateIsValid()
        {
            // Arrange
            var sut = new CreateGameViewModel();

            // Act
            var result = _gamesController.New(sut);

            // Assert

            _gameService.Verify(x => x.Create(It.IsAny<GameDto>()), Times.Once);
            Assert.IsInstanceOf(typeof(RedirectToRouteResult), result);
        }

        [Test]
        public void New_ShouldReturnRedirectResultNew_WhenModelStateIsNotValid()
        {
            // Arrange
            var sut = new CreateGameViewModel();
            _gamesController.ModelState.AddModelError("model", "error message");

            // Act
            var result = _gamesController.New(sut);

            // Assert
            _genreService.Verify(foo => foo.GetGenresByName(It.IsAny<List<string>>()), Times.Never);
            _platformService.Verify(x => x.GetPlatformTypesById(It.IsAny<List<int>>()), Times.Never);
            Assert.IsInstanceOf(typeof(RedirectToRouteResult), result);
        }

        [Test]
        public void Remove_ShouldReturnSuccessStatusCode_WhenIdIsValid()
        {
            // Arrange
            var id = Fixture.Create<string>();

            // Act
            var result = _gamesController.Remove(id);

            // Assert
            _gameService.Verify(foo => foo.Delete(It.IsAny<string>()), Times.Once);
            Assert.IsInstanceOf(typeof(ContentResult), result);
        }

        [Test]
        public void Update_ShouldReturnHttpBadRequest_WhenModelIsInvalid()
        {
            // Arrange
            var sut = new GameUpdateViewModel();
            _gamesController.ModelState.AddModelError("model", "error message");

            // Act
            var result = _gamesController.Update(sut);

            // Assert
            _gameService.Verify(foo => foo.Update(It.IsAny<GameDto>()), Times.Never);
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(RedirectToRouteResult), result);
        }

        [Test]
        public void Update_ShouldReturnHttpStatusCodeOK_WhenModelIsValid()
        {
            // Arrange
            var sut = new GameUpdateViewModel();

            // Act
            var result = _gamesController.Update(sut);

            // Assert
            _gameService.Verify(foo => foo.Update(It.IsAny<GameDto>()), Times.Once);
            Assert.IsInstanceOf(typeof(RedirectToRouteResult), result);
        }

        [Test]
        public void Details_ShouldReturnNotNullResult_WhenKeyIsValid()
        {
            // Arrange
            var key = Fixture.Create<string>();
            var sut = Fixture.Create<List<GameDto>>();
            _gameService.Setup(a => a.GetGameByKey(It.IsAny<string>())).Returns(sut[0]);

            // Act
            var result = _gamesController.Details(key);

            // Assert
            _gameService.Verify(foo => foo.GetGameByKey(It.IsAny<string>()), Times.Once);
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(ViewResult), result);
        }
    }
}
