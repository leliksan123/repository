﻿using System.Web.Mvc;
using GameStore.Areas.Admin.Controllers;
using GameStore.Authorization.Infrastructure;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.ViewModels.ViewModels.Role;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace GameStore.UnitTests.Controllers
{
    [TestFixture]
    public class RoleControllerTests : TestBase
    {
        private Mock<IRoleService> _roleService;
        private Mock<IAuthentication> _authenticationService;

        private RoleController _roleController;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            _roleService = new Mock<IRoleService>();
            _authenticationService = new Mock<IAuthentication>();
            _roleController = new RoleController(_authenticationService.Object, Mapper, _roleService.Object);
        }

        [Test]
        public void Create_ShouldCreateRole_IfModelIsValid()
        {
            //Arrange
            var sut = Fixture.Create<RoleViewModel>();
            _roleService.Setup(x => x.Create(It.IsAny<RoleDTo>()));

            //Act
            var result = _roleController.Create(sut);

            //Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(RedirectToRouteResult), result);
        }

        [Test]
        public void Create_ShouldReturnView_IfModelIsNotValid()
        {
            //Arrange
            _roleController.ModelState.AddModelError("model", "error message");
            _roleService.Setup(x => x.Create(It.IsAny<RoleDTo>()));

            //Act
            var result = _roleController.Create(null);

            //Assert
            Assert.IsInstanceOf(typeof(ViewResult), result);
        }
    }
}