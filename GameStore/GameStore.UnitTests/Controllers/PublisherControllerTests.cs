﻿using System.Web.Mvc;
using GameStore.Authorization.Infrastructure;
using GameStore.Controllers;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace GameStore.UnitTests.Controllers
{
    [TestFixture]
    public class PublisherControllerTests : TestBase
    {
        private Mock<IPublisherService> _publisherService;
        private Mock<IAuthentication> _authenticationService;

        private PublisherController _publisherController;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            _publisherService = new Mock<IPublisherService>();
            _authenticationService = new Mock<IAuthentication>();
            _publisherController = new PublisherController(Mapper, _publisherService.Object , _authenticationService.Object);
        }

        [Test]
        public void Details_ShouldReturnHttpNotFound_WhenCompanyNameIsInvalid()
        {
            // Arrange

            // Act
            var result = _publisherController.Details(null);

            // Assert            
            _publisherService.Verify(x => x.GetPublisherByCompanyName(It.IsAny<string>()), Times.Never);
            Assert.IsInstanceOf(typeof (HttpNotFoundResult), result);
        }

        [Test]
        public void Details_ShouldReturnViewResult_WhenCompanyNameIsValid()
        {
            // Arrange
            var gamekey = Fixture.Create<string>();
            var publisher = Fixture.Create<PublisherDto>();
            _publisherService.Setup(x => x.GetPublisherByCompanyName(It.IsAny<string>())).Returns(publisher);

            // Act
            var result = _publisherController.Details(gamekey);

            // Assert            
            _publisherService.Verify(x => x.GetPublisherByCompanyName(It.IsAny<string>()), Times.Once);
            Assert.IsNotNull(result);
        }
    }
}