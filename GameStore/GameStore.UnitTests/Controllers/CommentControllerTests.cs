using System.Collections.Generic;
using System.Web.Mvc;
using GameStore.Authorization.Infrastructure;
using GameStore.Controllers;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.ViewModels.ViewModels.Comment;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace GameStore.UnitTests.Controllers
{
    [TestFixture]
    public class CommentControllerTests : TestBase
    {
        private Mock<ICommentService> _commentService;
        private Mock<IGameService> _gameService;
        private Mock<IAuthentication> _authenticationService;

        private CommentController _commentsController;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            _commentService = new Mock<ICommentService>();
            _gameService = new Mock<IGameService>();
            _authenticationService = new Mock<IAuthentication>();
            _commentsController = new CommentController(_commentService.Object, Mapper, _gameService.Object , _authenticationService.Object);
        }

        [Test]
        public void Comments_ShouldReturnCommentsList_WhenGameKeyIsValid()
        {
            // Arrange
            var commentsList = Fixture.Create<List<CommentDto>>();
            _commentService.Setup(a => a.GetAllCommentsForGame(It.IsAny<string>())).Returns(commentsList);
            var gamekey = Fixture.Create<string>();

            // Act
            var result = _commentsController.Comments(gamekey);

            // Assert
            _commentService.Verify(foo => foo.GetAllCommentsForGame(It.IsAny<string>()), Times.Once);
            Assert.IsNotNull(result);
        }

        [Test]
        public void Comments_ShouldReturnViewResult_WhenGameKeyIsValid()
        {
            // Arrange
            var commentsList = Fixture.Create<List<CommentDto>>();
            _commentService.Setup(a => a.GetAllCommentsForGame(It.IsAny<string>())).Returns(commentsList);
            var gamekey = Fixture.Create<string>();

            // Act
            var result = _commentsController.Comments(gamekey);

            // Assert
            _commentService.Verify(foo => foo.GetAllCommentsForGame(It.IsAny<string>()), Times.Once);
            Assert.IsInstanceOf(typeof (ViewResult), result);
        }

        [Test]
        public void NewComment_ShouldReturnRedirectToAction_WhenModelIsNotValid()
        {
            // Arrange

            // Act
            _commentsController.ModelState.AddModelError("model", "error message");
            var result = _commentsController.NewComment(new CommentFormViewModel());

            // Assert
            _gameService.Verify(x => x.GetGameByKey(It.IsAny<string>()), Times.Never);
            _commentService.Verify(foo => foo.Create(It.IsAny<CommentDto>()), Times.Never);
            Assert.IsInstanceOf(typeof (RedirectToRouteResult), result);
        }

        [Test]
        public void NewComment_ShouldReturnRedirectToAction_WhenValidModelAndValidKey()
        {
            // Arrange
            var gamedto = Fixture.Create<GameDto>();
            var sut = Fixture.Create<CommentFormViewModel>();
            _gameService.Setup(x => x.GetGameByKey(It.IsAny<string>())).Returns(gamedto);
            // Act
            var result = _commentsController.NewComment(sut);

            // Assert
            _gameService.Verify(x => x.GetGameByKey(It.IsAny<string>()), Times.Once);
            _commentService.Verify(foo => foo.Create(It.IsAny<CommentDto>()), Times.Once);
            Assert.IsInstanceOf(typeof (RedirectToRouteResult), result);
        }
    }
}