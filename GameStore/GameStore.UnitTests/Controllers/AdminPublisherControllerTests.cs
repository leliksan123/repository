﻿using System.Web.Mvc;
using GameStore.Areas.Admin.Controllers;
using GameStore.Authorization.Infrastructure;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.ViewModels.ViewModels.Publisher;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace GameStore.UnitTests.Controllers
{
    [TestFixture]
    public class AdminPublisherControllerTests : TestBase
    {
        private Mock<IPublisherService> _publisherService;
        private Mock<IAuthentication> _authenticationService;
        private Mock<IPublisherTranslation> _publisherTranslateService;
        private Mock<ILanguageService> _languageService;

        private PublisherController _publisherController;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            _publisherService = new Mock<IPublisherService>();
            _authenticationService = new Mock<IAuthentication>();
            _languageService = new Mock<ILanguageService>();
            _publisherTranslateService = new Mock<IPublisherTranslation>();
            _publisherController = new PublisherController( _authenticationService.Object , Mapper , _publisherService.Object , _publisherTranslateService.Object , _languageService.Object);
        }


        [Test]
        public void New_ShouldReturnRedirectToAction_WhenModelIsValid()
        {
            // Arrange
            var model = Fixture.Create<PublisherViewModel>();

            // Act
            var result = _publisherController.New(model);

            // Assert            
            _publisherService.Verify(x => x.Create(It.IsAny<PublisherDto>()), Times.Once);
            Assert.IsInstanceOf(typeof(RedirectToRouteResult), result);
        }

        [Test]
        public void New_ShouldReturnViewResult_WhenModelIsInvalid()
        {
            // Arrange
            var model = Fixture.Create<PublisherViewModel>();
            _publisherController.ModelState.AddModelError("model", "error message");

            // Act
            var result = _publisherController.New(model);

            // Assert            
            _publisherService.Verify(x => x.Create(It.IsAny<PublisherDto>()), Times.Never);
            Assert.IsInstanceOf(typeof(ViewResult), result);
        }
    }
}
