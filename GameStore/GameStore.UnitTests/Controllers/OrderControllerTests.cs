﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using GameStore.Authorization.Infrastructure;
using GameStore.Controllers;
using GameStore.Extensions.Enums;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.Services.Notifications;
using GameStore.Tools.EmailSender;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace GameStore.UnitTests.Controllers
{
    [TestFixture]
    public class OrderControllerTests : TestBase
    {
        private Mock<IOrderService> _orderService;
        private Mock<IOrderDetailsService> _orderDetailsService;
        private Mock<IAuthentication> _authenticationService;
        private Mock<IUserService> _userService;
        private Mock<IObservable> _observable;
        private Mock<IEmailSender> _emailSender;
        private Mock<HttpSessionStateBase> _sessionState;
        private Mock<HttpContextBase> _contextBase;
        private Mock<HttpRequestBase> _requestBase;

        private OrderController _orderController;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();
            _orderService = new Mock<IOrderService>();
            _sessionState = new Mock<HttpSessionStateBase>();
            _userService = new Mock<IUserService>();
            _contextBase = new Mock<HttpContextBase>();
            _requestBase = new Mock<HttpRequestBase>();
            _authenticationService = new Mock<IAuthentication>();
            _orderDetailsService = new Mock<IOrderDetailsService>();
            _observable = new Mock<IObservable>();
            _emailSender = new Mock<IEmailSender>();
            _orderController = new OrderController(_orderService.Object, Mapper, _orderDetailsService.Object , _authenticationService.Object , _userService.Object , _observable.Object , _emailSender.Object);
        }

        [Test]
        public void CreateOrder_ShouldReturnFileContentResult_WhenParameterIsBank()
        {
            // Arrange
            var sum = Fixture.Create<decimal>();
            var order = Fixture.Create<OrderDto>();
            _contextBase.Setup(ctx => ctx.Session).Returns(_sessionState.Object);
            _contextBase.Setup(x => x.Request.Cookies).Returns(new HttpCookieCollection { new HttpCookie("Game")});
            _contextBase.Setup(x => x.Response.Cookies).Returns(new HttpCookieCollection { new HttpCookie("Game") });
            _orderDetailsService.Setup(x => x.OrderSum(It.IsAny<List<OrderDetailsDto>>())).Returns(sum);
            _orderService.Setup(x => x.LastCustomersOrder(It.IsAny<int>())).Returns(order);
            _contextBase.Setup(m => m.User.Identity.Name).Returns("Admin");
            _userService.Setup(x => x.GetUserByEmail(It.IsAny<string>())).Returns(new UserDTo());

            var rc = new RequestContext(_contextBase.Object, new RouteData());
            _orderController.ControllerContext = new ControllerContext(rc, _orderController);

            // Act
            var result = _orderController.CreateOrder(PaymentType.Bank);

            // Assert 
            Assert.IsInstanceOf(typeof(FileContentResult), result);
        }

        [Test]
        public void CreateOrder_ShouldReturnVisaPage_WhenParameterIsVisa()
        {
            // Arrange
            var sum = Fixture.Create<decimal>();
            var order = Fixture.Create<OrderDto>();
            _contextBase.Setup(ctx => ctx.Session).Returns(_sessionState.Object);
            _contextBase.Setup(x => x.Request.Cookies).Returns(new HttpCookieCollection { new HttpCookie("Game") });
            _contextBase.Setup(x => x.Response.Cookies).Returns(new HttpCookieCollection { new HttpCookie("Game") });
            _orderDetailsService.Setup(x => x.OrderSum(It.IsAny<List<OrderDetailsDto>>())).Returns(sum);
            _orderService.Setup(x => x.LastCustomersOrder(It.IsAny<int>())).Returns(order);
            _contextBase.Setup(m => m.User.Identity.Name).Returns("Admin");
            _userService.Setup(x => x.GetUserByEmail(It.IsAny<string>())).Returns(new UserDTo());

            var rc = new RequestContext(_contextBase.Object, new RouteData());
            _orderController.ControllerContext = new ControllerContext(rc, _orderController);

            // Act
            var result = _orderController.CreateOrder(PaymentType.VisaCard);

            // Assert 
            Assert.IsInstanceOf(typeof(ViewResult), result);
        }

        [Test]
        public void MakeOrder_ShouldReturnReturnViewResultWithOrderDetails_WhenCookiesIsNotNull()
        {
            // Arrange
            var listOrderDetails = Fixture.Create<List<OrderDetailsDto>>();
            var sutList = Fixture.Create<Dictionary<string, short>>();

            _orderDetailsService.Setup(x => x.GetOrderDetailsForOrder(It.IsAny<OrderDto>())).Returns(listOrderDetails);

            _contextBase.Setup(x => x.Request).Returns(_requestBase.Object);
            _contextBase.Setup(x => x.Request.Cookies)
                .Returns(new HttpCookieCollection { new HttpCookie("Game") { Value = new JavaScriptSerializer().Serialize(sutList) } });
            _contextBase.Setup(ctx => ctx.Session).Returns(_sessionState.Object);
            var rc = new RequestContext(_contextBase.Object, new RouteData());
            _orderController.ControllerContext = new ControllerContext(rc, _orderController);
            _contextBase.Setup(m => m.User.Identity.Name).Returns("Admin");
            _userService.Setup(x => x.GetUserByEmail(It.IsAny<string>())).Returns(new UserDTo());

            // Act
            var result = _orderController.MakeOrder();

            // Assert
            _orderDetailsService.Verify(x => x.GetOrderDetailsForOrder(It.IsAny<OrderDto>()), Times.Once);
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(ViewResult), result);
        }
    }
}