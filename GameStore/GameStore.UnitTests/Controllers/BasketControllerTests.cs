﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using GameStore.Authorization.Infrastructure;
using GameStore.Controllers;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace GameStore.UnitTests.Controllers
{
    [TestFixture]
    public class BasketControllerTests : TestBase
    {
        private Mock<IGameService> _gameService;
        private Mock<IAuthentication> _authService;

        private BasketController _busketController;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            _gameService = new Mock<IGameService>();
            _authService = new Mock<IAuthentication>();
            _busketController = new BasketController(Mapper, _gameService.Object , _authService.Object);
        }

        [Test]
        public void Delete_ShouldRedirectToActionIndex_IfCookiesIsNotNull()
        {
            // Arrange
            var ser = new JavaScriptSerializer();
            var sutList = Fixture.Create<Dictionary<string , short>>();
            var key = Fixture.Create<string>();

            var httpRequestBase = new Mock<HttpRequestBase>();
            var httpContext = new Mock<HttpContextBase>();
            httpContext.Setup(x => x.Request).Returns(httpRequestBase.Object);
            httpContext.Setup(x => x.Request.Cookies)
                .Returns(new HttpCookieCollection {new HttpCookie("Game") {Value = ser.Serialize(sutList)}});
            httpContext.SetupGet(x => x.Response.Cookies).Returns(new HttpCookieCollection());

            var rc = new RequestContext(httpContext.Object, new RouteData());
            _busketController.ControllerContext = new ControllerContext(rc, _busketController);

            // Act
            var result = _busketController.Delete(key);

            // Assert
            Assert.IsInstanceOf(typeof(RedirectToRouteResult), result);
        }


        [Test]
        public void Delete_ShouldReturnHttpStatusCodeResultBadRequest_IfCookiesIsNull()
        {
            // Arrange
            var httpRequestBase = new Mock<HttpRequestBase>();
            var httpContext = new Mock<HttpContextBase>();
            httpContext.Setup(x => x.Request).Returns(httpRequestBase.Object);
            httpContext.Setup(x => x.Request.Cookies).Returns(new HttpCookieCollection());

            var rc = new RequestContext(httpContext.Object, new RouteData());
            _busketController.ControllerContext = new ControllerContext(rc, _busketController);

            // Act
            var result = _busketController.Delete(It.IsAny<string>()) as HttpStatusCodeResult;

            // Assert
            Assert.IsInstanceOf(typeof(HttpStatusCodeResult), result);
            Assert.IsNotNull(result);
            Assert.AreEqual(400, result.StatusCode);
        }

        [Test]
        public void Index_ShouldReturnHttpStatusCodeResultBadRequest_IfCookiesIsNull()
        {
            // Arrange
            var sut = Fixture.Create<GameDto>();

            _gameService.Setup(a => a.GetGameByKey(It.IsAny<string>())).Returns(sut);

            var httpRequestBase = new Mock<HttpRequestBase>();
            var httpContext = new Mock<HttpContextBase>();
            httpContext.Setup(x => x.Request).Returns(httpRequestBase.Object);
            httpContext.Setup(x => x.Request.Cookies).Returns(new HttpCookieCollection());

            var rc = new RequestContext(httpContext.Object, new RouteData());
            _busketController.ControllerContext = new ControllerContext(rc, _busketController);

            // Act
            var result = _busketController.Index();

            // Assert
            _gameService.Verify(foo => foo.GetGameByKey(It.IsAny<string>()), Times.Never);
            Assert.IsInstanceOf(typeof(ViewResult), result);
            Assert.IsNotNull(result);
        }

        [Test]
        public void Index_ShouldReturnListGameQuatityViewModel_IfCookiesNotNull()
        {
            // Arrange
            var sut = Fixture.Create<GameDto>();
            var ser = new JavaScriptSerializer();
            var sutList = Fixture.Create<Dictionary<string,short>>();

            _gameService.Setup(a => a.GetGameByKey(It.IsAny<string>())).Returns(sut);

            var httpRequestBase = new Mock<HttpRequestBase>();
            var httpContext = new Mock<HttpContextBase>();
            httpContext.Setup(x => x.Request).Returns(httpRequestBase.Object);
            httpContext.Setup(x => x.Request.Cookies)
                .Returns(new HttpCookieCollection {new HttpCookie("Game") {Value = ser.Serialize(sutList)}});

            var rc = new RequestContext(httpContext.Object, new RouteData());
            _busketController.ControllerContext = new ControllerContext(rc, _busketController);

            // Act
            var result = _busketController.Index();

            // Assert
            _gameService.Verify(foo => foo.GetGameByKey(It.IsAny<string>()), Times.AtLeastOnce);
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(ViewResult), result);
        }
    }
}