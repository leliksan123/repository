﻿using System;
using System.Web.Mvc;
using GameStore.Areas.Admin.Controllers;
using GameStore.Authorization.Infrastructure;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.ViewModels.ViewModels.Comment;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace GameStore.UnitTests.Controllers
{
    [TestFixture]
    public class AdminCommentControllerTests : TestBase
    {
        private Mock<ICommentService> _commentService;
        private Mock<IFilterService> _filterService;
        private Mock<IAuthentication> _authenticationService;

        private CommentController _commentsController;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            _commentService = new Mock<ICommentService>();
            _filterService = new Mock<IFilterService>();
            _authenticationService = new Mock<IAuthentication>();
            _commentsController = new CommentController( _authenticationService.Object , Mapper , _commentService.Object , _filterService.Object);
        }


        [Test]
        public void Delete_ShouldReturnRedirect_WhenkeyIsValid()
        {
            // Arrange
            var key = Fixture.Create<string>();
            var id = Fixture.Create<Guid>();

            // Act
            var result = _commentsController.Delete(id,key);

            // Assert
            _commentService.Verify(foo => foo.DeleteComment(It.IsAny<Guid>()), Times.Once);
            Assert.IsInstanceOf(typeof(RedirectToRouteResult), result);
        }

        [Test]
        public void Update_ShouldReturnViewResult_WhenIdIsValid()
        {
            // Arrange
            var id = Fixture.Create<Guid>();

            // Act
            var result = _commentsController.Update(id);

            // Assert
            _commentService.Verify(foo => foo.GetCommentById(It.IsAny<Guid>()), Times.Once);
            Assert.IsInstanceOf(typeof(ViewResult), result);
        }

        [Test]
        public void Update_ShouldReturnRedirectToActionResult_WhenModelIsValid()
        {
            // Arrange
            var model = Fixture.Create<CommentViewModel>();

            // Act
            var result = _commentsController.Update(model);

            // Assert
            _commentService.Verify(foo => foo.Update(It.IsAny<CommentDto>()), Times.Once);
            Assert.IsInstanceOf(typeof(RedirectToRouteResult), result);
        }
    }
}
