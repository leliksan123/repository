﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GameStore.ViewModels.ViewModels.Game;
using Resources;

namespace GameStore.ViewModels.ViewModels.Order
{
    public class OrderDetailsViewModel
    {
        public Guid Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ProductIdRequired")]
        public int ProductId { get; set; }

        [Required]
        public short Quantity { get; set; }

        public double Discount { get; set; }

        [Required]
        public Guid OrderId { get; set; }

        public virtual ICollection<GameViewModel> Games { get; set; }

        public string SelectedGame { get; set; }
    }
}