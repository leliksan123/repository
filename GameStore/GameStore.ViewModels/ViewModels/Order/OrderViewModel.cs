﻿using System;
using System.Collections.Generic;
using GameStore.Extensions.Enums;
using GameStore.Services.DTO;

namespace GameStore.ViewModels.ViewModels.Order
{
    public class OrderViewModel
    {
        public Guid Id { get; set; }

        public int CustomerId { get; set; }

        public OrderStatus Status { get; set; }

        public DateTime? ShippedDate { get; set; }

        public DateTime OrderDate { get; set; }

        public virtual ICollection<OrderDetailsDto> OrderDetailses { get; set; }
    }
}