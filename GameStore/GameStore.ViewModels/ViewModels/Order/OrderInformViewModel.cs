﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GameStore.ViewModels.ViewModels.Order
{
    public class OrderInformViewModel
    {
        public int UserId { get; set; }

        public Guid OrderId { get; set; }

        [Required(ErrorMessage = "Sum is required.")]
        [Range(0, 10000)]
        public decimal Sum { get; set; }

        public DateTime OrderDate { get; set; }
    }
}