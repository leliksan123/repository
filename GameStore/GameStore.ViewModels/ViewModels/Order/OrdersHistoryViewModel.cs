﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GameStore.ViewModels.ViewModels.Order
{
    public class OrdersHistoryViewModel
    {
        [DataType(DataType.Date)]
        public DateTime DateFrom { get; set; }

        [DataType(DataType.Date)]
        public DateTime DateTo { get; set; }

        public ICollection<OrderViewModel> Orders { get; set; }
    }
}