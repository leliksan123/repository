﻿using System.Collections.Generic;
using GameStore.ViewModels.ViewModels.Game;

namespace GameStore.ViewModels.ViewModels.Basket
{
    public class BasketListGamesViewModel
    {
        public List<GameViewModel> GamesList { get; set; }

        public short Quantity { get; set; }
    }
}