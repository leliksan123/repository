﻿namespace GameStore.ViewModels.ViewModels.Basket
{
    public class BasketViewModel
    {
        public int GameId { get; set; }

        public string GameKey { get; set; }

        public string GameName { get; set; }

        public decimal GamePrice { get; set; }
    }
}