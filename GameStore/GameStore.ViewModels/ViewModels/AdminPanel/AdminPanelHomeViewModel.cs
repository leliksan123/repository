﻿using System.Collections.Generic;
using GameStore.ViewModels.ViewModels.User;

namespace GameStore.ViewModels.ViewModels.AdminPanel
{
    public class AdminPanelHomeViewModel
    {
        public List<UserViewModel> Users { get; set; }
    }
}