﻿using System;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace GameStore.ViewModels.ViewModels.User
{
    public class UserInformationViewModel
    {
        public Guid UserId { get; set; }

        public Guid OrderId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "SumRequired")]
        [Range(0, 10000, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Limit")]
        public decimal Sum { get; set; }
    }
}