﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using GameStore.ViewModels.ViewModels.Role;

namespace GameStore.ViewModels.ViewModels.User
{
    public class UserViewModel
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        public int Id { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Phone { get; set; }

        public bool NotificationEmail { get; set; }

        public bool NotificationSms { get; set; }

        public bool NotificationMobileApp { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime RegistrationTime { get; set; }

        public List<string> SelectedRoles { get; set; }

        public virtual ICollection<RoleViewModel> Roles { get; set; }
    }
}