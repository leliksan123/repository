﻿namespace GameStore.ViewModels.ViewModels.Shipper
{
    public class ShipperViewModel
    {
        public string Id { get; set; }

        public int ShipperId { get; set; }

        public string CompanyName { get; set; }

        public string Phone { get; set; }
    }
}