﻿using System;
using System.Collections.Generic;

namespace GameStore.ViewModels.ViewModels.Comment
{
    public class ApiComment
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Body { get; set; }

        public string Quote { get; set; }

        public string GameKey { get; set; }

        public Guid? ParentCommentId { get; set; }

        public ICollection<ApiComment> Answers { get; set; }

    }
}
