﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using GameStore.Services.DTO;
using Resources;

namespace GameStore.ViewModels.ViewModels.Comment
{
    public class CommentViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public Guid Id { get; set; }

        [DataType(DataType.EmailAddress)]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(Resource),
             ErrorMessageResourceName = "EnterEmail")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Invalid Email")]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(Resource),
             ErrorMessageResourceName = "EnterCommentBody")]
        public string Body { get; set; }

        public string Quote { get; set; }

        public string GameKey { get; set; }

        [Required]
        public int GameId { get; set; }

        public Guid? ParentCommentId { get; set; }

        public virtual ICollection<CommentViewModel> Answers { get; set; }

        public virtual GameDto Game { get; set; }
    }
}