﻿using System.Collections.Generic;

namespace GameStore.ViewModels.ViewModels.Comment
{
    public class CommentFormViewModel
    {
        public List<CommentViewModel> Comments { get; set; }

        public CommentViewModel Comment { get; set; }

        public string GameKey { get; set; }
    }
}