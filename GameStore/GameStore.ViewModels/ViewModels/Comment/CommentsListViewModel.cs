﻿using System.Collections.Generic;

namespace GameStore.ViewModels.ViewModels.Comment
{
    public class CommentsListViewModel
    {
        public List<CommentViewModel> Comments { get; set; }
    }
}