﻿using System.Collections.Generic;

namespace GameStore.ViewModels.ViewModels.Genre
{
    public class GenreListViewModel
    {
        public List<GenreViewModel> Genres { get; set; }
    }
}