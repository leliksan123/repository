﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;

namespace GameStore.ViewModels.ViewModels.Genre
{
    public class GenreViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "GameName")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "GenreName")]
        public string Name { get; set; }

        public bool Selected { get; set; }

        public int? ParentGenreId { get; set; }

        public List<GenreViewModel> Genres { get; set; }
    }
}