﻿using System.Collections.Generic;

namespace GameStore.ViewModels.ViewModels.Publisher
{
    public class PublishersListViewModel
    {
        public List<PublisherViewModel> Publishers { get; set; }
    }
}