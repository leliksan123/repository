﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using GameStore.ViewModels.ViewModels.Game;
using Resources;

namespace GameStore.ViewModels.ViewModels.Publisher
{
    public class PublisherViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "RequiredCompany")]
        [StringLength(40, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "CompanyLenght")]
        public string CompanyName { get; set; }

        public string Description { get; set; }

        public string DescriptionEnglish { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "RequiredHomePage")]
        public string HomePage { get; set; }

        public bool Selected { get; set; }

        public List<string> SelectedGames { get; set; }

        public virtual ICollection<GameViewModel> Games { get; set; }
    }
}