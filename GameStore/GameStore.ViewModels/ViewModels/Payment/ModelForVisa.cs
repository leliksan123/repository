﻿using System.ComponentModel.DataAnnotations;
using GameStore.Extensions.Enums;
using GameStore.ViewModels.Models;
using Resources;

namespace GameStore.ViewModels.ViewModels.Payment
{
    public class ModelForVisa
    {
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "CardHolder")]
        [StringLength(160, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "CardHolderName")]
        public string CartHoldersName { get; set; }

        [Required]
        public string CardHoldersSurName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "CardNumberRequired")]
        [StringLength(16, MinimumLength = 16, ErrorMessage = "Card number 16 characters")]
        public string CardNumber { get; set; }

        [Required]
        [Range(1, 12, ErrorMessage = "Invalid Month")]
        public int Month { get; set; }

        [Required]
        [Year(ErrorMessage = "Please, write Valid Year")]
        public int Year { get; set; }

        [RegularExpression(@"^(\d{3})$", ErrorMessageResourceType = typeof(Resource),
             ErrorMessageResourceName = "ValidCvv2")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Cvv2Required")]
        public int Cvv2 { get; set; }

        [Required]
        public CardTypes CardTypes { get; set; }

        [Required]
        public decimal AmounthOfPayment { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string Purpose { get; set; }
    }
}