﻿using System.Collections.Generic;
using GameStore.ViewModels.Models;

namespace GameStore.ViewModels.ViewModels.Payment
{
    public class PaymentsViewModel
    {
        public List<PaymentTableModel> PaymentTableModels { get; set; }
    }
}