﻿namespace GameStore.ViewModels.ViewModels.Language
{
    public class LanguageViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}