﻿namespace GameStore.ViewModels.ViewModels.Role
{
    public class RoleViewModel
    {
        public int Id { get; set; }

        public string RoleName { get; set; }
    }
}