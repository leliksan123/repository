﻿using System.Collections.Generic;

namespace GameStore.ViewModels.ViewModels.Role
{
    public class RolesListViewModel
    {
        public List<RoleViewModel> Roles { get; set; }
    }
}