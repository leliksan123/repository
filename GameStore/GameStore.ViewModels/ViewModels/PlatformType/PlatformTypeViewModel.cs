﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;

namespace GameStore.ViewModels.ViewModels.PlatformType
{
    public class PlatformTypeViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "TypeRequired")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "TypeLenght")]
        public string Type { get; set; }

        public bool Selected { get; set; }
    }
}