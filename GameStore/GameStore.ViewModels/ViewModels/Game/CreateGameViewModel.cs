﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using GameStore.Services.DTO;
using Resources;

namespace GameStore.ViewModels.ViewModels.Game
{
    public class CreateGameViewModel
    {

        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof (Resource), ErrorMessageResourceName = "KeyRequired")]
        [StringLength(50, ErrorMessageResourceType = typeof (Resource), ErrorMessageResourceName = "KeyLenght")]
        public string Key { get; set; }

        [Required(ErrorMessageResourceType = typeof (Resource), ErrorMessageResourceName = "GameName")]
        [StringLength(100, ErrorMessageResourceType = typeof (Resource), ErrorMessageResourceName = "NameLenght")]
        public string Name { get; set; }

        public string Description { get; set; }

        public string Picture { get; set; }

        public HttpPostedFileBase File { get; set; }

        public string DescriptionEnglish { get; set; }

        [Required(ErrorMessageResourceType = typeof (Resource), ErrorMessageResourceName = "PriceRequired")]
        [Range(typeof(decimal), "0", "10000", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Limit")]
        public decimal Price { get; set; }

        [Required(ErrorMessageResourceType = typeof (Resource), ErrorMessageResourceName = "UnitInStock")]
        [Range(0, 10000, ErrorMessageResourceType = typeof (Resource), ErrorMessageResourceName = "Limit")]
        [RegularExpression("([0-9]+)", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "NumberInvalid")]
        public short UnitsInStock { get; set; }

        public bool Discontinued { get; set; }

        public int ViewsNumber { get; set; }

        public bool IsDeleted { get; set; }

        public string PublisherName { get; set; }

        public DateTime AddedToGameStore { get; set; }

        public List<string> SelectedPlatformTypes { get; set; }

        public List<string> SelectedGenres { get; set; }

        public List<string> SelectedPublishers { get; set; }

        public virtual ICollection<PublisherDto> Publishers { get; set; }

        public virtual ICollection<PlatformTypeDto> PlatformTypes { get; set; }

        public virtual ICollection<GenreDto> Genres { get; set; }

        public virtual CommentDto Comment { get; set; }
    }
}