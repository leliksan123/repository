﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;

namespace GameStore.ViewModels.ViewModels.Game
{
    public class GameViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "KeyRequired")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "KeyLenght")]
        public string Key { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "GameName")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "NameLenght")]
        public string Name { get; set; }

        public string Description { get; set; }

        public string DescriptionEnglish { get; set; }

        public string Picture { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "PriceRequired")]
        [Range(typeof(decimal), "0", "10000", ErrorMessageResourceType = typeof(Resource),
             ErrorMessageResourceName = "Limit")]
        public decimal Price { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "UnitInStock")]
        [Range(0, 10000, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Limit")]
        [RegularExpression("([0-9]+)", ErrorMessageResourceType = typeof(Resource),
             ErrorMessageResourceName = "NumberInvalid")]
        public short UnitsInStock { get; set; }

        public bool IsDeleted { get; set; }
    }
}