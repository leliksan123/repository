﻿using System.Collections.Generic;

namespace GameStore.ViewModels.ViewModels.Game
{
    public class GamePublisherViewModel
    {
        public CreateGameViewModel Game { get; set; }

        public List<string> Names { get; set; }
    }
}