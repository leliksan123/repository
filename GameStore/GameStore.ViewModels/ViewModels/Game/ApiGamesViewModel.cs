﻿using System.Collections.Generic;
using GameStore.ViewModels.Models;

namespace GameStore.ViewModels.ViewModels.Game
{
    public class ApiGamesViewModel
    {
        public List<GameViewModel> Games { get; set; }

        public PageInfo PageInfo { get; set; }
    }
}
