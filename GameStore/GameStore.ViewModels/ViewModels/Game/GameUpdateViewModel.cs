﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using GameStore.ViewModels.ViewModels.Genre;
using GameStore.ViewModels.ViewModels.PlatformType;
using GameStore.ViewModels.ViewModels.Publisher;
using Resources;

namespace GameStore.ViewModels.ViewModels.Game
{
    public class GameUpdateViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof (Resource), ErrorMessageResourceName = "KeyRequired")]
        [StringLength(50, ErrorMessageResourceType = typeof (Resource), ErrorMessageResourceName = "KeyLenght")]
        public string Key { get; set; }

        [Required(ErrorMessageResourceType = typeof (Resource), ErrorMessageResourceName = "GameName")]
        [StringLength(100, ErrorMessageResourceType = typeof (Resource), ErrorMessageResourceName = "NameLenght")]
        public string Name { get; set; }

        public string DescriptionEnglish { get; set; }

        public string Description { get; set; }

        public HttpPostedFileBase File { get; set; }

        public string Picture { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "PriceRequired")]
        public decimal Price { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "UnitInStock")]
        [Range(0, 10000, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Limit")]
        [RegularExpression("([0-9]+)", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "NumberInvalid")]
        public short UnitsInStock { get; set; }

        public bool Discontinued { get; set; }

        public int ViewsNumber { get; set; }

        public string PublisherName { get; set; }

        public List<string> SelectedPlatformTypes { get; set; }

        public List<string> SelectedGenres { get; set; }

        public List<string> SelectedPublishers { get; set; }

        public virtual ICollection<PublisherViewModel> Publishers { get; set; }

        public virtual ICollection<PlatformTypeViewModel> PlatformTypes { get; set; }

        public virtual ICollection<GenreViewModel> Genres { get; set; }
    }
}