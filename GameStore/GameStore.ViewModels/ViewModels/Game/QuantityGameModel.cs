﻿namespace GameStore.ViewModels.ViewModels.Game
{
    public class QuantityGameModel
    {
        public string Key { get; set; }

        public short Quantity { get; set; }
    }
}