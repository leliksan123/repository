﻿namespace GameStore.ViewModels.ViewModels.Game
{
    public class GamesQuantityViewModel
    {
        public GameViewModel Game { get; set; }

        public short Quantity { get; set; }

        public decimal Sum { get; set; }
    }
}