﻿namespace GameStore.ViewModels.ViewModels.Game
{
    public class PopularGameViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}