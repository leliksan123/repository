﻿using System.Collections.Generic;
using GameStore.ViewModels.ViewModels.Genre;
using GameStore.ViewModels.ViewModels.PlatformType;
using GameStore.ViewModels.ViewModels.Publisher;

namespace GameStore.ViewModels.ViewModels.Game
{
    public class GameAllItemsViewModel
    {
        public CreateGameViewModel Game { get; set; }

        public virtual ICollection<PublisherViewModel> Publishers { get; set; }

        public virtual ICollection<PlatformTypeViewModel> PlatformTypes { get; set; }

        public virtual ICollection<GenreViewModel> Genres { get; set; }
    }
}