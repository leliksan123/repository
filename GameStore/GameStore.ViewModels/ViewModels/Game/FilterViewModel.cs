﻿using System.Collections.Generic;
using GameStore.Extensions.Enums;

namespace GameStore.ViewModels.ViewModels.Game
{
    public class FilterViewModel
    {
        public List<string> Genres { get; set; }

        public List<string> PlatformTypes { get; set; }

        public List<string> Publishers { get; set; }

        public OrderByType Parameters { get; set; }

        public decimal PriceFrom { get; set; }

        public decimal PriceTo { get; set; }

        public DateSortType SelectedYear { get; set; }

        public string PartOfName { get; set; }

        public ElementsPerPage? ElementsPerPage { get; set; }

        public int Page { get; set; }
    }
}