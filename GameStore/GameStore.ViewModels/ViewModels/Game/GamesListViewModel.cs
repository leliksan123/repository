﻿using System.Collections.Generic;
using GameStore.Extensions.Enums;
using GameStore.ViewModels.Models;
using GameStore.ViewModels.ViewModels.Genre;
using GameStore.ViewModels.ViewModels.PlatformType;
using GameStore.ViewModels.ViewModels.Publisher;

namespace GameStore.ViewModels.ViewModels.Game
{
    public class GamesListViewModel
    {
        public OrderByType OrderByType { get; set; }
                                                
        public List<GameIndexViewModel> Games { get; set; }

        public PageInfo PageInfo { get; set; }

        public string[] ElementsOnThePage { get; set; }

        public ElementsPerPage? SelectedElements { get; set; }

        public List<GenreViewModel> Genres { get; set; }

        public List<PlatformTypeViewModel> PlatformTypes { get; set; }

        public List<PublisherViewModel> Publishers { get; set; }

        public decimal PriceFrom { get; set; }

        public decimal PriceTo { get; set; }

        public DateSortType SelectedYear { get; set; }

        public string PartOfName { get; set; }

        public FilterViewModel Filter { get; set; }
    }
}