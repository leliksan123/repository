﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GameStore.ViewModels.Models
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class YearAttribute : RangeAttribute
    {
        private static readonly int CurrentYear = DateTime.UtcNow.Year;
        private static readonly int NextFiveYears = DateTime.UtcNow.Year + 6;

        public YearAttribute() : base(CurrentYear, NextFiveYears)
        {
        }
    }
}