﻿namespace GameStore.ViewModels.Models
{
    public class PaymentTableModel
    {
        public string Picture { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string PaymentType { get; set; }
    }
}