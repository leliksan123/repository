﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using GameStore.DAL.Entities;
using GameStore.Services.Filters.Interfaces;
using GameStore.Services.Models;
using LinqKit;

namespace GameStore.Services.Filters
{
    public class GameByPublisherFilter : IFilter<GameFilterModel>
    {
        private readonly IEnumerable<string> _publishersIds;

        public GameByPublisherFilter(GameFilterModel model)
        {
            _publishersIds = model.FilterDto.Publishers;
        }

        public GameFilterModel Execute(GameFilterModel input)
        {
            if (_publishersIds!=null && _publishersIds.Count() != 0)
            {
                var expandedInput = input.Predicate.Expand();
                Expression<Func<Game, bool>> expression =
                    p => expandedInput.Invoke(p) && _publishersIds.Contains(p.Name);

                input.Predicate = expression.Expand();
            }

            return input;
        }
    }
}