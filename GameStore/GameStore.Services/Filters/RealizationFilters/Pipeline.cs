﻿using System.Collections.Generic;
using GameStore.Services.Filters.Interfaces;

namespace GameStore.Services.Filters.RealizationFilters
{
    public class Pipeline<T> : IPipeLine<T>
    {
        protected readonly List<IFilter<T>> Filters = new List<IFilter<T>>();

        public IPipeLine<T> Register(IFilter<T> filter)
        {
            Filters.Add(filter);
            return this;
        }

        public T Execute(T model)
        {
            foreach (var filter in Filters)
            {
                filter.Execute(model);
            }

            return model;
        }
    }
}