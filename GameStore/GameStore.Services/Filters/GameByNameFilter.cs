﻿using System;
using System.Linq.Expressions;
using GameStore.DAL.Entities;
using GameStore.Services.Filters.Interfaces;
using GameStore.Services.Models;
using LinqKit;

namespace GameStore.Services.Filters
{
    public class GameByNameFilter : IFilter<GameFilterModel>
    {
        private readonly string _partOfName;

        public GameByNameFilter(GameFilterModel model)
        {
            _partOfName = model.FilterDto.PartOfName;
        }

        public GameFilterModel Execute(GameFilterModel input)
        {
            if (_partOfName != null)
            {
                var expandedInput = input.Predicate.Expand();
                Expression<Func<Game, bool>> expression =
                    p => expandedInput.Invoke(p) && p.Name.Contains(_partOfName);
                input.Predicate = expression.Expand();
            }

            return input;
        }
    }
}