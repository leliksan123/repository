﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using GameStore.DAL.Entities;
using GameStore.Services.Filters.Interfaces;
using GameStore.Services.Models;
using LinqKit;

namespace GameStore.Services.Filters
{
    public class GameByGenreFilter : IFilter<GameFilterModel>
    {
        private readonly IEnumerable<string> _genres;

        public GameByGenreFilter(GameFilterModel model)
        {
            _genres = model.FilterDto.Genres;
        }

        public GameFilterModel Execute(GameFilterModel input)
        {
            if (_genres != null &&_genres.Count() != 0)
            {
                var expandedInput = input.Predicate.Expand();
                Expression<Func<Game, bool>> expression =
                    p => expandedInput.Invoke(p) && _genres.Intersect(p.Genres.Select(x => x.Name)).Any();

                input.Predicate = expression.Expand();
            }

            return input;
        }
    }
}