﻿using System;
using System.Linq.Expressions;
using GameStore.DAL.Entities;
using GameStore.Services.Filters.Interfaces;
using GameStore.Services.Models;
using LinqKit;

namespace GameStore.Services.Filters
{
    public class GameByPriceOrderFilter : IFilter<GameFilterModel>
    {
        private readonly decimal _firstPrice;
        private readonly decimal _secondPrice;

        public GameByPriceOrderFilter(GameFilterModel model)
        {
            _firstPrice = model.FilterDto.PriceFrom;
            _secondPrice = model.FilterDto.PriceTo;
        }

        public GameFilterModel Execute(GameFilterModel input)
        {
            if (input.FilterDto.PriceTo > 0)
            {
                var expandedInput = input.Predicate.Expand();
                Expression<Func<Game, bool>> expression =
                    p => expandedInput.Invoke(p) && (p.Price > _firstPrice) && (p.Price < _secondPrice);
                input.Predicate = expression.Expand();
            }

            return input;
        }
    }
}