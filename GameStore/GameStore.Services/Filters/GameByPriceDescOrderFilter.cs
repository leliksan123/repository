﻿using System;
using System.Linq.Expressions;
using GameStore.DAL.Entities;
using GameStore.Services.Filters.Interfaces;
using GameStore.Services.Models;

namespace GameStore.Services.Filters
{
    public class GameByPriceDescOrderFilter : IFilter<GameFilterModel>
    {
        public GameFilterModel Execute(GameFilterModel input)
        {
            Expression<Func<Game, object>> expression = p => p.Price;

            input.SortPredicate = expression;

            return input;
        }
    }
}