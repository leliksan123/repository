﻿using System;
using System.Linq.Expressions;
using GameStore.DAL.Entities;
using GameStore.Services.Filters.Interfaces;
using GameStore.Services.Models;
using LinqKit;

namespace GameStore.Services.Filters
{
    public class GameByLastMonthFilter : IFilter<GameFilterModel>
    {
        public GameFilterModel Execute(GameFilterModel input)
        {
            var expandedInput = input.Predicate.Expand();
            var date = DateTime.UtcNow.AddMonths(-1);
            Expression<Func<Game, bool>> expression =
                p => expandedInput.Invoke(p) && (p.AddedToGameStore > date);

            input.Predicate = expression.Expand();

            return input;
        }
    }
}