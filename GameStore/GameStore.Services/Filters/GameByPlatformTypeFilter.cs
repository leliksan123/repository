﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using GameStore.DAL.Entities;
using GameStore.Services.Filters.Interfaces;
using GameStore.Services.Models;
using LinqKit;

namespace GameStore.Services.Filters
{
    public class GameByPlatformTypeFilter : IFilter<GameFilterModel>
    {
        private readonly IEnumerable<string> _platformTypes;

        public GameByPlatformTypeFilter(GameFilterModel model)
        {
            _platformTypes = model.FilterDto.PlatformTypes;
        }

        public GameFilterModel Execute(GameFilterModel input)
        {
            if (_platformTypes!=null && _platformTypes.Count() != 0)
            {
                var expandedInput = input.Predicate.Expand();
                Expression<Func<Game, bool>> expression =
                    p => expandedInput.Invoke(p) && _platformTypes.Intersect(p.PlatformTypes.Select(x => x.Type)).Any();

                input.Predicate = expression.Expand();
            }

            return input;
        }
    }
}