﻿namespace GameStore.Services.Filters.Interfaces
{
    public interface IFilter<T>
    {
        T Execute(T input);
    }
}