﻿namespace GameStore.Services.Filters.Interfaces
{
    public interface IPipeLine<T>
    {
        IPipeLine<T> Register(IFilter<T> filter);
    }
}