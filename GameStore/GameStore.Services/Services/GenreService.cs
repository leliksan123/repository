﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Mongo;
using GameStore.DAL.Interfaces;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.Tools.Logger;

namespace GameStore.Services.Services
{
    public class GenreService : IGenreService
    {
        private readonly ILoggerFileService _logger;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IValidationService _validationService;

        public GenreService(IUnitOfWork untiOfWork, ILoggerFileService logger, IMapper mapper,
            IValidationService validationService)
        {
            _unitOfWork = untiOfWork;
            _logger = logger;
            _mapper = mapper;
            _validationService = validationService;
        }

        public IEnumerable<GenreDto> GetAllGenres()
        {
            _logger.Info("Starting GenreService method GetAllGenres and returning all genres from database");

            var genres = _mapper.Map<IEnumerable<Genre>, List<GenreDto>>(_unitOfWork.Genres.GetAll());
            var categories = _mapper.Map<IEnumerable<Category>, List<Genre>>(_unitOfWork.Categories.GetAll());
            var genresMongo = _mapper.Map<List<GenreDto>>(categories);

            genres = genres.Union(genresMongo).Distinct(new Extensions.CompareExtension.Comparer<GenreDto>("Name")).Where(x => x.IsDeleted == false).ToList();

            _logger.Info("Service completed its work successfully");
            return genres;
        }

        public IEnumerable<GenreDto> GetGenresByName(IEnumerable<string> genresNames)
        {
            _logger.Info(
                "Starting GenreService method GetGenresByIdWhenCreatingNewGame and returning all genres from database with current names");
            var listOfGenres = new List<Genre>();

            var genresFromDb = _unitOfWork.Genres.GetAll().ToList();
            var genresMongo = _mapper.Map<List<Genre>>(_unitOfWork.Categories.GetAll());
            genresFromDb = genresFromDb.Union(genresMongo).Distinct(new Extensions.CompareExtension.Comparer<Genre>("Name")).ToList();

            foreach (var item in genresNames)
            {
                var genre = genresFromDb.FirstOrDefault(x => x.Name == item);
                _validationService.ValidateIfItemExist(genre);
                listOfGenres.Add(genre);
            }

            _logger.Info("Service completed its work successfully");
            return _mapper.Map<List<Genre>, List<GenreDto>>(listOfGenres);
        }

        public void Create(GenreDto genreDto)
        {
            _logger.Info(
                $"Starting GenreService method Create and added into database genre with Name {genreDto.Name}");
            var newGenre = _mapper.Map<GenreDto, Genre>(genreDto);
            _unitOfWork.Genres.Insert(newGenre);
            _unitOfWork.Save();
            _logger.Info("Service completed its work successfully");
        }

        public void Delete(string name)
        {
            _logger.Info(
                $"Starting GenreService method Delete and deleting genre with name {name}");

            var genreFromDb = _unitOfWork.Genres.Get(name);

            if (genreFromDb == null)
            {
                genreFromDb = _mapper.Map<Genre>(_unitOfWork.Categories.Get(name));
                genreFromDb.IsDeleted = true;
                _unitOfWork.Genres.Insert(genreFromDb);
            }
            else
            {
                genreFromDb.IsDeleted = true;
                _unitOfWork.Genres.Update(genreFromDb);
            }

            _validationService.ValidateIfItemExist(genreFromDb);
            _unitOfWork.Save();
            _logger.Info("Service completed its work successfully");
        }

        public void Update(GenreDto genreDto)
        {
            _logger.Info(
                $"Starting GenreService method Update and updating genre with name {genreDto.Name}");

            var genre = _mapper.Map<Genre>(genreDto);

            if (_unitOfWork.Genres.Get(genreDto.Name) != null)
            {
                _unitOfWork.Genres.Update(genre);
            }
            else
            {
                _unitOfWork.Genres.Insert(genre);
            }
            _logger.Info("Service completed its work successfully");
        }

        public GenreDto GetByName(string name)
        {
            _logger.Info(
                $"Starting GenreService method GetByName and returning genre with name {name}");

            var genre = _unitOfWork.Genres.Get(name);
            var genreDto = _mapper.Map<GenreDto>(genre);
            _logger.Info("Service completed its work successfully");

            return genreDto;
        }

        public IEnumerable<GenreDto> GetGenresOfGame(string key)
        {
            _logger.Info(
                $"Starting GenreService method GetGenresOfGame and returning genres for game with key {key}");

            var game = _unitOfWork.Games.Get(key);
            var genres = _mapper.Map<IEnumerable<GenreDto>>(game.Genres);

            _logger.Info("Service completed its work successfully");
            return genres;
        }
    }
}