﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using GameStore.DAL.Entities;
using GameStore.DAL.Interfaces;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.Tools.Logger;

namespace GameStore.Services.Services
{
    public class OrderDetailsService : IOrderDetailsService
    {
        private readonly IGameService _gameService;
        private readonly ILoggerFileService _logger;
        private readonly IMapper _mapper;
        private readonly IOrderService _orderService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IValidationService _validationService;

        public OrderDetailsService(IUnitOfWork untiOfWork, ILoggerFileService logger, IMapper mapper,
            IValidationService validationService, IGameService gameService, IOrderService orderService)
        {
            _unitOfWork = untiOfWork;
            _logger = logger;
            _mapper = mapper;
            _validationService = validationService;
            _gameService = gameService;
            _orderService = orderService;
        }

        public IEnumerable<OrderDetailsDto> GetOrderDetailsForOrder(OrderDto orderDto)
        {
            _logger.Info(
                $"Starting OrderDetailsService method GetOrderDetailsForOrder and returning all orderdetails for order {orderDto.Id}");
            var orderDetailsFromDb = _unitOfWork.OrderDetailses.Get(x => x.OrderId == orderDto.Id);
            var orderDetailsForOrder =
                _mapper.Map<IEnumerable<OrderDetails>, IEnumerable<OrderDetailsDto>>(orderDetailsFromDb);
            _logger.Info("Service completed its work successfully");
            return orderDetailsForOrder;
        }

        public decimal OrderSum(IEnumerable<OrderDetailsDto> orderDetails)
        {
            _logger.Info("Starting OrderDetailsService method OrderSum and returning sum for order");

            decimal sum = 0;

            foreach (var item in orderDetails)
            {
                var game = _unitOfWork.Games.Get(x => x.Id == item.ProductId).First();
                _validationService.ValidateIfItemExist(game);
                sum += game.Price*item.Quantity*(decimal) (1 - item.Discount);
            }

            _logger.Info("Service completed its work successfully");
            return sum;
        }

        public void CreateOrderDetailsWithQuantity(Dictionary<string,short> list , string user)
        {
            _logger.Info("Starting OrderDetailsService method CreateOrderDetailsWithQuantity");

            var userid = _unitOfWork.Users.Get(user).Id;

            foreach (var item in list)
            {
                var model = new OrderDetailsDto
                {
                    ProductId = _gameService.GetGameByKey(item.Key).Id,
                    Quantity = item.Value,
                    OrderId = _orderService.LastCustomersOrder(userid).Id
                };

                var orderDetail = _mapper.Map<OrderDetailsDto, OrderDetails>(model);
                _unitOfWork.OrderDetailses.Insert(orderDetail);
                _unitOfWork.Save();
                _logger.Info("Service completed its work successfully");
            }    
        }

        public void Delete(Guid id)
        {
            _logger.Info($"Starting OrderDetailsService method delete and deleting orderdetails with Id {id}");
            _unitOfWork.OrderDetailses.Delete(id);
            _unitOfWork.Save();
            _logger.Info("Service completed its work successfully");
        }

        public void AddToOrder(OrderDetailsDto orderDetails)
        {
            _logger.Info($"Starting OrderDetailsService method AddToOrder to order with Id {orderDetails.Id}");

            var game = _unitOfWork.Games.Get(orderDetails.SelectedGame);

            if (game == null)
            {
                game = _mapper.Map<Game>(_unitOfWork.Products.Get(orderDetails.SelectedGame));
                _unitOfWork.Games.Insert(game);
                _unitOfWork.Save();
            }

            var orderDetail = _mapper.Map<OrderDetailsDto, OrderDetails>(orderDetails);
            orderDetail.ProductId = game.Id;

            _unitOfWork.OrderDetailses.Insert(orderDetail);
            _unitOfWork.Save();
            _logger.Info("Service completed its work successfully");
        }

        public OrderDetailsDto GetDetailsById(Guid id)
        {
           var details =  _unitOfWork.OrderDetailses.GetById(id);
           var orderdetailsDto = _mapper.Map<OrderDetailsDto>(details);
           _logger.Info("Service completed its work successfully");
           return orderdetailsDto;
        }

        public void Update(OrderDetailsDto orderDetails)
        {
            var detailsFromDb = _unitOfWork.OrderDetailses.GetById(orderDetails.Id);
            detailsFromDb.Discount = orderDetails.Discount;
            detailsFromDb.Quantity = orderDetails.Quantity;

            _unitOfWork.OrderDetailses.Update(detailsFromDb);
            _unitOfWork.Save();
            _logger.Info("Service completed its work successfully");
        }
    }
}