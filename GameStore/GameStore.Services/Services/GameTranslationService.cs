﻿using System;
using System.Linq;
using System.Threading;
using AutoMapper;
using GameStore.DAL.Entities;
using GameStore.DAL.Interfaces;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.Tools.Logger;

namespace GameStore.Services.Services
{
    public class GameTranslationService : IGameTranslation
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILoggerFileService _logger;
        private const string DefaultLang = "en";

        public GameTranslationService(IMapper mapper, IUnitOfWork unitOfWork, ILoggerFileService logger)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        public GameTranslationDto GetTranslation(int gameId, int languageId)
        {
            _logger.Info($"Starting GameTranslationService method GetTranslation and returning translation for game {gameId}");

            var gameTranslation =
                _unitOfWork.GameTranslations.GetOne(x => x.GameId == gameId && x.LanguageId == languageId);
            var translationMap = _mapper.Map<GameTranslationDto>(gameTranslation);

            _logger.Info("Service completed its work successfully");
            return translationMap;
        }

        public void CreateTranslation(GameDto game, int languageId, string description)
        {
            _logger.Info($"Starting GameTranslationService method CreateTranslation for game {game.Id}");

            var cultureGame = new GameTranslation
            {
                Id = Guid.NewGuid(),
                GameId = game.Id,
                Description = description,
                LanguageId = languageId
            };

            _unitOfWork.GameTranslations.Insert(cultureGame);
            _unitOfWork.Save();
            _logger.Info("Service completed its work successfully");
        }

        public void UpdateTranslation(GameDto game, int languageId, string description)
        {
            _logger.Info($"Starting GameTranslationService method UpdateTranslation for game {game.Id}");

            var translation = _unitOfWork.GameTranslations.GetOne(x => x.GameId == game.Id && x.LanguageId == languageId);
            if (translation != null)
            {
                translation.Description = description;
                _unitOfWork.GameTranslations.Update(translation);
                _unitOfWork.Save();
            }
            else
            {
                CreateTranslation(game, languageId , description);
            }

            _logger.Info("Service completed its work successfully");
        }

        public void SetDescription(GameDto game)
        {
            var lang = Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName;
            var language = _unitOfWork.Languages.Get(lang);

            if (game.GameTranslations != null && game.GameTranslations.Any(x => x.LanguageId == language.Id))
            {
                var translation = game.GameTranslations.Single(x => x.LanguageId == language.Id);
                game.Description = translation.Description;
            }
            else
            {
                var translation = _unitOfWork.GameTranslations.GetOne(x => x.Language.Name == DefaultLang && x.GameId == game.Id);
                game.Description = translation?.Description;
            }
        }
    }
}
