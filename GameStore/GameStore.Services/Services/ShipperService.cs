﻿using System.Collections.Generic;
using AutoMapper;
using GameStore.DAL.Entities.Mongo;
using GameStore.DAL.Interfaces;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.Tools.Logger;

namespace GameStore.Services.Services
{
    public class ShipperService : IShipperService
    {
        private readonly ILoggerFileService _logger;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public ShipperService(IUnitOfWork untiOfWork, ILoggerFileService loggingService, IMapper mapper)
        {
            _unitOfWork = untiOfWork;
            _logger = loggingService;
            _mapper = mapper;
        }

        public IEnumerable<ShipperDto> GetAllShippers()
        {
            _logger.Info("Starting ShipperService method GetAllShippers and returning shippers from database");
            var shippersFromDb = _unitOfWork.Shippers.GetAll();
            var shippers = _mapper.Map<IEnumerable<Shipper>, List<ShipperDto>>(shippersFromDb);
            _logger.Info("Service completed its work successfully");
            return shippers;
        }
    }
}
