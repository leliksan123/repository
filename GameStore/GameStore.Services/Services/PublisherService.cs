﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Mongo;
using GameStore.DAL.Interfaces;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.Tools.Logger;

namespace GameStore.Services.Services
{
    public class PublisherService : IPublisherService
    {
        private readonly ILoggerFileService _logger;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IValidationService _validationService;
        private readonly IPublisherTranslation _publisherTranslation;
        private const string LangRus = "ru";
        private const string LangEn = "en";

        public PublisherService(ILoggerFileService logger, IMapper mapper, IUnitOfWork unitOfWork,
            IValidationService validationService , IPublisherTranslation publisherTranslation)
        {
            _logger = logger;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _validationService = validationService;
            _publisherTranslation = publisherTranslation;
        }

        public IEnumerable<PublisherDto> GetAllPublishers()
        {
            _logger.Info("Starting PublisherService method GetAllPublishers and returning publishers from database");
            var publishers = _mapper.Map<IEnumerable<Publisher>, List<PublisherDto>>(_unitOfWork.Publishers.GetAll());

            var suppliers = _mapper.Map<IEnumerable<Supplier>, List<Publisher>>(_unitOfWork.Suppliers.GetAll());

            var publishersMongo = _mapper.Map<List<PublisherDto>>(suppliers);

            publishers = publishers.Union(publishersMongo).Distinct(new Extensions.CompareExtension.Comparer<PublisherDto>("CompanyName")).Where(x=>x.IsDeleted == false).ToList();

            foreach (var item in publishers)
            {
               _publisherTranslation.SetDescription(item); 
            }

            _logger.Info("Service completed its work successfully");
            return publishers;
        }

        public PublisherDto GetPublisherByCompanyName(string companyName)
        {
            _logger.Info($"Starting PublisherService method GetPublisherByCompanyName with companyName {companyName}");

            var publisherFromDb = _unitOfWork.Publishers.Get(companyName);

            if (publisherFromDb == null)
            {
                var supplier = _unitOfWork.Suppliers.Get(companyName);
                publisherFromDb = _mapper.Map<Publisher>(supplier);

                var game = CreateNotExistsProduct(supplier);
                publisherFromDb = GetNotExistGame(game, publisherFromDb);
                publisherFromDb.Games.Clear();

                var gameFromDb = _unitOfWork.Games.Get(game.Id);
                publisherFromDb.Games = new List<Game> {gameFromDb};

                _unitOfWork.Publishers.Insert(publisherFromDb);
                _unitOfWork.Save();
            }

            var publisherByCompanyName = _mapper.Map<Publisher, PublisherDto>(publisherFromDb);
            _publisherTranslation.SetDescription(publisherByCompanyName);
            _logger.Info("Service completed its work successfully");
            return publisherByCompanyName;
        }

        public void Create(PublisherDto publisherDto)
        {
            if (_unitOfWork.Publishers.GetAll().Any(e => e.CompanyName == publisherDto.CompanyName))
            {
                throw new ArgumentException("Publisher with this CompanyName already exists in database");
            }

            _logger.Info(
                $"Starting PublisherService method Create and added into database publisher with  {publisherDto.Id}");
            var newPublisher = _mapper.Map<PublisherDto, Publisher>(publisherDto);
            _unitOfWork.Publishers.Insert(newPublisher);
            _unitOfWork.Save();

            var publisherFromDb = _mapper.Map<PublisherDto>(_unitOfWork.Publishers.Get(publisherDto.CompanyName));
            var languageRus = _unitOfWork.Languages.Get(LangRus);
            var languageEn = _unitOfWork.Languages.Get(LangEn);
            _publisherTranslation.CreateTranslation(publisherFromDb, languageRus.Id, publisherDto.Description);
            _publisherTranslation.CreateTranslation(publisherFromDb, languageEn.Id, publisherDto.DescriptionEnglish);
            _logger.Info("Service completed its work successfully");
        }

        public void Delete(string companyName)
        {
            _logger.Info(
                $"Starting PublisherService method Delete and deleting publisher with  companyName {companyName}");

            var publisherFromDb = _unitOfWork.Publishers.Get(companyName);

            if (publisherFromDb == null)
            {
                publisherFromDb = _mapper.Map<Publisher>(_unitOfWork.Suppliers.Get(companyName));
                publisherFromDb.IsDeleted = true;
                _unitOfWork.Publishers.Insert(publisherFromDb);
            }
            else
            {
                publisherFromDb.IsDeleted = true;
                _unitOfWork.Publishers.Update(publisherFromDb);
            }

            _validationService.ValidateIfItemExist(publisherFromDb);

            _unitOfWork.Save();
            _logger.Info("Service completed its work successfully");
        }

        public void Update(PublisherDto publisherDto)
        {
            _validationService.ValidateIfItemExist(_unitOfWork.Publishers.Get(publisherDto.CompanyName));
            _logger.Info($"Starting PublisherService method Update and updating publisher with companyName {publisherDto.CompanyName}");
            var publisherFromDb = _unitOfWork.Publishers.Get(publisherDto.CompanyName);
            publisherFromDb.Games.Clear();
            var games = new List<Game>();

            foreach (var gameKey in publisherDto.SelectedGames)
            {
                var game = _unitOfWork.Games.Get(gameKey);
                games.Add(game);
            }

            publisherFromDb.Games = games;
            publisherFromDb.HomePage = publisherDto.HomePage;

            _unitOfWork.Publishers.Update(publisherFromDb);

            var languageRus = _unitOfWork.Languages.Get(LangRus);
            var languageEn = _unitOfWork.Languages.Get(LangEn);
            _publisherTranslation.UpdateTranslation(publisherDto, languageRus.Id, publisherDto.Description);
            _publisherTranslation.UpdateTranslation(publisherDto, languageEn.Id, publisherDto.DescriptionEnglish);
            _unitOfWork.Save();
            _logger.Info("Service completed its work successfully");
        }

        private Product CreateNotExistsProduct(Supplier supplier)
        {
            var game = _unitOfWork.Products.Get(x => x.SupplierID == supplier.SupplierID).First();

            if (_unitOfWork.Games.Get(game.Id) == null)
            {
                var games = _mapper.Map<Game>(game);
                _unitOfWork.Games.Insert(games);
                _unitOfWork.Save();
            }
            return game;
        }

        private Publisher GetNotExistGame(Product game, Publisher publisherFromDb)
        {
            var gameses = new List<Product> { _unitOfWork.Products.Get(game.Id) };

            publisherFromDb = new Publisher
            {
                CompanyName = publisherFromDb.CompanyName,
                HomePage = publisherFromDb.HomePage,
                Games = _mapper.Map<List<Game>>(gameses)
            };
            return publisherFromDb;
        }
    }
}