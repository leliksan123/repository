﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using GameStore.DAL.Entities;
using GameStore.DAL.Interfaces;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.Tools.Logger;

namespace GameStore.Services.Services
{
    public class UserService : IUserService
    {
        private readonly ILoggerFileService _logger;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IValidationService _validationService;
        private readonly IMapper _mapper;

        public UserService(ILoggerFileService logger, IMapper mapper, IUnitOfWork unitOfWork , IValidationService validationService)
        {
            _logger = logger;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _validationService = validationService;
        }

        public void BanUser(string email)
        {
            throw new NotImplementedException();
        }

        public void Create(UserDTo user)
        {
            if (_unitOfWork.Users.GetAll().Any(e => e.Email == user.Email))
            {
                throw new ArgumentException("User with this Email already exists in database");
            }

            _logger.Info(
                $"Starting UserService method Create and added into database user with Email {user.Email}");

            var newUser = _mapper.Map<UserDTo, User>(user);
            newUser.RegistrationTime = DateTime.UtcNow;
            newUser.Roles = new List<Role> {_unitOfWork.Roles.Get(x=>x.RoleName == "User").FirstOrDefault()};
            _unitOfWork.Users.Insert(newUser);
            _unitOfWork.Save();
            _logger.Info("Service completed its work successfully");
        }

        public void Delete(string email)
        {
            _logger.Info($"Starting UserService method DeleteUser with email {email}");
            _unitOfWork.Users.Delete(email);
            _unitOfWork.Save();
            _logger.Info("Service completed its work successfully");
        }

        public void Update(UserDTo userDTo)
        {
            _validationService.ValidateIfItemExist(_unitOfWork.Users.Get(userDTo.Email));
            _logger.Info($"Starting UserService method Update and updating user with Email {userDTo.Email}");
            var userFromDb = _unitOfWork.Users.Get(userDTo.Email);
            userFromDb.Roles.Clear();
            var roles = new List<Role>();

            foreach (var rolename in userDTo.SelectedRoles)
            {
                var role = _unitOfWork.Roles.Get(rolename);
                roles.Add(role);
            }

            userFromDb.Roles = roles;
            userFromDb.Email = userDTo.Email;
            userFromDb.LastName = userDTo.LastName;
            userFromDb.FirstName = userDTo.FirstName;
            userFromDb.Phone = userDTo.Phone;
            userFromDb.RegistrationTime = Convert.ToDateTime(userDTo.RegistrationTime);

            if (userFromDb.Manager != null)
            {
                userFromDb.Manager.NotificationEmail = userDTo.NotificationEmail;
                userFromDb.Manager.NotificationMobileApp = userDTo.NotificationMobileApp;
                userFromDb.Manager.NotificationSms = userDTo.NotificationSms;
            }

            _unitOfWork.Users.Update(userFromDb);
            _unitOfWork.Save();
            _logger.Info("Service completed its work successfully");
        }

        public IEnumerable<UserDTo> GetUsersregisterToday()
        {
            _logger.Info("Starting UserService method GetUsersregisterToday");
            var users = _unitOfWork.Users.Get(x => x.RegistrationTime.ToShortDateString() == DateTime.Now.ToShortDateString());

            _logger.Info("Service completed its work successfully");
            return _mapper.Map<IEnumerable<UserDTo>>(users);
        }

        public IEnumerable<UserDTo> GetAll()
        {
            _logger.Info("Starting UserService method GetAll");
            var users = _unitOfWork.Users.GetAll();

            _logger.Info("Service completed its work successfully");
            return _mapper.Map<IEnumerable<UserDTo>>(users);
        }

        public UserDTo GetUserByEmail(string email)
        {
            _logger.Info("Starting UserService method GetuserByEmail");
            var user = _unitOfWork.Users.Get(x => x.Email == email).FirstOrDefault();

            _logger.Info("Service completed its work successfully");
            return _mapper.Map<UserDTo>(user);
        }
    }
}