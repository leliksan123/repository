﻿using System;
using System.Linq;
using System.Threading;
using AutoMapper;
using GameStore.DAL.Entities;
using GameStore.DAL.Interfaces;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.Tools.Logger;

namespace GameStore.Services.Services
{
    public class PublisherTranslationService : IPublisherTranslation
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILoggerFileService _logger;
        private readonly IMapper _mapper;
        private const string DefaultLang = "en";

        public PublisherTranslationService(IUnitOfWork unitOfWork, ILoggerFileService logger, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _mapper = mapper;
        }

        public void CreateTranslation(PublisherDto publisher, int languageId, string description)
        {
            _logger.Info($"Starting PublisherTranslationService method CreateTranslation for publisher {publisher.CompanyName}");

            var publisherTranslation = new PublisherTranslation
            {
                Id = Guid.NewGuid(),
                PublisherId = publisher.Id,
                Description = description,
                LanguageId = languageId
            };

            _unitOfWork.PublisherTranslations.Insert(publisherTranslation);
            _unitOfWork.Save();
            _logger.Info("Service completed its work successfully");
        }

        public void SetDescription(PublisherDto publisher)
        {
            var lang = Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName;
            var language = _unitOfWork.Languages.Get(lang);

            if (publisher.PublisherTranslations!= null && publisher.PublisherTranslations.Any(x => x.LanguageId == language.Id))
            {
                var translation = publisher.PublisherTranslations.Single(x => x.LanguageId == language.Id);
                publisher.Description = translation.Description;
            }
            else
            {
                var translation = _unitOfWork.GameTranslations.GetOne(x => x.Language.Name == DefaultLang);
                publisher.Description = translation?.Description;
            }
        }

        public void UpdateTranslation(PublisherDto publisher, int languageId, string description)
        {
            _logger.Info(
                $"Starting PublicherTranslationService method UpdateTranslation for publisher {publisher.CompanyName}");

            var translation = _unitOfWork.PublisherTranslations.GetOne( x => (x.PublisherId == publisher.Id) && (x.LanguageId == languageId));
            if (translation != null)
            {
                translation.Description = description;
                _unitOfWork.PublisherTranslations.Update(translation);
                _unitOfWork.Save();
            }
            else
            {
                CreateTranslation(publisher, languageId, description);
            }

            _logger.Info("Service completed its work successfully");
        }

        public PublisherTranslationDto GetTranslation(int publisherId, int languageId)
        {
            _logger.Info($"Starting PublisherTranslationService method GetTranslation and returning translation for publisher {publisherId}");

            var publisherTranslation =
                _unitOfWork.PublisherTranslations.GetOne(x => x.PublisherId == publisherId && x.LanguageId == languageId);
            var translationMap = _mapper.Map<PublisherTranslationDto>(publisherTranslation);

            _logger.Info("Service completed its work successfully");
            return translationMap;
        }
    }
}
