﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using GameStore.DAL.Entities;
using GameStore.DAL.Interfaces;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.Tools.Logger;

namespace GameStore.Services.Services
{
    public class RoleService : IRoleService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILoggerFileService _logger;

        public RoleService(IUnitOfWork unitOfWork, IMapper mapper, ILoggerFileService logger)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _logger = logger;
        }

        public IEnumerable<RoleDTo> GetAll()
        {
            _logger.Info("Starting RoleService method GetAll and returning roles from database");
            var rolesFromDb = _unitOfWork.Roles.GetAll().Where(x => x.IsDeleted == false);
            var roles = _mapper.Map<IEnumerable<RoleDTo>>(rolesFromDb);
            _logger.Info("Service completed its work successfully");
            return roles;
        }

        public RoleDTo GetRoleByName(string name)
        {
            _logger.Info("Starting RoleService method GetRoleByName");
            var role = _unitOfWork.Roles.Get(x => x.RoleName == name).FirstOrDefault();

            _logger.Info("Service completed its work successfully");
            return _mapper.Map<RoleDTo>(role);
        }

        public void Delete(string name)
        {
            _logger.Info($"Starting RoleService method Delete with name {name}");
            var roleFromDb = _unitOfWork.Roles.Get(name);

            roleFromDb.IsDeleted = true;
            _unitOfWork.Roles.Update(roleFromDb);

            _unitOfWork.Save();
            _logger.Info("Service completed its work successfully");
        }

        public void Create(RoleDTo roleDTo)
        {
            if (_unitOfWork.Roles.GetAll().Any(e => e.RoleName == roleDTo.RoleName))
            {
                throw new ArgumentException("Role with this Name already exists in database");
            }

            _logger.Info(
                $"Starting RoleService method Create and added into database role with name {roleDTo.RoleName}");

            var newRole = _mapper.Map<RoleDTo, Role>(roleDTo);
            _unitOfWork.Roles.Insert(newRole);
            _unitOfWork.Save();
            _logger.Info("Service completed its work successfully");
        }
    }
}
