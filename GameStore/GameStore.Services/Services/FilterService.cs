﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using GameStore.DAL.Entities;
using GameStore.DAL.Interfaces;
using GameStore.DAL.Realization;
using GameStore.Extensions.Enums;
using GameStore.Services.DTO;
using GameStore.Services.Filters;
using GameStore.Services.Filters.Interfaces;
using GameStore.Services.Filters.RealizationFilters;
using GameStore.Services.Interfaces;
using GameStore.Services.Models;

namespace GameStore.Services.Services
{
    public class FilterService : IFilterService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IGameTranslation _gameTranslate;
        private OrderType _orderType = OrderType.OrderBy;

        public FilterService(IUnitOfWork unitOfWork, IMapper mapper , IGameTranslation gameTranslate)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _gameTranslate = gameTranslate;
        }

        public IEnumerable<GameDto> Filter(FilterDto filterDto, int skip, int take, bool isDeleted = false)
        {
            var sortModel = new GameFilterModel
            {
                Predicate = x => true,
                SortPredicate = x => x.Id,
                FilterDto = filterDto
            };

            var pipeline = new Pipeline<GameFilterModel>();
            RegisterFilters(sortModel, pipeline);
            var executeFilters = pipeline.Execute(sortModel);

            var model = new ModelForSorting
            {
                SortPredicate = executeFilters.SortPredicate,
                OrderType = _orderType
            };

            var gamesFromRepo = isDeleted ? _unitOfWork.Games.GetSortedGames(executeFilters.Predicate, model, skip, take , true) :
                _unitOfWork.Games.GetSortedGames(executeFilters.Predicate, model, skip, take);

            IEnumerable<GameDto> gamesMap = _mapper.Map<IEnumerable<Game>, IEnumerable<GameDto>>(gamesFromRepo).ToList();
            foreach (var item in gamesMap)
            {
                _gameTranslate.SetDescription(item);
            }

            return gamesMap;
        }

        public int GamesCount(FilterDto filterDto , bool isDeleted = false)
        {
            var sortModel = new GameFilterModel
            {
                Predicate = x => true,
                SortPredicate = x => x.Id,
                FilterDto = filterDto
            };

            var pipeline = new Pipeline<GameFilterModel>();
            RegisterFilters(sortModel, pipeline);
            var executeFilters = pipeline.Execute(sortModel);

            var model = new ModelForSorting
            {
                SortPredicate = executeFilters.SortPredicate,
                OrderType = _orderType
            };

            var gamesFromRepo = isDeleted ? _unitOfWork.Games.GetAllFilteredGames(executeFilters.Predicate, model , true) 
                : _unitOfWork.Games.GetAllFilteredGames(executeFilters.Predicate, model);

            return gamesFromRepo;
        }

        private void RegisterFilters(GameFilterModel model, IPipeLine<GameFilterModel> pipeline)
        {
            pipeline.Register(new GameByGenreFilter(model)).Register(new GameByPublisherFilter(model))
                .Register(new GameByPlatformTypeFilter(model))
                .Register(new GameByNameFilter(model))
                .Register(new GameByPriceOrderFilter(model));

            RegisterCreatedDateFilter(model.FilterDto.SelectedYear, pipeline);

            RegisterOrderFilter(model.FilterDto.OrderByType, pipeline);
        }

        private void RegisterOrderFilter(OrderByType orderByType, IPipeLine<GameFilterModel> pipeline)
        {
            switch (orderByType)
            {
                case OrderByType.MostPopular:
                    pipeline.Register(new MostPopularGameOrderFilter());
                    _orderType = OrderType.OrderByDescending;
                    break;
                case OrderByType.MostCommented:
                    pipeline.Register(new MostCommentedGameOrderFilter());
                    break;
                case OrderByType.ByPriceAsc:
                    pipeline.Register(new GameByPriceAscOrderFilter());
                    break;
                case OrderByType.ByPriceDesc:
                    pipeline.Register(new GameByPriceDescOrderFilter());
                    _orderType = OrderType.OrderByDescending;
                    break;
                case OrderByType.NewBydate:
                    pipeline.Register(new NewByDateGameOrderFilter());
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void RegisterCreatedDateFilter(DateSortType dateSort, IPipeLine<GameFilterModel> pipeline)
        {
            switch (dateSort)
            {
                case DateSortType.LastWeek:
                    pipeline.Register(new GameByLastWeekFilter());
                    break;
                case DateSortType.LastMonth:
                    pipeline.Register(new GameByLastMonthFilter());
                    break;
                case DateSortType.LastYear:
                    pipeline.Register(new GameByLastYearFilter());
                    break;
                case DateSortType.LastTwoYears:
                    pipeline.Register(new GameByLastTwoYearsFilter());
                    break;
                case DateSortType.LastThreeYears:
                    pipeline.Register(new GameByLastThreeYearsFilter());
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}