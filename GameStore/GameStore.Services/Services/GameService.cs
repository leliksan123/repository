﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using AutoMapper;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Mongo;
using GameStore.DAL.Interfaces;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.Tools.Logger;

namespace GameStore.Services.Services
{
    public class GameService : IGameService
    {
        private readonly ILoggerFileService _logger;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IGameTranslation _gameTranslate;
        private readonly IValidationService _validation;
        private const string LangRus = "ru";
        private const string LangEn = "en";

        public GameService(IUnitOfWork untiOfWork, ILoggerFileService logger, IMapper mapper, IGameTranslation gameTranslation , IValidationService validation)
        {
            _unitOfWork = untiOfWork;
            _logger = logger;
            _mapper = mapper;
            _gameTranslate = gameTranslation;
            _validation = validation;
        }

        public IEnumerable<GameDto> GetAllGames()
        {
            _logger.Info("Starting GameServices method GetAllGames and returning all games from database");

            var games = _mapper.Map<IEnumerable<Game>, List<GameDto>>(_unitOfWork.Games.Get(x=>!x.IsDeleted));
            var products = _mapper.Map<IEnumerable<Product>, List<Game>>(_unitOfWork.Products.Get(x => !x.IsDeleted));
            var gamesMongo = _mapper.Map<List<GameDto>>(products);

            games = games.Union(gamesMongo).Distinct(new Extensions.CompareExtension.Comparer<GameDto>("Key")).ToList();

            foreach (var item in games)
            {
                _gameTranslate.SetDescription(item);
            }

            _logger.Info("Service completed its work successfully");
            return games;
        }

        public GameDto GetGameByKey(string key)
        {
            _logger.Info($"Starting GameServices method GetGameByKey and returning game with key {key} from database");

            var gameFromDb = _unitOfWork.Games.Get(key);

            if (gameFromDb == null)
            {
                var product = _unitOfWork.Products.Get(key);
                gameFromDb = _mapper.Map<Game>(product);

                var suppliers = new List<Supplier> { _unitOfWork.Suppliers.Get(x => x.SupplierID == product.SupplierID).First()};
                var categories = new List<Category> { _unitOfWork.Categories.Get(x => x.CategoryID == product.CategoryID).First() };

                gameFromDb.Publishers = CheckIfPublisherExists(suppliers.Select(x=>x.CompanyName).ToList(),suppliers);
                gameFromDb.Genres = CheckIfGenresExists(categories.Select(x => x.CategoryName).ToList(), categories);
                gameFromDb.PlatformTypes = new List<PlatformType>();
                gameFromDb.AddedToGameStore = DateTime.UtcNow;

                SetDefaultValue(gameFromDb);

                _unitOfWork.Games.Insert(gameFromDb);
                _unitOfWork.Save();
            }

            var gameByKey = _mapper.Map<Game, GameDto>(gameFromDb);
            _gameTranslate.SetDescription(gameByKey);
            _logger.Info("Service completed its work successfully");
            return gameByKey;
        }

        public void Create(GameDto gameDto)
        {
            _logger.Info(
                $"Starting GameServices method Create and creating game with Name {gameDto.Name} and Key {gameDto.Key}");

            if (_unitOfWork.Games.GetAll().Any(e => e.Key == gameDto.Key) ||
                _unitOfWork.Products.GetAll().Any(e => e.Id == gameDto.Key))
            {
                throw new ArgumentException("Game with this Key already exists in database");
            }

            var platformTypes = new List<PlatformType>();

            foreach (var platformTypeId in gameDto.SelectedPlatformTypes)
            {
                var platform = _unitOfWork.PlatformTypes.Get(platformTypeId);
                platformTypes.Add(platform);
            }

            var game = _mapper.Map<Game>(gameDto);
            game.Genres = CheckIfGenresExists(gameDto.SelectedGenres);
            game.PlatformTypes = platformTypes;
            game.Publishers = CheckIfPublisherExists(gameDto.SelectedPublishers);
            game.AddedToGameStore = DateTime.UtcNow;

            SetDefaultValue(game);

            _unitOfWork.Games.Insert(game);
            _unitOfWork.Save();

            var languageRus = _unitOfWork.Languages.Get(LangRus);
            var languageEn = _unitOfWork.Languages.Get(LangEn);
            var gameFromDb = _mapper.Map<GameDto>(_unitOfWork.Games.Get(game.Key));
            _gameTranslate.CreateTranslation(gameFromDb, languageRus.Id, gameDto.Description);
            _gameTranslate.CreateTranslation(gameFromDb, languageEn.Id, gameDto.DescriptionEnglish);
            _logger.Info("Service completed its work successfully");
        }
        
        public void Delete(string key)
        {
            _logger.Info($"Starting GameServices method Delete and deleting game with key {key}");

            var gameFromDb = _unitOfWork.Games.Get(key);

            if (gameFromDb == null)
            {
                gameFromDb = _mapper.Map<Game>(_unitOfWork.Products.Get(key));
                gameFromDb.IsDeleted = true;
                _unitOfWork.Games.Insert(gameFromDb);
            }
            else
            {
                gameFromDb.IsDeleted = true;
                _unitOfWork.Games.Update(gameFromDb);
            }

            _unitOfWork.Save();
            _logger.Info("Service completed its work successfully");
        }

        public void Update(GameDto gameDto)
        {
            _logger.Info($"Starting GameServices method Update and updating game with Id {gameDto.Id}");
            var gameFromDb = _unitOfWork.Games.Get(gameDto.Key);
            gameFromDb.Genres?.Clear();
            gameFromDb.PlatformTypes?.Clear();
            gameFromDb.Publishers?.Clear();
            var platformTypes = new List<PlatformType>();

            foreach (var platformTypeId in gameDto.SelectedPlatformTypes)
            {
                var platform = _unitOfWork.PlatformTypes.Get(platformTypeId);
                platformTypes.Add(platform);
            }

            gameFromDb.Publishers = CheckIfPublisherExists(gameDto.SelectedPublishers);
            gameFromDb.Genres = CheckIfGenresExists(gameDto.SelectedGenres);
            gameFromDb.PlatformTypes = platformTypes;
            gameFromDb.AddedToGameStore = DateTime.UtcNow;
            gameFromDb.Price = gameDto.Price;

            SetDefaultValue(gameFromDb);

            _unitOfWork.Games.Update(gameFromDb);

            var languageRus = _unitOfWork.Languages.Get(LangRus);
            var languageEn = _unitOfWork.Languages.Get(LangEn);
            _gameTranslate.UpdateTranslation(gameDto,languageRus.Id,gameDto.Description);
            _gameTranslate.UpdateTranslation(gameDto, languageEn.Id, gameDto.DescriptionEnglish);
            _unitOfWork.Save();
            _logger.Info("Service completed its work successfully");
        }

        public IEnumerable<GameDto> GetAllGamesForGenre(string name)
        {
            _logger.Info($"Starting GameServices method GetAllGamesForGenre with name {name}");
            var gamesFromDb = _unitOfWork.Games.Get(m => m.Genres.All(m1 => m1.Name == name));
            var games = _mapper.Map<IEnumerable<Game>, List<GameDto>>(gamesFromDb);

            var genreMongo = _unitOfWork.Categories.Get(name);
            var gamesMap = new List<Game>();

            if (genreMongo != null)
            {
                var gamesFromMongo = _unitOfWork.Products.Get(m => m.CategoryID == genreMongo.CategoryID);
                gamesMap = _mapper.Map<IEnumerable<Product>, List<Game>>(gamesFromMongo);
            }

            var gamesDto = _mapper.Map<List<GameDto>>(gamesMap);
            games = games.Union(gamesDto).Distinct(new Extensions.CompareExtension.Comparer<GameDto>("Key")).ToList();

            _logger.Info("Service completed its work successfully");
            return games;
        }

        public IEnumerable<GameDto> GetAllGamesForPublisher(string companyName)
        {
            _logger.Info($"Starting GameServices method GetAllGamesForPublisher with name {companyName}");
            var gamesFromDb = _unitOfWork.Games.Get(m => m.Publishers.All(m1 => m1.CompanyName == companyName));
            var games = _mapper.Map<IEnumerable<Game>, List<GameDto>>(gamesFromDb);

            var publisherMongo = _unitOfWork.Suppliers.Get(companyName);
            var gamesMap = new List<Game>();

            if (publisherMongo !=null)
            {
                var gamesFromMongo = _unitOfWork.Products.Get(m => m.SupplierID == publisherMongo.SupplierID);
                gamesMap = _mapper.Map<IEnumerable<Product>, List<Game>>(gamesFromMongo);
            }
            
            var gamesDto = _mapper.Map<List<GameDto>>(gamesMap);
            games = games.Union(gamesDto).Distinct(new Extensions.CompareExtension.Comparer<GameDto>("Key")).ToList();
            _logger.Info("Service completed its work successfully");
            return games;
        }

        public IEnumerable<GameDto> GetAllGamesForPlatformType(string type)
        {
            _logger.Info($"Starting GameServices method GetAllGamesForPlatformType with type {type}");
            var gamesFromDb = _unitOfWork.Games.Get(m => m.PlatformTypes.All(m1 => m1.Type == type));
            var games = _mapper.Map<IEnumerable<Game>, IEnumerable<GameDto>>(gamesFromDb);
            _logger.Info("Service completed its work successfully");
            return games;
        }

        public void WriteToFile(string filename, string text)
        {
            _logger.Info($"Starting GameService method WriteToFile with filename {filename}");

            using (var myWriter = new StreamWriter(filename))
            {
                myWriter.WriteLine(text);
            }
        }

        public byte[] GetImage(string key)
        {
            var game = _unitOfWork.Games.Get(key);
            _validation.ValidateIfItemExist(game);

            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, (game.Picture ?? "Content/File/gray.png"));

            var fileBytes = File.ReadAllBytes(path);

            return fileBytes;
        }

        private List<Publisher> CheckIfPublisherExists(IEnumerable<string> publisherNames, List<Supplier> suppliers = null)
        {
            var publishers = new List<Publisher>();

            foreach (var publisherName in publisherNames)
            {
                var publisher = _unitOfWork.Publishers.Get(publisherName);
                var supplier = suppliers?.FirstOrDefault(x => x.CompanyName == publisherName);

                if (publisher == null)
                {
                    publisher = _mapper.Map<Publisher>(supplier ?? _unitOfWork.Suppliers.Get(publisherName));
                    _unitOfWork.Publishers.Insert(publisher);
                }

                publishers.Add(publisher);
            }

            return publishers;
        } 

        private List<Genre> CheckIfGenresExists(IEnumerable<string> genreNames, List<Category> categories = null)
        {
            var genres = new List<Genre>();

            foreach (var genreName in genreNames)
            {
                var genre = _unitOfWork.Genres.Get(genreName);
                var category = categories?.FirstOrDefault(x => x.CategoryName == genreName);

                if (genre == null)
                {
                    genre = _mapper.Map<Genre>(category ?? _unitOfWork.Categories.Get(genreName));
                    _unitOfWork.Genres.Insert(genre);
                }

                genres.Add(genre);
            }

            return genres;
        }  

        private void SetDefaultValue(Game gameFromDb)
        {
            if (gameFromDb.Publishers.Count == 0)
            {
                var defaultPublisher = _unitOfWork.Publishers.Get(x => x.Id == 1).FirstOrDefault();
                gameFromDb.Publishers = new List<Publisher> { defaultPublisher };
            }

            if (gameFromDb.Genres.Count == 0)
            {
                var defaultGenre = _unitOfWork.Genres.Get(x => x.Id == 1).FirstOrDefault();
                gameFromDb.Genres = new List<Genre> { defaultGenre };
            }
        }
    }
}