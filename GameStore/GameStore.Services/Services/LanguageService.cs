﻿using System.Collections.Generic;
using AutoMapper;
using GameStore.DAL.Interfaces;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;

namespace GameStore.Services.Services
{
    public class LanguageService : ILanguageService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public LanguageService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public LanguageDto GetLanguageByName(string name)
        {
            var languageDb = _unitOfWork.Languages.Get(name);
            return _mapper.Map<LanguageDto>(languageDb);
        }

        public IEnumerable<LanguageDto> GetAllLanguage()
        {
            var languageDb = _unitOfWork.Languages.GetAll();

            return _mapper.Map<IEnumerable<LanguageDto>>(languageDb);
        }
    }
}
