﻿using System;
using GameStore.Services.Interfaces;
using GameStore.Tools.Logger;

namespace GameStore.Services.Services
{
    public class ValidationService : IValidationService
    {
        private readonly ILoggerFileService _loggingService;

        public ValidationService(ILoggerFileService loggingService)
        {
            _loggingService = loggingService;
        }

        public void ValidateIfItemExist<T>(T entity)
        {
            _loggingService.Info($"Starting ValidationService method ValidateIfItemExist with entity {typeof(T).Name}");

            if (entity == null)
            {
                _loggingService.Error($"{nameof(T)} not found");
                throw new ArgumentException($"{typeof(T).Name} not found");
            }
        }
    }
}