﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Mongo;
using GameStore.DAL.Interfaces;
using GameStore.Extensions.Enums;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.Tools.Logger;

namespace GameStore.Services.Services
{
    public class OrderService : IOrderService
    {
        private readonly ILoggerFileService _logger;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IValidationService _validationService;

        public OrderService(IUnitOfWork untiOfWork, ILoggerFileService loggingService, IMapper mapper,
            IValidationService validationService)
        {
            _unitOfWork = untiOfWork;
            _logger = loggingService;
            _mapper = mapper;
            _validationService = validationService;
        }

        public void Create(OrderDto orderDto)
        {
            _logger.Info($"Starting OrderService method Create and creating Order with id {orderDto.Id}");
            var newOrder = _mapper.Map<Order>(orderDto);
            newOrder.Status = OrderStatus.NotPaid;
            _unitOfWork.Orders.Insert(newOrder);
            _unitOfWork.Save();
            _logger.Info("Service completed its work successfully");
        }

        public OrderDto LastCustomersOrder(int id)
        {
            _logger.Info($"Starting OrderService method LastCustomersOrder with parameter customerId  {id}");
            var orderFromDb = _unitOfWork.Orders.Get(x => x.CustomerId == id).LastOrDefault();

            if (orderFromDb == null)
            {
                var ordersMongo = _unitOfWork.OrdersMongo.Get(x => x.EmployeeID == id).LastOrDefault();
                orderFromDb = _mapper.Map<OrderMongo, Order>(ordersMongo);
            }
            
            _validationService.ValidateIfItemExist(orderFromDb);
            var order = _mapper.Map<Order, OrderDto>(orderFromDb);

            _logger.Info("Service completed its work successfully");
            return order;
        }

        public IEnumerable<OrderDto> GetAllOrders()
        {
            _logger.Info("Starting OrderService method GetAllOrders");

            var ordersFromDb = _mapper.Map<IEnumerable<Order>, List<OrderDto>>(_unitOfWork.Orders.GetAll());

            var ordersMongo = _mapper.Map<IEnumerable<OrderMongo>, List<Order>>(_unitOfWork.OrdersMongo.GetAll());

            var ordersDto = _mapper.Map<List<OrderDto>>(ordersMongo);

            var orders = ordersFromDb.Union(ordersDto).Distinct().ToList();

            _logger.Info("Service completed its work successfully");
            return orders;
        }

        public IEnumerable<OrderDto> GetOrdersBetweenDates(DateTime from , DateTime to)
        {
            _logger.Info("Starting OrderService method GetOrdersBetweenDates");
            var ordersFromDb = _unitOfWork.Orders.Get(x=>x.OrderDate>from && x.OrderDate<to);

            var ordersMap = _mapper.Map<IEnumerable<Order>, List<OrderDto>>(ordersFromDb);

            var ordersMongo = _unitOfWork.OrdersMongo.Get(x => DateTime.Parse(x.OrderDate) > from && DateTime.Parse(x.OrderDate) < to);

            var ordersMongoMap = _mapper.Map<IEnumerable<OrderMongo>, List<Order>>(ordersMongo);

            var ordersDto = _mapper.Map<List<OrderDto>>(ordersMongoMap);

            var orders = ordersMap.Union(ordersDto).Distinct().ToList();

            _logger.Info("Service completed its work successfully");
            return orders;
        }

        public void Update(OrderDto orderDto)
        {
            _logger.Info($"Starting OrderService method Update and updating order with Id {orderDto.Id}");
            var orderFromDb = _unitOfWork.Orders.GetById(orderDto.Id);

            orderFromDb.Status = OrderStatus.Paid;

            _unitOfWork.Orders.Update(orderFromDb);
            _unitOfWork.Save();
            _logger.Info("Service completed its work successfully");
        }

        public OrderDto GetOrderById(Guid id)
        {
            _logger.Info($"Starting OrderService method GetOrderById return order with Id {id}");
            var orderFromDb = _unitOfWork.Orders.GetById(id);
            var ordermap = _mapper.Map<OrderDto>(orderFromDb);
            _logger.Info("Service completed its work successfully");
            return ordermap;
        }

        public void Delete(Guid id)
        {
            _logger.Info("Starting OrderService method Delete");

            var orderFromDb = _unitOfWork.Orders.GetById(id);

            if (orderFromDb == null)
            {
                orderFromDb = _mapper.Map<Order>(_unitOfWork.OrdersMongo.Get(id.ToString()));
                _unitOfWork.OrdersMongo.Delete(id.ToString());
            }
            else
            {
                _unitOfWork.Orders.Delete(id);
            }

            _validationService.ValidateIfItemExist(orderFromDb);

            _unitOfWork.Save();
        }

        public IEnumerable<OrderDto> GetOrdersHistory(DateTime orderDate, OrderSortType sortType)
        {
            _logger.Info("Starting OrderService method GetOrdersHistory");

            List<OrderDto> ordersFromDb;
            List<Order> ordersMongo;

            switch (sortType)
            {
                case OrderSortType.YoungOrders:
                    ordersFromDb =
                        _mapper.Map<List<OrderDto>>(_unitOfWork.Orders.Get(x => x.OrderDate > orderDate && x.Status != OrderStatus.Shipped));
                    ordersMongo =
                        _mapper.Map<List<Order>>(
                            _unitOfWork.OrdersMongo.Get(x => DateTime.Parse(x.OrderDate) > orderDate));
                    break;
                case OrderSortType.OldOrders:
                    ordersFromDb =
                        _mapper.Map<List<OrderDto>>(_unitOfWork.Orders.Get(x => x.OrderDate < orderDate));
                    ordersMongo =
                        _mapper.Map<List<Order>>(
                            _unitOfWork.OrdersMongo.Get(x => DateTime.Parse(x.OrderDate) < orderDate));
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(sortType), sortType, null);
            }

            var ordersDto = _mapper.Map<List<OrderDto>>(ordersMongo);
            var orders = ordersFromDb.Union(ordersDto).Distinct().ToList();

            _logger.Info("Service completed its work successfully");
            return orders;
        }

        public void EditStatus(OrderDto orderDto)
        {
            _logger.Info($"Starting OrderService method EditStatus and updating order with Id {orderDto.Id}");
            var orderFromDb = _unitOfWork.Orders.GetById(orderDto.Id);

            orderFromDb.Status = orderDto.Status;
            orderFromDb.ShippedDate = DateTime.UtcNow;

            _unitOfWork.Orders.Update(orderFromDb);
            _unitOfWork.Save();
            _logger.Info("Service completed its work successfully");
        }
    }
}