﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using GameStore.DAL.Entities;
using GameStore.DAL.Interfaces;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.Tools.Logger;

namespace GameStore.Services.Services
{
    public class PlatformTypeService : IPlatformTypeService
    {
        private readonly ILoggerFileService _logger;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IValidationService _validationService;

        public PlatformTypeService(IUnitOfWork untiOfWork, ILoggerFileService logger, IMapper mapper,
            IValidationService validationService)
        {
            _unitOfWork = untiOfWork;
            _logger = logger;
            _mapper = mapper;
            _validationService = validationService;
        }

        public IEnumerable<PlatformTypeDto> GetAllPlatformTypes()
        {
            _logger.Info(
                "Starting PlatformTypeServices method GetAllPlatformTypes and returning all platformtypes from database");
            var platformTypesFromDb = _unitOfWork.PlatformTypes.GetAll();
            var platformTypes = _mapper.Map<IEnumerable<PlatformType>, List<PlatformTypeDto>>(platformTypesFromDb);
            _logger.Info("Service completed its work successfully");
            return platformTypes;
        }

        public IEnumerable<PlatformTypeDto> GetPlatformTypesById(List<int> platformTypesIds)
        {
            _logger.Info("Starting PlatformTypeService method GetPlatformTypesByIdWhenCreatingNewGame");

            var listOfPlatformTypes = new List<PlatformType>();
            var platformTypesFromDb = _unitOfWork.PlatformTypes.GetAll().ToList();

            foreach (var item in platformTypesIds)
            {
                var platformType = platformTypesFromDb.FirstOrDefault(x => x.Id == item);
                _validationService.ValidateIfItemExist(platformType);
                listOfPlatformTypes.Add(platformType);
            }

            var platformTypeDto = _mapper.Map<List<PlatformType>, List<PlatformTypeDto>>(listOfPlatformTypes);
            _logger.Info("Service completed its work successfully");
            return platformTypeDto;
        }
    }
}