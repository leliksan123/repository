﻿using System;
using System.Collections.Generic;
using AutoMapper;
using GameStore.DAL.Entities;
using GameStore.DAL.Interfaces;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.Tools.Logger;

namespace GameStore.Services.Services
{
    public class CommentService : ICommentService
    {
        private readonly ILoggerFileService _logger;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IValidationService _validationService;

        public CommentService(IUnitOfWork untiOfWork, ILoggerFileService loggingService, IMapper mapper,
            IValidationService validationService)
        {
            _unitOfWork = untiOfWork;
            _logger = loggingService;
            _mapper = mapper;
            _validationService = validationService;
        }

        public IEnumerable<CommentDto> GetAllComments()
        {
            _logger.Info("Starting CommentServices method GetAllComments and returning comments from database");
            var commentsFromDb = _unitOfWork.Comments.GetAll();
            var comments = _mapper.Map<IEnumerable<Comment>, List<CommentDto>>(commentsFromDb);
            _logger.Info("Service completed its work successfully");
            return comments;
        }

        public IEnumerable<CommentDto> GetAllCommentsForGame(string key)
        {
            _logger.Info(
                $"Starting CommentServices method GetAllCommentsForGame and returning comments from database for game with key {key}");
            var game = _unitOfWork.Games.Get(key);

            if (game == null)
            {
                game = _mapper.Map<Game>(_unitOfWork.Products.Get(key));
                _unitOfWork.Games.Insert(game);
                _unitOfWork.Save();
            }

            _validationService.ValidateIfItemExist(game);

            var commentsFromDb =
                _unitOfWork.Comments.Get(x => (x.GameId == game.Id) && (x.ParentCommentId == null));
            var comments = _mapper.Map<IEnumerable<Comment>, List<CommentDto>>(commentsFromDb);
            _logger.Info("Service completed its work successfully");
            return comments;
        }

        public void Create(CommentDto commentDto)
        {
            _logger.Info(
                $"Starting CommentServices method Create and added into database comment with AuthorName {commentDto.Name} for Game {commentDto.GameId}");

            var newComment = _mapper.Map<CommentDto, Comment>(commentDto);
            _unitOfWork.Comments.Insert(newComment);
            _unitOfWork.Save();
            _logger.Info("Service completed its work successfully");
        }

        public CommentDto GetCommentById(Guid id)
        {
            _logger.Info($"Starting CommentService method DeleteComment with id {id}");
            var comment = _unitOfWork.Comments.GetById(id);
            var commentById = _mapper.Map<CommentDto>(comment);
            _logger.Info("Service completed its work successfully");
            return commentById;
        }

        public void Update(CommentDto commentDto)
        {
            _validationService.ValidateIfItemExist(_unitOfWork.Comments.GetById(commentDto.Id));
            _logger.Info($"Starting CommentServices method Update and updating comment with Id {commentDto.Id}");
            var commentFromDb = _unitOfWork.Comments.GetById(commentDto.Id);

            commentFromDb.Body = commentDto.Body;

            _unitOfWork.Comments.Update(commentFromDb);
            _unitOfWork.Save();
            _logger.Info("Service completed its work successfully");
        }

        public void DeleteComment(Guid id)
        {
            _logger.Info($"Starting CommentService method DeleteComment with id {id}");
            var comments = _unitOfWork.Comments.Get(x => x.ParentCommentId == id);

            foreach (var item in comments)
            {
                _unitOfWork.Comments.Delete(item.Id);
            }

            _unitOfWork.Comments.Delete(id);
            _unitOfWork.Save();
            _logger.Info("Service completed its work successfully");
        }
    }
}