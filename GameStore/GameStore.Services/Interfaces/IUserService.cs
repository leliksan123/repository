﻿using System.Collections.Generic;
using GameStore.Extensions.Enums;
using GameStore.Services.DTO;

namespace GameStore.Services.Interfaces
{
    public interface IUserService
    {
        /// <summary>
        /// Bans the user.
        /// </summary>
        /// <param name="email">The identifier.</param>
        void BanUser(string email);

        /// <summary>
        /// Creates the specified user.
        /// </summary>
        /// <param name="user">The user.</param>
        void Create(UserDTo user);

        /// <summary>
        /// Deletes the specified email.
        /// </summary>
        /// <param name="email">The email.</param>
        void Delete(string email);

        /// <summary>
        /// Updates the specified user dto.
        /// </summary>
        /// <param name="userDTo">The user dto.</param>
        void Update(UserDTo userDTo);

        /// <summary>
        /// Gets the usersregister today.
        /// </summary>
        /// <returns>UsersRegisterPerDay</returns>
        IEnumerable<UserDTo> GetUsersregisterToday();

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns>Users</returns>
        IEnumerable<UserDTo> GetAll();

        /// <summary>
        /// Gets the user by email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns>User</returns>
        UserDTo GetUserByEmail(string email);
    }
}