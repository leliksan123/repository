﻿using System;
using System.Collections.Generic;
using GameStore.Services.DTO;

namespace GameStore.Services.Interfaces
{
    public interface IGameService
    {
        /// <summary>
        ///     Gets all games.
        /// </summary>
        /// <returns></returns>
        IEnumerable<GameDto> GetAllGames();

        /// <summary>
        ///     Gets the game by key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        GameDto GetGameByKey(string key);

        /// <summary>
        ///     Creates the specified game dto.
        /// </summary>
        /// <param name="gameDto">The game dto.</param>
        void Create(GameDto gameDto);

        /// <summary>
        /// Deletes the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        void Delete(string key);

        /// <summary>
        ///     Updates the specified game dto.
        /// </summary>
        /// <param name="gameDto">The game dto.</param>
        void Update(GameDto gameDto);

        /// <summary>
        ///     Gets all games for genre.
        /// </summary>
        /// <param name="name">The identifier.</param>
        /// <returns></returns>
        IEnumerable<GameDto> GetAllGamesForGenre(string name);

        /// <summary>
        ///     Gets the type of all games for platform.
        /// </summary>
        /// <param name="type">The identifier.</param>
        /// <returns></returns>
        IEnumerable<GameDto> GetAllGamesForPlatformType(string type);

        /// <summary>
        /// Gets all games for publisher.
        /// </summary>
        /// <param name="companyName">Name of the company.</param>
        /// <returns>GamesDto</returns>
        IEnumerable<GameDto> GetAllGamesForPublisher(string companyName);

        /// <summary>
        ///     Writes to file.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <param name="text">The text.</param>
        void WriteToFile(string filename, string text);

        /// <summary>
        /// Gets the image.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>Image</returns>
        byte[] GetImage(string key);
    }
}