﻿using System.Collections.Generic;
using GameStore.Services.DTO;

namespace GameStore.Services.Interfaces
{
    public interface IRoleService
    {
        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns>Roles</returns>
        IEnumerable<RoleDTo> GetAll();

        /// <summary>
        /// Gets the name of the role by.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>Role</returns>
        RoleDTo GetRoleByName(string name);

        /// <summary>
        /// Deletes the specified name.
        /// </summary>
        /// <param name="name">The name.</param>
        void Delete(string name);

        /// <summary>
        /// Creates the specified role d to.
        /// </summary>
        /// <param name="roleDTo">The role dto.</param>
        void Create(RoleDTo roleDTo);
    }
}
