﻿using GameStore.Services.DTO;

namespace GameStore.Services.Interfaces
{
    public interface IGameTranslation
    {
        /// <summary>
        /// Creates the translation.
        /// </summary>
        /// <param name="game">The game.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <param name="description">The description.</param>
        void CreateTranslation(GameDto game, int languageId, string description);

        /// <summary>
        /// Updates the translation.
        /// </summary>
        /// <param name="game">The game.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <param name="description">The description.</param>
        void UpdateTranslation(GameDto game, int languageId, string description);

        /// <summary>
        /// Sets the description.
        /// </summary>
        /// <param name="game">The game.</param>
        void SetDescription(GameDto game);

        /// <summary>
        /// Gets the translate.
        /// </summary>
        /// <param name="gameId">The game identifier.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <returns>GameTranslation</returns>
        GameTranslationDto GetTranslation(int gameId, int languageId);
    }
}
