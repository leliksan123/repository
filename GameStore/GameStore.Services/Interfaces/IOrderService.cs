﻿using System;
using System.Collections.Generic;
using GameStore.Extensions.Enums;
using GameStore.Services.DTO;

namespace GameStore.Services.Interfaces
{
    public interface IOrderService
    {
        /// <summary>
        ///     Creates the specified order dto.
        /// </summary>
        /// <param name="orderDto">The order dto.</param>
        void Create(OrderDto orderDto);

        /// <summary>
        ///     Lasts the customers order.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>OrderDto</returns>
        OrderDto LastCustomersOrder(int id);

        /// <summary>
        /// Edits the status.
        /// </summary>
        /// <param name="orderDto">The order dto.</param>
        void EditStatus(OrderDto orderDto);

        /// <summary>
        /// Gets all orders.
        /// </summary>
        /// <returns>Ienumerable Orders</returns>
        IEnumerable<OrderDto> GetAllOrders();

        /// <summary>
        /// Gets the orders between dates.
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="to">To.</param>
        /// <returns>Orders</returns>
        IEnumerable<OrderDto> GetOrdersBetweenDates(DateTime from, DateTime to);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(Guid id);

        /// <summary>
        /// Gets the old orders.
        /// </summary>
        /// <returns>Orders</returns>
        IEnumerable<OrderDto> GetOrdersHistory(DateTime orderDate , OrderSortType sortType);

        /// <summary>
        /// Updates the specified order.
        /// </summary>
        /// <param name="order">The order.</param>
        void Update(OrderDto order);

        /// <summary>
        /// Gets the order by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        OrderDto GetOrderById(Guid id);
    }
}