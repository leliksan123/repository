﻿namespace GameStore.Services.Interfaces
{
    public interface IValidationService
    {
        /// <summary>
        ///     Validates if item exist.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">The item.</param>
        void ValidateIfItemExist<T>(T item);
    }
}