﻿using System.Collections.Generic;
using GameStore.Services.DTO;

namespace GameStore.Services.Interfaces
{
    public interface ILanguageService
    {
        /// <summary>
        /// Gets the name of the language by.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>Language</returns>
        LanguageDto GetLanguageByName(string name);

        /// <summary>
        /// Gets the al language.
        /// </summary>
        /// <returns>LanguageDto</returns>
        IEnumerable<LanguageDto> GetAllLanguage();
    }
}
