﻿using System;
using System.Collections.Generic;
using GameStore.Services.DTO;

namespace GameStore.Services.Interfaces
{
    public interface IOrderDetailsService
    {
        /// <summary>
        ///     Gets the order details for order.
        /// </summary>
        /// <param name="orderDto">The order dto.</param>
        /// <returns>Ienumerable OrderDetailsDTO</returns>
        IEnumerable<OrderDetailsDto> GetOrderDetailsForOrder(OrderDto orderDto);

        /// <summary>
        ///     Orders the sum.
        /// </summary>
        /// <param name="orderDetails">The order details.</param>
        /// <returns>OrderSum</returns>
        decimal OrderSum(IEnumerable<OrderDetailsDto> orderDetails);

        /// <summary>
        /// Creates the order details with quantity.
        /// </summary>
        /// <param name="list">The list.</param>
        /// <param name="user">The user.</param>
        void CreateOrderDetailsWithQuantity(Dictionary<string, short> list, string user);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(Guid id);

        /// <summary>
        /// Adds to order.
        /// </summary>
        /// <param name="orderDetails">The order details.</param>
        void AddToOrder(OrderDetailsDto orderDetails);

        /// <summary>
        /// Gets the details by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Orderdetails</returns>
        OrderDetailsDto GetDetailsById(Guid id);

        /// <summary>
        /// Updates the specified order details.
        /// </summary>
        /// <param name="orderDetails">The order details.</param>
        void Update(OrderDetailsDto orderDetails);
    }
}