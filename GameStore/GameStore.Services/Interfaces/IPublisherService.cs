﻿using System.Collections.Generic;
using GameStore.DAL.Entities;
using GameStore.Services.DTO;

namespace GameStore.Services.Interfaces
{
    public interface IPublisherService
    {
        /// <summary>
        ///     Gets all publishers.
        /// </summary>
        /// <returns>Ienumerable PublisherDto</returns>
        IEnumerable<PublisherDto> GetAllPublishers();

        /// <summary>
        ///     Gets the name of the publisher by company.
        /// </summary>
        /// <param name="companyName">Name of the company.</param>
        /// <returns>PublisherDto</returns>
        PublisherDto GetPublisherByCompanyName(string companyName);

        /// <summary>
        ///     Creates the specified publisher dto.
        /// </summary>
        /// <param name="publisherDto">The publisher dto.</param>
        void Create(PublisherDto publisherDto);

        /// <summary>
        /// Deletes the specified company name.
        /// </summary>
        /// <param name="companyName">Name of the company.</param>
        void Delete(string companyName);

        /// <summary>
        /// Updates the specified publisher dto.
        /// </summary>
        /// <param name="publisherDto">The publisher dto.</param>
        void Update(PublisherDto publisherDto);
    }
}