﻿using System;
using System.Collections.Generic;
using GameStore.Services.DTO;

namespace GameStore.Services.Interfaces
{
    public interface ICommentService
    {
        /// <summary>
        ///     Gets all comments.
        /// </summary>
        /// <returns></returns>
        IEnumerable<CommentDto> GetAllComments();

        /// <summary>
        ///     Gets all comments for game.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        IEnumerable<CommentDto> GetAllCommentsForGame(string key);

        /// <summary>
        ///     Creates the specified comment dto.
        /// </summary>
        /// <param name="commentDto">The comment dto.</param>
        void Create(CommentDto commentDto);

        /// <summary>
        ///     Updates the specified comment dto.
        /// </summary>
        /// <param name="commentDto">The comment dto.</param>
        void Update(CommentDto commentDto);

        /// <summary>
        ///     Deletes the comment.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteComment(Guid id);

        /// <summary>
        /// Gets the comment by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>CommentDto</returns>
        CommentDto GetCommentById(Guid id);
    }
}