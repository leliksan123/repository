﻿using System.Collections.Generic;
using GameStore.Services.DTO;

namespace GameStore.Services.Interfaces
{
    public interface IGenreService
    {
        /// <summary>
        ///     Gets all genres.
        /// </summary>
        /// <returns></returns>
        IEnumerable<GenreDto> GetAllGenres();

        /// <summary>
        ///     Gets the genres by identifier when creating new game.
        /// </summary>
        /// <param name="genresNames">The genres names.</param>
        /// <returns>Ienumerable GenreDTO</returns>
        IEnumerable<GenreDto> GetGenresByName(IEnumerable<string> genresNames);

        /// <summary>
        /// Creates the specified genre dto.
        /// </summary>
        /// <param name="genreDto">The genre dto.</param>
        void Create(GenreDto genreDto);

        /// <summary>
        /// Deletes the specified name.
        /// </summary>
        /// <param name="name">The name.</param>
        void Delete(string name);

        /// <summary>
        /// Updates the specified genre dto.
        /// </summary>
        /// <param name="genreDto">The genre dto.</param>
        void Update(GenreDto genreDto);

        /// <summary>
        /// Gets the name of the by.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>GenreDto</returns>
        GenreDto GetByName(string name);

        /// <summary>
        /// Gets the genres of game.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>GenreDtos</returns>
        IEnumerable<GenreDto> GetGenresOfGame(string key);
    }
}