﻿using GameStore.Services.DTO;

namespace GameStore.Services.Interfaces
{
    public interface IPublisherTranslation
    {
        /// <summary>
        /// Sets the description.
        /// </summary>
        /// <param name="publisher">The publisher.</param>
        void SetDescription(PublisherDto publisher);

        /// <summary>
        /// Updates the translation.
        /// </summary>
        /// <param name="publisher">The publisher.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <param name="description">The description.</param>
        void UpdateTranslation(PublisherDto publisher, int languageId, string description);

        /// <summary>
        /// Creates the translation.
        /// </summary>
        /// <param name="publisher">The publisher.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <param name="description">The description.</param>
        void CreateTranslation(PublisherDto publisher, int languageId, string description);

        /// <summary>
        /// Gets the translation.
        /// </summary>
        /// <param name="publisherId">The publisher identifier.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <returns></returns>
        PublisherTranslationDto GetTranslation(int publisherId, int languageId);
    }
}
