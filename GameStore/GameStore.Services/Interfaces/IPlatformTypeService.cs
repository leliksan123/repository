﻿using System.Collections.Generic;
using GameStore.Services.DTO;

namespace GameStore.Services.Interfaces
{
    public interface IPlatformTypeService
    {
        /// <summary>
        ///     Gets all platform types.
        /// </summary>
        /// <returns>Ienumerable PlatformTypeDto</returns>
        IEnumerable<PlatformTypeDto> GetAllPlatformTypes();

        /// <summary>
        ///     Gets the platform types by identifier.
        /// </summary>
        /// <param name="platformTypesIds">The platform types ids.</param>
        /// <returns>Ienumerable PlatformTypeDto</returns>
        IEnumerable<PlatformTypeDto> GetPlatformTypesById(List<int> platformTypesIds);
    }
}