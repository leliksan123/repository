﻿using System.Collections.Generic;
using GameStore.Services.DTO;
using GameStore.Services.Models;

namespace GameStore.Services.Interfaces
{
    public interface IFilterService
    {
        /// <summary>
        /// Filters the specified filter dto.
        /// </summary>
        /// <param name="filterDto">The filter dto.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="take">The take.</param>
        /// <param name="isDeleted"></param>
        /// <returns>Ienumerable GameDto</returns>
        IEnumerable<GameDto> Filter(FilterDto filterDto , int skip , int take , bool isDeleted = false);

        /// <summary>
        /// Gameses the count.
        /// </summary>
        /// <param name="filterDto">The filter dto.</param>
        /// <param name="isDeleted"></param>
        /// <returns></returns>
        int GamesCount(FilterDto filterDto , bool isDeleted = false);
    }
}