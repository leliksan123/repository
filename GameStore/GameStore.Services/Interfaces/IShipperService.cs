﻿using System.Collections.Generic;
using GameStore.Services.DTO;

namespace GameStore.Services.Interfaces
{
    public interface IShipperService
    {
        /// <summary>
        /// Gets all shippers.
        /// </summary>
        /// <returns>Ienumerable ShipperDTO</returns>
        IEnumerable<ShipperDto> GetAllShippers();
    }
}
