﻿using GameStore.DAL.Interfaces;
using GameStore.DAL.Realization;
using GameStore.DAL.Repositories;
using Ninject;

namespace GameStore.Services.Infrastructure
{
    public static class NinjectResolver
    {
        public static void Configure(IKernel kernel)
        {
            kernel.Bind<IUnitOfWork>().To<UnitOfWork>();

            kernel.Bind<IGameRepository>().To<GameRepository>();
        }
    }
}
