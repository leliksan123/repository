﻿using GameStore.Services.DTO;
using GameStore.Services.Models;

namespace GameStore.Services.Strategy
{
    public interface IPaymentStrategy
    {
        OrderInfo Pay(OrderInformDto model);
    }
}
