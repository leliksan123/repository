﻿using GameStore.Extensions.Enums;
using GameStore.Services.DTO;
using GameStore.Services.Models;

namespace GameStore.Services.Strategy
{
    public class BoxStrategy : IPaymentStrategy
    {
        public OrderInfo Pay(OrderInformDto model)
        {
            var resultModel = new OrderInfo
            {
                OrderDate = model.OrderDate,
                OrderId = model.OrderId,
                Sum = model.Sum,
                UserId = model.UserId,
                Type = PaymentType.Box
            };

            return resultModel;
        }
    }
}