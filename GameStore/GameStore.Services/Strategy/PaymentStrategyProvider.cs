﻿using System.Collections.Generic;
using GameStore.Extensions.Enums;

namespace GameStore.Services.Strategy
{
    public class PaymentStrategyProvider
    {
        private static Dictionary<PaymentType, IPaymentStrategy> _paymentStrategies;

        public PaymentStrategyProvider()
        {
            _paymentStrategies = new Dictionary<PaymentType, IPaymentStrategy>()
            {
                {PaymentType.Bank, new BankStrategy() },
                {PaymentType.Box, new BoxStrategy() },
                {PaymentType.VisaCard, new VisaStrategy() }
            };
        }

        public IPaymentStrategy GetStrategy(PaymentType paymentMethod)
        {
            return _paymentStrategies[paymentMethod];
        }

    }
}