﻿using System.Globalization;
using System.IO;
using GameStore.Extensions.Enums;
using GameStore.Services.DTO;
using GameStore.Services.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace GameStore.Services.Strategy
{
    public class BankStrategy : IPaymentStrategy
    {
        public OrderInfo Pay(OrderInformDto model)
        {
            var document = new Document();

            byte[] array;
            
            using (var stream = new MemoryStream())
            {
                var writer = PdfWriter.GetInstance(document,stream);
                document.Open();
                document.Add(new Paragraph(string.Concat("Date: ", model.OrderDate.ToShortDateString())));
                document.Add(new Paragraph(string.Concat("OrderId: ",model.OrderId)));
                document.Add(new Paragraph(string.Concat("UserId: ",model.UserId)));
                document.Add(new Paragraph(string.Concat("Summa: ",model.Sum.ToString(CultureInfo.InvariantCulture))));
                document.Close();
                writer.Close();
                array = stream.ToArray();
            }

            var resultModel = new OrderInfo
            {
                Path = array,
                Type = PaymentType.Bank
            };

            return resultModel;
        }
    }
}