﻿namespace GameStore.Services.DTO
{
    public class PlatformTypeDto
    {
        public int Id { get; set; }

        public string Type { get; set; }
    }
}