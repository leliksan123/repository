﻿using System;

namespace GameStore.Services.DTO
{
    public class OrderInformDto
    {
        public int UserId { get; set; }

        public Guid OrderId { get; set; }

        public decimal Sum { get; set; }

        public DateTime OrderDate { get; set; }
    }
}