﻿using System;
using System.Collections.Generic;
using GameStore.DAL.Entities;
using GameStore.Extensions.Enums;

namespace GameStore.Services.DTO
{
    public class OrderDto
    {
        public Guid Id { get; set; }

        public int CustomerId { get; set; }

        public OrderStatus Status { get; set; }

        public DateTime? ShippedDate { get; set; }

        public DateTime OrderDate { get; set; }

        public virtual ICollection<OrderDetails> OrderDetailses { get; set; }
    }
}