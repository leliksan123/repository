﻿using System;
using System.Collections.Generic;

namespace GameStore.Services.DTO
{
    public class GameDto
    {
        public int Id { get; set; }

        public string Key { get; set; }

        public string Name { get; set; }

        public string Picture { get; set; }

        public string Description { get; set; }

        public string DescriptionEnglish { get; set; }

        public decimal Price { get; set; }

        public short UnitsInStock { get; set; }

        public bool Discontinued { get; set; }

        public string PublisherName { get; set; }

        public bool IsDeleted { get; set; }

        public int ViewsNumber { get; set; }

        public DateTime AddedToGameStore { get; set; }

        public List<string>  SelectedGenres { get; set; }

        public List<string> SelectedPlatformTypes { get; set; }

        public List<string> SelectedPublishers { get; set; }

        public virtual ICollection<PlatformTypeDto> PlatformTypes { get; set; }

        public virtual ICollection<GenreDto> Genres { get; set; }

        public virtual ICollection<PublisherDto> Publishers { get; set; }

        public virtual ICollection<GameTranslationDto> GameTranslations { get; set; }

    }
}