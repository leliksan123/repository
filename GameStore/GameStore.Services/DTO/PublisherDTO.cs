﻿using System.Collections.Generic;

namespace GameStore.Services.DTO
{
    public class PublisherDto
    {
        public int Id { get; set; }

        public string CompanyName { get; set; }

        public string Description { get; set; }

        public string DescriptionEnglish { get; set; }

        public string HomePage { get; set; }

        public bool IsDeleted { get; set; }

        public List<string> SelectedGames { get; set; }

        public virtual ICollection<GameDto> Games { get; set; }

        public virtual ICollection<PublisherTranslationDto> PublisherTranslations { get; set; }
    }
}