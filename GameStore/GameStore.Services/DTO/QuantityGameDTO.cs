﻿namespace GameStore.Services.DTO
{
    public class QuantityGameDto
    {
        public string Key { get; set; }

        public short Quantity { get; set; }
    }
}