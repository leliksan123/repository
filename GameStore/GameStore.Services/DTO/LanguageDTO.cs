﻿namespace GameStore.Services.DTO
{
    public class LanguageDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
