﻿namespace GameStore.Services.DTO
{
    public class GenreDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool IsDeleted { get; set; }

        public int? ParentGenreId { get; set; }
    }
}