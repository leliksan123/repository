﻿using System;

namespace GameStore.Services.DTO
{
    public class OrderDetailsDto
    {
        public Guid Id { get; set; }

        public int ProductId { get; set; }

        public short Quantity { get; set; }

        public double Discount { get; set; }

        public Guid OrderId { get; set; }

        public string SelectedGame { get; set; }
    }
}