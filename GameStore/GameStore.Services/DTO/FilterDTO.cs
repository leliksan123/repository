﻿using System.Collections.Generic;
using GameStore.Extensions.Enums;

namespace GameStore.Services.DTO
{
    public class FilterDto
    {
        public List<string> Genres { get; set; }

        public List<string> PlatformTypes { get; set; }

        public List<string> Publishers { get; set; }

        public OrderByType OrderByType { get; set; }

        public decimal PriceFrom { get; set; }

        public decimal PriceTo { get; set; }

        public DateSortType SelectedYear { get; set; }

        public string PartOfName { get; set; }
    }
}