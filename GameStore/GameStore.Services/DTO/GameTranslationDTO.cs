﻿using System;

namespace GameStore.Services.DTO
{
    public class GameTranslationDto
    {
        public Guid Id { get; set; }

        public int GameId { get; set; }

        public string Description { get; set; }

        public int LanguageId { get; set; }
    }
}
