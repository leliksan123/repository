﻿namespace GameStore.Services.DTO
{
    public class ShipperDto
    {
        public string Id { get; set; }

        public int ShipperID { get; set; }

        public string CompanyName { get; set; }

        public string Phone { get; set; }
    }
}
