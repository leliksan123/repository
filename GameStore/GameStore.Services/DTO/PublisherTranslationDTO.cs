﻿using System;

namespace GameStore.Services.DTO
{
    public class PublisherTranslationDto
    {
        public Guid Id { get; set; }

        public int PublisherId { get; set; }

        public string Description { get; set; }

        public int LanguageId { get; set; }
    }
}
