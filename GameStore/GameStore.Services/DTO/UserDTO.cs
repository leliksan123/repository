﻿using System;
using System.Collections.Generic;

namespace GameStore.Services.DTO
{
    public class UserDTo
    {
        public int Id { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Phone { get; set; }

        public bool NotificationEmail { get; set; }

        public bool NotificationSms { get; set; }

        public bool NotificationMobileApp { get; set; }

        public List<string> SelectedRoles { get; set; }

        public DateTime RegistrationTime { get; set; }

        public virtual ICollection<RoleDTo> Roles { get; set; }
    }
}
