﻿using System;
using System.Collections.Generic;
using GameStore.DAL.Entities;

namespace GameStore.Services.DTO
{
    public class CommentDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Body { get; set; }

        public int GameId { get; set; }

        public string Quote { get; set; }

        public Guid? ParentCommentId { get; set; }

        public virtual Game Game { get; set; }

        public virtual CommentDto ParentComment { get; set; }

        public virtual ICollection<CommentDto> Answers { get; set; }
    }
}