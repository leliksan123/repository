﻿namespace GameStore.Services.DTO
{
    public class RoleDTo
    {
        public int Id { get; set; }

        public bool IsDeleted { get; set; }

        public string RoleName { get; set; }
    }
}
