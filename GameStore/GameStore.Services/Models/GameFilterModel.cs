﻿using System;
using System.Linq.Expressions;
using GameStore.DAL.Entities;
using GameStore.Services.DTO;

namespace GameStore.Services.Models
{
    public class GameFilterModel
    {
        public FilterDto FilterDto { get; set; }

        public Expression<Func<Game, bool>> Predicate { get; set; }

        public Expression<Func<Game, object>> SortPredicate { get; set; }
    }
}
