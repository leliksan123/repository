﻿using System;
using GameStore.Extensions.Enums;

namespace GameStore.Services.Models
{
    public class OrderInfo
    {
        public PaymentType Type { get; set; }

        public byte[] Path { get; set; }

        public int UserId { get; set; }

        public Guid OrderId { get; set; }

        public DateTime OrderDate { get; set; }

        public decimal Sum { get; set; }
    }
}