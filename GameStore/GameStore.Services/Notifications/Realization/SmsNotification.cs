﻿namespace GameStore.Services.Notifications.Realization
{
    public class SmsNotification : IObserver
    {

        public SmsNotification(IObservable notificate)
        {
            notificate.AddObserver(this);
        }

        public void Send()
        {
        }
    }
}
