﻿using System.Linq;
using GameStore.Services.Interfaces;
using GameStore.Tools.EmailSender;

namespace GameStore.Services.Notifications.Realization
{
    public class EmailNotification : IObserver
    {
        private readonly IEmailSender _emailSender;
        private readonly IUserService _userService;

        public EmailNotification(IObservable notificate , IEmailSender emailSender, IUserService userService)
        {
            notificate.AddObserver(this);
            _emailSender = emailSender;
            _userService = userService;
        }

        public void Send()
        {
            var users = _userService.GetAll().Where(x => x.NotificationEmail);

            foreach (var user in users)
            {
                _emailSender.SendEmail(user.Email, "Paid Order").GetAwaiter();
            }
        }
    }
}
