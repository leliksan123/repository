﻿namespace GameStore.Services.Notifications.Realization
{
    public class ModileAppNotification : IObserver
    {

        public ModileAppNotification(IObservable notificate)
        {
            notificate.AddObserver(this);
        }

        public void Send()
        {
        }
    }
}
