﻿namespace GameStore.Services.Notifications
{
    public interface IObserver
    {
        void Send();
    }
}
