﻿namespace GameStore.Models
{
    public class HandleErrorInfo
    {
        public string Message { get; set; }

        public string StackTrace { get; set; }
    }
}