﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using GameStore.ViewModels.ViewModels.Comment;

namespace GameStore.Helpers
{
    public static class CommentsHelper
    {
        public static MvcHtmlString RenderCommentHierarchy(this HtmlHelper helper,
            ICollection<CommentViewModel> comments, string key, int depth = -1)
        {
            depth++;

            var div = new TagBuilder("div");

            var media = new TagBuilder("div");
            media.AddCssClass("media col-md-offset-" + depth);

            foreach (var item in comments)
            {
                if (item != null)
                {
                    item.GameKey = key;
                    media.InnerHtml = RenderComment(helper, item).ToHtmlString();

                    div.InnerHtml += media.ToString();

                    if (item.Answers.Any())
                    {
                        media.InnerHtml = RenderCommentHierarchy(helper, item.Answers, key, depth).ToHtmlString();
                        div.InnerHtml += media.ToString();
                    }
                }
            }

            return new MvcHtmlString(div.ToString());
        }

        public static MvcHtmlString RenderComment(this HtmlHelper helper, CommentViewModel comment)
        {
            TagBuilder quote = null;

            var divMediaLeft = new TagBuilder("div");
            divMediaLeft.AddCssClass("media-left");
            var divMediaBody = new TagBuilder("div");
            divMediaBody.AddCssClass("panel panel-white post panel-shadow");

            var hName = new TagBuilder("div");
            hName.AddCssClass("post-heading");
            var picture = new TagBuilder("div");
            picture.AddCssClass("pull-left image");

            var image = new TagBuilder("img");
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);
            image.MergeAttribute("src", urlHelper.Content("~/Content/File/user_1.jpg"));
            image.AddCssClass("img-circle avatar");

            var author = new TagBuilder("div");
            author.AddCssClass("pull-left meta");
            var authorname = new TagBuilder("div");
            authorname.AddCssClass("title h5");
            var auth = new TagBuilder("a");
            auth.MergeAttribute("href", "#");
            
            var input = new TagBuilder("input");
            var paragraph = new TagBuilder("p");
            var hiddeninput = new TagBuilder("input");
            var text = new TagBuilder("div");
            text.AddCssClass("post-description");

            var buttonsdiv = new TagBuilder("div");
            buttonsdiv.AddCssClass("stats");
            var link = new TagBuilder("a");
            link.SetInnerText("Quote");
            link.AddCssClass("btn btn-info stat-item");
            link.MergeAttribute("data-type", "quote");

            var answerLink = new TagBuilder("a");
            answerLink.SetInnerText("Answer");
            answerLink.AddCssClass("btn btn-warning stat-item");
            answerLink.MergeAttribute("data-type", "answerLink");

            if (comment != null)
            {
                paragraph.SetInnerText(comment.Body);
                text.InnerHtml += paragraph.ToString();

                input.MergeAttribute("value", comment.Id.ToString());
                input.MergeAttribute("type", "hidden");
                hiddeninput.MergeAttribute("type", "hidden");
                hiddeninput.SetInnerText(comment.GameKey);

                if (comment.Quote != null)
                {
                    quote = new TagBuilder("p");
                    quote.MergeAttribute("id", "QuoteId");
                    quote.AddCssClass("quoteclass");

                    quote.SetInnerText(comment.Quote);
                }

                auth.SetInnerText(comment.Name);
                authorname.InnerHtml += auth.ToString();
                author.InnerHtml += authorname.ToString();
                picture.InnerHtml += image.ToString();
                hName.InnerHtml += picture.ToString();
                hName.InnerHtml += author.ToString();

                buttonsdiv.InnerHtml += link.ToString();
                buttonsdiv.InnerHtml += answerLink.ToString();
                text.InnerHtml += buttonsdiv.ToString();

                divMediaBody.InnerHtml = hName + Environment.NewLine + quote + input + text;

            }
            return new MvcHtmlString(divMediaLeft + Environment.NewLine + divMediaBody);
        }
    }
}