﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using GameStore.ViewModels.ViewModels.Comment;

namespace GameStore.Helpers
{
    public static class AdminCommentHelper
    {
        public static MvcHtmlString RenderComments(this HtmlHelper helper,
            ICollection<CommentViewModel> comments, string key, int depth = -1)
        {
            depth++;

            var div = new TagBuilder("div");

            var media = new TagBuilder("div");
            media.AddCssClass("media col-md-offset-" + depth);

            foreach (var item in comments)
            {
                if (item != null)
                {
                    item.GameKey = key;
                    media.InnerHtml = RenderComment(helper, item).ToHtmlString();

                    div.InnerHtml += media.ToString();

                    if (item.Answers.Any())
                    {
                        media.InnerHtml = RenderComments(helper, item.Answers, key, depth).ToHtmlString();
                        div.InnerHtml += media.ToString();
                    }
                }
            }

            return new MvcHtmlString(div.ToString());
        }

        public static MvcHtmlString RenderComment(this HtmlHelper helper, CommentViewModel comment)
        {
            TagBuilder quote = null;

            var divMediaLeft = new TagBuilder("div");
            divMediaLeft.AddCssClass("media-left");
            var divMediaBody = new TagBuilder("div");
            divMediaBody.AddCssClass("panel panel-primary");

            var hName = new TagBuilder("h4");
            hName.AddCssClass("panel-heading");
            var input = new TagBuilder("input");

            var paragraph = new TagBuilder("p");
            var hiddeninput = new TagBuilder("input");

            var deletelink = new TagBuilder("a");
            deletelink.SetInnerText("Delete");

            var testLink = new TagBuilder("button");
            testLink.SetInnerText("Delete");

            var updatelink = new TagBuilder("a");
            updatelink.SetInnerText("Update");
            var banlink = new TagBuilder("a");
            banlink.SetInnerText("Ban");
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);

            if (comment != null)
            {
                paragraph.SetInnerText(comment.Body);
                input.MergeAttribute("value", comment.Id.ToString());
                input.MergeAttribute("type", "hidden");
                hiddeninput.MergeAttribute("type", "hidden");
                hiddeninput.MergeAttribute("value",comment.GameKey);

                var urlDelete = urlHelper.Action("Delete", "Comment", new { id = comment.Id, key = comment.GameKey });
                deletelink.MergeAttribute("href", urlDelete);

                testLink.MergeAttribute("class" ,"btn btn-info btn-lg delcomment");
                testLink.MergeAttribute("data-toggle" , "modal");
                testLink.MergeAttribute("data-target", "#DelCommentModal");
                testLink.MergeAttribute("id", "delcomment");

                var urlUpdate = urlHelper.Action("Update", "Comment", new { id = comment.Id});
                updatelink.MergeAttribute("href", urlUpdate);
                var urlBan = urlHelper.Action("BanUser", "User", new {email = comment.Name});
                banlink.MergeAttribute("href" , urlBan);

                if (comment.Quote != null)
                {
                    quote = new TagBuilder("p");
                    quote.MergeAttribute("id", "QuoteId");
                    quote.AddCssClass("quoteclass");

                    quote.SetInnerText(comment.Quote);
                }

                hName.InnerHtml = comment.Name;

                divMediaBody.InnerHtml = hName + Environment.NewLine + quote + paragraph + hiddeninput+ input + testLink + updatelink + banlink;

            }
            return new MvcHtmlString(divMediaLeft + Environment.NewLine + divMediaBody);
        }
    }
}