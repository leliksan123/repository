<Query Kind="Program" />

public void Main()
{
string filename = Path.Combine(Server.MapPath("~/MultiThreading"), "MultiThreading.txt");
var data = File.ReadAllLines(filename);
var TotalChars = 0;
var charRecurrence = new Dictionary<char, int>();
	
	foreach(var line in data)
	{
			TotalChars+= line.Length;
	}
	
	var timerNotParallel = new Stopwatch();
	timerNotParallel.Start();
	var NotParallel= ReadFileSingleThread(data , charRecurrence);
	timerNotParallel.Stop();
	  
	var timerParallelTasks = new Stopwatch();
	timerParallelTasks.Start();
	var ParallelTasks= ReadFileMultiThread(data , charRecurrence);
	timerParallelTasks.Stop();
	  
	var timerParallel = new Stopwatch();
	timerParallel.Start();
	var Parallel = ReadFileMultiThreadParallel(data , charRecurrence);
	timerParallel.Stop();

    foreach (var character in NotParallel)
        {
            Console.WriteLine("{0,5}{1,15:0}{2,25:0.00}", character.Key, character.Value,
                    ((decimal)character.Value / TotalChars)*100);
        }

  Console.WriteLine("Single Thread: {0} seconds", timerNotParallel.TotalSeconds);
  Console.WriteLine("Multithreading with Tasks: {0} seconds", ParallelTasks.TotalSeconds);
	Console.WriteLine("Multithreading with Parallel: {0} seconds", timerParallel.TotalSeconds);
	Console.WriteLine(TotalChars);
}

private Dictionary<char, int> ReadFileSingleThread(string[] data ,Dictionary<char, int> charRecurrence)
{
    data.ToList().ForEach(line => CharactersIterate(charRecurrence,line));
	
	return charRecurrence;			
}

private Dictionary<char, int> ReadFileMultiThread(string[] data ,Dictionary<char, int> charRecurrence)
{

	foreach(var item in data)
	{
	System.Threading.Tasks.Task.Factory.StartNew(() => CharactersIterate(charRecurrence,item));
	}

	return charRecurrence;
}

private Dictionary<char, int> ReadFileMultiThreadParallel(string[] data, Dictionary<char, int> charRecurrence)
{
    System.Threading.Tasks.Parallel.ForEach(data,
                line => CharactersIterate(charRecurrence,line));
					
    return charRecurrence;
}

private void CharactersIterate(Dictionary<char, int> charRecurrence, string line)
{
	line.ToCharArray().ToList().ForEach(c =>
                        {
						if(c != ' ')
						{
						CalculateOccurrence(charRecurrence, char.ToLower(c));
						}
                                
                        });
}

private void CalculateOccurrence(Dictionary<char, int> charRecurrence, char character)
{
    if (charRecurrence.Keys.Contains(character))
        {
            charRecurrence[character]++;
        }
    else
        {
            charRecurrence.Add(character, 1);
        }           
}
