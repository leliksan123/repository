﻿using System.Web.Mvc;
using GameStore.Tools.Logger;

namespace GameStore.Filters
{
    public class IPLogFilter : ActionFilterAttribute
    {
        private readonly ILoggerFileService _loggingService;

        public IPLogFilter(ILoggerFileService loggingService)
        {
            _loggingService = loggingService;
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            _loggingService.Info($"User with IP {filterContext.HttpContext.Request.UserHostAddress} connected on site");
        }
    }
}