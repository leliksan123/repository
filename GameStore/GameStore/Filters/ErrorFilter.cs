﻿using System;
using System.Reflection;
using System.Web.Mvc;
using GameStore.Tools.Logger;

namespace GameStore.Filters
{
    public class ErrorFilter : FilterAttribute, IExceptionFilter
    {
        private readonly ILoggerFileService _loggingService;

        public ErrorFilter(ILoggerFileService loggingService)
        {
            _loggingService = loggingService;
        }

        public void OnException(ExceptionContext filterContext)
        {
            var controllerName = (string) filterContext.RouteData.Values["controller"];
            var actionName = (string) filterContext.RouteData.Values["action"];
            var exception = filterContext.Exception.StackTrace;
            var appDomain = Assembly.GetExecutingAssembly().GetName().Name;

            _loggingService.Error(
                $"Controller {controllerName}  in Action {actionName} generated an error: {exception} in Application {appDomain}");

            if (!filterContext.ExceptionHandled)
            {
                var model = new Models.HandleErrorInfo
                {
                    Message = filterContext.Exception.Message,
                    StackTrace = filterContext.Exception.StackTrace
                };

                var viewDictionary = new ViewDataDictionary()
                {
                    Model = model
                };

                filterContext.Result = new ViewResult
                {
                     ViewName = "Error",
                     ViewData = viewDictionary
                };

                if (filterContext.Exception is ArgumentException)
                {
                    filterContext.Result = new ViewResult
                    {
                        ViewName = "Error",
                        ViewData = viewDictionary
                    };
                }
                
                filterContext.ExceptionHandled = true;
            }
        }
    }
}