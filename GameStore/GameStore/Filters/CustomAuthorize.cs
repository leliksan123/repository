﻿using System;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Security;
using AutoMapper;
using GameStore.Authorization.Infrastructure;
using Ninject;

namespace GameStore.Filters
{
    public class CustomAuthorize : AuthorizeAttribute
    {
        private IPrincipal _currentUser;

        [Inject]
        public IMapper Mapper { get; set; }

        public override bool AllowMultiple => false;

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            string token = null;

            try
            {
                var authHeaderValue = actionContext.Request.Headers.GetValues("Authorization");
                token = authHeaderValue.FirstOrDefault();
            }
            catch (Exception)
            {
                FormsAuthentication.SignOut();
            }
            
            if (token !=null)
            {
                try
                {
                    var ticket = FormsAuthentication.Decrypt(token);
                    _currentUser = new UserProvider(ticket.Name, Mapper);
                    HttpContext.Current.User = _currentUser;
                }
                catch (Exception)
                {
                    FormsAuthentication.SignOut();
                }
            }

            base.OnAuthorization(actionContext);
        }
    }
}