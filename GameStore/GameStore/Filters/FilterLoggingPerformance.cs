﻿using System.Diagnostics;
using System.Web.Mvc;
using GameStore.Tools.Logger;

namespace GameStore.Filters
{
    public class FilterLoggingPerformance : ActionFilterAttribute
    {
        private readonly ILoggerFileService _loggingService;
        private readonly Stopwatch _sw = new Stopwatch();

        public FilterLoggingPerformance(ILoggerFileService loggingService)
        {
            _loggingService = loggingService;
        }

        public override void OnResultExecuting(ResultExecutingContext context)
        {
            _loggingService.Info($"Action {context.RouteData.Values["action"]} Started working");
            _sw.Start();
        }

        public override void OnResultExecuted(ResultExecutedContext context)
        {
            _sw.Stop();
            _loggingService.Info(
                $"Action {context.RouteData.Values["action"]} Worked {_sw.Elapsed.Milliseconds} milliseconds");
        }
    }
}