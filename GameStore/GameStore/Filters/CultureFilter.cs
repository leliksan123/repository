﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace GameStore.Filters
{
    public class CultureFilter : ActionFilterAttribute
    {
        private const string CultureEn = "en";
        private const string CultureRus = "ru";
        private const string Cookieparam = "lang";

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var cultureCookie = filterContext.HttpContext.Request.Cookies[Cookieparam];

            var routeLang = filterContext.RouteData.Values[Cookieparam].ToString();

            if (cultureCookie == null)
            {
                cultureCookie = new HttpCookie(Cookieparam)
                {
                    HttpOnly = false,
                    Value = CultureEn,
                    Expires = DateTime.UtcNow.AddDays(1)
                };
                filterContext.HttpContext.Response.Cookies.Add(cultureCookie);
            }
            else
            {
                if (cultureCookie.Value != routeLang)
                {
                    cultureCookie.Value = routeLang;
                    cultureCookie.Expires = DateTime.UtcNow.AddDays(1);
                    filterContext.HttpContext.Response.Cookies.Add(cultureCookie);
                }
            }

            var cultureName = cultureCookie.Value;

            var cultures = new List<string> { CultureEn, CultureRus };

            if (!cultures.Contains(cultureName))
            {
                cultureName = CultureEn;
            }

            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(cultureName);
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(cultureName);
        }
    }
}