﻿using System.Web.Mvc;

namespace GameStore.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName => "Admin";

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Details",
                "Admin/{lang}/game/{key}",
                new { area = "Admin", controller = "Game", action = "Details" },
                new { lang = @"ru|en" } ,
                new[] { "GameStore.Areas.Admin.Controllers" }
            );

            context.MapRoute(
                "NewGame",
                "Admin/{lang}/games/{action}",
                new { area = "Admin", controller = "Game" },
                new { lang = @"ru|en" },
                new[] { "GameStore.Areas.Admin.Controllers" }
            );

            context.MapRoute(
                "Comments",
                "Admin/{lang}/game/{key}/{action}",
                new { area = "Admin", controller = "Comment" },
                new { lang = @"ru|en" },
                new[] { "GameStore.Areas.Admin.Controllers" }
            );

            context.MapRoute(
                "Publisher",
                "Admin/{lang}/publisher/{action}/{name}",
                new { area = "Admin", controller = "Publisher", name= UrlParameter.Optional },
                new { lang = @"ru|en" },
                new[] { "GameStore.Areas.Admin.Controllers" }
            );

            context.MapRoute(
              "OrderActions",
              "Admin/{lang}/Orders/{action}",
              new { area = "Admin", controller = "Order", action = "Index" },
              new { lang = @"ru|en" },
              new[] { "GameStore.Areas.Admin.Controllers" }
          );

            context.MapRoute(
                "deleteComment",
                "Admin/{lang}/Comment/Delete/{id}/{key}",
                new { area = "Admin", controller = "Comment", action = "Delete" , id= UrlParameter.Optional , key=UrlParameter.Optional },
                new { lang = @"ru|en" },
                new[] { "GameStore.Areas.Admin.Controllers" }
            );

            context.MapRoute(
                "BanUser",
                "Admin/{lang}/User/BanUser/{id}",
                new { area = "Admin", controller = "User", action = "BanUser", id = UrlParameter.Optional },
                new { lang = @"ru|en" },
                new[] { "GameStore.Areas.Admin.Controllers" }
            );

            context.MapRoute(
                "AdminLang",
                "Admin/{lang}",
                new { area = "Admin", controller = "AdminPanel", action = "Index" },
                new { lang = @"ru|en" },
                new[] { "GameStore.Areas.Admin.Controllers" }
            );

            context.MapRoute(
                "Admin_default",
                "Admin/{lang}/{controller}/{action}/{id}",
                new { area = "Admin", controller = "AdminPanel", action = "Index", id = UrlParameter.Optional },
                new { lang = @"ru|en" },
                new[] { "GameStore.Areas.Admin.Controllers" }
            );
        }
    }
}