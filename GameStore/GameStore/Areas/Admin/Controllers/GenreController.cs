﻿using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using GameStore.Authorization.Infrastructure;
using GameStore.Controllers;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.ViewModels.ViewModels.Genre;

namespace GameStore.Areas.Admin.Controllers
{
    [Authorize(Roles = "Manager,Administrator")]
    public class GenreController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly IGenreService _genreService;

        public GenreController(IAuthentication authentication, IMapper mapper, IGenreService genreService) : base(authentication)
        {
            _mapper = mapper;
            _genreService = genreService;
        }

        public ActionResult Index()
        {
            var model = new GenreListViewModel
            {
                Genres = _mapper.Map<List<GenreViewModel>>(_genreService.GetAllGenres())
            };
            return View(model);
        }

        [HttpGet]
        public ActionResult New()
        {
            var model = new GenreViewModel {Genres = _mapper.Map<List<GenreViewModel>>(_genreService.GetAllGenres())};
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult New(GenreViewModel model)
        {
            ActionResult result;

            if (ModelState.IsValid)
            {
                var genreDto = _mapper.Map<GenreDto>(model);
                _genreService.Create(genreDto);
                result = RedirectToAction("Index", "Genre");
            }
            else
            {
                result = View(model);
            }

            return result;
        }

        public ActionResult Delete(string name)
        {
            _genreService.Delete(name);
            return RedirectToAction("Index");
        }
    }
}