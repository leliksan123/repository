﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using GameStore.Authorization.Infrastructure;
using GameStore.Controllers;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.ViewModels.ViewModels.AdminPanel;
using GameStore.ViewModels.ViewModels.Role;
using GameStore.ViewModels.ViewModels.User;

namespace GameStore.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class UserController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IRoleService _roleService;

        public UserController(IAuthentication authentication, IMapper mapper, IUserService userService, IRoleService roleService) : base(authentication)
        {
            _mapper = mapper;
            _userService = userService;
            _roleService = roleService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var model = new AdminPanelHomeViewModel {Users = _mapper.Map<List<UserViewModel>>(_userService.GetAll())};
            return View(model);
        }

        public ActionResult Delete(string email)
        {
             _userService.Delete(email);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Update(string email)
        {
            var user = _userService.GetUserByEmail(email);
            var userViewModel = _mapper.Map<UserViewModel>(user);
            userViewModel.Roles = _mapper.Map<List<RoleViewModel>>(_roleService.GetAll());
            userViewModel.SelectedRoles = user.Roles.Select(x => x.RoleName).ToList();
            return View(userViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(UserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userDto = _mapper.Map<UserDTo>(model);
                _userService.Update(userDto);
                return RedirectToAction("Index");
            }

            return View("Update", model);
        }

        public ActionResult BanUser(string email)
        {
            _userService.BanUser(email);

            return RedirectToAction("Index");
        }
    }
}