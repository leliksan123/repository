﻿using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using GameStore.Authorization.Infrastructure;
using GameStore.Controllers;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.ViewModels.ViewModels.Role;

namespace GameStore.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class RoleController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly IRoleService _roleService;

        public RoleController(IAuthentication authentication, IMapper mapper, IRoleService roleService) : base(authentication)
        {
            _mapper = mapper;
            _roleService = roleService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var model = new RolesListViewModel {Roles = _mapper.Map<List<RoleViewModel>>(_roleService.GetAll())};
            return View(model);
        }


        [HttpGet]
        public ActionResult Create()
        {
            var model = new RoleViewModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RoleViewModel model)
        {
            if (ModelState.IsValid)
            {
                var roleDto = _mapper.Map<RoleDTo>(model);
                _roleService.Create(roleDto);
                return RedirectToAction("Index");
            }

            return View("Create", model);
        }

        public ActionResult Delete(string name)
        {
            _roleService.Delete(name);
            return RedirectToAction("Index");
        }
    }
}