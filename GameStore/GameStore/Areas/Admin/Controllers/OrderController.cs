﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using GameStore.Authorization.Infrastructure;
using GameStore.Controllers;
using GameStore.Extensions.Enums;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.ViewModels.ViewModels.Game;
using GameStore.ViewModels.ViewModels.Order;

namespace GameStore.Areas.Admin.Controllers
{
    [Authorize(Roles = "Manager,Administrator")]
    public class OrderController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly IOrderService _orderService;
        private readonly IOrderDetailsService _orderDetailsService;
        private readonly IGameService _gameService;

        public OrderController(IAuthentication authentication, IMapper mapper, IOrderService orderService , IOrderDetailsService orderDetailsService , IGameService gameService) : base(authentication)
        {
            _mapper = mapper;
            _orderService = orderService;
            _orderDetailsService = orderDetailsService;
            _gameService = gameService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var date = DateTime.UtcNow.AddMonths(-1);

            var model = new OrdersHistoryViewModel
            {
                Orders = _mapper.Map<List<OrderViewModel>>(_orderService.GetOrdersHistory(date , OrderSortType.YoungOrders))
            };

            return View(model);
        }

        [HttpGet]
        public ActionResult History()
        {
            var date = DateTime.UtcNow.AddMonths(-1);

            var model = new OrdersHistoryViewModel
            {
                Orders = _mapper.Map<List<OrderViewModel>>(_orderService.GetOrdersHistory(date , OrderSortType.OldOrders))
            };

            return View(model);
        }

        [HttpGet]
        public ActionResult Update(Guid id)
        {
            var order = _orderService.GetOrderById(id);
            var orderViewModel = _mapper.Map<OrderViewModel>(order);
            return View(orderViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(OrderViewModel model)
        {
            if (ModelState.IsValid)
            {
                var orderDto = _mapper.Map<OrderDto>(model);
                _orderService.EditStatus(orderDto);
                return RedirectToAction("Index");
            }

            return View("Update", model);
        }

        [HttpGet]
        public ActionResult HistoryAction(OrdersHistoryViewModel model)
        {
            var ordersBetweenDates = _orderService.GetOrdersBetweenDates(model.DateFrom, model.DateTo);

            var viewModel = new OrdersHistoryViewModel
            {
                Orders = _mapper.Map<List<OrderViewModel>>(ordersBetweenDates)
            };

            return View("History", viewModel);
        }

        [HttpGet]
        public ActionResult DeleteOrderDetails(Guid id , Guid orderId)
        {
            _orderDetailsService.Delete(id);
            return RedirectToAction("Update", "Order" , new {id = orderId});
        }

        [HttpGet]
        public ActionResult AddItemToOrder(Guid id)
        {
            var model = new OrderDetailsViewModel
            {
                Games = _mapper.Map<List<GameViewModel>>(_gameService.GetAllGames()),
                OrderId = id
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddItemToOrder(OrderDetailsViewModel model)
        {
            if (ModelState.IsValid)
            {
                var orderDetailsDto = _mapper.Map<OrderDetailsDto>(model);
               _orderDetailsService.AddToOrder(orderDetailsDto);
                return RedirectToAction("Update", "Order", new { id = model.OrderId });
            }

            return RedirectToAction("Update", "Order", new { id = model.OrderId});
        }

        [HttpGet]
        public ActionResult UpdateDetails(Guid id)
        {
            var details = _orderDetailsService.GetDetailsById(id);
            var orderViewModel = _mapper.Map<OrderDetailsViewModel>(details);
            return View(orderViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateDetails(OrderDetailsViewModel model)
        {
            if (ModelState.IsValid)
            {
                var ordeDetailsrDto = _mapper.Map<OrderDetailsDto>(model);
                _orderDetailsService.Update(ordeDetailsrDto);
                return RedirectToAction("Update", new { id= model.OrderId});
            }

            return RedirectToAction("Update", new {id=  model.OrderId });
        }
    }
}