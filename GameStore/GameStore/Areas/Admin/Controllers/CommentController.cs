﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using GameStore.Authorization.Infrastructure;
using GameStore.Controllers;
using GameStore.Extensions.Enums;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.ViewModels.Models;
using GameStore.ViewModels.ViewModels.Comment;
using GameStore.ViewModels.ViewModels.Game;

namespace GameStore.Areas.Admin.Controllers
{
    [Authorize(Roles = "Moderator,Administrator")]
    public class CommentController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly ICommentService _commentService;
        private readonly IFilterService _filterService;

        public CommentController(IAuthentication authentication, IMapper mapper, ICommentService commentService , IFilterService filterService) : base(authentication)
        {
            _mapper = mapper;
            _commentService = commentService;
            _filterService = filterService;
        }

        [HttpGet]
        public ActionResult Index(ElementsPerPage? elementsPerPage, int page = 1)
        {
            var pagesSize = GetPagesSize(elementsPerPage,page);

            var gamesesModel = _mapper.Map<List<GameIndexViewModel>>(_filterService.Filter(new FilterDto(),
                     (page - 1) * pagesSize, pagesSize , true));

            var pageInfo = new PageInfo
            {
                PageNumber = page,
                PageSize = pagesSize,
                TotalItems = _filterService.GamesCount(new FilterDto(), true)
            };

            var model = new GamesListViewModel
            {
                SelectedElements = elementsPerPage,
                Games = gamesesModel,
                PageInfo = pageInfo
            };

            return View(model);
        }

        [HttpGet]
        public ActionResult Comments(string key)
        {
            var allCommentsForGame = _commentService.GetAllCommentsForGame(key);
            var comments = _mapper.Map<IEnumerable<CommentDto>, List<CommentViewModel>>(allCommentsForGame);

            var model = new CommentFormViewModel
            {
                Comments = comments,
                GameKey = key
            };

            return View(model);
        }

        [HttpGet]
        public ActionResult Delete(Guid id, string key)
        {
            _commentService.DeleteComment(id);
            return RedirectToAction("Comments", new { key });
        }

        [HttpGet]
        public ActionResult Update(Guid id)
        {
            var comment = _commentService.GetCommentById(id);
            var commentViewModel = _mapper.Map<CommentViewModel>(comment);
            return View(commentViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(CommentViewModel model)
        {
            if (ModelState.IsValid)
            {
                var commentDto = _mapper.Map<CommentDto>(model);
                _commentService.Update(commentDto);
                return RedirectToAction("Comments" , new {key = model.GameKey});
            }

            return View("Update", model);
        }

        private int GetPagesSize(ElementsPerPage? elementsPerPage , int page)
        {
            int pagesSize;

            if (elementsPerPage == ElementsPerPage.All)
            {
                pagesSize = _filterService.GamesCount(new FilterDto(), true);
            }
            else
            {
                pagesSize = elementsPerPage != null ? (int)elementsPerPage : (int)ElementsPerPage.Three;
            }
            return pagesSize;
        }
    }
}