﻿using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using GameStore.Authorization.Infrastructure;
using GameStore.Controllers;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.ViewModels.ViewModels.Publisher;

namespace GameStore.Areas.Admin.Controllers
{
    [Authorize(Roles = "Manager,Administrator")]
    public class PublisherController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly IPublisherService _publisherService;
        private readonly IPublisherTranslation _publisherTranslation;
        private readonly ILanguageService _languageService;
        private const string LangRus = "ru";
        private const string LangEn = "en";

        public PublisherController(IAuthentication authentication, IMapper mapper, IPublisherService publisherService , 
            IPublisherTranslation publisherTranslation, ILanguageService languageService) : base(authentication)
        {
            _mapper = mapper;
            _publisherService = publisherService;
            _publisherTranslation = publisherTranslation;
            _languageService = languageService;
        }

        public ActionResult Index()
        {
            var model = new PublishersListViewModel
            {
                Publishers = _mapper.Map<List<PublisherViewModel>>(_publisherService.GetAllPublishers())
            };
            return View(model);
        }

        [HttpGet]
        public ActionResult New()
        {
            var model = new PublisherViewModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult New(PublisherViewModel model)
        {
            ActionResult result;

            if (ModelState.IsValid)
            {
                var publisherDto = _mapper.Map<PublisherDto>(model);
                _publisherService.Create(publisherDto);
                result = RedirectToAction("Index", "Publisher");
            }
            else
            {
                result = View(model);
            }

            return result;
        }

        public ActionResult Delete(string companyName)
        {
            _publisherService.Delete(companyName);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Update(string companyName)
        {
            var publisher = _mapper.Map<PublisherViewModel>(_publisherService.GetPublisherByCompanyName(companyName));

            var languageRus = _languageService.GetLanguageByName(LangRus);
            var languageEn = _languageService.GetLanguageByName(LangEn);
            publisher.Description = _publisherTranslation.GetTranslation(publisher.Id, languageRus.Id)?.Description;
            publisher.DescriptionEnglish = _publisherTranslation.GetTranslation(publisher.Id, languageEn.Id)?.Description;

            return View(publisher);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(PublisherViewModel publisher)
        {
            ActionResult result;

            if (ModelState.IsValid)
            {
                var publisherDto = _mapper.Map<PublisherDto>(publisher);
                _publisherService.Update(publisherDto);
                result = RedirectToAction("Index");
            }
            else
            {
                result = RedirectToAction("Update", new { companyName = publisher.CompanyName });
            }

            return result;
        }
    }
}