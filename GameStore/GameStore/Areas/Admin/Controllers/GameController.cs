﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using GameStore.Authorization.Infrastructure;
using GameStore.Controllers;
using GameStore.Extensions.Enums;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.ViewModels.Models;
using GameStore.ViewModels.ViewModels.Game;
using GameStore.ViewModels.ViewModels.Genre;
using GameStore.ViewModels.ViewModels.PlatformType;
using GameStore.ViewModels.ViewModels.Publisher;

namespace GameStore.Areas.Admin.Controllers
{
    [Authorize(Roles = "Manager,Administrator")]
    public class GameController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly IGameService _gameService;
        private readonly IGenreService _genreService;
        private readonly IPlatformTypeService _platformTypeService;
        private readonly IPublisherService _publisherService;
        private readonly IGameTranslation _gameTranslation;
        private readonly IFilterService _filterService;
        private readonly ILanguageService _languageService;
        private const string LangRus = "ru";
        private const string LangEn = "en";

        public GameController(IAuthentication authentication, IMapper mapper, IGameService gameService , IGenreService genreService , 
            IPlatformTypeService platformTypeService , IPublisherService publisherService , IGameTranslation gameTranslation , 
            IFilterService filterService, ILanguageService languageService) : base(authentication)
        {
            _mapper = mapper;
            _gameService = gameService;
            _genreService = genreService;
            _platformTypeService = platformTypeService;
            _publisherService = publisherService;
            _gameTranslation = gameTranslation;
            _filterService = filterService;
            _languageService = languageService;
        }

        [HttpGet]
        public ActionResult Index(ElementsPerPage? elementsPerPage, int page = 1)
        {
            var pagesSize = GetPagesSize(elementsPerPage);

            var gamesesModel = _mapper.Map<List<GameIndexViewModel>>(_filterService.Filter(new FilterDto (),
                    (page - 1) * pagesSize, pagesSize));

            var pageInfo = new PageInfo
            {
                PageNumber = page,
                PageSize = pagesSize,
                TotalItems = _filterService.GamesCount(new FilterDto ())
            };

            var model = new GamesListViewModel
            {
                SelectedElements = elementsPerPage,
                Games = gamesesModel,
                PageInfo = pageInfo
            };

            return View(model);
        }

        [HttpGet]
        public ActionResult Details(string key)
        {
            var gameByKey = _gameService.GetGameByKey(key);
            var game = _mapper.Map<GameDto, CreateGameViewModel>(gameByKey);

            var namesList = game.Publishers.Select(publisher => publisher.CompanyName).ToList();

            var model = new GamePublisherViewModel
            {
                Game = game,
                Names = namesList
            };

            return View(model);

        }

        [HttpGet]
        public ActionResult Update(string key)
        {
            var game = _mapper.Map<GameUpdateViewModel>(_gameService.GetGameByKey(key));

            var languageRus = _languageService.GetLanguageByName(LangRus);
            var languageEn = _languageService.GetLanguageByName(LangEn);
            game.Description = _gameTranslation.GetTranslation(game.Id, languageRus.Id)?.Description;
            game.DescriptionEnglish = _gameTranslation.GetTranslation(game.Id, languageEn.Id)?.Description;

            game.Genres = _mapper.Map<List<GenreViewModel>>(_genreService.GetAllGenres());
            game.PlatformTypes = _mapper.Map<List<PlatformTypeViewModel>>(_platformTypeService.GetAllPlatformTypes());
            game.Publishers = _mapper.Map<List<PublisherViewModel>>(_publisherService.GetAllPublishers());

            return View(game);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(GameUpdateViewModel game)
        {
            ActionResult result;

            if (ModelState.IsValid)
            {
                if (game.File != null)
                {
                    var pic = Path.GetFileName(game.File.FileName);
                    var path = Path.Combine(Server.MapPath("~/Content/File"), pic);
                    game.File.SaveAs(path);
                    game.Picture = "/Content/File/" + pic;
                }

                var gameDto = _mapper.Map<GameDto>(game);
                _gameService.Update(gameDto);
                result = RedirectToAction("Index");
            }
            else
            {
                result = RedirectToAction("Update", new { key = game.Key });
            }

            return result;
        }

        [HttpGet]
        public ActionResult New()
        {
            var model = new CreateGameViewModel
            {
                Genres = _genreService.GetAllGenres().ToList(),
                PlatformTypes = _platformTypeService.GetAllPlatformTypes().ToList(),
                Publishers = _publisherService.GetAllPublishers().ToList()
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult New(CreateGameViewModel game)
        {
            ActionResult result;

            if (ModelState.IsValid)
            {
                if (game.File != null)
                {
                    var pic = Path.GetFileName(game.File.FileName);
                    var path = Path.Combine(Server.MapPath("~/Content/File"), pic);
                    game.File.SaveAs(path);
                    game.Picture = "/Content/File/" + pic;
                }

                var gameDto = _mapper.Map<GameDto>(game);
                _gameService.Create(gameDto);
                result = RedirectToAction("Index");
            }
            else
            {
                result = RedirectToAction("New");
            }

            return result;
        }

        [HttpGet]
        public ActionResult Remove(string key)
        {
            _gameService.Delete(key);
            return Content("The game has been removed");
        }

        private int GetPagesSize(ElementsPerPage? elementsPerPage)
        {
            int pagesSize;

            if (elementsPerPage == ElementsPerPage.All)
            {
                pagesSize = _filterService.GamesCount(new FilterDto());
            }
            else
            {
                pagesSize = elementsPerPage != null ? (int)elementsPerPage : (int)ElementsPerPage.Three;
            }
            return pagesSize;
        }
    }
}