﻿using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using GameStore.Authorization.Infrastructure;
using GameStore.Controllers;
using GameStore.Services.Interfaces;
using GameStore.ViewModels.ViewModels.AdminPanel;
using GameStore.ViewModels.ViewModels.User;

namespace GameStore.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrator,Manager,Moderator")]
    public class AdminPanelController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;

        public AdminPanelController(IAuthentication authentication, IMapper mapper, IUserService userService) : base(authentication)
        {
            _mapper = mapper;
            _userService = userService;
        }

        public ActionResult Index()
        {
            var model = new AdminPanelHomeViewModel
            {
                Users = _mapper.Map<List<UserViewModel>>(_userService.GetUsersregisterToday())
            };
            return View(model);
        }
    }
}