﻿using System.Web.Mvc;
using AutoMapper;
using GameStore.Authorization.Infrastructure;
using GameStore.Authorization.Models;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;

namespace GameStore.Controllers
{
    [AllowAnonymous]
    public class AccountController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;

        public AccountController(IMapper mapper , IUserService userService , IAuthentication authentication) : base(authentication)
        {
            _mapper = mapper;
            _userService = userService;
        }

        public ActionResult UserLogin()
        {
            var model = _mapper.Map<LoginModel>(CurrentUser);
            return PartialView(model);
        }

        public ActionResult Login()
        {
            var model = new LoginModel();
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var user = Authentication.Login(model.Email, model.Password, model.IsPersistent);

                if (user != null)
                {
                    return RedirectToAction("Index", "Game");
                }

                return RedirectToAction("Register", "Account");
            }
            return View(model);
        }

        public ActionResult Register()
        {
            var model = new RegisterModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                var userDto = _mapper.Map<UserDTo>(model);
                _userService.Create(userDto);
                Authentication.Login(model.Email, model.Password,true);
                return RedirectToAction("Index", "Game");
            }

            return View(model);
        }

        public ActionResult Logoff()
        {
            Authentication.LogOut();
            return RedirectToAction("Index", "Game");
        }

        public ActionResult SignIn()
        {
            var model = new LoginModel();
            return View(model);
        }
    }
}