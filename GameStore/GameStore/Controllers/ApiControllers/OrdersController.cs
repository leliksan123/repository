﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using GameStore.Filters;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.ViewModels.ViewModels.Order;

namespace GameStore.Controllers.ApiControllers
{
    public class OrdersController : ApiController
    {
        private readonly IMapper _mapper;
        private readonly IOrderService _orderService;

        public OrdersController(IMapper mapper, IOrderService orderService)
        {
            _mapper = mapper;
            _orderService = orderService;
        }

        [HttpGet]
        public HttpResponseMessage Get()
        {
            try
            {
                var orderDtos = _orderService.GetAllOrders();
                var orders = _mapper.Map<IEnumerable<OrderViewModel>>(orderDtos);
                return Request.CreateResponse(HttpStatusCode.OK, orders);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, ex.StackTrace);
            }
        }

        [HttpGet]
        public HttpResponseMessage Get(Guid key)
        {
            if (key == Guid.Empty)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            try
            {
                var orderDto = _orderService.GetOrderById(key);
                var order = _mapper.Map<OrderViewModel>(orderDto);
                return Request.CreateResponse(HttpStatusCode.OK, order);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.StackTrace);
            }
        }

        [HttpDelete]
        [CustomAuthorize(Roles = "Manager,Administrator")]
        public HttpResponseMessage Delete(Guid key)
        {
            if (key == Guid.Empty)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
                
            try
            {
                _orderService.Delete(key);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.StackTrace);
            }
        }

        [HttpPost]
        [CustomAuthorize(Roles = "Manager,Administrator")]
        public HttpResponseMessage Post(OrderViewModel model)
        {
            if (ModelState.IsValid)
            {
                var orderDto = _mapper.Map<OrderDto>(model);

                try
                {
                    _orderService.Create(orderDto);
                    return Request.CreateResponse(HttpStatusCode.Created);
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ex.StackTrace);
                }

            }

            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }

        [HttpPut]
        [CustomAuthorize(Roles = "Manager,Administrator")]
        public HttpResponseMessage Put(OrderViewModel model)
        {
            if (ModelState.IsValid)
            {
                var orderDto = _mapper.Map<OrderDto>(model);

                try
                {
                    _orderService.Update(orderDto);
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ex.StackTrace);
                }

            }

            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }
    }
}
