﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using GameStore.Extensions.Enums;
using GameStore.Filters;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.ViewModels.Models;
using GameStore.ViewModels.ViewModels.Game;
using GameStore.ViewModels.ViewModels.Genre;

namespace GameStore.Controllers.ApiControllers
{
    public class GamesController : ApiController
    {
        private readonly IGameService _gameService;
        private readonly IGenreService _genreService;
        private readonly IMapper _mapper;
        private readonly IFilterService _filterService;

        public GamesController(IGameService gameService, IMapper mapper, IGenreService genreService, IFilterService filterService)
        {
            _gameService = gameService;
            _mapper = mapper;
            _genreService = genreService;
            _filterService = filterService;
        }

        [HttpGet]
        public HttpResponseMessage Get([FromUri]FilterViewModel viewmodel)
        {
            try
            {
                var pagesSize = GetPagesSize(viewmodel.ElementsPerPage, viewmodel);

                var games =
                    _mapper.Map<List<GameViewModel>>(_filterService.Filter(_mapper.Map<FilterDto>(viewmodel),
                        (viewmodel.Page - 1) * pagesSize, pagesSize));

                var pageInfo = new PageInfo
                {
                    PageNumber = viewmodel.Page,
                    PageSize = pagesSize,
                    TotalItems = _filterService.GamesCount(_mapper.Map<FilterDto>(viewmodel))
                };

                var model = new ApiGamesViewModel()
                {
                    Games = games,
                    PageInfo = pageInfo,
                };

                return Request.CreateResponse(HttpStatusCode.OK, model);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, ex.StackTrace);
            }
        }

        [HttpGet]
        public HttpResponseMessage Get(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            try
            {
                var gameDto = _gameService.GetGameByKey(key);
                var game = _mapper.Map<GameViewModel>(gameDto);
                return Request.CreateResponse(HttpStatusCode.OK, game);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.StackTrace);
            }
        }

        [HttpDelete]
        [CustomAuthorize(Roles = "Manager,Administrator")]
        public HttpResponseMessage Delete(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            try
            {
                _gameService.Delete(key);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.StackTrace);
            }
        }

        [HttpPost]
        [CustomAuthorize(Roles = "Manager,Administrator")]
        public HttpResponseMessage Post(CreateGameViewModel model)
        {
            if (ModelState.IsValid)
            {
                var gameDto = _mapper.Map<GameDto>(model);

                try
                {
                    _gameService.Create(gameDto);
                    return Request.CreateResponse(HttpStatusCode.Created);
                }
                catch (Exception ex)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.StackTrace);
                }
            }

            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }

        [HttpPut]
        [CustomAuthorize(Roles = "Manager,Administrator")]
        public HttpResponseMessage Put(GameUpdateViewModel model)
        {
            if (ModelState.IsValid)
            {
                var gameDto = _mapper.Map<GameDto>(model);

                try
                {
                    _gameService.Update(gameDto);
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                catch (Exception ex)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.StackTrace);
                }
            }

            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }

        [HttpGet]
        [Route("api/games/{key}/genres")]
        public HttpResponseMessage GetGenresOfGame(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            try
            {
                var allGenresForGame = _genreService.GetGenresOfGame(key);
                var genres = _mapper.Map<IEnumerable<GenreDto>, List<GenreViewModel>>(allGenresForGame);
                return Request.CreateResponse(HttpStatusCode.OK, genres);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.StackTrace);
            }
        }

        [HttpGet]
        [Route("api/publishers/{companyName}/games")]
        public HttpResponseMessage GetGamesByPublisher(string companyName)
        {
            if (string.IsNullOrEmpty(companyName))
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            try
            {
                var allGamesForPublisher = _gameService.GetAllGamesForPublisher(companyName);
                var games = _mapper.Map<IEnumerable<GameDto>, List<GameViewModel>>(allGamesForPublisher);
                return Request.CreateResponse(HttpStatusCode.OK, games);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.StackTrace);
            }
        }

        private int GetPagesSize(ElementsPerPage? elementsPerPage, FilterViewModel viewmodel)
        {
            int pagesSize;

            if (elementsPerPage == ElementsPerPage.All)
            {
                pagesSize = _filterService.GamesCount(_mapper.Map<FilterDto>(viewmodel));
            }
            else
            {
                pagesSize = elementsPerPage != null ? (int)elementsPerPage : (int)ElementsPerPage.Three;
            }
            return pagesSize;
        }
    }
}
