﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using GameStore.Filters;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.ViewModels.ViewModels.Comment;

namespace GameStore.Controllers.ApiControllers
{
    public class CommentsController : ApiController
    {
        private readonly IMapper _mapper;
        private readonly ICommentService _commentService;

        public CommentsController(IMapper mapper, ICommentService commentService)
        {
            _mapper = mapper;
            _commentService = commentService;
        }

        [HttpDelete]
        [CustomAuthorize(Roles = "Moderator,Administrator")]
        public HttpResponseMessage Delete(Guid key)
        {
            if (key == Guid.Empty)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            try
            {
                _commentService.DeleteComment(key);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.StackTrace);
            }
        }

        [HttpPost]
        public HttpResponseMessage Post(CommentViewModel model)
        {
            if (ModelState.IsValid)
            {
                var commentDto = _mapper.Map<CommentDto>(model);

                try
                {
                    _commentService.Create(commentDto);
                    return Request.CreateResponse(HttpStatusCode.Created);
                }
                catch (Exception ex)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.StackTrace);
                }
            }

            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }

        [HttpPut]
        [CustomAuthorize(Roles = "Moderator,Administrator")]
        public HttpResponseMessage Put(CommentViewModel model)
        {
            if (ModelState.IsValid)
            {
                var commentDto = _mapper.Map<CommentDto>(model);

                try
                {
                    _commentService.Update(commentDto);
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                catch (Exception ex)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.StackTrace);
                }
            }

            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }

        [HttpGet]
        [Route("api/games/{key}/comments/{id}")]
        public HttpResponseMessage Get(string key, Guid id)
        {
            if (id == Guid.Empty)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            if (string.IsNullOrEmpty(key))
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            try
            {
                var commentDto = _commentService.GetCommentById(id);
                var comment = _mapper.Map<CommentViewModel>(commentDto);
                return Request.CreateResponse(HttpStatusCode.OK, comment);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.StackTrace);
            }
        }

        [HttpGet]
        [Route("api/games/{key}/comments")]
        public HttpResponseMessage Get(string key)
        {

            if (string.IsNullOrEmpty(key))
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            try
            {
                var commentDto = _commentService.GetAllCommentsForGame(key);
                var comment = _mapper.Map<List<ApiComment>>(commentDto);
                return Request.CreateResponse(HttpStatusCode.OK, comment);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.StackTrace);
            }
        }
    }
}
