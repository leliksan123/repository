﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using GameStore.Filters;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.ViewModels.ViewModels.Game;
using GameStore.ViewModels.ViewModels.Genre;

namespace GameStore.Controllers.ApiControllers
{
    public class GenresController : ApiController
    {
        private readonly IGenreService _genreService;
        private readonly IGameService _gameService;
        private readonly IMapper _mapper;

        public GenresController(IMapper mapper, IGenreService genreService, IGameService gameService)
        {
            _mapper = mapper;
            _genreService = genreService;
            _gameService = gameService;
        }

        [HttpGet]
        public HttpResponseMessage Get()
        {
            try
            {
                var genreDtos = _genreService.GetAllGenres();
                var genres = _mapper.Map<IEnumerable<GenreViewModel>>(genreDtos);
                return Request.CreateResponse(HttpStatusCode.OK, genres);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, ex.StackTrace);
            }
        }

        [HttpGet]
        public HttpResponseMessage Get(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            try
            {
                var genreDto = _genreService.GetByName(key);
                var genre = _mapper.Map<GenreViewModel>(genreDto);
                return Request.CreateResponse(HttpStatusCode.OK, genre);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.StackTrace);
            }
        }

        [HttpDelete]
        [CustomAuthorize(Roles = "Manager,Administrator")]
        public HttpResponseMessage Delete(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            try
            {
                _genreService.Delete(key);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.StackTrace);
            }
        }

        [HttpPost]
        [CustomAuthorize(Roles = "Manager,Administrator")]
        public HttpResponseMessage Post(GenreViewModel model)
        {
            if (ModelState.IsValid)
            {
                var genreDto = _mapper.Map<GenreDto>(model);

                try
                {
                    _genreService.Create(genreDto);
                    return Request.CreateResponse(HttpStatusCode.Created);
                }
                catch (Exception ex)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.StackTrace);
                }
            }

            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }

        [HttpPut]
        [CustomAuthorize(Roles = "Manager,Administrator")]
        public HttpResponseMessage Put(GenreViewModel model)
        {
            if (ModelState.IsValid)
            {
                var genreDto = _mapper.Map<GenreDto>(model);

                try
                {
                    _genreService.Update(genreDto);
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                catch (Exception ex)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.StackTrace);
                }
            }

            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }

        [HttpGet]
        [Route("api/genres/{name}/games")]
        public HttpResponseMessage GetGamesByGenre(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            try
            {
                var allGamesForGenre = _gameService.GetAllGamesForGenre(name);
                var games = _mapper.Map<IEnumerable<GameDto>, List<GameViewModel>>(allGamesForGenre);
                return Request.CreateResponse(HttpStatusCode.OK, games);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.StackTrace);
            }
        }
    }
}
