﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using GameStore.Filters;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.ViewModels.ViewModels.Publisher;

namespace GameStore.Controllers.ApiControllers
{
    public class PublishersController : ApiController
    {
        private readonly IPublisherService _publisherService;
        private readonly IMapper _mapper;

        public PublishersController(IPublisherService publisherService, IMapper mapper)
        {
            _publisherService = publisherService;
            _mapper = mapper;
        }

        [HttpGet]
        public HttpResponseMessage Get()
        {
            try
            {
                var publisherDtos = _publisherService.GetAllPublishers();
                var publishers = _mapper.Map<IEnumerable<PublisherViewModel>>(publisherDtos);
                return Request.CreateResponse(HttpStatusCode.OK, publishers);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, ex.StackTrace);
            }
        }

        [HttpGet]
        public HttpResponseMessage Get(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            try
            {
                var publisherDto = _publisherService.GetPublisherByCompanyName(key);
                var publisher = _mapper.Map<PublisherViewModel>(publisherDto);
                return Request.CreateResponse(HttpStatusCode.OK, publisher);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.StackTrace);
            }
        }

        [HttpDelete]
        [CustomAuthorize(Roles = "Manager,Administrator")]
        public HttpResponseMessage Delete(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            try
            {
                _publisherService.Delete(key);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.StackTrace);
            }
        }

        [HttpPost]
        [CustomAuthorize(Roles = "Manager,Administrator")]
        public HttpResponseMessage Post(PublisherViewModel model)
        {
            if (ModelState.IsValid)
            {
                var publisherDto = _mapper.Map<PublisherDto>(model);

                try
                {
                    _publisherService.Create(publisherDto);
                    return Request.CreateResponse(HttpStatusCode.Created);
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ex.StackTrace);
                }
            }

            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }

        [HttpPut]
        [CustomAuthorize(Roles = "Manager,Administrator")]
        public HttpResponseMessage Put(PublisherViewModel model)
        {
            if (ModelState.IsValid)
            {
                var publisherDto = _mapper.Map<PublisherDto>(model);

                try
                {
                    _publisherService.Update(publisherDto);
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ex.StackTrace);
                }
            }

            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }
    }
}
