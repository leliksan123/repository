﻿using System.Web.Mvc;
using AutoMapper;
using GameStore.Authorization.Infrastructure;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.ViewModels.ViewModels.Publisher;

namespace GameStore.Controllers
{
    [AllowAnonymous]
    public class PublisherController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly IPublisherService _publisherService;

        public PublisherController(IMapper mapper, IPublisherService publisherService , IAuthentication authentication) : base(authentication)
        {
            _mapper = mapper;
            _publisherService = publisherService;
        }

        [HttpGet]
        public ActionResult Details(string companyName)
        {
            if (companyName == null)
            {
                return HttpNotFound();
            }
                
            var publisherByCompanyName = _publisherService.GetPublisherByCompanyName(companyName);
            var publisher = _mapper.Map<PublisherDto, PublisherViewModel>(publisherByCompanyName);
            return View(publisher);
        }
    }
}