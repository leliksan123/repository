﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using AutoMapper;
using GameStore.Authorization.Infrastructure;
using GameStore.Extensions.Enums;
using GameStore.PaymentService;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.Services.Notifications;
using GameStore.Services.Strategy;
using GameStore.Tools.EmailSender;
using GameStore.ViewModels.ViewModels.Order;
using GameStore.ViewModels.ViewModels.Payment;
using PaymentInfo = GameStore.PaymentService.DataContracts.PaymentInfo;

namespace GameStore.Controllers
{
    [Authorize]
    public class OrderController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly IOrderDetailsService _orderDetailsService;
        private readonly IOrderService _orderService;
        private readonly IUserService _userService;
        private readonly IObservable _observable;

        public OrderController(IOrderService orderService, IMapper mapper, IOrderDetailsService orderDetailsService,
            IAuthentication authentication, IUserService userService, IObservable observable, IEmailSender emailSender) : base(authentication)
        {
            _orderService = orderService;
            _mapper = mapper;
            _orderDetailsService = orderDetailsService;
            _userService = userService;
            _observable = observable;
        }

        [HttpGet]
        public ActionResult MakeOrder()
        {
            var user = _userService.GetUserByEmail(HttpContext.User.Identity.Name);

            var order = new OrderViewModel
            {
                OrderDate = DateTime.UtcNow,
                CustomerId = user.Id
            };

            var orderDto = _mapper.Map<OrderDto>(order);
            _orderService.Create(orderDto);

            if (Request.Cookies["Game"] != null)
            {
                var list =
                    new JavaScriptSerializer().Deserialize<Dictionary<string, short>>(Request.Cookies["Game"].Value);

                _orderDetailsService.CreateOrderDetailsWithQuantity(list, user.Email);
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var orderDetailsDto = _orderDetailsService.GetOrderDetailsForOrder(_orderService.LastCustomersOrder(user.Id));
            Session.Add("Order", orderDetailsDto);
            var orderDetails = _mapper.Map<IEnumerable<OrderDetailsDto>, List<OrderDetailsViewModel>>(orderDetailsDto);
            return View(orderDetails);
        }

        public ActionResult CreateOrder(PaymentType paymentType)
        {
            if (Request.Cookies["Game"] != null)
            {
                var c = new HttpCookie("Game") {Expires = DateTime.Now.AddDays(-1)};
                Response.Cookies.Add(c);
            }

            var user = _userService.GetUserByEmail(HttpContext.User.Identity.Name);
            var sum = _orderDetailsService.OrderSum((List<OrderDetailsDto>) Session["Order"]);
            _orderService.Update(_orderService.LastCustomersOrder(user.Id));

            _observable.NotifyObservers();

            var orderdetails = new OrderInformViewModel
            {
                OrderDate = DateTime.UtcNow,
                UserId = user.Id,
                OrderId = _orderService.LastCustomersOrder(user.Id).Id,
                Sum = sum
            };

            return GetStrategy(paymentType, orderdetails);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreatePayment(ModelForVisa model)
        {
            if (ModelState.IsValid)
            {
                var paymentStatus = new PaymentServiceClient();

                var paymentInformation = _mapper.Map<PaymentInfo>(model);
                paymentInformation.FullName = string.Join(" ", model.CartHoldersName, model.CardHoldersSurName);

                try
                {
                    var status = await paymentStatus.PayAsync(paymentInformation);

                    return PartialView("CreatePayment", status.ToString());
                }
                catch (Exception)
                {
                    return PartialView("CreatePayment", PaymentStatus.Failed.ToString());
                }
            }

            return PartialView("CreatePayment", PaymentStatus.Failed.ToString());
        }

        private ActionResult GetStrategy(PaymentType paymentType, OrderInformViewModel orderdetails)
        {
            var provider = new PaymentStrategyProvider();
            var getStrategy = provider.GetStrategy(paymentType);
            var orderModel = getStrategy.Pay(_mapper.Map<OrderInformDto>(orderdetails));

            switch (orderModel.Type)
            {
                case PaymentType.Bank:
                    return File(orderModel.Path, "application/pdf");

                case PaymentType.Box:
                    return View("UserInformation", orderdetails);

                case PaymentType.VisaCard:
                    return View("VisaPage");
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}