﻿using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using GameStore.Authorization.Infrastructure;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.ViewModels.ViewModels.Comment;

namespace GameStore.Controllers
{
    [AllowAnonymous]
    public class CommentController : BaseController
    {
        private readonly ICommentService _commentService;
        private readonly IGameService _gameService;
        private readonly IMapper _mapper;

        public CommentController(ICommentService commentService, IMapper mapper, IGameService gameService , IAuthentication authentication) : base(authentication)
        {
            _commentService = commentService;
            _mapper = mapper;
            _gameService = gameService;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewComment(CommentFormViewModel model)
        {
            ActionResult result;

            if (ModelState.IsValid)
            {
                model.Comment.GameId = _gameService.GetGameByKey(model.GameKey).Id;
                var commentDto = _mapper.Map<CommentDto>(model.Comment);
                _commentService.Create(commentDto);
                result = RedirectToAction("Comments", new {model.GameKey});
            }
            else
            {
                result = RedirectToAction("Comments", new {model.GameKey});
            }

            return result;
        }

        [HttpGet]
        public ActionResult Comments(string key)
        {
            var allCommentsForGame = _commentService.GetAllCommentsForGame(key);
            var comments = _mapper.Map<IEnumerable<CommentDto>, List<CommentViewModel>>(allCommentsForGame);

            var model = new CommentFormViewModel
            {
                Comments = comments,
                GameKey = key 
            };

            return View(model);
        }
    }
}