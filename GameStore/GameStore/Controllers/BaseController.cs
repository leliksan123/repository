﻿using System.Web.Mvc;
using GameStore.Authorization.Infrastructure;
using GameStore.Services.DTO;

namespace GameStore.Controllers
{
    public class BaseController : Controller
    {
        protected readonly IAuthentication Authentication;

        public BaseController(IAuthentication authentication)
        {
            Authentication = authentication;
        }

        protected UserDTo CurrentUser => ((UserIndentity)Authentication.CurrentUser.Identity).User;
    }
}