﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.UI;
using AutoMapper;
using GameStore.Authorization.Infrastructure;
using GameStore.Extensions.Enums;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.ViewModels.Models;
using GameStore.ViewModels.ViewModels.Game;
using GameStore.ViewModels.ViewModels.Genre;
using GameStore.ViewModels.ViewModels.PlatformType;
using GameStore.ViewModels.ViewModels.Publisher;


namespace GameStore.Controllers
{
    [AllowAnonymous]
    public class GameController : BaseController
    {
        private readonly IFilterService _filterService;
        private readonly IGameService _gameService;
        private readonly IGenreService _genreService;
        private readonly IMapper _mapper;
        private readonly IPlatformTypeService _platformTypeService;
        private readonly IPublisherService _publisherService;

        public GameController(IGameService gameService, IMapper mapper, IGenreService genreService,
            IPlatformTypeService platformTypeService, IPublisherService publisherService, IFilterService filterService,IAuthentication authentication) : base(authentication)
        {
            _gameService = gameService;
            _mapper = mapper;
            _genreService = genreService;
            _platformTypeService = platformTypeService;
            _publisherService = publisherService;
            _filterService = filterService;
        }

        [HttpGet]
        public ActionResult Index(ElementsPerPage? elementsPerPage, FilterViewModel viewmodel, int page = 1)
        {
            var pagesSize = GetPagesSize(elementsPerPage, viewmodel);

            var gamesesModel =
                _mapper.Map<List<GameIndexViewModel>>(_filterService.Filter(_mapper.Map<FilterDto>(viewmodel),
                    (page - 1)*pagesSize, pagesSize));

            var pageInfo = new PageInfo
            {
                PageNumber = page,
                PageSize = pagesSize,
                TotalItems = _filterService.GamesCount(_mapper.Map<FilterDto>(viewmodel))
            };

            var model = new GamesListViewModel
            {
                SelectedElements = elementsPerPage,
                Genres = _mapper.Map<IEnumerable<GenreDto>, List<GenreViewModel>>(_genreService.GetAllGenres()),
                Publishers =
                    _mapper.Map<IEnumerable<PublisherDto>, List<PublisherViewModel>>(
                        _publisherService.GetAllPublishers()),
                PlatformTypes =
                    _mapper.Map<IEnumerable<PlatformTypeDto>, List<PlatformTypeViewModel>>(
                        _platformTypeService.GetAllPlatformTypes()),
                Games = gamesesModel,
                PageInfo = pageInfo,
                Filter = viewmodel
            };

            return View(model);
        }

        [HttpGet]
        [OutputCache(Duration = 60)]
        public ActionResult GamesNumber()
        {
            var games = _gameService.GetAllGames().Count();
            return Content($"{games}");
        }

        [HttpGet]
        public ActionResult Details(string key)
        {
            var gameByKey = _gameService.GetGameByKey(key);
            var game = _mapper.Map<GameDto, CreateGameViewModel>(gameByKey);

            var namesList = game.Publishers.Select(publisher => publisher.CompanyName).ToList();

            var model = new GamePublisherViewModel
            {
                Game = game,
                Names = namesList
            };

            return View(model);

        }

        [HttpGet]
        [OutputCache(Duration = 60, Location = OutputCacheLocation.Server)]
        public ActionResult GetGamesByGenre(string name)
        {
            var allGamesForGenre = _gameService.GetAllGamesForGenre(name);
            var game = _mapper.Map<IEnumerable<GameDto>, List<CreateGameViewModel>>(allGamesForGenre);
            return Json(game, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OutputCache(Duration = 60, Location = OutputCacheLocation.Server)]
        public ActionResult GetGamesByPlatformType(string type)
        {
            var allGamesForPlatformType = _gameService.GetAllGamesForPlatformType(type);
            var game = _mapper.Map<IEnumerable<GameDto>, List<CreateGameViewModel>>(allGamesForPlatformType);
            return Json(game, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OutputCache(Duration = 60, Location = OutputCacheLocation.Server)]
        public ActionResult Download(string key)
        {
            var path = Path.Combine(Server.MapPath("~/Content/File"), "GameInfo.txt");
            var gameByKey = _gameService.GetGameByKey(key);
            _gameService.WriteToFile(path, _mapper.Map<GameDto, CreateGameViewModel>(gameByKey).Name);
            return File(path, "application/txt", "GameInfo.txt");
        }

        [HttpGet]
        public ActionResult Buy(string key)
        {
            var game = _gameService.GetGameByKey(key);

            if (game.UnitsInStock == 0)
            {
                return Content("Sorry, game is not available now");
            }

            var listGames = new Dictionary<string, short>();

            HttpCookie cookie;

            if (Request.Cookies["Game"] != null)
            {
                cookie = Request.Cookies["Game"];

                listGames = new JavaScriptSerializer().Deserialize<Dictionary<string, short>>(cookie.Value);

                if (listGames.ContainsKey(key))
                {
                    listGames[key]++;
                }
                else
                {
                    listGames.Add(key, 1);
                }
            }
            else
            {
                cookie = new HttpCookie("Game");

                listGames.Add(key, 1);
            }

            cookie.Value = new JavaScriptSerializer().Serialize(listGames);
            cookie.Expires = DateTime.UtcNow.AddHours(10);

            Response.AppendCookie(cookie);

            return RedirectToAction("Details" ,new { key });
        }

        public ActionResult ReturnFile(string key)
        {
            var fileBytes =  _gameService.GetImage(key);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet);
        }

        public async Task<ActionResult> ReturnFileAsync(string key)
        {
            var fileBytes = await Task.Run(()=>_gameService.GetImage(key));
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet);
        }

        private int GetPagesSize(ElementsPerPage? elementsPerPage, FilterViewModel viewmodel)
        {
            int pagesSize;

            if (elementsPerPage == ElementsPerPage.All)
            {
                pagesSize = _filterService.GamesCount(_mapper.Map<FilterDto>(viewmodel));
            }
            else
            {
                pagesSize = elementsPerPage != null ? (int)elementsPerPage : (int)ElementsPerPage.Three;
            }
            return pagesSize;
        }
    }
}