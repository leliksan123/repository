﻿using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using AutoMapper;
using GameStore.Authorization.Infrastructure;
using GameStore.Services.DTO;
using GameStore.Services.Interfaces;
using GameStore.ViewModels.ViewModels.Game;

namespace GameStore.Controllers
{
    [Authorize]
    public class BasketController : BaseController
    {
        private readonly IGameService _gameService;

        private readonly IMapper _mapper;

        public BasketController(IMapper mapper, IGameService gameService , IAuthentication authentication) : base(authentication)
        {
            _mapper = mapper;
            _gameService = gameService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            decimal sum = 0;
            var viewModel = new List<GamesQuantityViewModel>();
            var ser = new JavaScriptSerializer();

            if (Request.Cookies["Game"] != null)
            {
                var list = ser.Deserialize<Dictionary<string, short>>(Request.Cookies["Game"].Value);

                foreach (var item in list)
                {
                    sum += _mapper.Map<GameDto, GameViewModel>(_gameService.GetGameByKey(item.Key)).Price*item.Value;
                    var model = new GamesQuantityViewModel
                    {
                        Game = _mapper.Map<GameDto, GameViewModel>(_gameService.GetGameByKey(item.Key)),
                        Quantity = item.Value,
                        Sum = sum 
                    };
                    viewModel.Add(model);
                }

                return View(viewModel);
            }

            return View(viewModel);
        }

        [HttpGet]
        public ActionResult CountItemsInBasket()
        {
            var ser = new JavaScriptSerializer();

            if (Request.Cookies["Game"] != null)
            {
                var list = ser.Deserialize<Dictionary<string, short>>(Request.Cookies["Game"].Value);
                return Content(list.Count.ToString());
            }

            return Content("0");
        }

        [HttpGet]
        public ActionResult Delete(string key)
        {
            ActionResult result;
            var ser = new JavaScriptSerializer();

            if (Request.Cookies["Game"] != null)
            {
                var list = ser.Deserialize<Dictionary<string, short>>(Request.Cookies["Game"].Value);

                for (var i = 0; i < list.Count; i++)
                {
                    if (list.ContainsKey(key))
                    {
                        list.Remove(key);
                    }
                }

                Request.Cookies["Game"].Value = ser.Serialize(list);
                Response.Cookies.Add(Request.Cookies["Game"]);
                result = RedirectToAction("Index");
            }
            else
            {
                result = new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return result;
        }

        [HttpGet]
        public ActionResult RenderSum()
        {
            decimal sum = 0;
            var ser = new JavaScriptSerializer();

            if (Request.Cookies["Game"] != null)
            {
                var list = ser.Deserialize<Dictionary<string, short>>(Request.Cookies["Game"].Value);

                foreach (var item in list)
                {
                    sum += _mapper.Map<GameDto, GameViewModel>(_gameService.GetGameByKey(item.Key)).Price * item.Value;
                }

                if (sum == 0)
                {
                    return Content(string.Empty);
                }

                return Content(sum.ToString(CultureInfo.InvariantCulture));
            }

            return Content(string.Empty);
        }
    }
}