﻿
$(document).on("click","#answerLink",function(e) {
            e.preventDefault();
            var input = $(this).parent().prev().prev().attr("value");
            document.getElementById("ParentCommentId").value = input;
        });


$(document).on("click","#quoteLink",function() {
            var inputText = $(this).parent().prev().prev().prev().text();
            $("#Quote").val(inputText);
            var input = $(this).parent().prev().prev().attr("value");
            document.getElementById("ParentCommentId").value = input;
        });


$("a[data-type='enLink']").click(function() {
        var href = window.location.href;

        if (href.indexOf("/ru/") === -1 && href.indexOf("/en/") === -1) {
            href = href + $(this).attr("href").replace("/", "");
        }

        var resultHref = href.replace(/\/en\/|\/ru\//g, $(this).attr("href"));
        $(this).attr("href", resultHref);
    });


$(document).on("click", ".deluser",function() {
            var userEmail = $(this).data("id");
            $(".modal-body #resultuser").text(userEmail);
            var resultHref = window.location.href + "/Delete?email=" + userEmail;
            $(".userdelete").attr("href", resultHref);
});

$("#Undo").click(function () {
    location.reload();
});

$('.delcomment').on('click', function () {

        var userLang = navigator.language || navigator.userLanguage;
        var input = $(this).prev().attr("value");
        var key = $(this).prev().prev().attr("value");

        var resultHref = "/Admin/" + userLang.substring(0, 2) + "/Comment/Delete?id=" + input + "&key=" + key;

        $(".commentDelete").attr("href",resultHref);

        var myModal = $("#DelCommentModal");
        clearTimeout(myModal.data('hideInterval'));

        myModal.data('hideInterval', setTimeout(function () {
            myModal.modal('hide');
        }, 5000));

        var count = 5;
        function updateTimer() {
            $("#contentHolder").fadeOut('slow', function () {
                $("#contentHolder").text(count);
                $("#contentHolder").fadeIn();
                count--;
            });
        }

        setInterval(function () { updateTimer() }, 1000);

        var searchTimer = setTimeout(function () {
            GetRequest(resultHref);
        }, 5000);
});

function GetRequest(resultHref) {
    $.get(resultHref)
            .done(function (data) {
                location.reload();
            });
}