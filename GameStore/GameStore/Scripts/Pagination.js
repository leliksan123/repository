﻿
var standartPageBlock = 5;

function generatePages(totalPages, currentPage, selector) {
    $(selector).empty();

    if (totalPages > standartPageBlock && currentPage >= standartPageBlock) {
        generateMultiplyPageBlock(totalPages, currentPage, selector);
    } else {
        generateSinglePageBlock(totalPages, currentPage, selector);
    }
};

function generateMultiplyPageBlock(totalPages, currentPage, selector) {
    var pageCount = 0;

    if (currentPage % 5 === 0) {
        pageCount = currentPage - 1;
    } else pageCount = currentPage - currentPage % 5;

    for (var i = pageCount; i <= (pageCount + standartPageBlock) && i <= totalPages; i++) {
        generateButtons(i, currentPage, selector);
    }
}

function generateSinglePageBlock(totalPages, currentPage, selector) {

    var pageCount = 0;

    if (currentPage < standartPageBlock && totalPages > standartPageBlock) {
        pageCount = standartPageBlock;
    } else pageCount = totalPages;

    for (var i = 1; i <= pageCount; i++) {

        generateButtons(i, currentPage, selector);
    }

}

function generateButtons(index, pageNumber, selector) {

    var a = $("<a>");

    if (index === pageNumber) {
        a.addClass("selected");
        a.addClass("btn-primary");
    }
    a.attr("href", "#");
    a.attr("data-type", "paging");
    a.text(index);
    a.addClass("btn");
    a.addClass("btn-default");
    $(selector).append(a);
}