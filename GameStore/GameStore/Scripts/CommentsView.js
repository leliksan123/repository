﻿$(function() {

    var gamekey = $("#gameKey").val();

    var render = function renderComments(comments, key) {

        for (var index in comments) {

            if (comments[index] != null) {

                comments[index].GameKey = key;
                renderComment(comments[index]);

                if (comments[index].Answers.length > 0) {

                    renderComments(comments[index].Answers, key);
                }
            }
        }
    };

    function renderComment(item) {

        if ("content" in document.createElement("template")) {

            var productrow = document.querySelector("#commentrow");
            var li = productrow.content.getElementById("userName");
            li.innerHTML = item.Name;

            var commentBody = productrow.content.getElementById("commentBody");
            commentBody.textContent = item.Body;
            var showCommentId = productrow.content.getElementById("showCommentId");
            showCommentId.value = item.Id;

            var showGameId = productrow.content.getElementById("showGameId");
            showGameId.value = item.GameKey;

            if (item.Quote != null) {
                var quote = productrow.content.getElementById("QuoteId");
                quote.style.display = "inline-block";
                quote.textContent = item.Quote;
            }

            var pricing = document.getElementById("commentsRen");
            var clone = document.importNode(productrow.content, true);
            pricing.appendChild(clone);

            return pricing.innerHTML;
        }
    }

    $("#getcomments")
        .click(function() {
            var url = "/api/games/" + gamekey + "/comments";
            $.get(url, gamekey)
                .done(function(data) {
                    if (data.length > 0) {
                        render(data, data[0].GameKey);
                    } else {
                        $("#noComments").html("This game doesnt have comments");
                    }
                    $("#add-comment").show();
                });
        });

    $("#commentFormPost")
        .submit(function(e) {

            var game = $("#Game").val();
            e.preventDefault();
            var url = "/api/comments/";
            $.post(url, $("#commentFormPost").serialize())
                .done(function(data) {
                    $("#commentsRen").empty();
                    $("#noComments").empty();
                    $("#Body").val("");
                    var url2 = "/api/games/" + game + "/comments";
                    $.get(url2, gamekey)
                        .done(function(data) {
                            render(data, data[0].GameKey);
                        });
                });
        });
});