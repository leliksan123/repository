﻿(function() {

    apiGetAllGames();
    var newlink = document.createElement("a");
    newlink.innerHTML = "Details";
    var userLang = navigator.language || navigator.userLanguage;

    var filter = function(data) {

        generatePages(data.PageInfo.TotalPages, data.PageInfo.PageNumber, ".pagination");

        if (data.Games.length > 0) {

            for (var index in data.Games) {

                if ("content" in document.createElement("template")) {

                    var productrow = document.querySelector("#productrow"),
                    li = productrow.content.querySelectorAll("li span");
                    li[0].textContent = data.Games[index].Name;
                    li[1].textContent = data.Games[index].Price;

                    var picture = productrow.content.getElementById("gamePicture");

                    if (data.Games[index].Picture != null) {
                        picture.src = data.Games[index].Picture;
                    } else {
                        picture.src = "/Content/File/gray.png";
                    }
                    
                    newlink.setAttribute("href", userLang.substring(0, 2) + "/game/" + data.Games[index].Key);

                    li[2].appendChild(newlink);

                    var pricing = document.getElementsByClassName("pricing");
                    var clone = document.importNode(productrow.content, true);
                    pricing[0].appendChild(clone);
                }
            }
        } else {
            $("#noGames").html("Games Not Found");
        }
    };

    function apiGetAllGames() {
        $.get("api/games", $("#filterForm").serialize())
            .done(function(data) {
                filter(data);
            });
    }

    $(".pagination")
        .on("click",
            "a[data-type='paging']",
            function(e) {
                e.preventDefault();
                if ($(this).parent().hasClass("selected"))
                    return;
                $("[name=ElementsPerPage]").val($("#ElementsOnThePage").val());
                $("[name=Page]").val($(this).text());
                $("#filterForm").submit();
            });

    $("#ElementsOnThePage")
        .change(function() {
            var selectedVal = $(this).val();
            $("[name=ElementsPerPage]").val(selectedVal);
            $("#filterForm").submit();
        });

    $("#filterForm")
        .submit(function(e) {
            $(".pricing").empty();
            e.preventDefault();
            $.get("api/games", $("#filterForm").serialize())
                .done(function(data) {
                    filter(data);
                });
        });

}());