﻿(function () {
    var defaultDropValue = 3;
    var queryString = window.location.search || "";
    var keyValPairs = [];
    var params = {};
    queryString = decodeURIComponent(queryString.substr(1).replace(/\+/g, " "));

    if (queryString.length) {
        keyValPairs = queryString.split("&");
        for (window.pairNum in keyValPairs) {
            if (keyValPairs.hasOwnProperty(window.pairNum)) {
                var key = keyValPairs[window.pairNum].split("=")[0];
                if (!key.length) continue;
                if (typeof params[key] === "undefined")
                    params[key] = [];
                params[key].push(keyValPairs[window.pairNum].split("=")[1]);
            }
        }
    }

    var dropdown = params["elementsPerPage"];

    if (!dropdown[0].length) {
        dropdown = defaultDropValue;
    }

    $("#ElementsOnThePage").val(dropdown);

    var genres = params["Genres"];

    if (genres != null) {
        for (var i = 0; i < genres.length; i++) {
            $(":checkbox[name=Genres][value=" + "'" + genres[i] + "']").prop("checked", "true");
        }
    }

    var publishers = params["Publishers"];

    if (publishers != null) {
        for (var j = 0; j < publishers.length; j++) {
            $(":checkbox[name=Publishers][value=" + "'" + publishers[j] + "']").prop("checked", "true");
        }
    }

    var platformTypes = params["PlatformTypes"];

    if (platformTypes != null) {
        for (var k = 0; k < platformTypes.length; k++) {
            $(":checkbox[name=PlatformTypes][value=" + "'" + platformTypes[k] + "']").prop("checked", "true");
        }
    }

    var parameters = params["Parameters"];

    if (parameters != null) {
        document.getElementById("#Parameters").value = parameters;
    }
}());
  