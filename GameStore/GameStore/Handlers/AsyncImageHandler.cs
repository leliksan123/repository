﻿using System.Threading.Tasks;
using System.Web;
using GameStore.Services.Interfaces;
using Ninject;

namespace GameStore.Handlers
{
    public class AsyncImageHandler : HttpTaskAsyncHandler
    {
        [Inject]
        private IGameService GameService { get; set; }

        public override async Task ProcessRequestAsync(HttpContext context)
        {
            var key = context.Request.Url.Segments[2];
            var picture = await Task.Run(() => GameService.GetImage(key));
            context.Response.ContentType = System.Net.Mime.MediaTypeNames.Application.Octet;
            context.Response.BinaryWrite(picture);
        }
    }
}