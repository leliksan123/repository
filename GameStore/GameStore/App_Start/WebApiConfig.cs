﻿using System.Web.Http;

namespace GameStore
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration configuration)
        {

            configuration.MapHttpAttributeRoutes();
            configuration.EnsureInitialized();

            configuration.Routes.MapHttpRoute(
                "DefaultApi",
                "api/{controller}/{key}",
                new {key= RouteParameter.Optional }
            );

            configuration.Routes.MapHttpRoute(
                "WithActionApi",
                "api/{controller}/{action}/{key}",
                new { key =RouteParameter.Optional }
                );
        }
    }
}