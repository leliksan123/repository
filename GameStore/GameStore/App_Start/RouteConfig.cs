﻿using System.Web.Mvc;
using System.Web.Routing;

namespace GameStore
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "DownloadGame",
                "{lang}/game/{key}/download",
                new { controller = "Game", action = "Download"},
                new {lang = @"ru|en"} ,
                new[] { "GameStore.Controllers" }
            );

            routes.MapRoute(
                "GameDetails",
                "{lang}/game/{key}",
                new { controller = "Game", action = "Details"},
                new { lang = @"ru|en" },
                new[] { "GameStore.Controllers" }
            );

            routes.MapRoute(
                "GameBuy",
                "{lang}/game/{key}/Buy",
                new { controller = "Game", action = "Buy"},
                new { lang = @"ru|en" },
                new[] { "GameStore.Controllers" }
            );

            routes.MapRoute(
                "GameComments",
                "{lang}/game/{key}/{action}",
                new { controller = "Comment"},
                new { lang = @"ru|en" },
                new[] { "GameStore.Controllers" }
            );

            routes.MapRoute(
                "Games",
                "{lang}/Games/{action}",
                new { controller = "Game", action = "Index"},
                new { lang = @"ru|en" },
                new[] { "GameStore.Controllers" }
            );

            routes.MapRoute(
                "Registration",
                "{lang}/Account/{action}",
                new { controller = "Account" },
                new { lang = @"ru|en" },
                new[] { "GameStore.Controllers" }
            );

            routes.MapRoute(
                "Basket",
                "{lang}/basket/{action}/{id}",
                new { controller = "Basket", action = "Index", id = UrlParameter.Optional},
                new { lang = @"ru|en" },
                new[] { "GameStore.Controllers" }
            );

            routes.MapRoute(
                 "AdminPublisherDetails",
                 "{lang}/publisher/{name}",
                 new { controller = "Publisher", action = "Details" },
                 new { lang = @"ru|en" },
                 new[] { "GameStore.Controllers" }
             );

            routes.MapRoute(
                "OrderAction",
                "{lang}/Orders/{action}",
                new {controller = "Order", action = "Index"},
                new { lang = @"ru|en" },
                new[] { "GameStore.Controllers" }
            );

            routes.MapRoute(
               "Default",
               "{lang}/{controller}/{action}",
               new { lang ="en",controller = "Game", action = "Index"},
               new { lang = @"ru|en" },
               new[] { "GameStore.Controllers" }
            );
        }
    }
}