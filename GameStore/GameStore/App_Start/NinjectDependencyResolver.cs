﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using GameStore.Authorization.Infrastructure;
using GameStore.Filters;
using GameStore.MapperConfiguration.Mapping;
using GameStore.Services.Infrastructure;
using GameStore.Services.Interfaces;
using GameStore.Services.Notifications;
using GameStore.Services.Services;
using GameStore.Tools.EmailSender;
using GameStore.Tools.Logger;
using Ninject;
using Ninject.Web.Mvc.FilterBindingSyntax;

namespace GameStore
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private readonly IKernel _kernel;

        public NinjectDependencyResolver(IKernel kernelParam)
        {
            _kernel = kernelParam;
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return _kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _kernel.GetAll(serviceType);
        }

        private void AddBindings()
        {
            _kernel.Bind<IGameService>().To<GameService>();
            _kernel.Bind<ICommentService>().To<CommentService>();
            _kernel.Bind<IValidationService>().To<ValidationService>();
            _kernel.Bind<IGenreService>().To<GenreService>();
            _kernel.Bind<IPlatformTypeService>().To<PlatformTypeService>();
            _kernel.Bind<IPublisherService>().To<PublisherService>();
            _kernel.Bind<IOrderDetailsService>().To<OrderDetailsService>();
            _kernel.Bind<IOrderService>().To<OrderService>();
            _kernel.Bind<IUserService>().To<UserService>();
            _kernel.Bind<IShipperService>().To<ShipperService>();
            _kernel.Bind<ILoggerMongoService>().To<MongoLogger>();
            _kernel.Bind<ILoggerFileService>().To<LoggingService>();
            _kernel.Bind<IFilterService>().To<FilterService>();
            _kernel.Bind<ILanguageService>().To<LanguageService>();
            _kernel.Bind<IGameTranslation>().To<GameTranslationService>();
            _kernel.Bind<IPublisherTranslation>().To<PublisherTranslationService>();
            _kernel.Bind<IAuthentication>().To<CustomAuthentication>();
            _kernel.Bind<IRoleService>().To<RoleService>();
            _kernel.Bind<IObservable>().To<NotificationObservable>();
            _kernel.Bind<IEmailSender>().To<EmailSender>();

            _kernel.BindFilter<IPLogFilter>(FilterScope.Global, 0);
            _kernel.BindFilter<ErrorFilter>(FilterScope.Global, 1);
            _kernel.BindFilter<FilterLoggingPerformance>(FilterScope.Global, 2);
            _kernel.BindFilter<CultureFilter>(FilterScope.Global, 3);

            var config = new Config();
            var mapper = config.Configure().CreateMapper();
            _kernel.Bind<IMapper>().ToConstant(mapper).InSingletonScope();

            NinjectResolver.Configure(_kernel);
        }
    }
}