﻿using System;
using System.Collections.Generic;
using AutoMapper;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Mongo;
using Publisher = GameStore.DAL.Entities.Publisher;

namespace GameStore.MapperConfiguration.Mapping
{
    public class MappingProfileDal : Profile
    {
        public MappingProfileDal()
        {
            CreateMap<Supplier, Publisher>().ForMember(dest => dest.CompanyName, opt => opt.MapFrom(src => src.CompanyName))
                .ForMember(dest => dest.HomePage, opt => opt.MapFrom(src => src.HomePage))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.SupplierID))
                .ForMember(dest=>dest.PublisherTranslations , opt=>opt.MapFrom(src=>new List<PublisherTranslation>()))
                .ForSourceMember(dest => dest.Id, opt => opt.Ignore());

            CreateMap<Category, Genre>().ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.CategoryName))
                .ForSourceMember(dest => dest.Id, opt => opt.Ignore())
                .ForSourceMember(dest => dest.CategoryID, opt => opt.Ignore())
                .ForMember(dest => dest.Id, opt => opt.Ignore()).ReverseMap();

            CreateMap<OrderMongo, Order>().ForMember(dest => dest.OrderDate, opt => opt.MapFrom(src => DateTime.Parse(src.OrderDate)))
                .ForMember(dest => dest.CustomerId, opt => opt.MapFrom(src => src.EmployeeID))
                .ForMember(dest=>dest.Id , opt=>opt.MapFrom(src=> Guid.NewGuid()));

            CreateMap<OrderDetailsMongo, OrderDetails>()
                .ForMember(dest => dest.Discount, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.ProductId, opt => opt.MapFrom(src => src.ProductID))
                .ForMember(dest => dest.Quantity, opt => opt.MapFrom(src => (short) src.Quantity))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => Guid.NewGuid()))
                .ForSourceMember(dest => dest.Id, opt => opt.Ignore())
                .ForSourceMember(dest => dest.ProductID, opt => opt.Ignore())
                .ForMember(dest => dest.OrderId, opt => opt.Ignore());

            CreateMap<Product, Game>().ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.UnitPrice))
                .ForMember(dest => dest.AddedToGameStore, opt => opt.MapFrom(src=>DateTime.UtcNow))
                .ForMember(dest => dest.UnitsInStock, opt => opt.MapFrom(src => (short) src.UnitInStock))
                .ForMember(dest => dest.Key, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src=> src.ProductName))
                .ForMember(dest => dest.Discontinued, opt => opt.MapFrom(src => Convert.ToBoolean(src.Discontinued)))
                .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.UnitPrice))
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest=>dest.IsDeleted , opt=>opt.MapFrom(src=>src.IsDeleted))
                .ForMember(dest => dest.Genres, opt => opt.MapFrom(src=>src.Genres))
                .ForMember(dest=>dest.Publishers , opt=>opt.MapFrom(src=>src.Publishers))
                .ForMember(dest=>dest.PlatformTypes , opt=>opt.MapFrom(src=> new List<PlatformType>()))
                .ForMember(dest=>dest.GameTranslations , opt=>opt.MapFrom(src=>new List<GameTranslation>()))
                .ForMember(dest=>dest.Comments , opt=>opt.MapFrom(src=> new List<Comment>())).ReverseMap();
        }
    }
}
