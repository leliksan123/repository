﻿using AutoMapper;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Mongo;
using GameStore.Services.DTO;

namespace GameStore.MapperConfiguration.Mapping
{
    public class MappingBll : Profile
    {
        private const int LimitRecurseDepth = 2;
        private const int LimitForComments = 4;

        public MappingBll()
        {
            CreateMap<Game, GameDto>().MaxDepth(LimitRecurseDepth).ReverseMap();

            CreateMap<Comment, CommentDto>().MaxDepth(LimitForComments).ReverseMap();

            CreateMap<PlatformTypeDto, PlatformType>().MaxDepth(LimitRecurseDepth).ReverseMap();

            CreateMap<GenreDto, Genre>().ReverseMap();

            CreateMap<PublisherDto, Publisher>().ForMember(dest => dest.Games, opt => opt.Ignore())
                .ForSourceMember(dest => dest.Games, opt => opt.Ignore())
                .MaxDepth(LimitRecurseDepth).ReverseMap();

            CreateMap<OrderDto, Order>().ReverseMap();

            CreateMap<OrderDetailsDto, OrderDetails>().ReverseMap();

            CreateMap<ShipperDto, Shipper>().ReverseMap();

            CreateMap<LanguageDto, Language>().ReverseMap();

            CreateMap<GameTranslation, GameTranslationDto>().ReverseMap();

            CreateMap<User, UserDTo>().ReverseMap();

            CreateMap<Role, RoleDTo>().ReverseMap();

            CreateMap<PublisherTranslation, PublisherTranslationDto>().ReverseMap();
        }
    }
}