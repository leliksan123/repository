﻿using AutoMapper;
using GameStore.Authorization.Models;
using GameStore.DAL.Entities;
using GameStore.PaymentService.DataContracts;
using GameStore.Services.DTO;
using GameStore.ViewModels.ViewModels.Comment;
using GameStore.ViewModels.ViewModels.Game;
using GameStore.ViewModels.ViewModels.Genre;
using GameStore.ViewModels.ViewModels.Language;
using GameStore.ViewModels.ViewModels.Order;
using GameStore.ViewModels.ViewModels.Payment;
using GameStore.ViewModels.ViewModels.PlatformType;
using GameStore.ViewModels.ViewModels.Publisher;
using GameStore.ViewModels.ViewModels.Role;
using GameStore.ViewModels.ViewModels.Shipper;
using GameStore.ViewModels.ViewModels.User;

namespace GameStore.MapperConfiguration.Mapping
{
    public class Mapping : Profile
    {
        private const int LimitRecurseDepth = 2;
        private const int LimitForComments = 4;

        public Mapping()
        {
            CreateMap<GameDto, CreateGameViewModel>().MaxDepth(LimitRecurseDepth).ReverseMap();

            CreateMap<GameDto, GameIndexViewModel>().MaxDepth(LimitRecurseDepth).ReverseMap();

            CreateMap<CommentViewModel, CommentDto>().MaxDepth(LimitForComments).ReverseMap();

            CreateMap<CommentDto, ApiComment>().MaxDepth(LimitForComments);

            CreateMap<PlatformTypeDto, PlatformTypeViewModel>().MaxDepth(LimitRecurseDepth).ReverseMap();

            CreateMap<GenreViewModel, GenreDto>().ReverseMap();

            CreateMap<PublisherViewModel, PublisherDto>().ForMember(dest=>dest.Games , opt=>opt.Ignore())
                .ForSourceMember(dest=>dest.Games , opt=>opt.Ignore())
                .MaxDepth(LimitRecurseDepth).ReverseMap();

            CreateMap<GameDto, GameViewModel>().MaxDepth(LimitRecurseDepth).ReverseMap();

            CreateMap<OrderDto, OrderViewModel>().ForSourceMember(dest=>dest.ShippedDate , opt=>opt.Ignore()).ReverseMap();

            CreateMap<OrderDetailsDto, OrderDetailsViewModel>().ReverseMap();

            CreateMap<QuantityGameModel, QuantityGameDto>().ReverseMap();

            CreateMap<FilterViewModel, FilterDto>().ReverseMap();

            CreateMap<ShipperViewModel, ShipperDto>().ReverseMap();

            CreateMap<GameUpdateViewModel, GameDto>().ReverseMap();

            CreateMap<LanguageViewModel, LanguageDto>().ReverseMap();

            CreateMap<GameTranslation, GameTranslationDto>().ReverseMap();

            CreateMap<PublisherTranslation, PublisherDto>().ReverseMap();

            CreateMap<OrderInformViewModel, OrderInformDto>().ReverseMap();

            CreateMap<LoginModel, UserDTo>().ForMember(dest=>dest.Id , opt=>opt.Ignore()).
                ForSourceMember(dest=>dest.IsPersistent , opt=>opt.Ignore()).ReverseMap();

            CreateMap<RegisterModel, UserDTo>().ForSourceMember(dest => dest.ConfirmPassword, opt => opt.Ignore());

            CreateMap<UserViewModel, UserDTo>().ReverseMap();

            CreateMap<RoleViewModel, RoleDTo>().ReverseMap();

            CreateMap<ModelForVisa, PaymentInfo>().ReverseMap();
        }
    }
}