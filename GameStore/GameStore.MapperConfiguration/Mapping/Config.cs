﻿namespace GameStore.MapperConfiguration.Mapping
{
    public class Config
    {
        public  AutoMapper.MapperConfiguration Configure()
        {
            var mapperConfiguration = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.AddProfile<Mapping>();
                cfg.AddProfile<MappingBll>();
                cfg.AddProfile<MappingProfileDal>();
            });

            return mapperConfiguration;
        }
    }
}