﻿using System.Configuration;
using GameStore.DAL.Entities.Mongo;
using MongoDB.Driver;

namespace GameStore.DAL.Context
{
    public class MongoContext
    {
        private readonly IMongoDatabase _database;

        public MongoContext()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["MongoDbConnection"].ConnectionString;
            var mongoUrl = MongoUrl.Create(connectionString);
            var client = new MongoClient(connectionString);
            _database = client.GetDatabase(mongoUrl.DatabaseName);
        }

        public IMongoCollection<TEntity> GetCollection<TEntity>(string collectionName)
            where TEntity : MongoBaseType
        {
            return _database.GetCollection<TEntity>(collectionName);
        }

        public IMongoCollection<Product> Products => _database.GetCollection<Product>("products");

        public IMongoCollection<Category> Categories => _database.GetCollection<Category>("categories");

        public IMongoCollection<OrderDetailsMongo> OrderDetails => _database.GetCollection<OrderDetailsMongo>("order-details");

        public IMongoCollection<OrderMongo> Orders => _database.GetCollection<OrderMongo>("orders");

        public IMongoCollection<Shipper> Shippers => _database.GetCollection<Shipper>("shippers");

        public IMongoCollection<Supplier> Suppliers => _database.GetCollection<Supplier>("suppliers");
    }
}