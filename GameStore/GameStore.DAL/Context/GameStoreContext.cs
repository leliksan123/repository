﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using GameStore.DAL.Entities;

namespace GameStore.DAL.Context
{
    public class GameStoreContext : DbContext
    {
        public GameStoreContext() : base("name=GameStore")
        {
        }

        public virtual DbSet<Game> Games { get; set; }

        public virtual DbSet<PlatformType> PlatformTypes { get; set; }

        public virtual DbSet<Genre> Genres { get; set; }

        public virtual DbSet<Comment> Comments { get; set; }

        public virtual DbSet<Publisher> Publishers { get; set; }

        public virtual DbSet<Order> Orders { get; set; }

        public virtual DbSet<OrderDetails> OrderDetailses { get; set; }

        public virtual DbSet<Language> Languages { get; set; }

        public virtual DbSet<PublisherTranslation> PublisherTranslates { get; set; }

        public virtual DbSet<GameTranslation> GameTranslates { get; set; }

        public virtual DbSet<User> Users { get; set; }

        public virtual DbSet<Manager> Managers { get; set; } 

        public virtual DbSet<Role> Roles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Game>().HasMany(x => x.Comments).WithRequired(y => y.Game).WillCascadeOnDelete(true);

            modelBuilder.Entity<Game>()
                .HasMany(x => x.GameTranslations)
                .WithRequired(y => y.Game)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Publisher>()
                .HasMany(x => x.PublisherTranslations)
                .WithRequired(y => y.Publisher)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Language>()
                .HasMany(x => x.GameTranslates)
                .WithRequired(y => y.Language)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Language>()
                .HasMany(x => x.PublisherTranslates)
                .WithRequired(y => y.Language)
                .WillCascadeOnDelete(true);
        }
    }
}