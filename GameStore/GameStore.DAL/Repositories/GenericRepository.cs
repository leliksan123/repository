﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using GameStore.DAL.Context;
using GameStore.DAL.Interfaces;
using GameStore.Tools.Logger;

namespace GameStore.DAL.Repositories
{
    public class GenericRepository<T> : IRepository<T> where T : class
    {
        protected readonly GameStoreContext Context;
        protected readonly ILoggerMongoService Logger;
        private readonly DbSet<T> _dbSet;

        public GenericRepository(GameStoreContext context, ILoggerMongoService logger)
        {
            Context = context;
            Logger = logger;
            _dbSet = context.Set<T>();
        }

        public virtual IEnumerable<T> GetAll()
        {
            return _dbSet.ToList();
        }

        public virtual T GetById(Guid id)
        {
            return _dbSet.Find(id);
        }

        public virtual void Insert(T newItem)
        {
            Logger.Info($"Inserting new {typeof(T)}");
            _dbSet.Add(newItem);
        }

        public virtual void Update(T editItem)
        {
            Logger.Info($"Updating {typeof(T)}");
            Context.Entry(editItem).State = EntityState.Modified;
        }

        public virtual void Delete(Guid id)
        {
            Logger.Info($"Deleting {typeof(T)}");
            var entityToDelete = _dbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual void Delete(T deleteItem)
        {
            if (Context.Entry(deleteItem).State == EntityState.Detached)
            {
                _dbSet.Attach(deleteItem);
            }

            _dbSet.Remove(deleteItem);
        }

        public virtual IEnumerable<T> Get(Func<T, bool> predicate)
        {
            return _dbSet.Where(predicate).ToList();
        }

        public virtual T GetOne(Func<T, bool> predicate)
        {
            return _dbSet.Where(predicate).FirstOrDefault();
        }
    }
}
