﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using GameStore.DAL.Context;
using GameStore.DAL.Entities.Mongo;
using GameStore.DAL.Interfaces;
using GameStore.Tools.Logger;
using MongoDB.Driver;

namespace GameStore.DAL.Repositories
{
    public class SupplierRepository : IMongoRepository<Supplier>
    {
        private readonly MongoContext _context;
        private readonly ILoggerMongoService _logger;

        public SupplierRepository(MongoContext context, ILoggerMongoService logger)
        {
            _context = context;
            _logger = logger;
        }

        public IEnumerable<Supplier> GetAll()
        {
            var publishersMongo = _context.Suppliers.AsQueryable().ToList();

            return publishersMongo;
        }

        public Supplier Get(string companyName)
        {
            var supplier = _context.Suppliers.Find(x => x.CompanyName == companyName).FirstOrDefault();
            return supplier;
        }

        public void Delete(string companyName)
        {
            _logger.Info($"Deleting Supplier with companyName : {companyName}");
            _context.Suppliers.DeleteOne(x => x.CompanyName == companyName);
        }

        public IEnumerable<Supplier> Get(Expression<Func<Supplier, bool>> predicate)
        {
            var filterCompile = predicate.Compile();
            var entities = _context.Suppliers.AsQueryable().Where(filterCompile);

            return entities.AsQueryable().ToList();
        }
    }
}