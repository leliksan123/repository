﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameStore.DAL.Context;
using GameStore.DAL.Entities.Mongo;
using GameStore.DAL.Interfaces;
using GameStore.Tools.Logger;
using MongoDB.Driver;
using System.Linq.Expressions;

namespace GameStore.DAL.Repositories
{
    public class CategoryRepository : IMongoRepository<Category>
    {
        private readonly MongoContext _context;
        private readonly ILoggerMongoService _logger;

        public CategoryRepository(MongoContext context, ILoggerMongoService logger)
        {
            _context = context;
            _logger = logger;
        }

        public IEnumerable<Category> GetAll()
        {
            return _context.Categories.AsQueryable().ToList();
        }

        public Category Get(string name)
        {
            return _context.Categories.Find(x => x.CategoryName == name).FirstOrDefault();
        }

        public void Delete(string name)
        {
            _logger.Info($"Deleting Category with name : {name}");
            _context.Categories.DeleteOne(x => x.CategoryName == name);
        }

        public IEnumerable<Category> Get(Expression<Func<Category, bool>> predicate)
        {
            var filterCompile = predicate.Compile();
            var entities = _context.Categories.AsQueryable().Where(filterCompile);

            return entities.AsQueryable().ToList();
        }
    }
}