﻿using System.Linq;
using GameStore.DAL.Context;
using GameStore.DAL.Entities;
using GameStore.DAL.Interfaces;
using GameStore.Tools.Logger;

namespace GameStore.DAL.Repositories
{
    public class PublisherRepository : GenericRepository<Publisher>, IPublisherRepository
    {
        public PublisherRepository(GameStoreContext context, ILoggerMongoService logger) : base(context, logger)
        {
        }

        public void Delete(string companyName)
        {
            Logger.Info($"Deleting Publisher with companyName : {companyName}");
            var publisher = Context.Publishers.FirstOrDefault(x => x.CompanyName == companyName);

            Delete(publisher);
        }

        public Publisher Get(string companyName)
        {
            var publisher = Context.Publishers.FirstOrDefault(o => o.CompanyName == companyName);

            return publisher;
        }
    }
}