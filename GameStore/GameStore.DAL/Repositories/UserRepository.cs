﻿using System;
using System.Linq;
using GameStore.DAL.Context;
using GameStore.DAL.Entities;
using GameStore.DAL.Interfaces;
using GameStore.Tools.Logger;

namespace GameStore.DAL.Repositories
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public UserRepository(GameStoreContext context , ILoggerMongoService logger) : base(context, logger)
        {
        }

        public User Get(string email)
        {
            return Context.Users.FirstOrDefault(x=>x.Email == email);
        }

        public void Delete(string email)
        {
            Logger.Info($"Deleting User with Email {email}");

            var user = Context.Users.FirstOrDefault(x => x.Email == email);

            Delete(user);
        }

        public User Login(string email, string password)
        {
            return Context.Users.FirstOrDefault(p => (string.Compare(p.Email, email, StringComparison.OrdinalIgnoreCase) == 0) &&
                        (p.Password == password));
        }
    }
}
