﻿using System.Linq;
using GameStore.DAL.Context;
using GameStore.DAL.Entities;
using GameStore.DAL.Interfaces;
using GameStore.Tools.Logger;

namespace GameStore.DAL.Repositories
{
    public class LanguageRepository : GenericRepository<Language>, ILanguageRepository
    {
        public LanguageRepository(GameStoreContext context, ILoggerMongoService logger) : base(context, logger)
        {
        }

        public Language Get(string name)
        {
            return Context.Languages.FirstOrDefault(x => x.Name == name);
        }

        public void Delete(int id)
        {
            var language = Context.Languages.Find(id);

            Delete(language);
        }
    }
}