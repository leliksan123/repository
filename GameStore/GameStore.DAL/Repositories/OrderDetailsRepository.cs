﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using GameStore.DAL.Context;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Mongo;
using GameStore.DAL.Interfaces;
using GameStore.Extensions.ObjectIdToGuid;
using GameStore.Tools.Logger;
using MongoDB.Driver;

namespace GameStore.DAL.Repositories
{
    public class OrderDetailsRepository : IRepository<OrderDetails>
    {
        private readonly GameStoreContext _context;
        private readonly MongoContext _mongoContext;
        private readonly IMapper _mapper;
        private readonly ILoggerMongoService _logger;

        public OrderDetailsRepository(GameStoreContext context, MongoContext mongoContext , IMapper mapper , ILoggerMongoService logger)
        {
            _context = context;
            _mongoContext = mongoContext;
            _mapper = mapper;
            _logger = logger;
        }

        public IEnumerable<OrderDetails> GetAll()
        {
            var orderDetails = _context.OrderDetailses.ToList();

            var orderDetailsMongo = _mongoContext.OrderDetails.AsQueryable().ToList();

            var orderDetailsMap = _mapper.Map<List<OrderDetailsMongo>, List<OrderDetails>>(orderDetailsMongo);

            var result = orderDetails.Union(orderDetailsMap).Distinct();

            return result.ToList();
        }

        public OrderDetails GetById(Guid id)
        {
            var orderDetails = _context.OrderDetailses.FirstOrDefault(o => o.Id == id);

            if (orderDetails == null)
            {
                var orderDetailsMongo = _mongoContext.OrderDetails.Find(x => x.Id == id.AsObjectId().ToString()).FirstOrDefault();
                var orderDetailsFromMongo = _mapper.Map<OrderDetails>(orderDetailsMongo);
                return orderDetailsFromMongo;
            }

            return orderDetails;
        }

        public void Insert(OrderDetails newItem)
        {
            _logger.Info($"Inserting new OrderDetails to Order with Id : {newItem.OrderId}");
            _context.OrderDetailses.Add(newItem);
        }

        public void Update(OrderDetails editItem)
        {
            if (_context.OrderDetailses.FirstOrDefault(x => x.Id == editItem.Id) != null)
            {
                _logger.Info($"Updating OrderDetails with id: {editItem.Id}");
                _context.Entry(editItem).State = EntityState.Modified;
            }
            else
            {
                _logger.Info($"Inserting new OrderDetails to Order with Id : {editItem.OrderId}");
                _context.OrderDetailses.Add(editItem);
            }
        }

        public void Delete(Guid id)
        {
            _logger.Info($"Deleting OrderDetails with Id : {id}");

            var orderDetails = _context.OrderDetailses.FirstOrDefault(x => x.Id == id);

            if (orderDetails != null)
            {
                Delete(orderDetails);
            }
            else
            {
                _mongoContext.OrderDetails.DeleteOne(x => x.Id == id.AsObjectId().ToString());
            }

        }

        public void Delete(OrderDetails deleteItem)
        {
            if (_context.Entry(deleteItem).State == EntityState.Detached)
            {
                _context.OrderDetailses.Attach(deleteItem);
            }
                
            _context.OrderDetailses.Remove(deleteItem);
        }

        public IEnumerable<OrderDetails> Get(Func<OrderDetails, bool> predicate)
        {
            var orderDetailsDb = _context.OrderDetailses.Where(predicate).ToList();

            var orderDetailsBson = _mongoContext.OrderDetails.AsQueryable().ToList();

            var map = _mapper.Map<List<OrderDetails>>(orderDetailsBson).Where(predicate).ToList();

            var orderDetails = orderDetailsDb.Union(map);

            return orderDetails;
        }

        public OrderDetails GetOne(Func<OrderDetails, bool> predicate)
        {
            return _context.OrderDetailses.Where(predicate).FirstOrDefault();
        }
    }
}
