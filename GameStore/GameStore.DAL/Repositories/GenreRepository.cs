﻿using System.Linq;
using GameStore.DAL.Context;
using GameStore.DAL.Entities;
using GameStore.DAL.Interfaces;
using GameStore.Tools.Logger;

namespace GameStore.DAL.Repositories
{
    public class GenreRepository : GenericRepository<Genre>, IGenreRepository
    {
        public GenreRepository(GameStoreContext context, ILoggerMongoService logger) : base(context, logger)
        {
        }

        public void Delete(string name)
        {
            Logger.Info($"Deleting Genre with Name: {name}");

            var genre = Context.Genres.FirstOrDefault(x => x.Name == name);

            Delete(genre);
        }

        public Genre Get(string name)
        {
            var genre = Context.Genres.FirstOrDefault(o => o.Name == name);

            return genre;
        }
    }
}