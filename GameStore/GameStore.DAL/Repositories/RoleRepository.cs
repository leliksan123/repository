﻿using System.Linq;
using GameStore.DAL.Context;
using GameStore.DAL.Entities;
using GameStore.DAL.Interfaces;
using GameStore.Tools.Logger;

namespace GameStore.DAL.Repositories
{
    public class RoleRepository : GenericRepository<Role>, IRoleRepository
    {
        public RoleRepository(GameStoreContext context, ILoggerMongoService logger) : base(context, logger)
        {
        }

        public Role Get(string name)
        {
            return Context.Roles.FirstOrDefault(x => x.RoleName == name);
        }

        public void Delete(string name)
        {
            var entityToDelete = Context.Roles.FirstOrDefault(x=>x.RoleName == name);

            Delete(entityToDelete);
        }
    }
}
