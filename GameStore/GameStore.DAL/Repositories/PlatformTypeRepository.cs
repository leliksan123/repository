﻿using System.Linq;
using GameStore.DAL.Context;
using GameStore.DAL.Entities;
using GameStore.DAL.Interfaces;
using GameStore.Tools.Logger;

namespace GameStore.DAL.Repositories
{
    public class PlatformTypeRepository : GenericRepository<PlatformType>, IPlatformTypeRepository
    {
        public PlatformTypeRepository(GameStoreContext context, ILoggerMongoService logger) : base(context, logger)
        {
        }

        public PlatformType Get(string type)
        {
            return Context.PlatformTypes.FirstOrDefault(x => x.Type == type);
        }

        public void Delete(string type)
        {
            Logger.Info($"Deleting PlatformType with name : {type}");
            var entityToDelete = Context.PlatformTypes.FirstOrDefault(x => x.Type == type);
            Delete(entityToDelete);
        }
    }
}