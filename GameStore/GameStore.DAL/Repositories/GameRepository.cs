﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using GameStore.DAL.Context;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Mongo;
using GameStore.DAL.Interfaces;
using GameStore.DAL.Realization;
using GameStore.Tools.Logger;
using MongoDB.Driver;
using GameStore.Extensions.OrderByExtension;

namespace GameStore.DAL.Repositories
{
    public class GameRepository : GenericRepository<Game>, IGameRepository
    {
        private readonly IMapper _mapper;
        private readonly MongoContext _mongoContext;

        public GameRepository(GameStoreContext context, MongoContext mongoContext, IMapper mapper, ILoggerMongoService logger) : base(context, logger)
        {
            _mongoContext = mongoContext;
            _mapper = mapper;
        }

        public IEnumerable<Game> GetSortedGames(Expression<Func<Game, bool>> predicate, ModelForSorting sortPredicate,int? skip, int? take, bool isDeleted = false)
        {
            var sortedGames = isDeleted
                ? GetSortedGames(predicate, sortPredicate).ToList()
                : GetSortedGames(predicate, sortPredicate).Where(x => x.IsDeleted == false).ToList();


            if (skip.HasValue && take.HasValue)
            {
                sortedGames = sortedGames.Skip(skip.Value).Take(take.Value).ToList();
            }

            return sortedGames;
        }

        public int GetAllFilteredGames(Expression<Func<Game, bool>> predicate, ModelForSorting sortPredicate , bool isDeleted = false)
        {
            var sortedGames = isDeleted
               ? GetSortedGames(predicate, sortPredicate).ToList()
               : GetSortedGames(predicate, sortPredicate).Where(x => x.IsDeleted == false).ToList();

            return sortedGames.ToList().Count;
        }

        public Game Get(string key)
        {
            var game = Context.Games.FirstOrDefault(o => o.Key == key);
            return game;
        }

        public void Delete(string key)
        {
            Logger.Info($"Deleting Game with Key: {key}");

            var game = Context.Games.FirstOrDefault(x => x.Key == key);

            Delete(game);
        }

        private IEnumerable<Game> GetSortedGames(Expression<Func<Game, bool>> predicate, ModelForSorting sortPredicate)
        {
            var gamesDb = Context.Games.ToList();
            var gamesBson = _mongoContext.Products.AsQueryable().ToList();

            foreach (var item in gamesBson)
            {
                var category = _mongoContext.Categories.Find(x => x.CategoryID == item.CategoryID).FirstOrDefault();
                item.Genres = new List<Genre> {_mapper.Map<Genre>(category)};

                var supplier = _mongoContext.Suppliers.Find(x => x.SupplierID == item.SupplierID).FirstOrDefault();
                item.Publishers = new List<Publisher> { _mapper.Map<Publisher>(supplier)};
            }

            var source = _mapper.Map<List<Product>, List<Game>>(gamesBson);
            var sortedGames = gamesDb.Union(source).Distinct(new Extensions.CompareExtension.Comparer<Game>("Key"));

            sortedGames = sortedGames.AsQueryable().Where(predicate).ToList();

            sortedGames =
                sortedGames.AsQueryable().OrderBy(sortPredicate.SortPredicate, sortPredicate.OrderType).ToList();

            return sortedGames;
        }
    }
}