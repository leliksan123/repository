﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MongoDB.Driver;
using GameStore.DAL.Context;
using GameStore.DAL.Entities.Mongo;
using GameStore.DAL.Interfaces;
using GameStore.Tools.Logger;

namespace GameStore.DAL.Repositories
{
    public class MongoGenericRepository<TEntity> : IMongoRepository<TEntity> where TEntity : MongoBaseType, new()
    {
        protected readonly MongoContext Context;
        protected readonly ILoggerMongoService Logger;
        private readonly string _collectionName;

        public MongoGenericRepository(ILoggerMongoService logger, MongoContext context)
        {
            Logger = logger;
            Context = context;
            _collectionName = new TEntity().CollectionName;
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            var collection = Context.GetCollection<TEntity>(_collectionName);

            return collection.AsQueryable().ToList();
        }

        public virtual TEntity Get(string id)
        {
            var collection = Context.GetCollection<TEntity>(_collectionName);
            var entity = collection.Find(x => x.Id == id).FirstOrDefault();

            return entity;
        }

        public void Delete(string id)
        {
            var collection = Context.GetCollection<TEntity>(_collectionName);

            collection.DeleteOneAsync(x=>x.Id == id);
        }

        public virtual IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> predicate)
        {
            var collection = Context.GetCollection<TEntity>(_collectionName);
            var filterCompile = predicate.Compile();
            var entities = collection.AsQueryable().Where(filterCompile);

            return entities.AsQueryable().ToList();
        }
    }
}
