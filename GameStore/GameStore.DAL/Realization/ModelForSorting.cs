﻿using System;
using System.Linq.Expressions;
using GameStore.DAL.Entities;
using GameStore.Extensions.Enums;

namespace GameStore.DAL.Realization
{
    public class ModelForSorting
    {
        public OrderType OrderType { get; set; }

        public Expression<Func<Game, dynamic>> SortPredicate { get; set; }
    }
}