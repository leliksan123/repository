﻿using System;
using AutoMapper;
using GameStore.DAL.Context;
using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Mongo;
using GameStore.DAL.Interfaces;
using GameStore.DAL.Repositories;
using GameStore.Tools.Logger;

namespace GameStore.DAL.Realization
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly GameStoreContext _context;
        private readonly Lazy<IRoleRepository> _roleRepository;
        private readonly Lazy<IUserRepository> _userRepository;
        private readonly Lazy<IGameRepository> _gameRepository;
        private readonly Lazy<ILanguageRepository> _languageRepository;
        private readonly Lazy<IPlatformTypeRepository> _platformTypeRepository;
        private readonly Lazy<IPublisherRepository> _publisherRepository;
        private readonly Lazy<IGenreRepository> _genreRepository;
        private readonly Lazy<IRepository<OrderDetails>> _orderDetailsRepository;
        private readonly Lazy<IMongoRepository<Supplier>> _supplierRepository;
        private readonly Lazy<IMongoRepository<Category>> _categoryRepository;
        private readonly Lazy<IMongoRepository<Product>> _productRepository;
        private readonly Lazy<IMongoRepository<Shipper>> _shipperRepository;
        private readonly Lazy<IMongoRepository<OrderMongo>> _ordersMongoRepository;
        private readonly Lazy<IRepository<GameTranslation>> _gameTranslationRepository;
        private readonly Lazy<IRepository<PublisherTranslation>> _publisherTranslationRepository;
        private readonly Lazy<IRepository<Comment>> _commentRepository;
        private readonly Lazy<IRepository<Order>> _orderRepository;

        public UnitOfWork(GameStoreContext context, MongoContext mongoContext, IMapper mapper, ILoggerMongoService logger)
        {
            _context = context;
            var mongoDbContext = mongoContext;
            var autoMapper = mapper;
            var loggerMongo = logger;

            _platformTypeRepository = new Lazy<IPlatformTypeRepository>(() => new PlatformTypeRepository(_context, loggerMongo));
            _gameRepository = new Lazy<IGameRepository>(() => new GameRepository(_context, mongoDbContext, autoMapper, loggerMongo));
            _roleRepository = new Lazy<IRoleRepository>(() => new RoleRepository(_context, loggerMongo));
            _genreRepository = new Lazy<IGenreRepository>(() => new GenreRepository(_context, loggerMongo));

            _publisherRepository = new Lazy<IPublisherRepository>(() => new PublisherRepository(_context, loggerMongo));
            _orderDetailsRepository = new Lazy<IRepository<OrderDetails>>(() => new OrderDetailsRepository(_context, mongoDbContext, autoMapper, loggerMongo));
            _supplierRepository = new Lazy<IMongoRepository<Supplier>>(() => new MongoGenericRepository<Supplier>(loggerMongo, mongoDbContext));
            _categoryRepository = new Lazy<IMongoRepository<Category>>(() => new CategoryRepository(mongoDbContext, loggerMongo));
            _userRepository = new Lazy<IUserRepository>(() => new UserRepository(_context, loggerMongo));

            _languageRepository = new Lazy<ILanguageRepository>(() => new LanguageRepository(_context, loggerMongo));
            _gameTranslationRepository = new Lazy<IRepository<GameTranslation>>(() => new GenericRepository<GameTranslation>(_context, loggerMongo));
            _publisherTranslationRepository = new Lazy<IRepository<PublisherTranslation>>(() => new GenericRepository<PublisherTranslation>(_context, loggerMongo));
            _orderRepository = new Lazy<IRepository<Order>>(() => new GenericRepository<Order>(_context, loggerMongo));

            _commentRepository = new Lazy<IRepository<Comment>>(() => new GenericRepository<Comment>(_context, loggerMongo));
            _productRepository = new Lazy<IMongoRepository<Product>>(() => new MongoGenericRepository<Product>(loggerMongo, mongoDbContext));
            _shipperRepository = new Lazy<IMongoRepository<Shipper>>(() => new MongoGenericRepository<Shipper>(loggerMongo, mongoDbContext));
            _ordersMongoRepository = new Lazy<IMongoRepository<OrderMongo>>(() => new MongoGenericRepository<OrderMongo>(loggerMongo, mongoDbContext));
        }

        public IPlatformTypeRepository PlatformTypes => _platformTypeRepository.Value;

        public IGameRepository Games => _gameRepository.Value;

        public IRoleRepository Roles => _roleRepository.Value;

        public IGenreRepository Genres => _genreRepository.Value;

        public IPublisherRepository Publishers => _publisherRepository.Value;

        public IRepository<OrderDetails> OrderDetailses => _orderDetailsRepository.Value;

        public IMongoRepository<Supplier> Suppliers => _supplierRepository.Value;

        public IMongoRepository<Category> Categories => _categoryRepository.Value;

        public IUserRepository Users => _userRepository.Value;

        public ILanguageRepository Languages => _languageRepository.Value;

        public IRepository<GameTranslation> GameTranslations => _gameTranslationRepository.Value;

        public IRepository<PublisherTranslation> PublisherTranslations => _publisherTranslationRepository.Value;

        public IRepository<Order> Orders => _orderRepository.Value;

        public IRepository<Comment> Comments => _commentRepository.Value;

        public IMongoRepository<Product> Products => _productRepository.Value;

        public IMongoRepository<Shipper> Shippers => _shipperRepository.Value;

        public IMongoRepository<OrderMongo> OrdersMongo => _ordersMongoRepository.Value;

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}