namespace GameStore.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DoublePrice : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderDetails", "Discount", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.OrderDetails", "Discount");
        }
    }
}
