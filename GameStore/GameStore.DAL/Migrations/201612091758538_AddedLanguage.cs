using System.Data.Entity.Migrations;

namespace GameStore.DAL.Migrations
{
    public partial class AddedLanguage : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                    "dbo.GameTranslate",
                    c => new
                    {
                        Id = c.Int(false, true),
                        GameId = c.Int(false),
                        Description = c.String(),
                        LanguageId = c.Int(false)
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Language", t => t.LanguageId, true)
                .ForeignKey("dbo.Game", t => t.GameId, true)
                .Index(t => t.GameId)
                .Index(t => t.LanguageId);

            CreateTable(
                    "dbo.Language",
                    c => new
                    {
                        Id = c.Int(false, true),
                        Name = c.String()
                    })
                .PrimaryKey(t => t.Id);

            CreateTable(
                    "dbo.PublisherTranslate",
                    c => new
                    {
                        Id = c.Int(false, true),
                        PublisherId = c.Int(false),
                        Description = c.String(storeType: "ntext"),
                        LanguageId = c.Int(false)
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Language", t => t.LanguageId, true)
                .ForeignKey("dbo.Publisher", t => t.PublisherId, true)
                .Index(t => t.PublisherId)
                .Index(t => t.LanguageId);

            DropColumn("dbo.Game", "Description");
            DropColumn("dbo.Publisher", "Description");
        }

        public override void Down()
        {
            AddColumn("dbo.Publisher", "Description", c => c.String(storeType: "ntext"));
            AddColumn("dbo.Game", "Description", c => c.String());
            DropForeignKey("dbo.PublisherTranslate", "PublisherId", "dbo.Publisher");
            DropForeignKey("dbo.GameTranslate", "GameId", "dbo.Game");
            DropForeignKey("dbo.PublisherTranslate", "LanguageId", "dbo.Language");
            DropForeignKey("dbo.GameTranslate", "LanguageId", "dbo.Language");
            DropIndex("dbo.PublisherTranslate", new[] {"LanguageId"});
            DropIndex("dbo.PublisherTranslate", new[] {"PublisherId"});
            DropIndex("dbo.GameTranslate", new[] {"LanguageId"});
            DropIndex("dbo.GameTranslate", new[] {"GameId"});
            DropTable("dbo.PublisherTranslate");
            DropTable("dbo.Language");
            DropTable("dbo.GameTranslate");
        }
    }
}