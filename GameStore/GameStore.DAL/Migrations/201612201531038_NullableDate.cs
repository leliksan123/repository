namespace GameStore.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NullableDate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Order", "ShippedDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Order", "ShippedDate", c => c.DateTime(nullable: false));
        }
    }
}
