namespace GameStore.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangePublisherId : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Game", "PublisherId", "dbo.Publisher");
            DropIndex("dbo.Game", new[] { "PublisherId" });
            CreateTable(
                "dbo.PublisherGame",
                c => new
                    {
                        Publisher_Id = c.Int(nullable: false),
                        Game_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Publisher_Id, t.Game_Id })
                .ForeignKey("dbo.Publisher", t => t.Publisher_Id, cascadeDelete: true)
                .ForeignKey("dbo.Game", t => t.Game_Id, cascadeDelete: true)
                .Index(t => t.Publisher_Id)
                .Index(t => t.Game_Id);
            
            DropColumn("dbo.Game", "PublisherId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Game", "PublisherId", c => c.Int());
            DropForeignKey("dbo.PublisherGame", "Game_Id", "dbo.Game");
            DropForeignKey("dbo.PublisherGame", "Publisher_Id", "dbo.Publisher");
            DropIndex("dbo.PublisherGame", new[] { "Game_Id" });
            DropIndex("dbo.PublisherGame", new[] { "Publisher_Id" });
            DropTable("dbo.PublisherGame");
            CreateIndex("dbo.Game", "PublisherId");
            AddForeignKey("dbo.Game", "PublisherId", "dbo.Publisher", "Id");
        }
    }
}
