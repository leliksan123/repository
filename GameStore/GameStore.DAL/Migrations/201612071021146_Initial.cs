using System.Data.Entity.Migrations;

namespace GameStore.DAL.Migrations
{
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                    "dbo.Comment",
                    c => new
                    {
                        Id = c.Guid(false, true),
                        Name = c.String(),
                        Body = c.String(),
                        GameId = c.Int(false),
                        ParentCommentId = c.Guid(),
                        Quote = c.String()
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Comment", t => t.ParentCommentId)
                .ForeignKey("dbo.Game", t => t.GameId, true)
                .Index(t => t.GameId)
                .Index(t => t.ParentCommentId);

            CreateTable(
                    "dbo.Game",
                    c => new
                    {
                        Id = c.Int(false, true),
                        Key = c.String(maxLength: 50),
                        Name = c.String(),
                        Description = c.String(),
                        Price = c.Decimal(false, 18, 2),
                        UnitsInStock = c.Short(false),
                        Discontinued = c.Int(false),
                        PublisherId = c.Int(),
                        ViewsNumber = c.Int(false),
                        AddedToGameStore = c.DateTime(false)
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Publisher", t => t.PublisherId)
                .Index(t => t.Key, unique: true)
                .Index(t => t.PublisherId);

            CreateTable(
                    "dbo.Genre",
                    c => new
                    {
                        Id = c.Int(false, true),
                        Name = c.String(maxLength: 50),
                        ParentGenreId = c.Int()
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);

            CreateTable(
                    "dbo.PlatformType",
                    c => new
                    {
                        Id = c.Int(false, true),
                        Type = c.String(maxLength: 50)
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Type, unique: true);

            CreateTable(
                    "dbo.Publisher",
                    c => new
                    {
                        Id = c.Int(false, true),
                        CompanyName = c.String(maxLength: 40),
                        Description = c.String(storeType: "ntext"),
                        HomePage = c.String(storeType: "ntext")
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.CompanyName, unique: true);

            CreateTable(
                    "dbo.Customer",
                    c => new
                    {
                        Id = c.Int(false, true),
                        Name = c.String()
                    })
                .PrimaryKey(t => t.Id);

            CreateTable(
                    "dbo.Order",
                    c => new
                    {
                        Id = c.Guid(false, true),
                        CustomerId = c.Int(false),
                        OrderDate = c.DateTime(false)
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customer", t => t.CustomerId, true)
                .Index(t => t.CustomerId);

            CreateTable(
                    "dbo.OrderDetails",
                    c => new
                    {
                        Id = c.Guid(false, true),
                        ProductId = c.Int(false),
                        Quantity = c.Short(false),
                        Discount = c.Single(false),
                        OrderId = c.Guid(false)
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Order", t => t.OrderId, true)
                .Index(t => t.OrderId);

            CreateTable(
                    "dbo.GenreGame",
                    c => new
                    {
                        Genre_Id = c.Int(false),
                        Game_Id = c.Int(false)
                    })
                .PrimaryKey(t => new {t.Genre_Id, t.Game_Id})
                .ForeignKey("dbo.Genre", t => t.Genre_Id, true)
                .ForeignKey("dbo.Game", t => t.Game_Id, true)
                .Index(t => t.Genre_Id)
                .Index(t => t.Game_Id);

            CreateTable(
                    "dbo.PlatformTypeGame",
                    c => new
                    {
                        PlatformType_Id = c.Int(false),
                        Game_Id = c.Int(false)
                    })
                .PrimaryKey(t => new {t.PlatformType_Id, t.Game_Id})
                .ForeignKey("dbo.PlatformType", t => t.PlatformType_Id, true)
                .ForeignKey("dbo.Game", t => t.Game_Id, true)
                .Index(t => t.PlatformType_Id)
                .Index(t => t.Game_Id);
        }

        public override void Down()
        {
            DropForeignKey("dbo.OrderDetails", "OrderId", "dbo.Order");
            DropForeignKey("dbo.Order", "CustomerId", "dbo.Customer");
            DropForeignKey("dbo.Game", "PublisherId", "dbo.Publisher");
            DropForeignKey("dbo.PlatformTypeGame", "Game_Id", "dbo.Game");
            DropForeignKey("dbo.PlatformTypeGame", "PlatformType_Id", "dbo.PlatformType");
            DropForeignKey("dbo.GenreGame", "Game_Id", "dbo.Game");
            DropForeignKey("dbo.GenreGame", "Genre_Id", "dbo.Genre");
            DropForeignKey("dbo.Comment", "GameId", "dbo.Game");
            DropForeignKey("dbo.Comment", "ParentCommentId", "dbo.Comment");
            DropIndex("dbo.PlatformTypeGame", new[] {"Game_Id"});
            DropIndex("dbo.PlatformTypeGame", new[] {"PlatformType_Id"});
            DropIndex("dbo.GenreGame", new[] {"Game_Id"});
            DropIndex("dbo.GenreGame", new[] {"Genre_Id"});
            DropIndex("dbo.OrderDetails", new[] {"OrderId"});
            DropIndex("dbo.Order", new[] {"CustomerId"});
            DropIndex("dbo.Publisher", new[] {"CompanyName"});
            DropIndex("dbo.PlatformType", new[] {"Type"});
            DropIndex("dbo.Genre", new[] {"Name"});
            DropIndex("dbo.Game", new[] {"PublisherId"});
            DropIndex("dbo.Game", new[] {"Key"});
            DropIndex("dbo.Comment", new[] {"ParentCommentId"});
            DropIndex("dbo.Comment", new[] {"GameId"});
            DropTable("dbo.PlatformTypeGame");
            DropTable("dbo.GenreGame");
            DropTable("dbo.OrderDetails");
            DropTable("dbo.Order");
            DropTable("dbo.Customer");
            DropTable("dbo.Publisher");
            DropTable("dbo.PlatformType");
            DropTable("dbo.Genre");
            DropTable("dbo.Game");
            DropTable("dbo.Comment");
        }
    }
}