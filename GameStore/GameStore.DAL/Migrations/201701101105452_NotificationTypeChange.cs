namespace GameStore.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NotificationTypeChange : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.User", "NotificationType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.User", "NotificationType");
        }
    }
}
