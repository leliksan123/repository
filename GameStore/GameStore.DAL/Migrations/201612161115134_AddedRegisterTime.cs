namespace GameStore.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedRegisterTime : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.User", "RegistrationTime", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.User", "RegistrationTime");
        }
    }
}
