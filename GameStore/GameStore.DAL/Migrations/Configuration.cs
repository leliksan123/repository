﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using GameStore.DAL.Context;
using GameStore.DAL.Entities;
using Language = GameStore.DAL.Entities.Language;

namespace GameStore.DAL.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<GameStoreContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "GameStore.DAL.GameStoreContext";
        }

        protected override void Seed(GameStoreContext context)
        {
            var languages = new List<Language>
            {
                new Language {Id = 1, Name = "en"},
                new Language {Id = 2, Name = "ru"}
            };

            languages.ForEach(item => context.Languages.AddOrUpdate(item));
            context.SaveChanges();

            var genre0 = new Genre {Id = 1, Name = "Other"};
            var genre1 = new Genre {Id = 2, Name = "Sports"};
            var genre2 = new Genre {Id = 3, Name = "Strategy"};
            var genre3 = new Genre {Id = 4, Name = "RPG"};
            var genre4 = new Genre {Id = 5, Name = "Races"};

            var geners = new List<Genre>
            {
                genre0,
                genre1,
                genre2,
                genre3,
                genre4,
                new Genre {Id = 6, Name = "Action"},
                new Genre {Id = 7, Name = "Adventure"},
                new Genre {Id = 8, Name = "Puzzle&Skill"},
                new Genre {Id = 9, Name = "Misc"},
                new Genre {Id = 10, Name = "RTS", ParentGenreId = 2},
                new Genre {Id = 11, Name = "TBS", ParentGenreId = 2},
                new Genre {Id = 12, Name = "Rally", ParentGenreId = 4},
                new Genre {Id = 13, Name = "Arcade", ParentGenreId = 4},
                new Genre {Id = 14, Name = "Formula", ParentGenreId = 4},
                new Genre {Id = 15, Name = "Off-road", ParentGenreId = 4},
                new Genre {Id = 16, Name = "FPS", ParentGenreId = 5},
                new Genre {Id = 17, Name = "TPS", ParentGenreId = 5}
            };

            geners.ForEach(item => context.Genres.AddOrUpdate(item));
            context.SaveChanges();

            var platform1 = new PlatformType {Id = 1, Type = "Mobile"};
            var platform2 = new PlatformType {Id = 2, Type = "Browser"};
            var platform3 = new PlatformType {Id = 3, Type = "Desktop"};
            var platform4 = new PlatformType {Id = 4, Type = "Console"};

            var platformTypes = new List<PlatformType>
            {
                platform1,
                platform2,
                platform3,
                platform4
            };

            platformTypes.ForEach(item => context.PlatformTypes.AddOrUpdate(item));
            context.SaveChanges();

            var publisher1 = new Publisher {Id = 2, CompanyName = "Valve", HomePage = "www/www"};
            var publisher2 = new Publisher {Id = 3, CompanyName = "SEGA", HomePage = "sega.ru"};
            var publisher3 = new Publisher {Id = 4, CompanyName = "Activision", HomePage = "Activision.ua"};

            var publishers = new List<Publisher>
            {
                new Publisher {Id = 1, CompanyName = "Unknown", HomePage = "Unknown"},
                publisher1,
                publisher2,
                publisher3
            };

            publishers.ForEach(item => context.Publishers.AddOrUpdate(item));
            context.SaveChanges();

            var translatePublishers = new List<PublisherTranslation>
            {
                new PublisherTranslation
                {
                    Id = Guid.NewGuid(),
                    PublisherId = 2,
                    Description = "Big company",
                    LanguageId = 1
                },
                new PublisherTranslation
                {
                    Id = Guid.NewGuid(),
                    PublisherId = 2,
                    Description = "Большая компания",
                    LanguageId = 2
                },
                new PublisherTranslation
                {
                    Id = Guid.NewGuid(),
                    PublisherId = 3,
                    Description = "Famous company",
                    LanguageId = 1
                },
                new PublisherTranslation
                {
                    Id = Guid.NewGuid(),
                    PublisherId = 3,
                    Description = "Известная компания",
                    LanguageId = 2
                },
                new PublisherTranslation
                {
                    Id = Guid.NewGuid(),
                    PublisherId = 4,
                    Description = "Great company",
                    LanguageId = 1
                },
                new PublisherTranslation
                {
                    Id = Guid.NewGuid(),
                    PublisherId = 4,
                    Description = "Супер компания",
                    LanguageId = 2
                }
            };

            translatePublishers.ForEach(item => context.PublisherTranslates.AddOrUpdate(item));
            context.SaveChanges();

            var game1 = new Game
            {
                Id = 1,
                Key = "123",
                Name = "GTA",
                Genres = new List<Genre> {genre1, genre2},
                PlatformTypes = new List<PlatformType> {platform3, platform2},
                Price = 12.25m,
                UnitsInStock = 25,
                ViewsNumber = 12,
                AddedToGameStore = DateTime.UtcNow.AddDays(-3),
                Publishers = new List<Publisher> {publisher1 },
                IsDeleted = false,
                Picture = "Content/File/game1.jpg"
            };
            var game2 = new Game
            {
                Id = 2,
                Key = "124",
                Name = "Soccer",
                Genres = new List<Genre> {genre3},
                PlatformTypes = new List<PlatformType> {platform3, platform4},
                Price = 99.99m,
                UnitsInStock = 12,
                ViewsNumber = 45,
                AddedToGameStore = DateTime.UtcNow.AddMonths(-1),
                Publishers = new List<Publisher> { publisher1},
                IsDeleted = false,
                Picture = "Content/File/game2.png"
            };
            var game3 = new Game
            {
                Id = 3,
                Key = "125",
                Name = "World Of Tanks",
                Genres = new List<Genre> {genre3, genre2},
                PlatformTypes = new List<PlatformType> {platform1},
                Price = 12,
                UnitsInStock = 50,
                ViewsNumber = 2,
                AddedToGameStore = DateTime.UtcNow.AddHours(-5),
                Publishers = new List<Publisher> { publisher1 },
                IsDeleted = false,
                Picture = "Content/File/game3.jpg"
            };
            var game4 = new Game
            {
                Id = 4,
                Key = "128",
                Name = "Skyrim",
                Genres = new List<Genre> {genre1},
                PlatformTypes = new List<PlatformType> {platform1},
                Price = 1200,
                UnitsInStock = 500,
                ViewsNumber = 300,
                AddedToGameStore = DateTime.UtcNow.AddDays(-2)  ,
                Publishers = new List<Publisher> { publisher1 },
                IsDeleted = false,
                Picture = "Content/File/game4.jpg"
            };
            var game5 = new Game
            {
                Id = 5,
                Key = "129a",
                Name = "Dota",
                Genres = new List<Genre> {genre2},
                PlatformTypes = new List<PlatformType> {platform2},
                Price = 120,
                UnitsInStock = 5,
                ViewsNumber = 123,
                AddedToGameStore = DateTime.UtcNow.AddHours(-1),
                Publishers = new List<Publisher> { publisher1 },
                IsDeleted = false,
                Picture = "Content/File/game5.jpg"
            };
            var game6 = new Game
            {
                Id = 6,
                Key = "ac43b",
                Name = "Warcraft",
                Genres = new List<Genre> {geners.FirstOrDefault(x => x.Id == 12)},
                PlatformTypes = new List<PlatformType> {platform4},
                Price = 12,
                UnitsInStock = 50,
                ViewsNumber = 75,
                AddedToGameStore = DateTime.UtcNow.AddDays(-3),
                Publishers = new List<Publisher> { publisher2 },
                IsDeleted = false
            };
            var game7 = new Game
            {
                Id = 7,
                Key = "ac43bui",
                Name = "Steep",
                Genres = new List<Genre> {geners.FirstOrDefault(x => x.Id == 12)},
                PlatformTypes = new List<PlatformType> {platform4},
                Price = 12,
                UnitsInStock = 50,
                ViewsNumber = 190,
                AddedToGameStore = DateTime.UtcNow.AddHours(-1),
                Publishers = new List<Publisher> { publisher2 },
                IsDeleted = false
            };
            var game8 = new Game
            {
                Id = 8,
                Key = "678",
                Name = "Watch Dogs",
                Genres = new List<Genre> {geners.FirstOrDefault(x => x.Id == 12)},
                PlatformTypes = new List<PlatformType> {platform4},
                Price = 12,
                UnitsInStock = 50,
                ViewsNumber = 900,
                AddedToGameStore = DateTime.UtcNow.AddDays(-4),
                Publishers = new List<Publisher> { publisher3 },
                IsDeleted = false
            };
            var game9 = new Game
            {
                Id = 9,
                Key = "111",
                Name = "Titanfall",
                Genres = new List<Genre> {geners.FirstOrDefault(x => x.Id == 12)},
                PlatformTypes = new List<PlatformType> {platform4},
                Price = 12,
                UnitsInStock = 50,
                ViewsNumber = 345,
                AddedToGameStore = DateTime.UtcNow,
                Publishers = new List<Publisher> { publisher2 },
                IsDeleted = false
            };
            var game10 = new Game
            {
                Id = 10,
                Key = "key456",
                Name = "Mafia",
                Genres = new List<Genre> {geners.FirstOrDefault(x => x.Id == 12)},
                PlatformTypes = new List<PlatformType> {platform4},
                Price = 12,
                UnitsInStock = 50,
                ViewsNumber = 76,
                AddedToGameStore = DateTime.UtcNow.AddMonths(-1),
                Publishers = new List<Publisher> { publisher2 },
                IsDeleted = false
            };
            var game11 = new Game
            {
                Id = 11,
                Key = "6790",
                Name = "Battlefield",
                Genres = new List<Genre> {geners.FirstOrDefault(x => x.Id == 12)},
                PlatformTypes = new List<PlatformType> {platform4},
                Price = 12,
                UnitsInStock = 50,
                ViewsNumber = 90,
                AddedToGameStore = DateTime.UtcNow.AddHours(-6),
                Publishers = new List<Publisher> { publisher3 },
                IsDeleted = false
            };

            var games = new List<Game>
            {
                game1,
                game2,
                game3,
                game4,
                game5,
                game6,
                game7,
                game8,
                game9,
                game10,
                game11
            };
            games.ForEach(item => context.Games.AddOrUpdate(item));
            context.SaveChanges();

            var gameTranslates = new List<GameTranslation>
            {
                new GameTranslation {Id = Guid.NewGuid(), LanguageId = 1, GameId = 1, Description = "Popular game"},
                new GameTranslation {Id = Guid.NewGuid(), LanguageId = 2, GameId = 1, Description = "Популярная игра"},
                new GameTranslation {Id = Guid.NewGuid(), LanguageId = 1, GameId = 2, Description = "Cooool"},
                new GameTranslation {Id = Guid.NewGuid(), LanguageId = 2, GameId = 2, Description = "Круто"},
                new GameTranslation {Id = Guid.NewGuid(), LanguageId = 1, GameId = 3, Description = "Popular game"},
                new GameTranslation {Id = Guid.NewGuid(), LanguageId = 2, GameId = 3, Description = "Популярная игра"},
                new GameTranslation {Id = Guid.NewGuid(), LanguageId = 1, GameId = 4, Description = "Popular game"},
                new GameTranslation {Id = Guid.NewGuid(), LanguageId = 2, GameId = 4, Description = "Популярная игра"},
                new GameTranslation {Id = Guid.NewGuid(), LanguageId = 1, GameId = 5, Description = "Popular game"},
                new GameTranslation {Id = Guid.NewGuid(), LanguageId = 2, GameId = 5, Description = "Популярная игра"},
                new GameTranslation {Id = Guid.NewGuid(), LanguageId = 1, GameId = 6, Description = "Popular game"},
                new GameTranslation {Id = Guid.NewGuid(), LanguageId = 2, GameId = 6, Description = "Популярная игра"},
                new GameTranslation {Id = Guid.NewGuid(), LanguageId = 1, GameId = 7, Description = "Popular game"},
                new GameTranslation {Id = Guid.NewGuid(), LanguageId = 2, GameId = 7, Description = "Популярная игра"},
                new GameTranslation {Id = Guid.NewGuid(), LanguageId = 1, GameId = 8, Description = "Popular game"},
                new GameTranslation {Id = Guid.NewGuid(), LanguageId = 2, GameId = 8, Description = "Популярная игра"},
                new GameTranslation {Id = Guid.NewGuid(), LanguageId = 1, GameId = 9, Description = "Popular game"},
                new GameTranslation {Id = Guid.NewGuid(), LanguageId = 2, GameId = 9, Description = "Популярная игра"},
                new GameTranslation {Id = Guid.NewGuid(), LanguageId = 1, GameId = 10, Description = "Popular game"},
                new GameTranslation {Id = Guid.NewGuid(), LanguageId = 2, GameId = 10, Description = "Популярная игра"},
                new GameTranslation {Id = Guid.NewGuid(), LanguageId = 1, GameId = 11, Description = "Popular game"},
                new GameTranslation {Id = Guid.NewGuid(), LanguageId = 2, GameId = 11, Description = "Популярная игра"}
            };

            gameTranslates.ForEach(item => context.GameTranslates.AddOrUpdate(item));
            context.SaveChanges();

            var guide1 = new Guid("7c9e6679-7425-40de-944b-e07fc1f90ae7");
            var guide2 = new Guid("8c9e6679-7425-40de-944b-e07fc1f90ae7");
            var guide3 = new Guid("7c9e6679-7425-40de-944b-e07fc1f90ab6");
            var guide4 = new Guid("7c9e6679-7425-40de-944b-e07fc1f90ab5");
            var guide5 = new Guid("7c9e6679-7425-40de-944b-e07fc1f90ab4");

            var guide6 = new Guid("7c9e6679-7425-40de-944b-e07fc1f90ab3");
            var guide7 = new Guid("7c9e6679-7425-40de-944b-e07fc1f90ab2");
            var guide8 = new Guid("7c9e6679-7425-40de-944b-e07fc1f90ab1");
            var guide9 = new Guid("7c9e6679-7425-40de-944b-e07fc1f90ac2");
            var guide10 = new Guid("7c9e6679-7425-40de-944b-e07fc1f90ac3");

            var comment1 = new Comment {Name = "Admin@mail.ru", GameId = 1, Body = "Comment1", Id = guide1};
            var comment2 = new Comment
            {
                Name = "Alex@mail.ru",
                GameId = 1,
                Body = "Comment2",
                ParentCommentId = comment1.Id,
                Id = guide3
            };
            var comment3 = new Comment {Name = "Andrew@gmail.com", GameId = 2, Body = "Comment3", Id = guide2};
            var comment4 = new Comment
            {
                Name = "Admin@mail.ru",
                GameId = 2,
                Body = "Comment4",
                ParentCommentId = comment3.Id,
                Id = guide6
            };
            var comment5 = new Comment
            {
                Name = "Pol@mail.ru",
                GameId = 1,
                Body = "Comment5",
                ParentCommentId = comment2.Id,
                Id = guide4
            };
            var comment6 = new Comment
            {
                Name = "Pol@mail.ru",
                GameId = 1,
                Body = "Comment6",
                ParentCommentId = comment2.Id,
                Id = guide7
            };
            var comment7 = new Comment
            {
                Name = "Pol@gmail.com",
                GameId = 1,
                Body = "Comment7",
                ParentCommentId = comment5.Id,
                Id = guide8
            };
            var comment8 = new Comment
            {
                Name = "Pol@gmail.com",
                GameId = 1,
                Body = "Comment8",
                ParentCommentId = comment1.Id,
                Id = guide9
            };
            var comment9 = new Comment
            {
                Name = "Pol@gmail.com",
                GameId = 1,
                Body = "Comment9",
                ParentCommentId = comment1.Id,
                Id = guide5
            };
            var comment10 = new Comment
            {
                Name = "Pol@mail.ru",
                GameId = 1,
                Body = "Comment10",
                ParentCommentId = comment9.Id,
                Id = guide10
            };

            var comments = new List<Comment>
            {
                comment1,
                comment2,
                comment3,
                comment4,
                comment5,
                comment6,
                comment7,
                comment8,
                comment9,
                comment10
            };
            comments.ForEach(item => context.Comments.AddOrUpdate(item));
            context.SaveChanges();

            var order1 = new Order
            {
                CustomerId = 1,
                Id = Guid.NewGuid(),
                OrderDate = DateTime.UtcNow,
                ShippedDate = DateTime.UtcNow
            };

            var order2 = new Order
            {
                CustomerId = 1,
                Id = Guid.NewGuid(),
                OrderDate = DateTime.UtcNow,
                ShippedDate = DateTime.UtcNow
            };

            var orders = new List<Order>
            {
                order1,
                order2
            };

            orders.ForEach(item => context.Orders.AddOrUpdate(item));
            context.SaveChanges();

            var orderdetail = new OrderDetails
            {
                Discount = 1,
                OrderId = order1.Id,
                ProductId = 1,
                Quantity = 1
            };

            var orderdetailses = new List<OrderDetails>
            {
                orderdetail
            };

            orderdetailses.ForEach(item => context.OrderDetailses.AddOrUpdate(item));
            context.SaveChanges();

            var role1 = new Role {Id = 1, RoleName = "Administrator"};
            var role2 = new Role {Id = 2, RoleName = "Manager"};
            var role3 = new Role {Id = 3, RoleName = "Moderator"};
            var role4 = new Role {Id = 4, RoleName = "User"};
            var role5 = new Role {Id = 5, RoleName = "Guest"};

            var roles = new List<Role>
            {
                role1,
                role2,
                role3,
                role4,
                role5
            };

            roles.ForEach(item => context.Roles.AddOrUpdate(item));
            context.SaveChanges();

            var users = new List<User>
            {
                new User {Id = 1 , Email = "admin@mail.ru" , Password = "123123123" , Roles = new List<Role> {role1} , FirstName = "Admin" , LastName = "Admin" , Phone = "123123123123" , RegistrationTime = DateTime.UtcNow},
                new User {Id = 2 , Email = "moderator@mail.ru" , Password = "123123123", Roles = new List<Role> {role3} , FirstName = "Moderator" , LastName = "Moderator" , Phone = "12212121" , RegistrationTime = DateTime.UtcNow},
                new User {Id = 3 , Email = "manager@mail.ru" , Password = "123123123" , Roles = new List<Role> {role2} , FirstName = "Manager" , LastName = "Manager" , Phone = "123123123123" , RegistrationTime = DateTime.UtcNow.AddDays(-1)},
                new User {Id = 4 , Email = "user@mail.ru" , Password = "123123123", Roles = new List<Role> {role4} , FirstName = "User" , LastName = "User" , Phone = "83113131313"  , RegistrationTime = DateTime.UtcNow.AddMonths(-1)},
                new User {Id = 5 , Email = "guest@mail.ru" , Password = "123123123" , Roles = new List<Role> {role5} , FirstName = "Guest" , LastName = "Guest" , Phone = "123123123123" , RegistrationTime = DateTime.UtcNow},
                new User {Id = 6 , Email = "manager2@mail.ru" , Password = "123123123" , Roles = new List<Role> {role2} , FirstName = "Manager" , LastName = "Manager" , Phone = "123123123123" , RegistrationTime = DateTime.UtcNow.AddDays(-1)},
                new User {Id = 7 , Email = "manager3@mail.ru" , Password = "123123123" , Roles = new List<Role> {role2} , FirstName = "Manager" , LastName = "Manager" , Phone = "123123123123" , RegistrationTime = DateTime.UtcNow.AddDays(-1)}
            };

            users.ForEach(item => context.Users.AddOrUpdate(item));
            context.SaveChanges();

            var manager1 = new Manager { NotificationEmail = true , NotificationMobileApp = false , NotificationSms = false , User = users.First(x=>x.Id==6)};
            var manager2 = new Manager
            {
                NotificationEmail = true,
                NotificationMobileApp = false,
                NotificationSms = false,
                User = users.First(x => x.Id == 7)
            };

            var manager = new List<Manager>
            {
                manager1,
                manager2
            };

            manager.ForEach(item => context.Managers.AddOrUpdate(item));
            context.SaveChanges();
        }
    }
}