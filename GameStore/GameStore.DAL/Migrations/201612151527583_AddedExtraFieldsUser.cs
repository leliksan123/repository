namespace GameStore.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedExtraFieldsUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.User", "FirstName", c => c.String());
            AddColumn("dbo.User", "LastName", c => c.String());
            AddColumn("dbo.User", "Phone", c => c.String());
            AlterColumn("dbo.Role", "RoleName", c => c.String(maxLength: 30));
            CreateIndex("dbo.Role", "RoleName", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.Role", new[] { "RoleName" });
            AlterColumn("dbo.Role", "RoleName", c => c.String(nullable: false));
            DropColumn("dbo.User", "Phone");
            DropColumn("dbo.User", "LastName");
            DropColumn("dbo.User", "FirstName");
        }
    }
}
