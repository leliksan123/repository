namespace GameStore.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddManagers : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.UserRole", newName: "RoleUser");
            DropPrimaryKey("dbo.RoleUser");
            CreateTable(
                "dbo.Manager",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        NotificationEmail = c.Boolean(nullable: false),
                        NotificationSms = c.Boolean(nullable: false),
                        NotificationMobileApp = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.Id)
                .Index(t => t.Id);
            
            AddPrimaryKey("dbo.RoleUser", new[] { "Role_Id", "User_Id" });
            DropColumn("dbo.User", "NotificationType");
        }
        
        public override void Down()
        {
            AddColumn("dbo.User", "NotificationType", c => c.Int(nullable: false));
            DropForeignKey("dbo.Manager", "Id", "dbo.User");
            DropIndex("dbo.Manager", new[] { "Id" });
            DropPrimaryKey("dbo.RoleUser");
            DropTable("dbo.Manager");
            AddPrimaryKey("dbo.RoleUser", new[] { "User_Id", "Role_Id" });
            RenameTable(name: "dbo.RoleUser", newName: "UserRole");
        }
    }
}
