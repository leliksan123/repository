namespace GameStore.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GameIsDeleted : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Game", "IsDeleted", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Game", "IsDeleted");
        }
    }
}
