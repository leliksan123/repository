namespace GameStore.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditName : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.GameTranslate", newName: "GameTranslation");
            RenameTable(name: "dbo.PublisherTranslate", newName: "PublisherTranslation");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.PublisherTranslation", newName: "PublisherTranslate");
            RenameTable(name: "dbo.GameTranslation", newName: "GameTranslate");
        }
    }
}
