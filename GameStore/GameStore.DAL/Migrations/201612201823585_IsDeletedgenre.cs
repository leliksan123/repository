namespace GameStore.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IsDeletedgenre : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Publisher", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.Genre", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.Role", "IsDeleted", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Role", "IsDeleted");
            DropColumn("dbo.Genre", "IsDeleted");
            DropColumn("dbo.Publisher", "IsDeleted");
        }
    }
}
