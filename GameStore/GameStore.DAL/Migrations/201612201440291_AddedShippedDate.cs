namespace GameStore.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedShippedDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Order", "ShippedDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Order", "ShippedDate");
        }
    }
}
