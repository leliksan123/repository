namespace GameStore.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditPrice : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.OrderDetails", "Discount");
        }
        
        public override void Down()
        {
            AddColumn("dbo.OrderDetails", "Discount", c => c.Single(nullable: false));
        }
    }
}
