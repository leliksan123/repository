using System.Data.Entity.Migrations;

namespace GameStore.DAL.Migrations
{
    public partial class ChangeId : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.GameTranslate");
            DropPrimaryKey("dbo.PublisherTranslate");

            DropColumn("dbo.GameTranslate", "Id");
            AddColumn("dbo.GameTranslate", "Id", c => c.Guid(false, true));

            DropColumn("dbo.PublisherTranslate", "Id");
            AddColumn("dbo.PublisherTranslate", "Id", c => c.Guid(false, true));

            AddPrimaryKey("dbo.GameTranslate", "Id");
            AddPrimaryKey("dbo.PublisherTranslate", "Id");
        }

        public override void Down()
        {
            DropPrimaryKey("dbo.PublisherTranslate");
            DropPrimaryKey("dbo.GameTranslate");

            DropColumn("dbo.GameTranslate", "Id");
            AddColumn("dbo.GameTranslate", "Id", c => c.Int(false, true));

            DropColumn("dbo.PublisherTranslate", "Id");
            AddColumn("dbo.PublisherTranslate", "Id", c => c.Int(false, true));

            AddPrimaryKey("dbo.PublisherTranslate", "Id");
            AddPrimaryKey("dbo.GameTranslate", "Id");
        }
    }
}