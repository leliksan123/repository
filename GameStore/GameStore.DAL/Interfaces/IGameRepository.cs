﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using GameStore.DAL.Entities;
using GameStore.DAL.Realization;

namespace GameStore.DAL.Interfaces
{
    public interface IGameRepository : IRepository<Game>
    {
        /// <summary>
        /// Sorteds the data.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <param name="sortPredicate">The sort predicate.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="take">The take.</param>
        /// <param name="isDeleted"></param>
        /// <returns>Ienumerable Games</returns>
        IEnumerable<Game> GetSortedGames(Expression<Func<Game, bool>> predicate, ModelForSorting sortPredicate , int? skip , int? take, bool isDeleted = false);

        /// <summary>
        /// Gets all filtered games.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <param name="sortPredicate">The sort predicate.</param>
        /// <param name="isDeleted"></param>
        /// <returns>Count of filtered games</returns>
        int GetAllFilteredGames(Expression<Func<Game, bool>> predicate, ModelForSorting sortPredicate, bool isDeleted = false);

        /// <summary>
        /// Gets the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        Game Get(string key);

        /// <summary>
        /// Deletes the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        void Delete(string key);
    }
}