﻿using GameStore.DAL.Entities;

namespace GameStore.DAL.Interfaces
{
    public interface IRoleRepository : IRepository<Role>
    {
        /// <summary>
        /// Gets the specified name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>Role</returns>
        Role Get(string name);

        /// <summary>
        /// Deletes the specified name.
        /// </summary>
        /// <param name="name">The name.</param>
        void Delete(string name);
    }
}
