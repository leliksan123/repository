﻿using GameStore.DAL.Entities;

namespace GameStore.DAL.Interfaces
{
    public interface ILanguageRepository : IRepository<Language>
    {
        /// <summary>
        /// Gets the specified name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        Language Get(string name);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(int id);
    }
}
