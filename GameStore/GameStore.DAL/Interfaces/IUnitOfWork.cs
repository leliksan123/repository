﻿using GameStore.DAL.Entities;
using GameStore.DAL.Entities.Mongo;

namespace GameStore.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        /// <summary>
        ///     Gets the games.
        /// </summary>
        /// <value>
        ///     The games.
        /// </value>
        IGameRepository Games { get; }

        /// <summary>
        /// Gets the users.
        /// </summary>
        /// <value>
        /// The users.
        /// </value>
        IUserRepository Users { get; }

        /// <summary>
        ///     Gets the genres.
        /// </summary>
        /// <value>
        ///     The genres.
        /// </value>
        IGenreRepository Genres { get; }

        /// <summary>
        ///     Gets the platform types.
        /// </summary>
        /// <value>
        ///     The platform types.
        /// </value>
        IPlatformTypeRepository PlatformTypes { get; }

        /// <summary>
        ///     Gets the publishers.
        /// </summary>
        /// <value>
        ///     The publishers.
        /// </value>
        IPublisherRepository Publishers { get; }

        /// <summary>
        ///     Gets the order detailses.
        /// </summary>
        /// <value>
        ///     The order detailses.
        /// </value>
        IRepository<OrderDetails> OrderDetailses { get; }

        /// <summary>
        ///     Gets the shippers.
        /// </summary>
        /// <value>
        ///     The shippers.
        /// </value>
        IMongoRepository<Shipper> Shippers { get; }

        /// <summary>
        /// Gets or sets the suppliers.
        /// </summary>
        /// <value>
        ///     The suppliers.
        /// </value>
        IMongoRepository<Supplier> Suppliers { get; }

        /// <summary>
        ///     Gets the orders mongo.
        /// </summary>
        /// <value>
        ///     The orders mongo.
        /// </value>
        IMongoRepository<OrderMongo> OrdersMongo { get; }

        /// <summary>
        ///     Gets the categories.
        /// </summary>
        /// <value>
        ///     The categories.
        /// </value>
        IMongoRepository<Category> Categories { get; }

        /// <summary>
        ///     Gets the products.
        /// </summary>
        /// <value>
        ///     The products.
        /// </value>
        IMongoRepository<Product> Products { get; }

        /// <summary>
        /// Gets the roles.
        /// </summary>
        /// <value>
        /// The roles.
        /// </value>
        IRoleRepository Roles { get; }

            /// <summary>
        ///     Gets the languages.
        /// </summary>
        /// <value>
        ///     The languages.
        /// </value>
        ILanguageRepository Languages { get; }


        /// <summary>
        /// Gets the game translations.
        /// </summary>
        /// <value>
        /// The game translations.
        /// </value>
        IRepository<GameTranslation> GameTranslations { get; }

        /// <summary>
        /// Gets the publisher translations.
        /// </summary>
        /// <value>
        /// The publisher translations.
        /// </value>
        IRepository<PublisherTranslation> PublisherTranslations { get; }

        /// <summary>
        /// Gets the comments.
        /// </summary>
        /// <value>
        /// The comments.
        /// </value>
        IRepository<Comment> Comments { get; }

        /// <summary>
        /// Gets the orders.
        /// </summary>
        /// <value>
        /// The orders.
        /// </value>
        IRepository<Order> Orders { get; }

        /// <summary>
        ///     Saves this instance.
        /// </summary>
        void Save();
    }
}