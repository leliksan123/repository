﻿using GameStore.DAL.Entities;

namespace GameStore.DAL.Interfaces
{
    public interface IGenreRepository : IRepository<Genre>
    {
        /// <summary>
        /// Deletes the specified name.
        /// </summary>
        /// <param name="name">The name.</param>
        void Delete(string name);

        /// <summary>
        /// Gets the specified name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        Genre Get(string name);
    }
}
