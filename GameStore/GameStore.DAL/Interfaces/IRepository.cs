﻿using System;
using System.Collections.Generic;

namespace GameStore.DAL.Interfaces
{
    public interface IRepository<T> where T : class
    {
        /// <summary>
        ///     Gets all.
        /// </summary>
        /// <returns></returns>
        IEnumerable<T> GetAll();

        /// <summary>
        ///     Gets by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        T GetById(Guid id);

        /// <summary>
        ///     Inserts the specified new item.
        /// </summary>
        /// <param name="newItem">The new item.</param>
        void Insert(T newItem);

        /// <summary>
        ///     Updates the specified edit item.
        /// </summary>
        /// <param name="editItem">The edit item.</param>
        void Update(T editItem);

        /// <summary>
        ///     Deletes with specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(Guid id);

        /// <summary>
        ///     Deletes the specified delete item.
        /// </summary>
        /// <param name="deleteItem">The delete item.</param>
        void Delete(T deleteItem);

        /// <summary>
        ///     Gets the specified predicate.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <returns></returns>
        IEnumerable<T> Get(Func<T, bool> predicate);

        /// <summary>
        /// Gets the one.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <returns></returns>
        T GetOne(Func<T, bool> predicate);
    }
}