﻿using GameStore.DAL.Entities;

namespace GameStore.DAL.Interfaces
{
    public interface IPublisherRepository : IRepository<Publisher>
    {
        /// <summary>
        /// Deletes the specified company name.
        /// </summary>
        /// <param name="companyName">Name of the company.</param>
        void Delete(string companyName);

        /// <summary>
        /// Gets the specified company name.
        /// </summary>
        /// <param name="companyName">Name of the company.</param>
        /// <returns></returns>
        Publisher Get(string companyName);
    }
}
