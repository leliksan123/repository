﻿using GameStore.DAL.Entities;

namespace GameStore.DAL.Interfaces
{
    public interface IPlatformTypeRepository : IRepository<PlatformType>
    {
        /// <summary>
        /// Gets the specified type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        PlatformType Get(string type);

        /// <summary>
        /// Deletes the specified type.
        /// </summary>
        /// <param name="type">The type.</param>
        void Delete(string type);
    }
}
