﻿using GameStore.DAL.Entities;

namespace GameStore.DAL.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        /// <summary>
        /// Logins the specified email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="password">The password.</param>
        /// <returns>User</returns>
        User Login(string email, string password);

        /// <summary>
        /// Gets the specified email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns></returns>
        User Get(string email);

        /// <summary>
        /// Deletes the specified email.
        /// </summary>
        /// <param name="email">The email.</param>
        void Delete(string email);
    }
}
