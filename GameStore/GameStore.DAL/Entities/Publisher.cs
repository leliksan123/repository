﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GameStore.DAL.Entities
{
    public class Publisher : BaseType
    {
        [StringLength(40)]
        [Index(IsUnique = true)]
        public string CompanyName { get; set; }

        [Column(TypeName = "ntext")]
        public string HomePage { get; set; }

        public bool IsDeleted { get; set; }

        public virtual ICollection<Game> Games { get; set; }

        public virtual ICollection<PublisherTranslation> PublisherTranslations { get; set; }
    }
}