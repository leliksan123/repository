﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GameStore.DAL.Entities
{
    public class Manager
    {
        [Key]
        [ForeignKey("User")]
        public int Id { get; set; }

        public bool NotificationEmail { get; set; }

        public bool NotificationSms { get; set; }

        public bool NotificationMobileApp { get; set; }

        public virtual User User { get; set; }
    }
}
