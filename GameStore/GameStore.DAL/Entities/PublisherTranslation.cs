﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace GameStore.DAL.Entities
{
    public class PublisherTranslation
    {
        public Guid Id { get; set; }

        public int PublisherId { get; set; }

        [Column(TypeName = "ntext")]
        public string Description { get; set; }

        public int LanguageId { get; set; }

        public virtual Language Language { get; set; }

        public virtual Publisher Publisher { get; set; }
    }
}