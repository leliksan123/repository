﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace GameStore.DAL.Entities
{
    public class User : BaseType
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Phone { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime RegistrationTime { get; set; }

        public virtual ICollection<Role> Roles { get; set; }

        public virtual Manager Manager { get; set; }
    }
}
