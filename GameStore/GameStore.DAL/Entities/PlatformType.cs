﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GameStore.DAL.Entities
{
    public class PlatformType : BaseType
    {
        [StringLength(50)]
        [Index(IsUnique = true)]
        public string Type { get; set; }

        public virtual ICollection<Game> Games { get; set; }
    }
}