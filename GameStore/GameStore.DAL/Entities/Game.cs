﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GameStore.DAL.Entities
{
    public class Game : BaseType
    {
        [StringLength(50)]
        [Index(IsUnique = true)]
        public string Key { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public string Picture { get; set; }

        [Column(TypeName = "smallint")]
        public short UnitsInStock { get; set; }

        public bool Discontinued { get; set; }

        public int ViewsNumber { get; set; }

        public DateTime AddedToGameStore { get; set; }

        public bool IsDeleted { get; set; }

        public virtual ICollection<Publisher> Publishers { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

        public virtual ICollection<PlatformType> PlatformTypes { get; set; }

        public virtual ICollection<Genre> Genres { get; set; }

        public virtual ICollection<GameTranslation> GameTranslations { get; set; }
    }
}