﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GameStore.DAL.Entities
{
    public class Genre : BaseType
    {
        [StringLength(50)]
        [Index(IsUnique = true)]
        public string Name { get; set; }

        public bool IsDeleted { get; set; }

        public int? ParentGenreId { get; set; }

        public virtual ICollection<Game> Games { get; set; }
    }
}