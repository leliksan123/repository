﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GameStore.DAL.Entities
{
    public class Comment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Body { get; set; }

        public int GameId { get; set; }

        public Guid? ParentCommentId { get; set; }

        public string Quote { get; set; }

        public virtual Game Game { get; set; }

        public virtual Comment ParentComment { get; set; }

        public virtual ICollection<Comment> Answers { get; set; }
    }
}