﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GameStore.DAL.Entities
{
    public class OrderDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public int ProductId { get; set; }

        [Column(TypeName = "smallint")]
        public short Quantity { get; set; }

        public double Discount { get; set; }

        public Guid OrderId { get; set; }

        public virtual Order Order { get; set; }
    }
}