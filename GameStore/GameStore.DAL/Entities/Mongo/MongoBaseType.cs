﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GameStore.DAL.Entities.Mongo
{
    public abstract class MongoBaseType
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        public abstract string CollectionName { get; }
    }
}
