﻿using MongoDB.Bson.Serialization.Attributes;

namespace GameStore.DAL.Entities.Mongo
{
    [BsonIgnoreExtraElements]
    public class Supplier : MongoBaseType
    {
        public int SupplierID { get; set; }

        public string CompanyName { get; set; }

        public string HomePage { get; set; }

        public override string CollectionName => "suppliers";
    }
}
