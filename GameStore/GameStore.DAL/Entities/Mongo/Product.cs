﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GameStore.DAL.Entities.Mongo
{
    [BsonIgnoreExtraElements]
    public class Product : MongoBaseType
    {
        public string ProductName { get; set; }

        public int UnitInStock { get; set; }

        [BsonRepresentation(BsonType.Double)]
        public decimal UnitPrice { get; set; }

        public int Discontinued { get; set; }

        public string  QuantityPerUnit { get; set; }

        public int SupplierID { get; set; }

        public int CategoryID { get; set; }

        public bool IsDeleted { get; set; }

        public virtual ICollection<Publisher> Publishers { get; set; }

        public virtual ICollection<Genre> Genres { get; set; }

        public override string CollectionName => "products";
    }
}
