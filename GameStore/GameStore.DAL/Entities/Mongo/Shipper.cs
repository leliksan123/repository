﻿namespace GameStore.DAL.Entities.Mongo
{
    public class Shipper : MongoBaseType
    {
        public int ShipperID { get; set; }

        public string CompanyName { get; set; }

        public string Phone { get; set; }

        public override string CollectionName => "shippers";
    }
}
