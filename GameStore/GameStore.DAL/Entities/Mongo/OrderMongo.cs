﻿using MongoDB.Bson.Serialization.Attributes;

namespace GameStore.DAL.Entities.Mongo
{
    [BsonIgnoreExtraElements]
    public class OrderMongo : MongoBaseType
    {
        public int OrderID { get; set; }

        public int EmployeeID { get; set; }

        public string OrderDate { get; set; }

        public override string CollectionName => "orders";
    }
}
