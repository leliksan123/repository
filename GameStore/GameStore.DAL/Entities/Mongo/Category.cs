﻿using MongoDB.Bson.Serialization.Attributes;

namespace GameStore.DAL.Entities.Mongo
{
    [BsonIgnoreExtraElements]
    public class Category : MongoBaseType
    {
        public int CategoryID { get; set; }

        public bool IsDeleted { get; set; }

        public string CategoryName { get; set; }

        public override string CollectionName => "categories";
    }
}
