﻿using MongoDB.Bson.Serialization.Attributes;

namespace GameStore.DAL.Entities.Mongo
{
    [BsonIgnoreExtraElements]
    public class OrderDetailsMongo : MongoBaseType
    {
        public int OrderID { get; set; }

        public int ProductID { get; set; }

        public int Quantity { get; set; }

        public double Discount { get; set; }

        public override string CollectionName => "order-details";
    }
}
