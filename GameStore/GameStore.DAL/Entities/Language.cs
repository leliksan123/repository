﻿using System.Collections.Generic;

namespace GameStore.DAL.Entities
{
    public class Language : BaseType
    {
        public string Name { get; set; }

        public virtual ICollection<GameTranslation> GameTranslates { get; set; }

        public virtual ICollection<PublisherTranslation> PublisherTranslates { get; set; }
    }
}