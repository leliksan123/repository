﻿using System;

namespace GameStore.DAL.Entities
{
    public class GameTranslation
    {
        public Guid Id { get; set; }

        public int GameId { get; set; }

        public string Description { get; set; }

        public int LanguageId { get; set; }

        public virtual Language Language { get; set; }

        public virtual Game Game { get; set; }
    }
}