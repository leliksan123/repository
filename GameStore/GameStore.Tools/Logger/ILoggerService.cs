﻿namespace GameStore.Tools.Logger
{
    public interface ILoggerService
    {
        void Info(string message);
    }
}
