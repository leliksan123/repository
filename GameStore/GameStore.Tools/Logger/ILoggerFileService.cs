﻿namespace GameStore.Tools.Logger
{
    public interface ILoggerFileService : ILoggerService
    {
        void Warn(string message);

        void Debug(string message);

        void Error(string message);

        void Fatal(string message);
    }
}