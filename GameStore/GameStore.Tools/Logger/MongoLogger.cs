﻿using System;
using System.Configuration;
using GameStore.Tools.Models;
using MongoDB.Driver;

namespace GameStore.Tools.Logger
{
    public class MongoLogger : ILoggerMongoService
    {
        private readonly string _connectionString = ConfigurationManager.ConnectionStrings["MongoDbConnection"].ConnectionString;
        private readonly IMongoCollection<Log> _collection;

        public MongoLogger()
        {
            var client = new MongoClient(_connectionString);
            var database = client.GetDatabase("Northwind");
            _collection = database.GetCollection<Log>("Logger");
        }

        public void Info(string message)
        {
            var log = new Log {LogDateTime = DateTime.UtcNow, Message = message};

            _collection.InsertOne(log);
        }
    }
}