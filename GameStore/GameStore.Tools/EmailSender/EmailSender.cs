﻿using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace GameStore.Tools.EmailSender
{
    public class EmailSender : IEmailSender
    {
        public async Task SendEmail(string email, string subject = null, string body = null)
        {

            var m = new MailMessage("rockking151@gmail.com", email)
            {
                Subject = subject,
                Body = body
            };

            var smtp = new SmtpClient("smtp.gmail.com", 587)
            {
                Credentials = new NetworkCredential("rockking151@gmail.com", "qAz123123"),
                EnableSsl = true
            };

            await smtp.SendMailAsync(m);
        }
    }
}
