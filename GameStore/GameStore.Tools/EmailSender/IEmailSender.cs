﻿using System.Threading.Tasks;

namespace GameStore.Tools.EmailSender
{
    public interface IEmailSender
    {
        Task SendEmail(string email, string subject = null, string body = null);
    }
}
