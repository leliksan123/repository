﻿using System;

namespace GameStore.Tools.Models
{
    public class Log
    {
        public DateTime LogDateTime { get; set; }

        public string Message { get; set; }
    }
}