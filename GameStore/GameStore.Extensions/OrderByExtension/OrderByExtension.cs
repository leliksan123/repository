﻿using System;
using System.Linq;
using System.Linq.Expressions;
using GameStore.Extensions.Enums;

namespace GameStore.Extensions.OrderByExtension
{
    public static class OrderByExtension
    {
        public static IQueryable<T> OrderBy<T>(this IQueryable<T> source, Expression<Func<T, object>> sortPredicate,
            OrderType ordering)
        {
            var body = sortPredicate.Body;

            if (body.NodeType == ExpressionType.Convert)
            {
                body = ((UnaryExpression)body).Operand;
            }

            var type = typeof(T);

            var orderByExp = Expression.Lambda(body, sortPredicate.Parameters);

            var resultExp = Expression.Call(typeof(Queryable), ordering == OrderType.OrderBy ? "OrderBy" : "OrderByDescending",
                new[] { type, body.Type }, source.Expression, Expression.Quote(orderByExp));

            return source.Provider.CreateQuery<T>(resultExp);
        }
    }
}
