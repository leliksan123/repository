﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace GameStore.Extensions.CompareExtension
{
    public class Comparer<T> : IEqualityComparer<T>
    {
        private readonly PropertyInfo _propertyInfo;

        public Comparer(string propertyName)
        {
            _propertyInfo = typeof(T).GetProperty(propertyName,
                BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.Public);

            if (_propertyInfo == null)
                throw new ArgumentException($"{propertyName}is not a property of type {typeof(T)}.");
        }

        public bool Equals(T x, T y)
        {
            var xValue = _propertyInfo.GetValue(x, null);
            var yValue = _propertyInfo.GetValue(y, null);

            if (xValue == null)
                return yValue == null;

            return xValue.Equals(yValue);
        }

        public int GetHashCode(T obj)
        {
            var propertyValue = _propertyInfo.GetValue(obj, null);

            if (propertyValue == null)
                return 0;

            return propertyValue.GetHashCode();
        }
    }
}