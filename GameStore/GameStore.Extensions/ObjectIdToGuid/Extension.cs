﻿using System;
using System.Linq;
using MongoDB.Bson;

namespace GameStore.Extensions.ObjectIdToGuid
{
    public static class Extension
    {
        public static Guid AsGuid(this ObjectId oid)
        {
            var array = new byte[16];
            oid.ToByteArray().CopyTo(array, 0);
            var gid = new Guid(array);
            return gid;
        }

        public static ObjectId AsObjectId(this Guid gid)
        {
            var bytes = gid.ToByteArray().Take(12).ToArray();
            var oid = new ObjectId(bytes);
            return oid;
        }
    }
}