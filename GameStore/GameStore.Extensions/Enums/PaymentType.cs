﻿namespace GameStore.Extensions.Enums
{
    public enum PaymentType
    {
        Bank,
        Box,
        VisaCard
    }
}
