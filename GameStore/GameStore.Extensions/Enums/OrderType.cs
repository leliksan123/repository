﻿namespace GameStore.Extensions.Enums
{
    public enum OrderType
    {
        OrderBy,
        OrderByDescending
    }
}