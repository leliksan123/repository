﻿namespace GameStore.Extensions.Enums
{
    public enum DateSortType
    {
        LastWeek,
        LastMonth,
        LastYear,
        LastTwoYears,
        LastThreeYears
    }
}
