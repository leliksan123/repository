﻿namespace GameStore.Extensions.Enums
{
    public enum PaymentStatus
    {
        Successful,
        NotEnough,
        NotExists,
        Failed
    }
}
