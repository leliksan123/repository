﻿namespace GameStore.Extensions.Enums
{
    public enum OrderSortType
    {
        YoungOrders,
        OldOrders
    }
}
