﻿namespace GameStore.Extensions.Enums
{
    public enum OrderByType
    {
        MostPopular,
        MostCommented,
        ByPriceAsc,
        ByPriceDesc,
        NewBydate
    }
}
