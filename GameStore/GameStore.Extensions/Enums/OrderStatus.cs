﻿namespace GameStore.Extensions.Enums
{
    public enum OrderStatus
    {
        NotPaid,
        Paid,
        Shipped
    }
}
