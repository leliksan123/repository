﻿using System.ComponentModel.DataAnnotations;
using Resources;

namespace GameStore.Authorization.Models
{
    public class LoginModel
    {
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "RequiredEmail")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "InvalidEmail")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "MinPassword", MinimumLength = 6)]
        public string Password { get; set; }

        public bool IsPersistent { get; set; }
    }
}
