﻿using System;
using System.Linq;
using GameStore.Services.DTO;

namespace GameStore.Authorization.Models
{
    public static class UserInRoleExtension
    {
        public static bool InRole(this UserDTo user, string roles)
        {
            if (string.IsNullOrWhiteSpace(roles))
            {
                return false;
            }

            var rolesArray = roles.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var role in rolesArray)
            {
                var hasRole = user.Roles.Any(p => string.Compare(p.RoleName, role, StringComparison.OrdinalIgnoreCase) == 0);
                if (hasRole)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
