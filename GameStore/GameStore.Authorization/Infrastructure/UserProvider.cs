﻿using System.Security.Principal;
using AutoMapper;
using GameStore.Authorization.Models;

namespace GameStore.Authorization.Infrastructure
{
    public class UserProvider : IPrincipal
    {
        private UserIndentity UserIdentity { get; }

        public UserProvider(string name, IMapper mapper)
        {
            UserIdentity = new UserIndentity(mapper);
            UserIdentity.Init(name);
        }

        public IIdentity Identity => UserIdentity;

        public bool IsInRole(string role)
        {
            if (UserIdentity.User == null)
            {
                return false;
            }

            return UserIdentity.User.InRole(role);
        }

        public override string ToString()
        {
            return UserIdentity.Name;
        }
    }
}
