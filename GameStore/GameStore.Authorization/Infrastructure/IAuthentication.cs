﻿using System.Security.Principal;
using System.Web;
using GameStore.Services.DTO;

namespace GameStore.Authorization.Infrastructure
{
    public interface IAuthentication
    {
        HttpContext HttpContext { set; }

        UserDTo Login(string login, string password, bool isPersistent);

        void LogOut();

        IPrincipal CurrentUser { get; }
    }
}
