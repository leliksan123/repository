﻿using System;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using AutoMapper;
using GameStore.DAL.Interfaces;
using GameStore.Services.DTO;

namespace GameStore.Authorization.Infrastructure
{
    public class CustomAuthentication : IAuthentication
    {
        private IPrincipal _currentUser;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        private const string CookieName = "__AUTH_COOKIE";

        public HttpContext HttpContext { get; set; }

        public CustomAuthentication(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public UserDTo Login(string userName, string password, bool isPersistent)
        {
            var retUser = _mapper.Map<UserDTo>(_unitOfWork.Users.Login(userName, password));

            if (retUser != null)
            {
                CreateCookie(userName, isPersistent);
            }

            return retUser;
        }

        private static void CreateCookie(string userName, bool isPersistent = false)
        {
            var ticket = new FormsAuthenticationTicket(
                  1,
                  userName,
                  DateTime.Now,
                  DateTime.Now.Add(FormsAuthentication.Timeout),
                  isPersistent,
                  string.Empty,
                  FormsAuthentication.FormsCookiePath);

            var encTicket = FormsAuthentication.Encrypt(ticket);

            var authCookie = new HttpCookie(CookieName)
            {
                Value = encTicket,
                Expires = DateTime.Now.Add(FormsAuthentication.Timeout)
            };

            HttpContext.Current.Response.Cookies.Set(authCookie);
        }

        public void LogOut()
        {
            var httpCookie = HttpContext.Current.Response.Cookies[CookieName];

            if (httpCookie != null)
            {
                httpCookie.Value = string.Empty;
            }
        }

        public IPrincipal CurrentUser
        {
            get
            {
                if (_currentUser == null)
                {
                    try
                    {
                        var authCookie = HttpContext.Current.Request.Cookies.Get(CookieName);
                        
                        if (!string.IsNullOrEmpty(authCookie?.Value))
                        {
                            var ticket = FormsAuthentication.Decrypt(authCookie.Value);
                            _currentUser = new UserProvider(ticket.Name, _mapper);
                        }
                        else
                        {
                            _currentUser = new UserProvider(null, _mapper);
                        }
                    }
                    catch (Exception)
                    {
                        _currentUser = new UserProvider(null, _mapper);
                    }
                }
                return _currentUser;
            }
        }
    }
}
