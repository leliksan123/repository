﻿using System.Security.Principal;
using AutoMapper;
using GameStore.DAL.Context;
using GameStore.DAL.Entities;
using GameStore.DAL.Realization;
using GameStore.Services.DTO;
using GameStore.Tools.Logger;

namespace GameStore.Authorization.Infrastructure
{
    public class UserIndentity : IIdentity
    {
        private readonly MongoContext _mongoContext = new MongoContext();
        private readonly GameStoreContext _context = new GameStoreContext();
        private readonly MongoLogger _logger = new MongoLogger();
        private readonly UnitOfWork _unitOfWork;

        public UserDTo User { get; set; }

        private readonly IMapper _mapper;

        public UserIndentity(IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = new UnitOfWork(_context, _mongoContext, _mapper, _logger);
        }

        public string AuthenticationType => typeof(User).ToString();

        public bool IsAuthenticated => User != null;  

        public string Name => User != null ? User.Email : "anonym";

        public void Init(string email)
        {
            if (!string.IsNullOrEmpty(email))
            {
                User = _mapper.Map<UserDTo>(_unitOfWork.Users.Get(email));
            }
        }
    }
}
