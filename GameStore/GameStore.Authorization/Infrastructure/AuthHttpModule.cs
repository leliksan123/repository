﻿using System;
using System.Web;
using System.Web.Mvc;

namespace GameStore.Authorization.Infrastructure
{
    public class AuthHttpModule : IHttpModule
    {

        public void Init(HttpApplication context)
        {
            context.AuthenticateRequest += Authenticate;
        }

        private static void Authenticate(object source, EventArgs e)
        {
            var app = (HttpApplication)source;
            var context = app.Context;

            var auth = DependencyResolver.Current.GetService<IAuthentication>();
            auth.HttpContext = context;
            context.User = auth.CurrentUser;
        }

        public void Dispose()
        {
        }
    }
}
