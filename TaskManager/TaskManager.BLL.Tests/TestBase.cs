﻿using AutoMapper;
using TaskManager.WEB.Infrastructure.Automapper;

namespace TaskManager.BLL.Tests
{
    public class TestBase
    {
        protected IMapper Mapper { get; set; }

        protected void SetUp()
        {
            var config = new AutoMapperConfiguration();
            Mapper = config.Configure().CreateMapper();
        }
    }
}
