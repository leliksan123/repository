﻿using System;
using System.Collections.Generic;
using Moq;
using TaskManager.BLL.DTO;
using TaskManager.BLL.Interfaces;
using TaskManager.BLL.Services;
using Xunit;

namespace TaskManager.BLL.Tests.Services
{
    public class StatisticServiceTest
    {
        private readonly IStatisticService _sut;
        private readonly Mock<ITicketService> _ticketService;

        public StatisticServiceTest()
        {
            _ticketService = new Mock<ITicketService>();

            _sut = new StatisticService(_ticketService.Object);
        }

        [Fact]
        public void GetStatisticFiltered_ReturnsStatisticForLastTwoWeeks_WhenDateFromIsDefault()
        {
            const int defaultStatisticDaysNumber = 14;
            const string userName = "stub-username";
            var dateFrom = DateTime.UtcNow.AddDays(-defaultStatisticDaysNumber);

            var listTickets = new List<TicketDto> { new TicketDto() };
            
            _ticketService.Setup(ticketService => ticketService.GetAllFiltered(userName, dateFrom)).Returns(listTickets);
            var result = _sut.GetStatisticFiltered(userName, dateFrom);

            Assert.Equal(defaultStatisticDaysNumber, result.DateCountOfTicketsDictionary.Count);
        }

        [Fact]
        public void GetStatisticFiltered_ReturnsStatisticFromStartDate()
        {
            const int statisticForDaysNumber = 5;
            const string userName = "stub-username";
            var dateFrom = DateTime.UtcNow.AddDays(-statisticForDaysNumber);

            var listTickets = new List<TicketDto> { new TicketDto() };

            _ticketService
                .Setup(ticketService => ticketService.GetAllFiltered(userName, dateFrom))
                 .Returns(listTickets);

            var result = _sut.GetStatisticFiltered(userName, dateFrom);

            Assert.Equal(statisticForDaysNumber, result.DateCountOfTicketsDictionary.Count);
        }

        [Fact]
        public void GetStatisticFiltered_ReturnsNull_WhenCollectionIsEmpty()
        {
            _ticketService
                .Setup(ticketService => ticketService.GetAllFiltered(It.IsAny<string>(), It.IsAny<DateTime>()))
                .Returns(new List<TicketDto>());

            var result = _sut.GetStatisticFiltered(It.IsAny<string>(), It.IsAny<DateTime>());

            Assert.Null(result);
        }
    }
}