﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Bson;
using Moq;
using TaskManager.Auth.Infrastructure;
using TaskManager.BLL.DTO;
using TaskManager.BLL.Infrastructure.Exceptions;
using TaskManager.BLL.Interfaces;
using TaskManager.BLL.Services;
using TaskManager.DAL.Entities;
using TaskManager.DAL.Interfaces;
using Xunit;

namespace TaskManager.BLL.Tests.Services
{
    public class CommentServiceTest : TestBase
    {
        private readonly ICommentService _commentService;
        private readonly Mock<IUnitOfWork> _unitOfWorkMock;
        private readonly Mock<IUserAssignService> _userAssignService;

        public CommentServiceTest()
        {
            SetUp();
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _userAssignService = new Mock<IUserAssignService>();
            _commentService = new CommentService(_unitOfWorkMock.Object, Mapper, _userAssignService.Object);
        }

        [Fact]
        public void GetAll_ReturnsCommentsWithOwners_IfDataExists()
        {
            var ticketId = ObjectId.GenerateNewId();
            var userId1 = ObjectId.GenerateNewId();
            var userId2 = ObjectId.GenerateNewId();

            var comments = new List<Comment>
            {
                new Comment { UserId = userId1 },
                new Comment { UserId = userId2 }
            };

            var returnedCommentDtos = new List<CommentDto>
            {
                new CommentDto { User = new UserDto() },
                new CommentDto { User = new UserDto() }
            };

            _unitOfWorkMock.Setup(unitOfWork => unitOfWork.Comments.Find(It.IsAny<Expression<Func<Comment, bool>>>()))
                .Returns(comments.AsQueryable());
            _userAssignService.Setup(
                userAssignService => userAssignService.AssignUsersToComments(It.IsAny<IList<CommentDto>>()))
                .Returns(returnedCommentDtos);

            var commentList = _commentService.GetAll(ticketId.AsGuid());

            Assert.True(commentList.Any(dto => dto.User != null));
        }

        [Fact]
        public async Task GetAsync_ReturnsEntityNotFoundException_IfDataNotExists()
        {
            _unitOfWorkMock.Setup(unitOfWork => unitOfWork.Comments.GetAsync(It.IsAny<Guid>())).ReturnsAsync(null);

            await Assert.ThrowsAsync<EntityNotFoundException>(() => _commentService.GetAsync(It.IsAny<Guid>()));
        }

        [Fact]
        public async Task GetAsync_ReturnsCommentDto_IfDataExists()
        {
            var comment = new Comment
            {
                UserId = ObjectId.GenerateNewId(),
                Id = ObjectId.GenerateNewId()
            };

            var returnedCommentDto = new CommentDto
            {
                UserId = comment.UserId.AsGuid(),
                Id = comment.Id.AsGuid()
            };
                
            _unitOfWorkMock.Setup(unitOfWork => unitOfWork.Users.GetAll()).Returns(new List<User>().AsQueryable());
            _unitOfWorkMock.Setup(unitOfWork => unitOfWork.Comments.GetAsync(It.IsAny<Guid>())).ReturnsAsync(comment);
            _userAssignService.Setup(
                service => service.AssignUserToComment(It.IsAny<CommentDto>(), comment.UserId)).Returns(returnedCommentDto);

            var commentDto = await _commentService.GetAsync(It.IsAny<Guid>());

            Assert.Equal(comment.Id.AsGuid(), commentDto.Id);
        }

        [Fact]
        public async Task CreateAsync_ThrowsException_WhenInputIsNull()
        {
            _unitOfWorkMock.Setup(unitOfWork => unitOfWork.Comments.CreateAsync(It.IsAny<Comment>()));

            await Assert.ThrowsAsync<NullReferenceException>(() => _commentService.CreateAsync(null));
        }

        [Fact]
        public void CreateAsync_CallsCreateAsyncMethod_WhenValidComment()
        {
            var model = new CommentDto();

            _unitOfWorkMock.Setup(unitOfWork => unitOfWork.Comments.CreateAsync(It.IsAny<Comment>()));

            _commentService.CreateAsync(model);

            _unitOfWorkMock.Verify(unitOfWork => unitOfWork.Comments.CreateAsync(It.IsAny<Comment>()), Times.Once);
        }

        [Fact]
        public void UpdateAsync_CallsUpdateAsyncMethod_WhenValidComment()
        {
            var model = new CommentDto();

            _unitOfWorkMock.Setup(unitOfWork => unitOfWork.Comments.UpdateAsync(It.IsAny<Comment>()));

            _commentService.UpdateAsync(model);

            _unitOfWorkMock.Verify(unitOfWork => unitOfWork.Comments.UpdateAsync(It.IsAny<Comment>()), Times.Once);
        }

        [Fact]
        public async Task UpdateAsync_ThrowsException_WhenInputIsNull()
        {
            await Assert.ThrowsAsync<NullReferenceException>(() => _commentService.UpdateAsync(null));
        }

        [Fact]
        public async Task DeleteAsync_CallsDeleteAsyncMethod_WhenValidId()
        {
            var comment = new Comment();

            _unitOfWorkMock.Setup(unitOfWork => unitOfWork.Comments.GetAsync(It.IsAny<Guid>())).ReturnsAsync(comment);

            await _commentService.DeleteAsync(It.IsAny<Guid>());

            _unitOfWorkMock.Verify(unitOfWork => unitOfWork.Comments.DeleteAsync(It.IsAny<Guid>()), Times.Once);
        }
    }
}