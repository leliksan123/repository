﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Bson;
using Moq;
using TaskManager.Core.Enums;
using TaskManager.BLL.DTO;
using TaskManager.BLL.Infrastructure.Exceptions;
using TaskManager.BLL.Interfaces;
using TaskManager.BLL.Services;
using TaskManager.DAL.Entities;
using TaskManager.DAL.Infrastructure;
using TaskManager.DAL.Interfaces;
using Xunit;

namespace TaskManager.BLL.Tests.Services
{
    public class TicketServiceTest : TestBase
    {
        private readonly ITicketService _sut;
        private readonly Mock<IUnitOfWork> _unitOfWorkMock;
        private readonly Mock<ITicketLinkService> _ticketLinkServiceMock;
        private readonly Mock<IUserAssignService> _userAssignService;

        public TicketServiceTest()
        {
            SetUp();
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            var tagServiceMock = new Mock<ITagService>();
            var userServiceMock = new Mock<IUserService>();
            _ticketLinkServiceMock = new Mock<ITicketLinkService>();
            _userAssignService = new Mock<IUserAssignService>();

            _sut = new TicketService(
                _unitOfWorkMock.Object, 
                tagServiceMock.Object, 
                userServiceMock.Object, 
                Mapper, 
                _ticketLinkServiceMock.Object, 
                _userAssignService.Object);
        }

        [Fact]
        public void GetAllFiltered_ReturnsTickets()
        {
            var tickets = new List<TicketDto>();

            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Tickets.Find(It.IsAny<Expression<Func<Ticket, bool>>>()))
                .Returns(new List<Ticket>().AsQueryable());

            var result = _sut.GetAllFiltered(It.IsAny<string>(), It.IsAny<DateTime>());

            Assert.Equal(tickets.Count, result.Count());
        }

        [Fact]
        public void GetAll_ReturnsTicketsWithAssignees_IfDataExists()
        {
            var testUser = new User { Id = ObjectId.GenerateNewId() };
            var testTickets = new List<Ticket>
            {
                new Ticket
                {
                    Id = ObjectId.GenerateNewId(),
                    AssigneeId = testUser.Id
                },
                new Ticket
                {
                    Id = ObjectId.GenerateNewId(),
                    AssigneeId = testUser.Id
                }
            };

            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Tickets.GetAll())
                .Returns(testTickets.AsQueryable());
            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Users.GetAll())
                .Returns(new List<User> { testUser }.AsQueryable());

            var res = _sut.GetAll().ToList();

            Assert.True(res.All(dto => string.Equals(dto.Assignee.Email, testUser.Email)));
            Assert.True(res.All(dto => string.Equals(dto.Assignee.FullName, testUser.FullName)));
        }

        [Fact]
        public async Task GetAsync_ReturnsTicketWithAssignee_IfDataExists()
        {
            var ticketId = ObjectId.GenerateNewId();
            var userId = ObjectId.GenerateNewId();

            var testTicket = new Ticket
            {
                Id = ticketId,
                AssigneeId = userId
            };

            var testUser = new User { Id = userId, Email = "stub", FullName = "stub" };

            var testUserDto = new UserDto { Id = testUser.Id.AsGuid(), Email = testUser.Email, FullName = testUser.FullName };

            var testTicketDto = new TicketDto
            {
                Id = testTicket.Id.AsGuid(),
                Assignee = testUserDto
            };

            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Tickets.GetAsync(ticketId.AsGuid()))
                .ReturnsAsync(testTicket);
            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Users.GetAsync(userId.AsGuid()))
                .ReturnsAsync(testUser);
            _unitOfWorkMock.Setup(unitOfWork => unitOfWork.Users.Find(It.IsAny<Expression<Func<User, bool>>>()))
                .Returns(new List<User>().AsQueryable());
            _userAssignService.Setup(
                    service => service.AssignAssigneeAndWatcher(It.IsAny<TicketDto>()))
                .Returns(testTicketDto);

            var res = await _sut.GetAsync(ticketId.AsGuid());

            Assert.True(string.Equals(res.Assignee.Email, testUser.Email));
            Assert.True(string.Equals(res.Assignee.FullName, testUser.FullName));
        }

        [Fact]
        public async Task GetAsync_ThrowsEntityNotFoundException_IfTicketIsAbsent()
        {
            var ticketId = ObjectId.GenerateNewId();

            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Tickets.GetAsync(ticketId.AsGuid()))
                .ReturnsAsync(null);

            await Assert.ThrowsAsync<EntityNotFoundException>(() => _sut.GetAsync(ticketId.AsGuid()));
        }

        [Fact]
        public void GetAll_ReturnTickets_WhenInputIsNull()
        {
            var ticketList = new List<Ticket>
            {
                new Ticket
                {
                    Id = ObjectId.GenerateNewId(),
                    AssigneeId = ObjectId.Empty
                }
            };

            var ticketDtoList = new List<TicketDto>
            {
                new TicketDto
                {
                    Id = ticketList[0].Id.AsGuid(),
                    Assignee = new UserDto { Id = ticketList[0].Id.AsGuid() }
                }
            };

            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Tickets.GetAll())
                .Returns(ticketList.AsQueryable());
            _unitOfWorkMock.Setup(unitOfWork => unitOfWork.Users.Find(It.IsAny<Expression<Func<User, bool>>>()))
                .Returns(new List<User>().AsQueryable());
            _userAssignService.Setup(
                    service => service.AssignAssigneesAndWatchersToTicket(It.IsAny<IList<TicketDto>>()))
                .Returns(ticketDtoList);

            var tickets = _sut.GetAll();

            Assert.Equal(1, tickets.ToList().Count);
        }

        [Fact]
        public async Task GetAsync_ReturnsTicket_WhenTicketExists()
        {
            var ticketId = ObjectId.GenerateNewId();

            var ticketData = new Ticket
            {
                Id = ticketId,
                AssigneeId = ObjectId.Empty
            };

            var testTicketDto = new TicketDto
            {
                Id = ticketData.Id.AsGuid(),
                Assignee = new UserDto { Id = ticketData.AssigneeId.AsGuid() }
            };

            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Users.GetAsync(It.IsAny<Guid>()))
                .ReturnsAsync(new User());
            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Tickets.GetAsync(It.IsAny<Guid>()))
                .ReturnsAsync(ticketData);
            _userAssignService.Setup(
                    service => service.AssignAssigneeAndWatcher(It.IsAny<TicketDto>()))
                .Returns(testTicketDto);

            var ticket = await _sut.GetAsync(Guid.NewGuid());
            
            Assert.Equal(ticketId.AsGuid(), ticket.Id);
        }

        [Fact]
        public async Task GetAsync_ThrowsException_WhenTicketIsNotExisted()
        {
            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Tickets.GetAsync(It.IsAny<Guid>()))
                .ReturnsAsync(null);
            
            await Assert.ThrowsAsync<EntityNotFoundException>(() => _sut.GetAsync(Guid.NewGuid()));
        }

        [Fact]
        public async Task CreateAsync_ThrowsException_WhenNotValidTicket()
        {
            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Users.GetAsync(It.IsAny<Guid>()))
                .ReturnsAsync(null);

            await Assert.ThrowsAsync<NullReferenceException>(() => _sut.CreateAsync(new TicketDto()));
        }

        [Fact]
        public async Task CreateAsync_ThrowsException_WhenInputIsNull()
        {
            _unitOfWorkMock.Setup(unitOfWork => unitOfWork.Tickets.CreateAsync(It.IsAny<Ticket>()));
            
            await Assert.ThrowsAsync<NullReferenceException>(() => _sut.CreateAsync(null));
        }

        [Fact]
        public void CreateAsync_CallsCreateAsyncMethod_WhenValidTicket()
        {
            var model = new TicketDto();

            _unitOfWorkMock.Setup(unitOfWork => unitOfWork.Tickets.CreateAsync(It.IsAny<Ticket>()));

            _sut.CreateAsync(model);

            _unitOfWorkMock.Verify(unitOfWork => unitOfWork.Tickets.CreateAsync(It.IsAny<Ticket>()), Times.Once);
        }

        [Fact]
        public void UpdateAsync_CallsUpdateAsyncMethod_WhenValidTicket()
        {
            var model = new TicketDto();

            _unitOfWorkMock.Setup(unitOfWork => unitOfWork.Tickets.UpdateAsync(It.IsAny<Ticket>()));

            _sut.UpdateAsync(model);

            _unitOfWorkMock.Verify(unitOfWork => unitOfWork.Tickets.UpdateAsync(It.IsAny<Ticket>()), Times.Once);
        }

        [Fact]
        public async Task UpdateAsync_ThrowsException_WhenNotValidTicket()
        {
            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Users.GetAsync(It.IsAny<Guid>()))
                .ReturnsAsync(null);

            await Assert.ThrowsAsync<NullReferenceException>(() => _sut.UpdateAsync(new TicketDto()));
        }

        [Fact]
        public async Task UpdateAsync_ThrowsException_WhenInputIsNull()
        {
            await Assert.ThrowsAsync<NullReferenceException>(() => _sut.CreateAsync(null));
        }

        [Fact]
        public async Task DeleteAsync_ThrowsException_WhenInputIsNull()
        {
            _unitOfWorkMock.Setup(unitOfWork => unitOfWork.Tickets.DeleteAsync(It.IsAny<Guid>()));
          
            await Assert.ThrowsAsync<EntityNotFoundException>(() => _sut.DeleteAsync(Guid.Empty));
        }

        [Fact]
        public void DeleteAsync_CallsDeleteMethod_WhenTicketIsExisted()
        {
            var ticketId = Guid.NewGuid();

            _unitOfWorkMock
                .Setup(x => x.Tickets.GetAsync(It.IsAny<Guid>()))
                .ReturnsAsync(new Ticket());

            _sut.DeleteAsync(ticketId);

            _unitOfWorkMock.Verify(unitOfWork => unitOfWork.Tickets.DeleteAsync(It.IsAny<Guid>()), Times.Once);
        }

        [Fact]
        public void DeleteAsync_CallsUnlinkTicketsMethod_WhenTicketIsExisted()
        {
            var ticketIdId = ObjectId.GenerateNewId();

            var ticket = new Ticket
            {
                LinkedTicketIds = new List<ObjectId> { ticketIdId }
            };

            _unitOfWorkMock
               .Setup(x => x.Tickets.GetAsync(It.IsAny<Guid>()))
               .ReturnsAsync(ticket);
            _ticketLinkServiceMock
                .Setup(ticketLinkService => ticketLinkService.UnlinkTicketsAsync(It.IsAny<Guid>(), It.IsAny<Guid>()));

            _sut.DeleteAsync(Guid.NewGuid());

            _ticketLinkServiceMock.Verify(
                ticketLinkService => ticketLinkService.UnlinkTicketsAsync(It.IsAny<Guid>(), It.IsAny<Guid>()), Times.Once);
        }

        [Fact]
        public void UpdateStatusAsync_UpdatesStatus_WhenTicketExists()
        {
            var model = new Ticket();

            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Tickets.GetAsync(It.IsAny<Guid>()))
                .ReturnsAsync(model);

            _sut.UpdateStatusAsync(It.IsAny<Guid>(), It.IsAny<Status>());

            _unitOfWorkMock.Verify(unitOfWork => unitOfWork.Tickets.UpdateAsync(It.IsAny<Ticket>()), Times.Once);
        }

        [Fact]
        public async Task UpdateStatusAsync_ThrowsException_WhenTicketNotExists()
        {
            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Tickets.GetAsync(It.IsAny<Guid>()))
                .ReturnsAsync(null);

            await Assert.ThrowsAsync<EntityNotFoundException>(() => _sut.UpdateStatusAsync(Guid.NewGuid(), It.IsAny<Status>()));
        }

        [Fact]
        public void GetCount_ShouldReturnCorrectNumberOfTickets_IfModelIsValid()
        {
            var stub = new FilterDto
            {
                SelectedStatuses = new List<string>(),
                SelectedPriorities = new List<string>()
            };

            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Tickets.GetCount(It.IsAny<Expression<Func<Ticket, bool>>>()))
                .Returns(1);

            var result = _sut.GetCount(stub);

            Assert.Equal(1, result);
        }

        [Fact]
        public void GetAllFiltered_ShouldReturnTicketList__IfModelIsValid()
        {
            var stub = new FilterDto
            {
                SelectedStatuses = new List<string> { string.Empty },
                SelectedPriorities = new List<string> { string.Empty },
                DateFrom = DateTime.MinValue,
                DateTo = DateTime.MaxValue
            };

            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Tickets.Find(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<Expression<Func<Ticket, bool>>>()))
                .Returns(new List<Ticket>().AsQueryable());

            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Users.GetAll())
                .Returns(new List<User>().AsQueryable());

            _sut.GetAllFiltered(stub);

            _unitOfWorkMock.Verify(
                unitOfWork => unitOfWork.Tickets.Find(
                    It.IsAny<int>(), 
                    It.IsAny<int>(), 
                    It.IsAny<Expression<Func<Ticket, bool>>>()),
                Times.Once);
        }
    }
}