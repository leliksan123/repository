﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Moq;
using TaskManager.BLL.Infrastructure.Exceptions;
using TaskManager.BLL.Interfaces;
using TaskManager.BLL.Services;
using TaskManager.DAL.Entities;
using TaskManager.DAL.Interfaces;
using Xunit;

namespace TaskManager.BLL.Tests.Services
{
    public class UserServiceTest : TestBase
    {
        private readonly IUserService _sut;
        private readonly Mock<IUnitOfWork> _unitOfWorkMock;

        public UserServiceTest()
        {
            SetUp();
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _sut = new UserService(_unitOfWorkMock.Object, Mapper);
        }

        [Fact]
        public void Get_ReturnUser_WhenUserExists()
        {
            const string userEmail = "fullname@mail.com";

            var user = new User { Email = userEmail };
            var users = new List<User> { user };

            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Users.Find(It.IsAny<Expression<Func<User, bool>>>()))
                .Returns(users.AsQueryable());

            var result = _sut.Get(userEmail);

            Assert.Equal(user.Email, result.Email);
        }

        [Fact]
        public void Get_ThrowsEntityNotFoundException_WhenUserNotExists()
        {
            var users = new List<User>().AsQueryable();

            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Users.Find(It.IsAny<Expression<Func<User, bool>>>()))
                .Returns(users);

            Assert.Throws<EntityNotFoundException>(() => _sut.Get(It.IsAny<string>()));
        }

        [Fact]
        public void GetAll_CallsGetAll()
        {
            var users = new List<User>().AsQueryable();

            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Users.GetAll())
                .Returns(users);

            _sut.GetAll();

            _unitOfWorkMock.Verify(unitOfWork => unitOfWork.Users.GetAll(), Times.Once);
        }

        [Fact]
        public async Task GetAsync_ThrowsEntityNotFoundException_IfUserWithGuidNotExists()
        {
            var users = new List<User>().AsQueryable();

            _unitOfWorkMock
                .Setup(m => m.Users.Find(It.IsAny<Expression<Func<User, bool>>>()))
                .Returns(users);

            await Assert.ThrowsAsync<EntityNotFoundException>(() => _sut.GetAsync(It.IsAny<Guid>()));
        }

        [Fact]
        public async Task GetAsync_ReturnsUser_WhenUserWithGuidExists()
        {
            var user = new User();

            _unitOfWorkMock
                .Setup(m => m.Users.GetAsync(It.IsAny<Guid>())).ReturnsAsync(user);

            var result = await _sut.GetAsync(It.IsAny<Guid>());

            Assert.NotNull(result);
        }

        [Fact]
        public void GetEmailsForSearch_ReturnsEmail_IfTheyExists()
        {
            const string stubEmail = "stub";

            var user = new User
            {
                Email = stubEmail
            };

            var users = new List<User> { user };

            _unitOfWorkMock
                .Setup(m => m.Users.GetAll()).Returns(users.AsQueryable());

            var result = _sut.GetEmailsForSearch(stubEmail);

            Assert.Equal(user.Email, result.First());
        }
    }
}