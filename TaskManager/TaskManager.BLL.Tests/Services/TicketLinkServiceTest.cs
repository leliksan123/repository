﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Bson;
using Moq;
using TaskManager.BLL.Infrastructure.Exceptions;
using TaskManager.BLL.Interfaces;
using TaskManager.BLL.Services;
using TaskManager.DAL.Entities;
using TaskManager.DAL.Interfaces;
using Xunit;
using TaskManager.DAL.Infrastructure;

namespace TaskManager.BLL.Tests.Services
{
    public class TicketLinkServiceTest : TestBase
    {
        private readonly ITicketLinkService _sut;
        private readonly Mock<IUnitOfWork> _unitOfWorkMock;

        public TicketLinkServiceTest()
        {
            SetUp();
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _sut = new TicketLinkService(_unitOfWorkMock.Object, Mapper);
        }

        [Fact]
        public void GetLinkedTickets_ReturnsLinkedTickets_IfTheyExist()
        {
            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Tickets.Find(It.IsAny<Expression<Func<Ticket, bool>>>()))
                .Returns(new List<Ticket>().AsQueryable());

            _sut.GetLinkedTickets(Guid.NewGuid());

            _unitOfWorkMock.Verify(unitOfWork => unitOfWork.Tickets.Find(It.IsAny<Expression<Func<Ticket, bool>>>()), Times.Once);
        }

        [Fact]
        public void GetUnlinkedTickets_ReturnsUnlinkedTickets_IfTheyExist()
        {
            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Tickets.Find(It.IsAny<Expression<Func<Ticket, bool>>>()))
                .Returns(new List<Ticket>().AsQueryable());

            _sut.GetUnlinkedTickets(Guid.NewGuid());

            _unitOfWorkMock.Verify(unitOfWork => unitOfWork.Tickets.Find(It.IsAny<Expression<Func<Ticket, bool>>>()), Times.Once);
        }

        [Fact]
        public async Task LinkTickets_UpdatesBothTickets_IfTheyExist()
        {
            var stubTicketId1 = ObjectId.GenerateNewId();
            var stubTicketId2 = ObjectId.GenerateNewId();

            var stubTickets = new List<Ticket>
            {
                new Ticket { Id = stubTicketId1 },
                new Ticket { Id = stubTicketId2 }
            };

            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Tickets.Find(It.IsAny<Expression<Func<Ticket, bool>>>()))
                .Returns(stubTickets.AsQueryable());

            await _sut.LinkTicketsAsync(stubTicketId1.AsGuid(), stubTicketId2.AsGuid());

            _unitOfWorkMock.Verify(unitOfWork => unitOfWork.Tickets.UpdateAsync(It.IsAny<Ticket>()), Times.Exactly(2));
        }

        [Fact]
        public async Task UnlinkTickets_UpdatesBothTickets_IfTheyExist()
        {
            var stubTicketId1 = ObjectId.GenerateNewId();
            var stubTicketId2 = ObjectId.GenerateNewId();

            var stubTickets = new List<Ticket>
            {
                new Ticket { Id = stubTicketId1 },
                new Ticket { Id = stubTicketId2 }
            };

            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Tickets.Find(It.IsAny<Expression<Func<Ticket, bool>>>()))
                .Returns(stubTickets.AsQueryable());

            await _sut.UnlinkTicketsAsync(stubTicketId1.AsGuid(), stubTicketId2.AsGuid());

            _unitOfWorkMock.Verify(x => x.Tickets.UpdateAsync(It.IsAny<Ticket>()), Times.Exactly(2));
        }

        [Fact]
        public async Task UnlinkTickets_ThrowsEntityNotFoundException_IfTicketDoesNotExist()
        {
            var stubTicketId1 = ObjectId.GenerateNewId();
            var stubTicketId2 = ObjectId.GenerateNewId();

            var stubTicket1 = new Ticket { Id = stubTicketId1 };

            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Tickets.GetAsync(stubTicketId1.AsGuid()))
                .ReturnsAsync(stubTicket1);
            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Tickets.GetAsync(stubTicketId2.AsGuid()))
                .ReturnsAsync(null);

            await Assert.ThrowsAsync<EntityNotFoundException>(() => _sut.UnlinkTicketsAsync(stubTicketId1.AsGuid(), stubTicketId2.AsGuid()));
        }
    }
}