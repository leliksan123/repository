﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Bson;
using Moq;
using TaskManager.BLL.DTO;
using TaskManager.BLL.Infrastructure.Exceptions;
using TaskManager.BLL.Interfaces;
using TaskManager.BLL.Services;
using TaskManager.DAL.Entities;
using TaskManager.DAL.Interfaces;
using Xunit;

namespace TaskManager.BLL.Tests.Services
{
    public class UserAssignServiceTest : TestBase
    {
        private readonly Mock<IUnitOfWork> _unitOfWorkMock;
        private readonly IUserAssignService _userAssignService;

        public UserAssignServiceTest()
        {
            SetUp();

            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _userAssignService = new UserAssignService(_unitOfWorkMock.Object, Mapper);
        }

        [Fact]
        public void AssignUserToComment_ChecksIfUserAssigned_WhenUserExists()
        {
            var user = new List<User>
            {
                new User
                {
                    Email = "stub"
                }
            };

            var commentDto = new CommentDto();

            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Users.Find(It.IsAny<Expression<Func<User, bool>>>()))
                .Returns(user.AsQueryable());

            var result = _userAssignService.AssignUserToComment(commentDto, It.IsAny<ObjectId>());

            Assert.Equal(result.User.Email, user.FirstOrDefault().Email);
        }

        [Fact]
        public void AssignUserToComment_CallsFindMethod_WhenUserExists()
        {
            _unitOfWorkMock
                .Setup(x => x.Users.Find(It.IsAny<Expression<Func<User, bool>>>()))
                .Returns(new List<User>().AsQueryable());

             _userAssignService.AssignUserToComment(new CommentDto(), It.IsAny<ObjectId>());

            _unitOfWorkMock.Verify(unitOfWork => unitOfWork.Users.Find(It.IsAny<Expression<Func<User, bool>>>()), Times.Once);
        }

        [Fact]
        public void AssignUsersToComments_ChecksIfUsersWasAssigned_WhenUsersExist()
        {
            var user = new User { Email = "stub" };
            var users = new List<User> { user };

            var commentDto = new CommentDto
            {
                User = new UserDto
                {
                    Email = "stub"
                }
            };

            var listCommentDtos = new List<CommentDto> { commentDto };

            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Users.Find(It.IsAny<Expression<Func<User, bool>>>()))
                .Returns(users.AsQueryable());

            var result = _userAssignService.AssignUsersToComments(listCommentDtos);

            Assert.Equal(result.FirstOrDefault().User.Email, users.FirstOrDefault().Email);
        }

        [Fact]
        public void AssignAssigneeAndWatcher_ChecksAssigneeWasAdded_WhenAssigneeExist()
        {
            var user = new User { Email = "stub" };
            var ticketDto = new TicketDto
            {
                Watchers = new List<UserDto>(),
                Assignee = new UserDto()
            };

            var users = new List<User> { user };

            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Users.Find(It.IsAny<Expression<Func<User, bool>>>()))
                .Returns(users.AsQueryable());

            var result = _userAssignService.AssignAssigneeAndWatcher(ticketDto);

            Assert.Equal(result.Assignee.Email, users.FirstOrDefault().Email);
        }

        [Fact]
        public void AssignAssigneeAndWatcher_ChecksWatcherWasAdded_WhenWatcherExist()
        {
            var user = new User { Email = "stub" };
            var users = new List<User> { user };

            var watchers = new List<UserDto>
            {
                new UserDto
                {
                    Email = "stub"
                }
            };

            var ticketDto = new TicketDto
            {
                Watchers = watchers,
                Assignee = new UserDto()
            };

            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Users.Find(It.IsAny<Expression<Func<User, bool>>>()))
                .Returns(users.AsQueryable());

            var result = _userAssignService.AssignAssigneeAndWatcher(ticketDto);

            Assert.Equal(result.Watchers.FirstOrDefault().Email, users.FirstOrDefault().Email);
        }

        [Fact]
        public async Task ValidateTicketAssigneeAsync_CallsGetAsync_WhenAssigneeNotNull()
        {
            var ticketDto = new TicketDto
            {
                Assignee = new UserDto
                {
                    Id = Guid.NewGuid()
                }
            };

            var user = new User();
            _unitOfWorkMock.Setup(unitOfWork => unitOfWork.Users.GetAsync(It.IsAny<Guid>())).ReturnsAsync(user);

            await _userAssignService.ValidateTicketAssigneeAsync(ticketDto);

            _unitOfWorkMock.Verify(unitOfWork => unitOfWork.Users.GetAsync(It.IsAny<Guid>()), Times.Once);
        }

        [Fact]
        public async Task ValidateTicketAssigneeAsync_ThrowsEntityNotFoundException_WhenUserNotExists()
        {
            var ticketDto = new TicketDto
            {
                Assignee = new UserDto
                {
                    Id = Guid.NewGuid()
                },
            };

            _unitOfWorkMock.Setup(unitOfWork => unitOfWork.Users.GetAsync(It.IsAny<Guid>())).ReturnsAsync(null);

            await Assert.ThrowsAsync<EntityNotFoundException>(() => _userAssignService.ValidateTicketAssigneeAsync(ticketDto));
        }

        [Fact]
        public void AssignAssigneesAndWatchersToTicket_ChecksAssigneeWasAddedToTickets_WhenTheyExist()
        {
            var assignee = new UserDto { Email = "stub" };
            var watchers = new List<UserDto> { assignee };
            var user = new User { Email = "stub" };

            var ticketDtos = new List<TicketDto>
            {
                new TicketDto
                {
                    Assignee = assignee,
                    Watchers = watchers 
                }
            };

            var users = new List<User> { user };

            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Users.Find(It.IsAny<Expression<Func<User, bool>>>()))
                .Returns(users.AsQueryable());

            var result = _userAssignService.AssignAssigneesAndWatchersToTicket(ticketDtos);

            Assert.Equal(result.FirstOrDefault().Watchers.FirstOrDefault().Email, users.FirstOrDefault().Email);
        }

        [Fact]
        public void AssignAssigneesAndWatchersToTicket_ChecksWatchersWasAddedToTickets_WhenTheyExist()
        {
            var assignee = new UserDto { Email = "stub" };
            var watchers = new List<UserDto> { assignee };
            var user = new User { Email = "stub" };

            var ticketDtos = new List<TicketDto>
            {
                new TicketDto
                {
                    Assignee = assignee,
                    Watchers = watchers
                }
            };

            var users = new List<User> { user };

            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Users.Find(It.IsAny<Expression<Func<User, bool>>>()))
                .Returns(users.AsQueryable());

            var result = _userAssignService.AssignAssigneesAndWatchersToTicket(ticketDtos);

            Assert.Equal(result.FirstOrDefault().Assignee.Email, users.FirstOrDefault().Email);
        }
    }
}
