﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Moq;
using TaskManager.BLL.DTO;
using TaskManager.BLL.Infrastructure.Notification;
using TaskManager.BLL.Interfaces;
using TaskManager.BLL.Services;
using TaskManager.Core.Enums;
using TaskManager.DAL.Entities;
using Xunit;

namespace TaskManager.BLL.Tests.Services
{
    public class NotificationServiceTest : TestBase
    {
        private readonly INotificationService _sut;
        private readonly Mock<ITicketService> _ticketServiceMock;
        private readonly Mock<IEmailSender> _emailSenderMock;

        public NotificationServiceTest()
        {
            SetUp();
            _ticketServiceMock = new Mock<ITicketService>();
            _emailSenderMock = new Mock<IEmailSender>();
            _sut = new NotificationService(_emailSenderMock.Object, _ticketServiceMock.Object);
        }

        [Fact]
        public async Task NotifyTicketWatchersAsync_SendsMailsToAllWatchers_IfDataExistsAndNotificationTypeIsTicketDeleted()
        {
            var testUserDtos = new List<UserDto>
            {
                new UserDto { Email = "email-1" },
                new UserDto { Email = "email-2" }
            };

            var testTicketDto = new TicketDto { Watchers = testUserDtos };

            var testNotificationInfo = new NotificationInfo<TicketDto>
            {
               NotificationType = NotificationType.TicketDeleted,
               OldTicket = testTicketDto
            };

            _ticketServiceMock.Setup(ticketService => ticketService.GetAsync(It.IsAny<Guid>())).ReturnsAsync(testTicketDto);

            await _sut.NotifyTicketWatchersAsync<Ticket>(Guid.NewGuid(), testNotificationInfo);

            var emails = new List<string>
            {
                testUserDtos[0].Email,
                testUserDtos[1].Email
            };

            _emailSenderMock.Verify(emailSender => emailSender.SendAsync(emails, It.IsAny<string>(), "TaskManager notification"), Times.Once);
        }

        [Fact]
        public async Task NotifyTicketWatchersAsync_SendsMailsToAllWatchers_IfDataExistsAndNotificationTypeIsAssigneeChanged()
        {
            var testUserDtos = new List<UserDto>
            {
                new UserDto { Email = "email-1" },
                new UserDto { Email = "email-2" }
            };

            var testTicketDto = new TicketDto { Watchers = testUserDtos };

            var testNotificationInfo = new NotificationInfo<TicketDto>
            {
                NotificationType = NotificationType.AssigneeChanged,
                NewTicket = testTicketDto
            };

            _ticketServiceMock.Setup(ticketService => ticketService.GetAsync(It.IsAny<Guid>())).ReturnsAsync(testTicketDto);

            await _sut.NotifyTicketWatchersAsync<Ticket>(Guid.NewGuid(), testNotificationInfo);

            var emails = new List<string>
            {
                testUserDtos[0].Email,
                testUserDtos[1].Email
            };

            _emailSenderMock.Verify(emailSender => emailSender.SendAsync(emails, It.IsAny<string>(), "TaskManager notification"), Times.Once);
        }

        [Fact]
        public async Task NotifyTicketWatchersAsync_SendsMailsToAllWatchers_IfDataExistsAndNotificationTypeIsTicketUpdated()
        {
            var testUserDtos = new List<UserDto>
            {
                new UserDto { Email = "email-1" },
                new UserDto { Email = "email-2" }
            };

            var testTicketDto = new TicketDto { Watchers = testUserDtos };

            var testNotificationInfo = new NotificationInfo<TicketDto>
            {
                NotificationType = NotificationType.TicketUpdated,
                OldTicket = testTicketDto,
                NewTicket = testTicketDto
            };

            _ticketServiceMock.Setup(ticketService => ticketService.GetAsync(It.IsAny<Guid>())).ReturnsAsync(testTicketDto);

            await _sut.NotifyTicketWatchersAsync<Ticket>(Guid.NewGuid(), testNotificationInfo);

            var emails = new List<string>
            {
                testUserDtos[0].Email,
                testUserDtos[1].Email
            };

            _emailSenderMock.Verify(emailSender => emailSender.SendAsync(emails, It.IsAny<string>(), "TaskManager notification"), Times.Once);
        }

        [Fact]
        public async Task NotifyTicketWatchersAsync_CallsSendAsyncMethod_IfDataExistsAndNotificationTypeIsStatusUpdated()
        {
            var testUserDtos = new List<UserDto>
            {
                new UserDto { Email = "email-1" },
                new UserDto { Email = "email-2" }
            };

            var testTicketDto = new TicketDto { Watchers = testUserDtos };

            var testNotificationInfo = new NotificationInfo<TicketDto>
            {
                NotificationType = NotificationType.StatusUpdated,
                NewTicket = testTicketDto,
                OldTicket = testTicketDto
            };

            _ticketServiceMock.Setup(ticketService => ticketService.GetAsync(It.IsAny<Guid>())).ReturnsAsync(testTicketDto);

            await _sut.NotifyTicketWatchersAsync<Ticket>(Guid.NewGuid(), testNotificationInfo);
            var emailList = new List<string> { "email-1", "email-2" };

            _emailSenderMock.Verify(
                emailSender => emailSender
                .SendAsync(emailList, It.IsAny<string>(), "TaskManager notification"), 
                Times.Exactly(1));
        }
    }
}