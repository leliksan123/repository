﻿using System.Collections.Generic;
using System.Linq;
using Moq;
using TaskManager.BLL.Interfaces;
using TaskManager.BLL.Services;
using TaskManager.DAL.Entities;
using TaskManager.DAL.Interfaces;
using Xunit;

namespace TaskManager.BLL.Tests.Services
{
    public class TagServiceTest : TestBase
    {
        private readonly ITagService _sut;
        private readonly Mock<IUnitOfWork> _unitOfWorkMock;

        public TagServiceTest()
        {
            SetUp();
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _sut = new TagService(_unitOfWorkMock.Object, Mapper);
        }

        [Fact]
        public void GetAll_ReturnsTags_IfDataExists()
        {
            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Tags.GetAll())
                .Returns(new List<Tag>().AsQueryable());

            var res = _sut.GetAll().ToList();

            Assert.NotNull(res);
        }

        [Fact]
        public void Add_AddTag_IfDataExists()
        {
            _unitOfWorkMock
                .Setup(unitOfWork => unitOfWork.Tags.GetAll())
                .Returns(new List<Tag>().AsQueryable());

            _sut.AddAsync(new List<string> { "tag-stub" });

            _unitOfWorkMock.Verify(unitOfWork => unitOfWork.Tags.CreateAsync(It.IsAny<Tag>()), Times.Once);
        }
    }
}