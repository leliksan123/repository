﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using TaskManager.Auth.Exceptions;
using TaskManager.Auth.Models;
using TaskManager.Auth.Models.AccountModels;
using TaskManager.Auth.Services;
using Xunit;

namespace TaskManager.Auth.Tests.Services
{
    public class AccountServiceTest
    {
        private readonly Mock<SignInManager<User>> _mockSignInManager;
        private readonly Mock<UserManager<User>> _mockUserManager;
        private readonly AccountService _accountService;
        public AccountServiceTest()
        {
            _mockUserManager = MockUserManager<User>();
            _mockSignInManager = MockSignInManager(_mockUserManager);
            _accountService = new AccountService(_mockUserManager.Object, _mockSignInManager.Object);
        }

        [Fact]
        public async Task Login_FailedLogin_IfDataInCorrectOrUserDoesntExists()
        {
            const string email = "invalidmail";
            const string pass = "invalidPass";
            const bool rememberMe = false;

            var model = new LoginModel
            {
                Email = email,
                Password = pass,
                RememberMe = rememberMe
            };

            _mockSignInManager
                .Setup(manager => manager.PasswordSignInAsync(email, pass, rememberMe, false))
                .ReturnsAsync(SignInResult.Failed);

            await Assert.ThrowsAsync<AuthException>(() => _accountService.Login(model));
        }

        [Fact]
        public async Task Register_FailedRegister_IfDataInCorrect()
        {
            const string fullName = "Stub Name";
            const string email = "invalidmail";
            const string pass = "invalidPass";

            var model = new RegisterModel
            {
                FullName = fullName,
                Email = email,
                Password = pass
            };

            _mockUserManager
                .Setup(manager => manager.CreateAsync(It.IsAny<User>(), pass))
                .ReturnsAsync(IdentityResult.Failed());

            await Assert.ThrowsAsync<AuthException>(() => _accountService.Register(model));
        }

        [Fact]
        public async Task Register_CallsSignIn_IfDataIsCorrect()
        {
            const string fullName = "Stub Name";
            const string email = "valid@email.com";
            const string pass = "ValidPass_12";
            var model = new RegisterModel
            {
                FullName = fullName,
                Email = email,
                Password = pass
            };

            _mockUserManager
                .Setup(manager => manager.CreateAsync(It.IsAny<User>(), pass))
                .ReturnsAsync(IdentityResult.Success);
            _mockSignInManager
                .Setup(manager => manager.SignInAsync(It.IsAny<User>(), false, null))
                .Returns(Task.CompletedTask).Verifiable();

            await _accountService.Register(model);

            _mockSignInManager.Verify();
        }

        private static Mock<UserManager<TUser>> MockUserManager<TUser>() where TUser : class
        {
            var userStore = new Mock<IUserStore<TUser>>();
            var logger = new Mock<ILogger<UserManager<TUser>>>();
            var passwordHasher = new PasswordHasher<TUser>();
            var normalizer = new UpperInvariantLookupNormalizer();
            var identityErrorDescriber = new IdentityErrorDescriber();

            var mockIdentityOptions = MockIdentityOptions();
            var userValidators = GetUserValidators<TUser>();

            var passwordValidators = new List<PasswordValidator<TUser>>
            {
                new PasswordValidator<TUser>()
            };

            var mockUserManager = new Mock<UserManager<TUser>>(
                userStore.Object,
                mockIdentityOptions.Object,
                passwordHasher,
                userValidators,
                passwordValidators,
                normalizer,
                identityErrorDescriber,
                null,
                logger.Object);

            return mockUserManager;
        }

        private static List<IUserValidator<TUser>> GetUserValidators<TUser>() where TUser : class
        {
            var userValidators = new List<IUserValidator<TUser>>();
            var validator = new Mock<IUserValidator<TUser>>();
            userValidators.Add(validator.Object);
            return userValidators;
        }

        private static Mock<IOptions<IdentityOptions>> MockIdentityOptions()
        {
            var mockIdentityOptions = new Mock<IOptions<IdentityOptions>>();
            var identityOptions = new IdentityOptions
            {
                Lockout = { AllowedForNewUsers = false }
            };

            mockIdentityOptions.Setup(o => o.Value).Returns(identityOptions);

            return mockIdentityOptions;
        }

        private static Mock<SignInManager<TUser>> MockSignInManager<TUser>(IMock<UserManager<TUser>> mockUserManager) where TUser : class
        {
            var context = new Mock<HttpContext>();
            var claimsPrincipalFactory = new Mock<IUserClaimsPrincipalFactory<TUser>>();

            var httpContextAccessor = new HttpContextAccessor
            {
                HttpContext = context.Object
            };

            var signInManagerMock = new Mock<SignInManager<TUser>>(mockUserManager.Object, httpContextAccessor, claimsPrincipalFactory.Object, null, null)
            {
                CallBase = true
            };

            return signInManagerMock;
        }
    }
}