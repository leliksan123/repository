﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TaskManager.BLL.DTO;

namespace TaskManager.BLL.Interfaces
{
    public interface ICommentService
    {
        IEnumerable<CommentDto> GetAll(Guid ticketId);

        Task<CommentDto> GetAsync(Guid id);

        Task UpdateAsync(CommentDto commentDto);

        Task CreateAsync(CommentDto commentDto);

        Task DeleteAsync(Guid id);
    }
}