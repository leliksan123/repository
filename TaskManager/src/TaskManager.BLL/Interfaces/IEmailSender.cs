﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TaskManager.BLL.Interfaces
{
    public interface IEmailSender
    {
        Task SendAsync(IEnumerable<string> emails, string message, string subject);
    }
}