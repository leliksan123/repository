﻿using System;
using TaskManager.BLL.DTO;

namespace TaskManager.BLL.Interfaces
{
    public interface IStatisticService
    {
        UserStatisticDto GetStatisticFiltered(string userName, DateTime startDate);
    }
}