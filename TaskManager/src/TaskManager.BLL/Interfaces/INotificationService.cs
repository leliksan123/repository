﻿using System;
using System.Threading.Tasks;
using TaskManager.BLL.DTO;
using TaskManager.BLL.Infrastructure.Notification;

namespace TaskManager.BLL.Interfaces
{
    public interface INotificationService
    {
        Task NotifyTicketWatchersAsync<TEntity>(Guid ticketId, NotificationInfo<TicketDto> info);
    }
}