﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TaskManager.BLL.DTO;

namespace TaskManager.BLL.Interfaces
{
    public interface IUserService
    {
        IEnumerable<UserDto> GetAll();

        Task<UserDto> GetAsync(Guid id);

        UserDto Get(string userName);

        IEnumerable<string> GetEmailsForSearch(string term);
    }
}