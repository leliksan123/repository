﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using TaskManager.BLL.DTO;

namespace TaskManager.BLL.Interfaces
{
    public interface IUserAssignService
    {
        IEnumerable<CommentDto> AssignUsersToComments(IList<CommentDto> commentDtos);

        CommentDto AssignUserToComment(CommentDto commentDto, ObjectId userId);

        IEnumerable<TicketDto> AssignAssigneesAndWatchersToTicket(IList<TicketDto> ticketDtos);

        TicketDto AssignAssigneeAndWatcher(TicketDto ticketDto);

        Task ValidateTicketAssigneeAsync(TicketDto ticket);
    }
}
