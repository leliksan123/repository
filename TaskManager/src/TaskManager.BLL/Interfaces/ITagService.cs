﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TaskManager.BLL.DTO;

namespace TaskManager.BLL.Interfaces
{
    public interface ITagService
    {
        IEnumerable<TagDto> GetAll();

        Task AddAsync(IEnumerable<string> tags);
    }
}