﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TaskManager.BLL.DTO;

namespace TaskManager.BLL.Interfaces
{
    public interface ITicketLinkService
    {
        IEnumerable<TicketDto> GetLinkedTickets(Guid id);

        IEnumerable<TicketDto> GetUnlinkedTickets(Guid id);

        Task UnlinkTicketsAsync(Guid sourceTicketId, Guid destinationTicketId);

        Task LinkTicketsAsync(Guid sourceTicketId, Guid destinationTicketId);
    }
}