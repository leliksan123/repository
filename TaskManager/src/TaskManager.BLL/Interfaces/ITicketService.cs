﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TaskManager.BLL.DTO;
using TaskManager.Core.Enums;

namespace TaskManager.BLL.Interfaces
{
    public interface ITicketService
    {
        IEnumerable<TicketDto> GetAll();

        int GetCount(FilterDto filterDto);

        IEnumerable<TicketDto> GetAllFiltered(FilterDto filterDto);

        IEnumerable<TicketDto> GetAllFiltered(string userName, DateTime startDate);

        Task<TicketDto> GetAsync(Guid id);

        Task CreateAsync(TicketDto ticketDto);

        Task UpdateAsync(TicketDto ticketDto);

        Task DeleteAsync(Guid id);

        Task UpdateStatusAsync(Guid id, Status status);
    }
}