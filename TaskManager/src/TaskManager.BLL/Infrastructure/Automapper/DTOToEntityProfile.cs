﻿using System.Linq;
using AutoMapper;
using MongoDB.Bson;
using TaskManager.BLL.DTO;
using TaskManager.DAL.Entities;
using TaskManager.DAL.Infrastructure;

namespace TaskManager.BLL.Infrastructure.Automapper
{
    public class DTOToEntityProfile : Profile
    {
        public DTOToEntityProfile()
        {
            CreateMap<TicketDto, Ticket>()
                .ForMember(ticket => ticket.Id, expression => expression.MapFrom(dto => dto.Id.AsObjectId()))
                .ForMember(
                    ticket => ticket.AssigneeId, 
                    expression => expression.MapFrom(dto => dto.Assignee == null ? ObjectId.Empty : dto.Assignee.Id.AsObjectId()))
                .ForMember(
                    ticket => ticket.LinkedTicketIds, 
                    expression => expression.MapFrom(obj => obj.LinkedTicketIds.Select(guid => guid.AsObjectId()).ToList()))
                .ForMember(
                    ticket => ticket.WatcherIds, 
                    expression => expression.MapFrom(dto => dto.Watchers.Select(userDto => userDto.Id.AsObjectId()).ToList()));

            CreateMap<CommentDto, Comment>()
                .ForMember(comment => comment.Id, expression => expression.MapFrom(dto => dto.Id.AsObjectId()))
                .ForMember(comment => comment.TicketId, expression => expression.MapFrom(dto => dto.TicketId.AsObjectId()))
                .ForMember(comment => comment.UserId, expression => expression.MapFrom(dto => dto.UserId.AsObjectId()));

            CreateMap<UserDto, User>()
                .ForMember(user => user.Id, expression => expression.MapFrom(dto => dto.Id.AsObjectId()));
               
            CreateMap<TagDto, Tag>()
                .ForMember(tag => tag.Id, expression => expression.MapFrom(dto => dto.Id.AsObjectId()));
        }
    }
}