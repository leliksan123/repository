﻿using System.Linq;
using AutoMapper;
using TaskManager.BLL.DTO;
using TaskManager.DAL.Entities;
using TaskManager.DAL.Infrastructure;

namespace TaskManager.BLL.Infrastructure.Automapper
{
    public class EntityToDTOProfile : Profile
    {
        public EntityToDTOProfile()
        {
            CreateMap<Ticket, TicketDto>()
                .ForMember(dto => dto.Id, expression => expression.MapFrom(ticket => ticket.Id.AsGuid()))
                .ForMember(dto => dto.Assignee, expression => expression.MapFrom(ticket => new UserDto { Id = ticket.AssigneeId.AsGuid() }))
                .ForMember(
                    dto => dto.LinkedTicketIds, 
                    expression => expression.MapFrom(obj => obj.LinkedTicketIds.Any() ? obj.LinkedTicketIds.Select(guid => guid.AsGuid()).ToList() : null))
                .ForMember(
                    dto => dto.Watchers, 
                    expression => expression.MapFrom(ticket => ticket.WatcherIds.Select(watcherId => new UserDto { Id = watcherId.AsGuid() }).ToList()));

            CreateMap<User, UserDto>()
                .ForMember(dto => dto.Id, expression => expression.MapFrom(user => user.Id.AsGuid()));

            CreateMap<Tag, TagDto>()
                .ForMember(dto => dto.Id, expression => expression.MapFrom(tag => tag.Id.AsGuid()));

            CreateMap<Comment, CommentDto>()
                .ForMember(dto => dto.Id, expression => expression.MapFrom(comment => comment.Id.AsGuid()))
                .ForMember(dto => dto.TicketId, expression => expression.MapFrom(dto => dto.TicketId.AsGuid()))
                .ForMember(dto => dto.UserId, expression => expression.MapFrom(comment => comment.UserId.AsGuid()));
        }
    }
}