﻿using System;
using TaskManager.BLL.DTO;
using TaskManager.Core.Enums;

namespace TaskManager.BLL.Infrastructure.Notification
{
    public static class MessageFormater
    {
        public static string FormNotificationMassage(NotificationInfo<TicketDto> info)
        {
            var message = string.Empty;

            switch (info.NotificationType)
            {
                case NotificationType.StatusUpdated:
                    return StatusUpdatedMessage(info);
                case NotificationType.TicketDeleted:
                    return TicketDeletedMessage(info);
                case NotificationType.AssigneeChanged:
                    return AssigneeChangedMessage(info);
                case NotificationType.TicketUpdated:
                    return TicketUpdatedMessage(info);
            }

            return message;
        }

        private static string StatusUpdatedMessage(NotificationInfo<TicketDto> info)
        {
            return $"Ticket name: {info.NewTicket.Name} (id: {info.NewTicket.Id})\n\nStatus changed from '{info.OldTicket.Status}' to '{info.NewTicket.Status}'";
        }

        private static string TicketUpdatedMessage(NotificationInfo<TicketDto> info)
        {
            return $"Ticket name: {info.NewTicket} (id: {info.NewTicket.Id})\n\n" +
                             $"New ticket info \n\tName: {info.NewTicket.Name}\n\tText: {info.NewTicket.Text}\n\tPriority: {info.NewTicket.Priority}\n\tStatus: {info.NewTicket.Status}\n\n" +
                             $"Old ticket info \n\tName: {info.OldTicket.Name}\n\tText: {info.OldTicket.Text}\n\tPriority: {info.OldTicket.Priority}\n\tStatus: {info.OldTicket.Status}";
        }

        private static string AssigneeChangedMessage(NotificationInfo<TicketDto> info)
        {
            var assigneeInfo = info.NewTicket.Assignee == null || info.NewTicket.Assignee.Id == Guid.Empty
                ? "Assignee was deleted"
                : $"New assignee: {info.NewTicket.Assignee.FullName}";

            return $"Ticket name: {info.NewTicket.Name} (id: {info.NewTicket.Id})\n\n{assigneeInfo}";
        }

        private static string TicketDeletedMessage(NotificationInfo<TicketDto> info)
        {
            return $"Ticket name: {info.OldTicket.Name} (id: {info.OldTicket.Id})\n\nTicket was deleted.";
        }
    }
}