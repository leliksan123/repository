﻿using TaskManager.Core.Enums;

namespace TaskManager.BLL.Infrastructure.Notification
{
    public class NotificationInfo<TEntity>
    {
        public NotificationType NotificationType { get; set; }

        public TEntity NewTicket { get; set; }

        public TEntity OldTicket { get; set; }
    }
}