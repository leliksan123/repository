﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using TaskManager.BLL.DTO;
using TaskManager.BLL.Infrastructure.Exceptions;
using TaskManager.BLL.Interfaces;
using TaskManager.DAL.Entities;
using TaskManager.DAL.Infrastructure;
using TaskManager.DAL.Interfaces;

namespace TaskManager.BLL.Services
{
    public class TicketLinkService : ITicketLinkService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TicketLinkService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public IEnumerable<TicketDto> GetLinkedTickets(Guid id)
        {
            var expression = BuildExpressionForLinkedTickets(id);

            var tickets = _unitOfWork.Tickets.Find(expression);
            var ticketDtos = _mapper.Map<List<TicketDto>>(tickets);

            return ticketDtos;
        }

        public IEnumerable<TicketDto> GetUnlinkedTickets(Guid id)
        {
            var expression = BuildExpressionForUnLinkedTickets(id);

            var tickets = _unitOfWork.Tickets.Find(expression);
            var ticketDtos = _mapper.Map<List<TicketDto>>(tickets);

            return ticketDtos;
        }

        public async Task UnlinkTicketsAsync(Guid sourceTicketId, Guid destinationTicketId)
        {
            var tickets = GetTicketsByIds(sourceTicketId, destinationTicketId);

            var sourceTicket = tickets.FirstOrDefault(ticket => ticket.Id == sourceTicketId.AsObjectId());
            var destinationTicket = tickets.FirstOrDefault(ticket => ticket.Id == destinationTicketId.AsObjectId());

            sourceTicket.LinkedTicketIds = sourceTicket.LinkedTicketIds.Where(id => id != destinationTicketId.AsObjectId());
            destinationTicket.LinkedTicketIds = destinationTicket.LinkedTicketIds.Where(id => id != sourceTicketId.AsObjectId());

            await _unitOfWork.Tickets.UpdateAsync(sourceTicket);
            await _unitOfWork.Tickets.UpdateAsync(destinationTicket);
        }

        public async Task LinkTicketsAsync(Guid sourceTicketId, Guid destinationTicketId)
        {
            var tickets = GetTicketsByIds(sourceTicketId, destinationTicketId);

            var sourceTicket = tickets.FirstOrDefault(ticket => ticket.Id == sourceTicketId.AsObjectId());
            var destinationTicket = tickets.FirstOrDefault(ticket => ticket.Id == destinationTicketId.AsObjectId());

            sourceTicket.LinkedTicketIds = sourceTicket.LinkedTicketIds.Append(destinationTicketId.AsObjectId());
            destinationTicket.LinkedTicketIds = destinationTicket.LinkedTicketIds.Append(sourceTicketId.AsObjectId());

            await _unitOfWork.Tickets.UpdateAsync(sourceTicket);
            await _unitOfWork.Tickets.UpdateAsync(destinationTicket);
        }

        private IQueryable<Ticket> GetTicketsByIds(params Guid[] ticketIds)
        {
            var tickets = _unitOfWork.Tickets.Find(ticket => ticketIds.Contains(ticket.Id.AsGuid()));

            var foundTicketIds = tickets.Select(ticket => ticket.Id);
            var missingIds = ticketIds.Where(guid => !foundTicketIds.Contains(guid.AsObjectId())).ToList();

            if (missingIds.Any())
            {
                var message = missingIds
                    .Aggregate("Tickets with such ids do not exist. Ids: ", (current, id) => current + id.ToString() + ", ");

                throw new EntityNotFoundException(message, "Ticket");
            }

            return tickets;
        }

        private Expression<Func<Ticket, bool>> BuildExpressionForLinkedTickets(Guid id)
        {
            Expression<Func<Ticket, bool>> expression =
                ticket => ticket.LinkedTicketIds.Contains(id.AsObjectId()) && ticket.Id != id.AsObjectId();
            return expression;
        }

        private Expression<Func<Ticket, bool>> BuildExpressionForUnLinkedTickets(Guid id)
        {
            Expression<Func<Ticket, bool>> expression =
                ticket => !ticket.LinkedTicketIds.Contains(id.AsObjectId()) && ticket.Id != id.AsObjectId();
            return expression;
        }
    }
}