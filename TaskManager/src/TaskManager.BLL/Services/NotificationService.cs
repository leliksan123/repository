﻿using System;
using System.Linq;
using System.Threading.Tasks;
using TaskManager.BLL.DTO;
using TaskManager.BLL.Infrastructure.Notification;
using TaskManager.BLL.Interfaces;

namespace TaskManager.BLL.Services
{
    public class NotificationService : INotificationService
    {
        private readonly IEmailSender _emailSender;
        private readonly ITicketService _ticketService;

        public NotificationService(IEmailSender emailSender, ITicketService ticketService)
        {
            _emailSender = emailSender;
            _ticketService = ticketService;
        }

        public async Task NotifyTicketWatchersAsync<TEntity>(Guid ticketId, NotificationInfo<TicketDto> info)
        {
            var message = FormMessage(info);
            var ticketDto = await _ticketService.GetAsync(ticketId);
            var watcherMails = ticketDto.Watchers.Select(w => w.Email);

            await _emailSender.SendAsync(watcherMails, message, "TaskManager notification");
        }

        private static string FormMessage(NotificationInfo<TicketDto> info)
        {
            var message = MessageFormater.FormNotificationMassage(info);

            return message;
        }
    }
}