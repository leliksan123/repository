﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using TaskManager.BLL.DTO;
using TaskManager.BLL.Infrastructure.Exceptions;
using TaskManager.BLL.Interfaces;
using TaskManager.DAL.Entities;
using TaskManager.DAL.Interfaces;

namespace TaskManager.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public IEnumerable<UserDto> GetAll()
        {
            var users = _unitOfWork.Users.GetAll();
            var userDtos = _mapper.Map<List<UserDto>>(users).ToList();

            return userDtos;
        }

        public async Task<UserDto> GetAsync(Guid id)
        {
            var user = await _unitOfWork.Users.GetAsync(id);

            if (user == null)
            {
                throw new EntityNotFoundException($"User with such id does not exist. Id: {id}", "Ticket");
            }

            var userDto = _mapper.Map<UserDto>(user);

            return userDto;
        }

        public UserDto Get(string userName)
        {
            var user = _unitOfWork.Users.Find(u => u.Email == userName).SingleOrDefault();

            if (user == null)
            {
                throw new EntityNotFoundException($"User with such username doesn't exist. UserName: {userName}", nameof(Ticket));
            }

            var userDto = _mapper.Map<UserDto>(user);

            return userDto;
        }

        public IEnumerable<string> GetEmailsForSearch(string term)
        {
            var users = _unitOfWork.Users.GetAll();

            var emails = users
                .Where(a => a.Email.ToLower().Contains(term.ToLower()))
                .Select(a => a.Email);

            return emails;
        }
    }
}