﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using TaskManager.BLL.DTO;
using TaskManager.BLL.Infrastructure.Exceptions;
using TaskManager.BLL.Interfaces;
using TaskManager.DAL.Entities;
using TaskManager.DAL.Interfaces;
using TaskManager.DAL.Infrastructure;

namespace TaskManager.BLL.Services
{
    public class CommentService : ICommentService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IUserAssignService _userAssignService;

        public CommentService(IUnitOfWork unitOfWork, IMapper mapper, IUserAssignService userAssignService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _userAssignService = userAssignService;
        }

        public IEnumerable<CommentDto> GetAll(Guid ticketId)
        {
            var comments = _unitOfWork.Comments.Find(comment => comment.TicketId.Equals(ticketId.AsObjectId()));
            var commentDtos = _mapper.Map<List<CommentDto>>(comments);

            commentDtos = _userAssignService.AssignUsersToComments(commentDtos).ToList();

            return commentDtos;
        }

        public async Task<CommentDto> GetAsync(Guid id)
        {
            var comment = await _unitOfWork.Comments.GetAsync(id);

            if (comment == null)
            {
                throw new EntityNotFoundException($"Comment with such id doesn't exist. Id: {id}", "Comment");
            }

            var commentDto = _mapper.Map<CommentDto>(comment);
            commentDto = _userAssignService.AssignUserToComment(commentDto, comment.UserId);

            return commentDto;
        }

        public async Task CreateAsync(CommentDto commentDto)
        {
            commentDto.Date = DateTime.UtcNow;

            var comment = _mapper.Map<Comment>(commentDto);

            await _unitOfWork.Comments.CreateAsync(comment);
        }

        public async Task UpdateAsync(CommentDto commentDto)
        {
            var comment = _mapper.Map<Comment>(commentDto);

            await _unitOfWork.Comments.UpdateAsync(comment);
        }

        public async Task DeleteAsync(Guid id)
        {
            await _unitOfWork.Comments.DeleteAsync(id);
        }
    }
}