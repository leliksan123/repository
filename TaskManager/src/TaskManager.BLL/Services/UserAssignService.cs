﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MongoDB.Bson;
using TaskManager.BLL.DTO;
using TaskManager.BLL.Infrastructure.Exceptions;
using TaskManager.BLL.Interfaces;
using TaskManager.DAL.Entities;
using TaskManager.DAL.Infrastructure;
using TaskManager.DAL.Interfaces;

namespace TaskManager.BLL.Services
{
    public class UserAssignService : IUserAssignService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UserAssignService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public IEnumerable<CommentDto> AssignUsersToComments(IList<CommentDto> commentDtos)
        {
            var users = GetRequiredUsers(commentDtos);
            var userDtos = _mapper.Map<List<UserDto>>(users);

            foreach (var commentDto in commentDtos)
            {
                commentDto.User = userDtos.First(userDto => userDto.Id == commentDto.UserId);
            }

            return commentDtos;
        }

        public CommentDto AssignUserToComment(CommentDto commentDto, ObjectId userId)
        {
            var commentUser = _unitOfWork.Users.Find(user => user.Id == userId).SingleOrDefault();
            var commentUserDto = _mapper.Map<UserDto>(commentUser);
            commentDto.User = commentUserDto;

            return commentDto;
        }

        public IEnumerable<TicketDto> AssignAssigneesAndWatchersToTicket(IList<TicketDto> ticketDtos)
        {
            var userDtos = GetRequiredUsers(ticketDtos);

            AssignAssignees(ticketDtos, userDtos);
            AssignWatchers(ticketDtos, userDtos);

            return ticketDtos;
        }

        public TicketDto AssignAssigneeAndWatcher(TicketDto ticketDto)
        {
            var users = GetRequiredUsers(ticketDto);

            var userDtos = _mapper.Map<List<UserDto>>(users);

            ticketDto.Assignee = userDtos.SingleOrDefault(userDto => userDto.Id == ticketDto.Assignee.Id);

            var watchersIds = ticketDto.Watchers.Select(watcher => watcher.Id);
            ticketDto.Watchers = userDtos.Where(userDto => watchersIds.Contains(userDto.Id));

            return ticketDto;
        }

        public async Task ValidateTicketAssigneeAsync(TicketDto ticket)
        {
            if (ticket.Assignee != null && ticket.Assignee.Id != Guid.Empty)
            {
                var user = await _unitOfWork.Users.GetAsync(ticket.Assignee.Id);

                if (user == null)
                {
                    throw new EntityNotFoundException($"There is no user with id: { ticket.Assignee.Id } to assign ticket", "User");
                }
            }
        }

        private void AssignWatchers(IEnumerable<TicketDto> ticketDtos, IList<UserDto> userDtos)
        {
            foreach (var ticketDto in ticketDtos)
            {
                var ids = ticketDto.Watchers.Select(userDto => userDto.Id);
                ticketDto.Watchers = userDtos.Where(dto => ids.Contains(dto.Id));
            }
        }

        private void AssignAssignees(IEnumerable<TicketDto> ticketDtos, IList<UserDto> userDtos)
        {
            foreach (var ticketDto in ticketDtos)
            {
                if (ticketDto.Assignee.Id != Guid.Empty)
                {
                    ticketDto.Assignee = userDtos.FirstOrDefault(userDto => userDto.Id == ticketDto.Assignee.Id);
                }
            }
        }

        private List<UserDto> GetRequiredUsers(IList<TicketDto> ticketDtos)
        {
            var assigneeIds = ticketDtos.Select(t => t.Assignee.Id);
            var watchersIds = ticketDtos.SelectMany(t => t.Watchers.Select(w => w.Id));

            var users = _unitOfWork.Users.Find(
                user => watchersIds.Contains(user.Id.AsGuid()) || assigneeIds.Contains(user.Id.AsGuid()));
            var userDtos = _mapper.Map<List<UserDto>>(users);

            return userDtos;
        }

        private IQueryable<User> GetRequiredUsers(TicketDto ticketDto)
        {
            var watchersIds = ticketDto.Watchers.Select(watcher => watcher.Id);
            var users = _unitOfWork.Users.Find(
                user =>
                    (ticketDto.Assignee != null 
                    && user.Id.AsGuid() == ticketDto.Assignee.Id) 
                    || watchersIds.Contains(user.Id.AsGuid()));

            return users;
        }

        private IList<User> GetRequiredUsers(IEnumerable<CommentDto> commentDtos)
        {
            return _unitOfWork.Users.Find(
                user => commentDtos.Select(comment => comment.UserId).Contains(user.Id.AsGuid())).ToList();
        }
    }
}
