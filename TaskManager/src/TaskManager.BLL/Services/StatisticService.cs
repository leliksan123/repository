﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskManager.BLL.DTO;
using TaskManager.BLL.Interfaces;
using TaskManager.Core.Enums;

namespace TaskManager.BLL.Services
{
    public class StatisticService : IStatisticService
    {
        private const int DefaultStatisticDaysNumber = 14;

        private readonly ITicketService _ticketService;

        public StatisticService(ITicketService ticketService)
        {
            _ticketService = ticketService;
        }

        public UserStatisticDto GetStatisticFiltered(string userName, DateTime startDate)
        {
            var tickets = _ticketService.GetAllFiltered(userName, startDate).ToList();

            if (!tickets.Any())
            {
                return null;
            }

            var statisticDaysNumber = GetStatisticDaysNumber(startDate);
            startDate = startDate == default(DateTime) ? DateTime.UtcNow.AddDays(-statisticDaysNumber) : startDate;

            var result = new UserStatisticDto
            {
                Tickets = tickets,
                DateCountOfTicketsDictionary = CountTicketsPerEachDay(tickets, startDate, statisticDaysNumber),
                StatusCountDictionary = GetStatusCountDictionary(tickets),
                PriorityCountDictionary = GetPriorityCountDictionary(tickets)
            };

            return result;
        }

        private int GetStatisticDaysNumber(DateTime startDate)
        {
            return startDate == default(DateTime) ? DefaultStatisticDaysNumber : (DateTime.UtcNow - startDate).Days;
        }

        private Dictionary<Priority, int> GetPriorityCountDictionary(IReadOnlyCollection<TicketDto> tickets)
        {
            var priorityCountDictionary = new Dictionary<Priority, int>();

            foreach (Priority priority in Enum.GetValues(typeof(Priority)))
            {
                var statusCount = tickets.Count(ticketDto => ticketDto.Priority == priority);
                priorityCountDictionary.Add(priority, statusCount);
            }

            return priorityCountDictionary;
        }

        private Dictionary<Status, int> GetStatusCountDictionary(IReadOnlyCollection<TicketDto> tickets)
        {
            var statusCountDictionary = new Dictionary<Status, int>();

            foreach (Status status in Enum.GetValues(typeof(Status)))
            {
                var statusCount = tickets.Count(ticketDto => ticketDto.Status == status);
                statusCountDictionary.Add(status, statusCount);
            }

            return statusCountDictionary;
        }

        private Dictionary<DateTime, int> CountTicketsPerEachDay(
            IReadOnlyCollection<TicketDto> tickets,
            DateTime startDate,
            int daysInterval)
        {
            var ticketsPerEachDay = new Dictionary<DateTime, int>();

            for (var i = 1; i <= daysInterval; i++)
            {
                var date = startDate.AddDays(i);
                var ticketCount = tickets.Count(ticketDto => ticketDto.CreationDate.Date == date.Date);

                ticketsPerEachDay.Add(date, ticketCount);
            }

            return ticketsPerEachDay;
        }
    }
}