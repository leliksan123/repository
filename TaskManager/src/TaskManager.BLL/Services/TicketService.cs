﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using LinqKit;
using TaskManager.BLL.DTO;
using TaskManager.BLL.Infrastructure.Exceptions;
using TaskManager.BLL.Interfaces;
using TaskManager.Core.Enums;
using TaskManager.DAL.Entities;
using TaskManager.DAL.Interfaces;
using TaskManager.DAL.Infrastructure;

namespace TaskManager.BLL.Services
{
    public class TicketService : ITicketService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ITagService _tagService;
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly ITicketLinkService _ticketLinkService;
        private readonly IUserAssignService _userAssignService;

        public TicketService(
            IUnitOfWork unitOfWork, 
            ITagService tagService, 
            IUserService userService, 
            IMapper mapper, 
            ITicketLinkService ticketLinkService,
            IUserAssignService userAssignService)
        {
            _unitOfWork = unitOfWork;
            _tagService = tagService;
            _userService = userService;
            _mapper = mapper;
            _ticketLinkService = ticketLinkService;
            _userAssignService = userAssignService;
        }

        public IEnumerable<TicketDto> GetAllFiltered(string userName, DateTime startDate)
        {
            var user = _userService.Get(userName);
            var tickets = _unitOfWork.Tickets.Find(t => t.AssigneeId.AsGuid() == user.Id && t.CreationDate >= startDate);
            var ticketDtos = _mapper.Map<IEnumerable<TicketDto>>(tickets);

            return ticketDtos;
        }

        public IEnumerable<TicketDto> GetAll()
        {
            var tickets = _unitOfWork.Tickets.GetAll();
            var ticketDtos = _mapper.Map<List<TicketDto>>(tickets);

            ticketDtos = _userAssignService.AssignAssigneesAndWatchersToTicket(ticketDtos).ToList();

            return ticketDtos;
        }

        public int GetCount(FilterDto filterDto)
        {
            var filterExpression = CreateSortingExpression(filterDto);
            var tickets = _unitOfWork.Tickets.GetCount(filterExpression);

            return tickets;
        }

        public IEnumerable<TicketDto> GetAllFiltered(FilterDto filterDto)
        {
            var filterExpression = CreateSortingExpression(filterDto);

            var skipTickets = (filterDto.Page - 1) * filterDto.PageSize;
            var takeTickets = filterDto.PageSize;

            var tickets = _unitOfWork.Tickets.Find(skipTickets, takeTickets, filterExpression);
            var ticketDtos = _mapper.Map<List<TicketDto>>(tickets);

            ticketDtos = _userAssignService.AssignAssigneesAndWatchersToTicket(ticketDtos).ToList();

            return ticketDtos;
        }

        public async Task<TicketDto> GetAsync(Guid id)
        {
            var ticket = await _unitOfWork.Tickets.GetAsync(id);

            if (ticket == null)
            {
                throw new EntityNotFoundException($"Ticket with such id does not exist. Id: {id}", "Ticket");
            }

            var ticketDto = _mapper.Map<TicketDto>(ticket);
            ticketDto = _userAssignService.AssignAssigneeAndWatcher(ticketDto);

            return ticketDto;
        }

        public async Task CreateAsync(TicketDto ticketDto)
        {
            await _userAssignService.ValidateTicketAssigneeAsync(ticketDto);

            var ticket = _mapper.Map<Ticket>(ticketDto);
            await _tagService.AddAsync(ticketDto.Tags);
            ticket.CreationDate = DateTime.UtcNow;

            await _unitOfWork.Tickets.CreateAsync(ticket);
        }

        public async Task UpdateAsync(TicketDto ticketDto)
        {
            await _userAssignService.ValidateTicketAssigneeAsync(ticketDto);

            var ticket = _mapper.Map<Ticket>(ticketDto);
            await _tagService.AddAsync(ticketDto.Tags);

            await _unitOfWork.Tickets.UpdateAsync(ticket);
        }

        public async Task DeleteAsync(Guid id)
        {
            var ticket = await _unitOfWork.Tickets.GetAsync(id);

            if (ticket == null)
            {
                throw new EntityNotFoundException($"Ticket with such id does not exist. Id: {id}", "Ticket");
            }

            var unlinkTicketsTasks = ticket.LinkedTicketIds.Select(
                linkedTicketId => _ticketLinkService.UnlinkTicketsAsync(id, linkedTicketId.AsGuid())).ToArray();

            await Task.WhenAll(unlinkTicketsTasks);

            await _unitOfWork.Tickets.DeleteAsync(id);
        }

        public async Task UpdateStatusAsync(Guid id, Status status)
        {
            var ticket = await _unitOfWork.Tickets.GetAsync(id);

            if (ticket == null)
            {
                throw new EntityNotFoundException($"Ticket with such id does not exist. Id: {id}", "Ticket");
            }

            ticket.Status = status;

            await _unitOfWork.Tickets.UpdateAsync(ticket);
        }

        private Expression<Func<Ticket, bool>> CreateSortingExpression(FilterDto input)
        {
            Expression<Func<Ticket, bool>> expression = p => true;

            if (input.DateFrom.HasValue && input.DateTo.HasValue)
            {
                expression = expression
                    .And(ticket => ticket.CreationDate >= input.DateFrom && ticket.CreationDate <= input.DateTo);
            }

            if (!input.DateFrom.HasValue && input.DateTo.HasValue)
            {
                expression = expression
                    .And(p => p.CreationDate >= DateTime.MinValue && p.CreationDate <= input.DateTo);
            }

            if (input.DateFrom.HasValue && !input.DateTo.HasValue)
            {
                expression = expression
                    .And(p => p.CreationDate >= input.DateFrom && p.CreationDate <= DateTime.UtcNow);
            }

            if (input.SelectedStatuses.Any())
            {
                expression = expression
                    .And(p => input.SelectedStatuses.Contains(p.Status.ToString()));
            }

            if (input.SelectedPriorities.Any())
            {
                expression = expression
                    .And(p => input.SelectedPriorities.Contains(p.Priority.ToString()));
            }

            return expression.Expand();
        }
    }
}