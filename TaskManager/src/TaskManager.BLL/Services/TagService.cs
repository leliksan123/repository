﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using TaskManager.BLL.DTO;
using TaskManager.BLL.Interfaces;
using TaskManager.DAL.Entities;
using TaskManager.DAL.Interfaces;

namespace TaskManager.BLL.Services
{
    public class TagService : ITagService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TagService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public Task AddAsync(IEnumerable<string> tags)
        {
            var allTags = GetAll().ToList();
            var tasks = new List<Task>();

            foreach (var tag in tags)
            {
                if (allTags.All(t => t.Name != tag))
                {
                    var createTask = _unitOfWork.Tags.CreateAsync(new Tag { Name = tag });
                    tasks.Add(createTask);
                }
            }

            return Task.WhenAll(tasks.ToArray());
        }

        public IEnumerable<TagDto> GetAll()
        {
            var tags = _unitOfWork.Tags.GetAll();
            var tagDtos = _mapper.Map<List<TagDto>>(tags);

            return tagDtos;
        }
    }
}