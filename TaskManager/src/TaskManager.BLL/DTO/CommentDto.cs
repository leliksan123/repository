﻿using System;

namespace TaskManager.BLL.DTO
{
    public class CommentDto
    {
        public Guid Id { get; set; }

        public string Text { get; set; }

        public DateTime Date { get; set; }

        public Guid TicketId { get; set; }

        public Guid UserId { get; set; }

        public UserDto User { get; set; }
    }
}