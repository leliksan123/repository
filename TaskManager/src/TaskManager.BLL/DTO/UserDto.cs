﻿using System;

namespace TaskManager.BLL.DTO
{
    public class UserDto
    {
        public Guid Id { get; set; }

        public string Email { get; set; }

        public string FullName { get; set; }
    }
}