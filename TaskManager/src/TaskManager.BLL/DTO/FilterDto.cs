﻿using System;
using System.Collections.Generic;

namespace TaskManager.BLL.DTO
{
    public class FilterDto
    {
        public DateTime? DateFrom { get; set; }

        public DateTime? DateTo { get; set; }

        public List<string> SelectedStatuses { get; set; }

        public List<string> SelectedPriorities { get; set; }

        public int Page { get; set; }

        public int PageSize { get; set; }
    }
}