﻿using System;

namespace TaskManager.BLL.DTO
{
    public class TagDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}