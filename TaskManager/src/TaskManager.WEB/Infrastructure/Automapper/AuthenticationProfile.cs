﻿using AutoMapper;
using TaskManager.Auth.Models.AccountModels;
using TaskManager.BLL.DTO;
using TaskManager.WEB.ViewModels.AccountViewModels;
using TaskManager.WEB.ViewModels.CommentViewModels;

namespace TaskManager.WEB.Infrastructure.Automapper
{
    public class AuthenticationProfile : Profile
    {
        public AuthenticationProfile()
        {
            CreateMap<LoginViewModel, LoginModel>();
            CreateMap<LoginModel, LoginViewModel>();

            CreateMap<RegisterViewModel, RegisterModel>();
            CreateMap<RegisterModel, RegisterViewModel>();

            CreateMap<CommentDto, CommentViewModel>();
        }
    }
}
