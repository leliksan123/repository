﻿using AutoMapper;
using TaskManager.BLL.Infrastructure.Automapper;

namespace TaskManager.WEB.Infrastructure.Automapper
{
    public class AutoMapperConfiguration
    {
        public MapperConfiguration Configure()
        {
            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<DtoToViewModelProfile>();
                cfg.AddProfile<ViewModelToDtoProfile>();
                cfg.AddProfile<AuthenticationProfile>();

                ServiceAutoMapperConfiguration.Initialize(cfg);
            });

            return mapperConfiguration;
        }
    }
}