﻿using System.Linq;
using AutoMapper;
using TaskManager.BLL.DTO;
using TaskManager.WEB.ViewModels.AccountViewModels;
using TaskManager.WEB.ViewModels.BackLogViewModels;
using TaskManager.WEB.ViewModels.CommentViewModels;
using TaskManager.WEB.ViewModels.TicketViewModels;

namespace TaskManager.WEB.Infrastructure.Automapper
{
    public class ViewModelToDtoProfile : Profile
    {
        public ViewModelToDtoProfile()
        {
            CreateMap<UserViewModel, UserDto>(); 
            CreateMap<TicketViewModel, TicketDto>()
                .ForMember(
                ticketDto => ticketDto.LinkedTicketIds,
                expression => expression.MapFrom(viewModel => viewModel.LinkedTickets.Select(ticket => ticket.Id)));
            CreateMap<BackLogViewModel, FilterDto>();
            CreateMap<CommentViewModel, CommentDto>();
        }
    }
}