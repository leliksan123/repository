﻿using System.Linq;
using AutoMapper;
using TaskManager.BLL.DTO;
using TaskManager.WEB.ViewModels.AccountViewModels;
using TaskManager.WEB.ViewModels.StatisticViewModels;
using TaskManager.WEB.ViewModels.TicketViewModels;

namespace TaskManager.WEB.Infrastructure.Automapper
{
    public class DtoToViewModelProfile : Profile
    {
        public DtoToViewModelProfile()
        {            
            CreateMap<TicketDto, TicketViewModel>()
                .ForMember(
                ticketViewModel => ticketViewModel.LinkedTickets, 
                expression => expression.MapFrom(dto => dto.LinkedTicketIds.Select(guid => new TicketViewModel { Id = guid })));
            CreateMap<UserStatisticDto, StatisticViewModel>();
            CreateMap<UserDto, UserViewModel>();
            CreateMap<TicketDto, TicketViewModel>();
        }
    }
}