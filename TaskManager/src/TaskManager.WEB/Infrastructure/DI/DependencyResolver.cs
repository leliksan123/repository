﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TaskManager.Auth.Interfaces;
using TaskManager.Auth.Services;
using TaskManager.BLL.Infrastructure.DI;
using TaskManager.BLL.Interfaces;
using TaskManager.BLL.Services;

namespace TaskManager.WEB.Infrastructure.DI
{
    public static class DependencyResolver
    {
        public static void Resolve(IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<ITicketService, TicketService>();
            services.AddTransient<ICommentService, CommentService>();
            services.AddTransient<ITagService, TagService>();
            services.AddTransient<ITicketLinkService, TicketLinkService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IAccountService, AccountService>();
            services.AddTransient<IAccountManager, AccountManager>();
            services.AddTransient<IStatisticService, StatisticService>();
            services.AddTransient<INotificationService, NotificationService>();

            DependencyResolverModule.Configure(services, configuration);
        }
    }
}