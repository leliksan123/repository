﻿using System;
using System.ComponentModel.DataAnnotations;
using TaskManager.WEB.ViewModels.AccountViewModels;

namespace TaskManager.WEB.ViewModels.CommentViewModels
{
    public class CommentViewModel
    {
        public Guid Id { get; set; }

        [Required]
        public string Text { get; set; }

        public DateTime Date { get; set; }

        public Guid TicketId { get; set; }

        public Guid UserId { get; set; }

        public UserViewModel User { get; set; }
    }
}
