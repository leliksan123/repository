﻿using System.Collections.Generic;
using TaskManager.WEB.ViewModels.AccountViewModels;
using TaskManager.WEB.ViewModels.CommentViewModels;

namespace TaskManager.WEB.ViewModels.TicketViewModels
{
    public class TicketDetailsViewModel
    {
        public IEnumerable<CommentViewModel> Comments { get; set; }

        public TicketViewModel Ticket { get; set; }

        public IEnumerable<UserViewModel> Assignees { get; set; }

        public IEnumerable<TicketViewModel> UnlinkedTickets { get; set; }
    }
}
