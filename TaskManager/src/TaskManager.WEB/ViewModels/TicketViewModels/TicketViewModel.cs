﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TaskManager.Core.Enums;
using TaskManager.WEB.ViewModels.AccountViewModels;

namespace TaskManager.WEB.ViewModels.TicketViewModels
{
    public class TicketViewModel
    {
        public TicketViewModel()
        {
            LinkedTickets = new List<TicketViewModel>();
        }

        public Guid Id { get; set; }

        [Required(ErrorMessage = "Name field is required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Text field is required")]
        public string Text { get; set; }

        public UserViewModel Assignee { get; set; }

        public IEnumerable<UserViewModel> Watchers { get; set; }

        public Status Status { get; set; }

        public string[] Tags { get; set; }

        public string AllTags { get; set; }

        public Priority Priority { get; set; }

        public DateTime CreationDate { get; set; }

        public IEnumerable<TicketViewModel> LinkedTickets { get; set; }
    }
}