﻿using System.Collections.Generic;
using TaskManager.WEB.ViewModels.AccountViewModels;

namespace TaskManager.WEB.ViewModels.TicketViewModels
{
    public class TicketBoardViewModel
    {
        public IEnumerable<TicketViewModel> PlannedTasks { get; set; }

        public IEnumerable<TicketViewModel> ProgressTasks { get; set; }

        public IEnumerable<TicketViewModel> DoneTasks { get; set; }

        public IEnumerable<UserViewModel> Assignees { get; set; }
    }
}