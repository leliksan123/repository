﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace TaskManager.WEB.ViewModels.TicketViewModels
{
    public class TicketEditViewModel
    {
        public TicketViewModel Ticket { get; set; }

        public MultiSelectList TicketsToLink { get; set; }

        public List<Guid> LinkedTickets { get; set; }
    }
}