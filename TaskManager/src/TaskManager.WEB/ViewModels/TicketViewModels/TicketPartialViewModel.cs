﻿namespace TaskManager.WEB.ViewModels.TicketViewModels
{
    public class TicketPartialViewModel
    {
        public TicketBoardViewModel Board { get; set; }

        public TicketViewModel Ticket { get; set; }
    }
}