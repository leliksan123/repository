﻿using System.Collections.Generic;

namespace TaskManager.WEB.ViewModels.TicketViewModels
{
    public class LinkBoardViewModel
    {
        public TicketViewModel Ticket { get; set; }

        public IEnumerable<TicketViewModel> LinkedTickets { get; set; }

        public IEnumerable<TicketViewModel> UnlinkedTickets { get; set; }
    }
}
