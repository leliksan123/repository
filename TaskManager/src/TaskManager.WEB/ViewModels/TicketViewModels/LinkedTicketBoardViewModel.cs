﻿using System.Collections.Generic;

namespace TaskManager.WEB.ViewModels.TicketViewModels
{
    public class LinkedTicketBoardViewModel
    {
        public TicketViewModel LinkedTicket { get; set; }

        public IEnumerable<TicketViewModel> PlannedTasks { get; set; }

        public IEnumerable<TicketViewModel> ProgressTasks { get; set; }

        public IEnumerable<TicketViewModel> DoneTasks { get; set; }
    }
}
