﻿using System;

namespace TaskManager.WEB.ViewModels.AccountViewModels
{
    public class UserViewModel
    {
        public Guid Id { get; set; }

        public string Email { get; set; }

        public string FullName { get; set; }
    }
}