﻿using System;
using System.Collections.Generic;
using TaskManager.Core.Enums;
using TaskManager.WEB.ViewModels.TicketViewModels;

namespace TaskManager.WEB.ViewModels.StatisticViewModels
{
    public class StatisticViewModel
    {      
        public IDictionary<DateTime, int> DateCountOfTicketsDictionary { get; set; }

        public IDictionary<Status, int> StatusCountDictionary { get; set; }

        public IDictionary<Priority, int> PriorityCountDictionary { get; set; }
 
        public IEnumerable<TicketViewModel> Tickets { get; set; }
    }
}
