﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using TaskManager.BLL.Infrastructure.Exceptions;
using TaskManager.WEB.Models;

namespace TaskManager.WEB.Filters
{
    public class ErrorFilter : Attribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled)
            {
                var model = new HandleErrorInfo
                {
                    Message = filterContext.Exception.Message,
                    StackTrace = filterContext.Exception.StackTrace
                };
                var viewDictionary = new ViewDataDictionary(new EmptyModelMetadataProvider(), new ModelStateDictionary())
                {
                    Model = model
                };
                if (filterContext.Exception is EntityNotFoundException)
                {
                    filterContext.Result = new ViewResult
                    {
                        ViewName = "ResourceNotFound",
                        ViewData = viewDictionary
                    };
                }
                else
                {                  
                    filterContext.Result = new ViewResult
                    {
                        ViewName = "Error",
                        ViewData = viewDictionary
                    };
                }

                filterContext.ExceptionHandled = true;
            }
        }
    }
}