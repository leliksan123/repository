﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TaskManager.BLL.Interfaces;

namespace TaskManager.WEB.Controllers
{
    public class TicketLinkController : Controller
    {
        private readonly ITicketLinkService _ticketLinkService;

        public TicketLinkController(ITicketLinkService ticketLinkService)
        {
            _ticketLinkService = ticketLinkService;
        }

        [HttpGet]
        public async Task<IActionResult> LinkTickets(Guid? sourceTicket, Guid? destinationTicket)
        {
            if (!sourceTicket.HasValue || !destinationTicket.HasValue)
            {
                return View("BadRequest");
            }

            await _ticketLinkService.LinkTicketsAsync(sourceTicket.Value, destinationTicket.Value);

            return RedirectToAction("GetTicket", "Ticket", new { ticketId = sourceTicket.Value });
        }

        [HttpGet]
        public async Task<IActionResult> UnLinkTickets(Guid? sourceTicket, Guid? destinationTicket)
        {
            if (!sourceTicket.HasValue || !destinationTicket.HasValue)
            {
                return View("BadRequest");
            }

            await _ticketLinkService.UnlinkTicketsAsync(sourceTicket.Value, destinationTicket.Value);

            return RedirectToAction("GetTicket", "Ticket", new { ticketId = sourceTicket.Value });
        }
    }
}