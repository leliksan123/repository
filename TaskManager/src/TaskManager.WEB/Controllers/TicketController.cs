﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using TaskManager.BLL.DTO;
using TaskManager.BLL.Infrastructure.Notification;
using TaskManager.BLL.Interfaces;
using TaskManager.Core.Enums;
using TaskManager.WEB.ViewModels.AccountViewModels;
using TaskManager.WEB.ViewModels.CommentViewModels;
using TaskManager.WEB.ViewModels.TicketViewModels;

namespace TaskManager.WEB.Controllers
{
    public class TicketController : Controller
    {
        private readonly ITicketService _ticketService;
        private readonly ICommentService _commentService;
        private readonly IUserService _userService;
        private readonly ITagService _tagService;
        private readonly IMapper _mapper;
        private readonly INotificationService _notificationService;
        private readonly ITicketLinkService _ticketLinkService;

        public TicketController(
            ITicketService ticketService, 
            IUserService userService, 
            ITagService tagService,
            INotificationService notificationService,
            IMapper mapper,
            ICommentService commentService,
            ITicketLinkService ticketLinkService)
        {
            _ticketService = ticketService;
            _commentService = commentService;
            _tagService = tagService;
            _userService = userService;
            _mapper = mapper;
            _notificationService = notificationService;
            _ticketLinkService = ticketLinkService;
        }

        [HttpGet]
        public IActionResult Board()
        {
            var ticketDtos = _ticketService.GetAll();
            var ticketViewModels = _mapper.Map<List<TicketViewModel>>(ticketDtos);

            var assigneeDtos = _userService.GetAll();
            var assigneeViewModels = _mapper.Map<List<UserViewModel>>(assigneeDtos);

            var ticketBoard = new TicketBoardViewModel
            {
                PlannedTasks = ticketViewModels.Where(ticketViewModel => ticketViewModel.Status == Status.Open),
                ProgressTasks = ticketViewModels.Where(ticketViewModel => ticketViewModel.Status == Status.InProgress),
                DoneTasks = ticketViewModels.Where(ticketViewModel => ticketViewModel.Status == Status.Done),
                Assignees = assigneeViewModels
            };

            return View(ticketBoard);
        }

        [HttpGet]
        public async Task<IActionResult> GetTicket(Guid ticketId)
        {
            var commentDtos = _commentService.GetAll(ticketId);
            var commentViewModels = _mapper.Map<IEnumerable<CommentViewModel>>(commentDtos);

            var ticketDto = await _ticketService.GetAsync(ticketId);
            var ticketViewModel = _mapper.Map<TicketViewModel>(ticketDto);

            var linkedTicketsDto = _ticketLinkService.GetLinkedTickets(ticketId);
            var linkedTicketsViewModel = _mapper.Map<IEnumerable<TicketViewModel>>(linkedTicketsDto);

            var unlinkedTicketsDto = _ticketLinkService.GetUnlinkedTickets(ticketId);
            var unlinkedTicketsViewModel = _mapper.Map<IEnumerable<TicketViewModel>>(unlinkedTicketsDto);

            ticketViewModel.LinkedTickets = linkedTicketsViewModel;

            var assigneeDtos = _userService.GetAll();
            var assigneeViewModels = _mapper.Map<List<UserViewModel>>(assigneeDtos);

            var ticketDetails = new TicketDetailsViewModel
            {
                Comments = commentViewModels,
                Ticket = ticketViewModel,
                Assignees = assigneeViewModels,
                UnlinkedTickets = unlinkedTicketsViewModel
            };

            return View(ticketDetails);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var tags = _tagService.GetAll();
            var model = new TicketViewModel
            {
                AllTags = string.Join(",", tags.Select(tagDto => tagDto.Name)),
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(TicketViewModel ticketViewModel)
        {
            if (ModelState.IsValid)
            {
                var ticketDto = _mapper.Map<TicketDto>(ticketViewModel);
                await _ticketService.CreateAsync(ticketDto);
               
                return RedirectToAction("Board");
            }

            return View(ticketViewModel);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (!id.HasValue)
            {
                return View("BadRequest");
            }

            var ticketDto = await _ticketService.GetAsync(id.Value);
            var ticketViewModel = _mapper.Map<TicketViewModel>(ticketDto);

            var allTags = _tagService.GetAll();
            ticketViewModel.AllTags = string.Join(",", allTags.Select(tagDto => tagDto.Name));

            return View(ticketViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(TicketViewModel ticketModel)
        {
            if (ModelState.IsValid)
            {
                var oldTicketDto = await _ticketService.GetAsync(ticketModel.Id);
                var ticketDto = _mapper.Map<TicketDto>(ticketModel);

                ticketDto.Assignee = oldTicketDto.Assignee;
                ticketDto.Watchers = oldTicketDto.Watchers;
                ticketDto.CreationDate = oldTicketDto.CreationDate;

                await _ticketService.UpdateAsync(ticketDto);

                await NotifyAsync(NotificationType.TicketUpdated, ticketDto, oldTicketDto);

                return RedirectToAction("Board");
            }

            return View(ticketModel);
        }

        [HttpGet]
        public async Task<IActionResult> EditStatus(Guid? id, Status status)
        {
            if (!id.HasValue)
            {
                return View("BadRequest");
            }

            var oldTicketDto = await _ticketService.GetAsync(id.Value);
            await _ticketService.UpdateStatusAsync(id.Value, status);
            var newTicketDto = await _ticketService.GetAsync(id.Value);

            await NotifyAsync(NotificationType.StatusUpdated, newTicketDto, oldTicketDto);

            return RedirectToAction("Board");
        }

        [HttpGet]
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (!id.HasValue)
            {
                return View("BadRequest");
            }

            var ticketDto = await _ticketService.GetAsync(id.Value);
            await NotifyAsync(NotificationType.TicketDeleted, null, ticketDto);
            await _ticketService.DeleteAsync(ticketDto.Id);

            return RedirectToAction("Board");
        }

        [HttpGet]
        public async Task<IActionResult> Assign(Guid? ticketId, Guid? userId)
        {
            if (!ticketId.HasValue || !userId.HasValue)
            {
                return View("BadRequest");
            }

            var ticketDto = await _ticketService.GetAsync(ticketId.Value);
            ticketDto.Assignee = new UserDto
            {
                Id = userId.Value
            };

            await _ticketService.UpdateAsync(ticketDto);

            var newTicketDto = await _ticketService.GetAsync(ticketId.Value);
            await NotifyAsync(NotificationType.AssigneeChanged, newTicketDto, null);

            return RedirectToAction("GetTicket", new { ticketId = ticketId.Value });
        }

        [HttpGet]
        public async Task<IActionResult> Unassign(Guid? ticketId)
        {
            if (!ticketId.HasValue)
            {
                return View("BadRequest");
            }

            var ticketDto = await _ticketService.GetAsync(ticketId.Value);
            ticketDto.Assignee.Id = Guid.Empty;

            await _ticketService.UpdateAsync(ticketDto);

            await NotifyAsync(NotificationType.AssigneeChanged, ticketDto, null);

            return RedirectToAction("GetTicket", new { ticketId = ticketId.Value });
        }

        [HttpGet]
        public async Task<IActionResult> Watch(Guid? ticketId, Guid? userId)
        {
            if (!ticketId.HasValue || !userId.HasValue)
            {
                return View("BadRequest");
            }

            var ticketDto = await _ticketService.GetAsync(ticketId.Value);
            ticketDto.Watchers = ticketDto.Watchers.Append(new UserDto { Id = userId.Value });

            await _ticketService.UpdateAsync(ticketDto);

            return RedirectToAction("GetTicket", new { ticketId = ticketId.Value });
        }

        [HttpGet]
        public async Task<IActionResult> Unwatch(Guid? ticketId, Guid? userId)
        {
            if (!ticketId.HasValue || !userId.HasValue)
            {
                return View("BadRequest");
            }

            var ticketDto = await _ticketService.GetAsync(ticketId.Value);
            ticketDto.Watchers = ticketDto.Watchers.Where(x => x.Id != userId.Value);

            await _ticketService.UpdateAsync(ticketDto);

            return RedirectToAction("GetTicket", new { ticketId = ticketId.Value });
        }

        private async Task NotifyAsync(NotificationType notificationType, TicketDto newTicketDto, TicketDto oldTicketDto)
        {
            await _notificationService.NotifyTicketWatchersAsync<TicketDto>(
                newTicketDto?.Id ?? oldTicketDto.Id,
                new NotificationInfo<TicketDto>
                {
                    NotificationType = notificationType,
                    NewTicket = newTicketDto,
                    OldTicket = oldTicketDto
                });
        }
    }
}