﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TaskManager.Auth.Exceptions;
using TaskManager.Auth.Interfaces;
using TaskManager.Auth.Models.AccountModels;
using TaskManager.WEB.ViewModels.AccountViewModels;

namespace TaskManager.WEB.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAccountService _accountService;
        private readonly IMapper _mapper;

        public AccountController(IAccountService accountService, IMapper mapper)
        {
            _accountService = accountService;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var loginModel = _mapper.Map<LoginModel>(model);

                try
                {
                    await _accountService.Login(loginModel);

                    return RedirectToAction("Board", "Ticket");
                }
                catch (AuthException ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var registerModel = _mapper.Map<RegisterModel>(model);

                try
                {
                    await _accountService.Register(registerModel);

                    return RedirectToAction("Board", "Ticket");
                }
                catch (AuthException ex)
                {
                    foreach (var error in ex.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error);
                    }                
                }
            }

            return View(model);
        }

        [HttpGet]
        [Authorize]
        public IActionResult LogOff()
        {
            _accountService.LogOff();

            return RedirectToAction("Board", "Ticket");
        }
    }
}