﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using TaskManager.BLL.DTO;
using TaskManager.BLL.Infrastructure.Exceptions;
using TaskManager.BLL.Interfaces;
using TaskManager.WEB.ViewModels.StatisticViewModels;

namespace TaskManager.WEB.Controllers
{
    public class StatisticController : Controller
    {
        private readonly IStatisticService _statisticService;
        private readonly IUserService _userService;

        public StatisticController(IStatisticService statisticService, IUserService userService)
        {
            _statisticService = statisticService;
            _userService = userService;
        }

        [HttpGet]
        public IActionResult Dashboard()
        {
            const int defaultStatisticDays = 14;

            var model = new DashboardViewModel
            {
                StartDate = DateTime.UtcNow.AddDays(-defaultStatisticDays)
            };

            return View(model);
        }

        [HttpGet]
        public JsonResult GetStatistic(string userName, DateTime startDate)
        {
            UserStatisticDto statistic;

            try
            {
                statistic = _statisticService.GetStatisticFiltered(userName, startDate);
            }
            catch (EntityNotFoundException)
            {
                return Json(null);
            }

            var jsonSettings = new JsonSerializerSettings
            {
                DateFormatString = "dd/MM/yyy",
                Formatting = Formatting.Indented,
                Converters = new List<JsonConverter> { new StringEnumConverter() }
            };

            var modelJson = JsonConvert.SerializeObject(statistic, jsonSettings);

            return Json(modelJson);
        }

        [HttpGet]
        public JsonResult AutocompleteSearch(string term)
        {
            var model = _userService.GetEmailsForSearch(term);

            return Json(model);
        }
    }
}