﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using TaskManager.BLL.DTO;
using TaskManager.BLL.Interfaces;
using TaskManager.WEB.ViewModels.BackLogViewModels;
using TaskManager.WEB.ViewModels.TicketViewModels;

namespace TaskManager.WEB.Controllers
{
    public class BacklogController : Controller
    {
        private readonly ITicketService _ticketService;
        private readonly IMapper _mapper;

        public BacklogController(ITicketService ticketService, IMapper mapper)
        {
            _ticketService = ticketService;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index(BackLogViewModel model)
        {
            var ticketDtos = _ticketService.GetAllFiltered(_mapper.Map<FilterDto>(model));
            var countTickets = _ticketService.GetCount(_mapper.Map<FilterDto>(model));
            var pageViewModel = new PageViewModel(countTickets, model.Page, model.PageSize);

            var backlogViewModel = new BackLogViewModel
            {
                Tickets = _mapper.Map<IEnumerable<TicketViewModel>>(ticketDtos),
                SelectedStatuses = model.SelectedStatuses,
                SelectedPriorities = model.SelectedPriorities,
                PageViewModel = pageViewModel
            };

            return View(backlogViewModel);
        }
    }
}