﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TaskManager.BLL.Interfaces;
using AutoMapper;
using TaskManager.BLL.DTO;
using TaskManager.Auth.Interfaces;
using Microsoft.AspNetCore.Authorization;
using TaskManager.WEB.ViewModels.CommentViewModels;

namespace TaskManager.WEB.Controllers
{
    public class CommentController : Controller
    {
        private readonly IAccountManager _accountManager;
        private readonly ICommentService _commentService;
        private readonly IMapper _mapper;

        public CommentController(ICommentService commentService, IAccountManager accountManager, IMapper mapper)
        {
            _commentService = commentService;
            _accountManager = accountManager;
            _mapper = mapper;
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Add(CommentViewModel comment)
        {
            if (ModelState.IsValid)
            {
                var requestUrl = Request.Headers["Referer"].ToString();

                comment.UserId = _accountManager.GetUserId(User);
                var commentDto = _mapper.Map<CommentDto>(comment);

                await _commentService.CreateAsync(commentDto);

                return Redirect(requestUrl);
            }

            return View("~/Views/Partials/Comment/_Add.cshtml");
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(Guid? commentId, Guid? ticketId)
        {
            if (!commentId.HasValue || !ticketId.HasValue)
            {
                return View("BadRequest");
            }

            await _commentService.DeleteAsync(commentId.Value);

            return RedirectToAction("GetTicket", "Ticket", new { ticketId = ticketId.Value });
        }
    }
}