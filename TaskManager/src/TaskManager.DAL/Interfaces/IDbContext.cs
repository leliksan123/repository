﻿using MongoDB.Driver;
using TaskManager.DAL.Entities;

namespace TaskManager.DAL.Interfaces
{
    public interface IDbContext
    {
        IMongoCollection<TEntity> GetCollection<TEntity>(string collectionName) where TEntity : BaseType;
    }
}