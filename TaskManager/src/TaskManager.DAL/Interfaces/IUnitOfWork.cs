﻿using TaskManager.DAL.Entities;

namespace TaskManager.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<Ticket> Tickets { get; }

        IRepository<Tag> Tags { get; }

        IRepository<Comment> Comments { get; }

        IRepository<User> Users { get; }
    }
}