﻿using System;
using System.Linq;
using MongoDB.Bson;

namespace TaskManager.DAL.Infrastructure
{
    public static class IdConverter
    {
        public const int ObjectIdSize = 16;
        public const int GuidSize = 12;
        public const int ArrayStart = 0;

        public static Guid AsGuid(this ObjectId objectId)
        {
            if (objectId == ObjectId.Empty)
            {
                return Guid.Empty;
            }

            var array = new byte[ObjectIdSize];
            objectId.ToByteArray().CopyTo(array, ArrayStart);
            var gid = new Guid(array);

            return gid;
        }

        public static ObjectId AsObjectId(this Guid guid)
        {
            if (guid == Guid.Empty)
            {
                return ObjectId.Empty;
            }

            var bytes = guid.ToByteArray().Take(GuidSize).ToArray();
            var oid = new ObjectId(bytes);

            return oid;
        }
    }
}