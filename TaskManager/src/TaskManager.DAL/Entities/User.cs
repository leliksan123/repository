﻿using MongoDB.Bson.Serialization.Attributes;

namespace TaskManager.DAL.Entities
{
    [BsonIgnoreExtraElements]
    public class User : BaseType
    {
        public string Email { get; set; }

        public string FullName { get; set; }

        public override string CollectionName => "users";
    }
}