﻿using System.Collections.Generic;
using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using TaskManager.Core.Enums;

namespace TaskManager.DAL.Entities
{
    public class Ticket : BaseType
    {
        public Ticket()
        {
            LinkedTicketIds = new List<ObjectId>();
            Tags = new List<string>();
        }

        public string Name { get; set; }

        public string Text { get; set; }

        public ObjectId AssigneeId { get; set; }

        public IEnumerable<ObjectId> WatcherIds { get; set; }

        [BsonDateTimeOptions(Representation = BsonType.DateTime)]
        public DateTime CreationDate { get; set; }

        public Status Status { get; set; }

        public Priority Priority { get; set; }

        public IEnumerable<ObjectId> LinkedTicketIds { get; set; }

        public IEnumerable<string> Tags { get; set; }

        public override string CollectionName => "tickets";
    }
}