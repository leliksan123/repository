﻿using System;
using MongoDB.Bson;

namespace TaskManager.DAL.Entities
{
    public class Comment : BaseType
    {
        public string Text { get; set; }

        public DateTime Date { get; set; }

        public ObjectId TicketId { get; set; }

        public ObjectId UserId { get; set; }

        public override string CollectionName => "comments";
    }
}