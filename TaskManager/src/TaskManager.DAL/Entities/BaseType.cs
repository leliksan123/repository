﻿using MongoDB.Bson;

namespace TaskManager.DAL.Entities
{
    public abstract class BaseType
    {
        public ObjectId Id { get; set; }

        public abstract string CollectionName { get; }
    }
}