﻿using System;
using TaskManager.DAL.Entities;
using TaskManager.DAL.Interfaces;
using TaskManager.DAL.Repositories;

namespace TaskManager.DAL.UnitOfWorks
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly Lazy<IRepository<Ticket>> _ticketRepository;

        private readonly Lazy<IRepository<Comment>> _commentRepository;

        private readonly Lazy<IRepository<Tag>> _tagRepository;

        private readonly Lazy<IRepository<User>> _userRepository;

        public UnitOfWork(IDbContext context)
        {
            var db = context;
            _ticketRepository = new Lazy<IRepository<Ticket>>(() => new CommonRepository<Ticket>(db));
            _tagRepository = new Lazy<IRepository<Tag>>(() => new CommonRepository<Tag>(db));
            _commentRepository = new Lazy<IRepository<Comment>>(() => new CommonRepository<Comment>(db));
            _userRepository = new Lazy<IRepository<User>>(() => new CommonRepository<User>(db));
        }

        public IRepository<Ticket> Tickets => _ticketRepository.Value;

        public IRepository<Comment> Comments => _commentRepository.Value;

        public IRepository<User> Users => _userRepository.Value;

        public IRepository<Tag> Tags => _tagRepository.Value;
    }
}