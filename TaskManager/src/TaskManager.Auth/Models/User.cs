﻿using Microsoft.AspNetCore.Identity.MongoDB;

namespace TaskManager.Auth.Models
{
    public class User : IdentityUser
    {
        public string FullName { get; set; }
    }
}