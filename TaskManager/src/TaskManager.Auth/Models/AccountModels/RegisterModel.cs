﻿namespace TaskManager.Auth.Models.AccountModels
{
    public class RegisterModel
    {
        public string FullName { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }
    }
}