﻿using System;
using System.Security.Claims;

namespace TaskManager.Auth.Interfaces
{
    public interface IAccountManager
    {
        string GetUserName(ClaimsPrincipal claim);

        bool IsSignedIn(ClaimsPrincipal claim);

        Guid GetUserId(ClaimsPrincipal claim);
    }
}