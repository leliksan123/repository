﻿using System.Threading.Tasks;
using TaskManager.Auth.Models.AccountModels;

namespace TaskManager.Auth.Interfaces
{
    public interface IAccountService
    {
        Task Login(LoginModel model);

        Task Register(RegisterModel model);

        Task LogOff();
    }
}