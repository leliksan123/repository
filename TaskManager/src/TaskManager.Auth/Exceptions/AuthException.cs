﻿using System;
using System.Collections.Generic;

namespace TaskManager.Auth.Exceptions
{
    public class AuthException : Exception
    {
        public AuthException(string message = "Authentication error", IEnumerable<string> errors = null) : base(message)
        {
            Errors = errors ?? new List<string>();
        }

        public IEnumerable<string> Errors { get; set; }
    }
}