﻿using System;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using MongoDB.Bson;
using TaskManager.Auth.Infrastructure;
using TaskManager.Auth.Interfaces;
using TaskManager.Auth.Models;

namespace TaskManager.Auth.Services
{
    public class AccountManager : IAccountManager
    {
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;

        public AccountManager(SignInManager<User> signInManager, UserManager<User> userManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;
        }

        public string GetUserName(ClaimsPrincipal claim)
        {
            var userName = _userManager.GetUserName(claim);

            return userName;
        }

        public bool IsSignedIn(ClaimsPrincipal claim)
        {
            var isSignedIn = _signInManager.IsSignedIn(claim);

            return isSignedIn;
        }

        public Guid GetUserId(ClaimsPrincipal claim)
        {
            var userId = _userManager.GetUserId(claim);
            var objectId = ObjectId.Parse(userId);

            return objectId.AsGuid();
        }
    }
}