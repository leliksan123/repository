﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using TaskManager.Auth.Exceptions;
using TaskManager.Auth.Interfaces;
using TaskManager.Auth.Models;
using TaskManager.Auth.Models.AccountModels;

namespace TaskManager.Auth.Services
{
    public class AccountService : IAccountService
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        public AccountService(
            UserManager<User> userManager,
            SignInManager<User> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public async Task Login(LoginModel model)
        {
            var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false);

            if (!result.Succeeded)
            {
                throw new AuthException("Wrong Email or Password");
            }
        }

        public async Task Register(RegisterModel model)
        {
            var user = new User { UserName = model.Email, Email = model.Email, FullName = model.FullName };

            var result = await _userManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
            {
                throw new AuthException(errors: result.Errors.Select(error => error.Description));
            }

            await _signInManager.SignInAsync(user, false);
        }

        public async Task LogOff()
        {
            await _signInManager.SignOutAsync();
        }
    }
}