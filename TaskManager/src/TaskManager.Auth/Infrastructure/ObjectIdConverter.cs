﻿using System;
using MongoDB.Bson;

namespace TaskManager.Auth.Infrastructure
{
    public static class ObjectIdConverter
    {
        public const int ObjectIdSize = 16;
        public const int ArrayStart = 0;

        public static Guid AsGuid(this ObjectId objectId)
        {
            if (objectId == ObjectId.Empty)
            {
                return Guid.Empty;
            }

            var array = new byte[ObjectIdSize];
            objectId.ToByteArray().CopyTo(array, ArrayStart);
            var gid = new Guid(array);

            return gid;
        }
    }
}