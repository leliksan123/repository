﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using TaskManager.BLL.DTO;
using TaskManager.BLL.Interfaces;
using TaskManager.WEB.Controllers;
using TaskManager.WEB.ViewModels.BackLogViewModels;
using Xunit;

namespace TaskManager.WEB.Tests.Controllers
{
    public class BacklogControllerTest : TestBase
    {
        private readonly Mock<ITicketService> _ticketServiceMock;
        private readonly BacklogController _sut;

        public BacklogControllerTest()
        {
            SetUp();
            _ticketServiceMock = new Mock<ITicketService>();
            _sut = new BacklogController(_ticketServiceMock.Object, Mapper);
        }

        [Fact]
        public void Index_ShouldReturnView_WhenModelIsValid()
        {
            var model = new BackLogViewModel();

            var result = _sut.Index(model);

            Assert.IsType(typeof(ViewResult), result);
        }

        [Fact]
        public void Index_ShouldCallGetCount_WhenModelIsValid()
        {
            var model = new BackLogViewModel();

            _sut.Index(model);

            _ticketServiceMock.Verify(x => x.GetCount(It.IsAny<FilterDto>()), Times.Once);
        }

        [Fact]
        public void Index_CallGetAllFiltered_WhenModelIsValid()
        {
            var model = new BackLogViewModel();

            _sut.Index(model);

            _ticketServiceMock.Verify(x => x.GetAllFiltered(It.IsAny<FilterDto>()), Times.Once);
        }
    }
}