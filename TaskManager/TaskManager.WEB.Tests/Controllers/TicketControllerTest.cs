﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using TaskManager.BLL.DTO;
using TaskManager.BLL.Infrastructure.Exceptions;
using TaskManager.BLL.Interfaces;
using TaskManager.Core.Enums;
using TaskManager.WEB.Controllers;
using Xunit;
using TaskManager.WEB.ViewModels.TicketViewModels;

namespace TaskManager.WEB.Tests.Controllers
{
    public class TicketControllerTest : TestBase
    {
        private readonly Mock<ITicketService> _ticketServiceMock;
        private readonly Mock<ITagService> _tagServiceMock;
        private readonly Mock<IUserService> _userServiceMock;
        private readonly TicketController _sut;

        public TicketControllerTest()
        {
            SetUp();
            _ticketServiceMock = new Mock<ITicketService>();
            _tagServiceMock = new Mock<ITagService>();
            var commentServiceMock = new Mock<ICommentService>();
            _userServiceMock = new Mock<IUserService>();
            var notificationServiceMock = new Mock<INotificationService>();
            _sut = new TicketController(
                _ticketServiceMock.Object, 
                _userServiceMock.Object, 
                _tagServiceMock.Object, 
                notificationServiceMock.Object, 
                Mapper,
                commentServiceMock.Object);
        }

        [Fact]
        public async Task Create_ReturnsRedirectToActionResult_WhenValidTicketViewModel()
        {
            var model = new TicketViewModel();

            var result = await _sut.Create(model);

            Assert.Equal(typeof(RedirectToActionResult), result.GetType());
        }

        [Fact]
        public async Task Create_ReturnsViewResult_WhenNotValidTicketViewModel()
        {
            var model = new TicketViewModel
            {
                Id = Guid.NewGuid()
            };

            _sut.ModelState.AddModelError(string.Empty, string.Empty);
            var result = await _sut.Create(model);

            Assert.Equal(typeof(ViewResult), result.GetType());
        }

        [Fact]
        public async Task Edit_ReturnsRedirectToActionResult_WhenValidTicketViewModel()
        {
            var ticketDto = new TicketDto { Id = Guid.NewGuid() };
            var model = new TicketViewModel();

            _ticketServiceMock
                .Setup(manager => manager.GetAsync(It.IsAny<Guid>()))
                .ReturnsAsync(ticketDto);

            var result = await _sut.Edit(model);

            Assert.Equal(typeof(RedirectToActionResult), result.GetType());
        }

        [Fact]
        public async Task Edit_ReturnsViewResult_WhenNotValidTicketViewModel()
        {
            var model = new TicketViewModel();

            _sut.ModelState.AddModelError(string.Empty, string.Empty);

            var result = await _sut.Edit(model);

            Assert.Equal(typeof(ViewResult), result.GetType());
        }

        [Fact]
        public async Task Delete_DeleteTicket_WhenTicketIsExicted()
        {
            var ticketId = Guid.NewGuid();
            var model = new TicketDto { Id = Guid.NewGuid() };

            _ticketServiceMock
                .Setup(manager => manager.GetAsync(It.IsAny<Guid>()))
                .ReturnsAsync(model);

            var result = await _sut.Delete(ticketId);

            Assert.Equal(typeof(RedirectToActionResult), result.GetType());
        }

        [Fact]
        public async Task Delete_ReturnsViewResult_WhenNullInput()
        {
            var result = await _sut.Delete(null);

            Assert.Equal(typeof(ViewResult), result.GetType());
        }

        [Fact]
        public async Task GetTicket_ReturnsViewWithTicketAndComments_IfIdIsValid()
        {
            var guid = Guid.NewGuid();

            var result = await _sut.GetTicket(guid);
            var view = result as ViewResult;

            Assert.NotNull(view?.Model);
        }

        [Fact]
        public async Task Assign_UpdatesTicketAssignee_PassedValidIds()
        {
            var ticketId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var ticketToUpdate = new TicketDto { Id = ticketId };
            var assignee = new UserDto { Id = userId };

            _ticketServiceMock
                .Setup(service => service.GetAsync(ticketId))
                .ReturnsAsync(ticketToUpdate);
            _userServiceMock
                .Setup(service => service.GetAsync(userId))
                .ReturnsAsync(assignee);

            await _sut.Assign(ticketId, userId);
            
            Assert.Equal(assignee.Id, ticketToUpdate.Assignee.Id);
        }

        [Fact]
        public async Task Assign_ThrowsEntityNotFoundException_IfTicketIdIsInvalid()
        {
            var ticketId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            _ticketServiceMock
                .Setup(service => service.GetAsync(ticketId))
                .ThrowsAsync(new EntityNotFoundException(string.Empty, string.Empty));

            await Assert.ThrowsAsync<EntityNotFoundException>(() => _sut.Assign(ticketId, userId));
        }

        [Fact]
        public async Task Assign_ThrowsEntityNotFoundException_IfUserIdIsInvalid()
        {
            var ticketId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var ticketToUpdate = new TicketDto { Id = ticketId };

            _ticketServiceMock
                .Setup(service => service.GetAsync(ticketId))
                .ReturnsAsync(ticketToUpdate);
            _ticketServiceMock
                .Setup(service => service.UpdateAsync(ticketToUpdate))
                .Throws(new EntityNotFoundException(string.Empty, string.Empty));

            await Assert.ThrowsAsync<EntityNotFoundException>(() => _sut.Assign(ticketId, userId));
        }

        [Fact]
        public async Task Unassign_RemovesTicketAssignee_PassedValidId()
        {
            var ticketId = Guid.NewGuid();

            var ticketToUpdate = new TicketDto
            {
                Id = ticketId,
                Assignee = new UserDto
                {
                    Id = Guid.NewGuid()
                }
            };

            _ticketServiceMock
                .Setup(service => service.GetAsync(ticketId))
                .ReturnsAsync(ticketToUpdate);

            await _sut.Unassign(ticketId);
            
            Assert.Equal(ticketToUpdate.Assignee.Id, Guid.Empty);
        }

        [Fact]
        public async Task Unassign_CallsUpdate_PassedValidId()
        {
            var ticketId = Guid.NewGuid();

            var ticketToUpdate = new TicketDto
            {
                Id = ticketId,
                Assignee = new UserDto
                {
                    Id = Guid.NewGuid()
                }
            };

            _ticketServiceMock
                .Setup(service => service.GetAsync(ticketId))
                .ReturnsAsync(ticketToUpdate);

            await _sut.Unassign(ticketId);
            
            _ticketServiceMock.Verify(uow => uow.UpdateAsync(ticketToUpdate));
        }

        [Fact]
        public async Task Unassign_ThrowsEntityNotFoundException_IfTicketIdIsInvalid()
        {
            var ticketId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            _ticketServiceMock
                .Setup(service => service.GetAsync(ticketId))
                .ThrowsAsync(new EntityNotFoundException(string.Empty, string.Empty));

            await Assert.ThrowsAsync<EntityNotFoundException>(() => _sut.Assign(ticketId, userId));
        }

        [Fact]
        public async Task EditStatus_ReturnsBadRequestView_WhenIdNull()
        {
            var result = await _sut.EditStatus(null, It.IsAny<Status>());
            var viewResult = result as ViewResult;

            Assert.Equal("BadRequest", viewResult?.ViewName);
        }

        [Fact]
        public async Task EditStatus_ReturnsRedirectToBoardAction_WhenArgumentsValid()
        {
            var model = new TicketDto { Id = Guid.NewGuid() };
            _ticketServiceMock.Setup(manager => manager.GetAsync(It.IsAny<Guid>())).ReturnsAsync(model);

            var result = await _sut.EditStatus(Guid.NewGuid(), It.IsAny<Status>());
            var redirectToActionResult = result as RedirectToActionResult;

            Assert.Equal("Board", redirectToActionResult?.ActionName);
        }

        [Fact]
        public async Task Watch_AddsWatcher_WhenArgumentsValid()
        {
            var ticketId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var testTicket = new TicketDto
            {
                Id = ticketId,
                Name = "name-stub",
                Watchers = new List<UserDto>
                {
                    new UserDto(),
                    new UserDto()
                }
            };

            var initialWatchersCount = testTicket.Watchers.Count();

            _ticketServiceMock.Setup(service => service.GetAsync(ticketId)).ReturnsAsync(testTicket);

            await _sut.Watch(ticketId, userId);

            Assert.Equal(testTicket.Watchers.Count(), ++initialWatchersCount);
        }

        [Fact]
        public async Task Watch_ThrowsEntityNotFoundException_IfTicketIdIsInvalid()
        {
            var ticketId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            _ticketServiceMock
                .Setup(service => service.GetAsync(ticketId)).ThrowsAsync(new EntityNotFoundException(string.Empty, string.Empty));

            await Assert.ThrowsAsync<EntityNotFoundException>(() => _sut.Watch(ticketId, userId));
        }

        [Fact]
        public async Task Unwatch_RemovesWatcher_WhenArgumentsValid()
        {
            var ticketId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var testTicket = new TicketDto
            {
                Id = ticketId,
                Name = "name-stub",
                Watchers = new List<UserDto>
                {
                    new UserDto { Id = userId },
                    new UserDto()
                }
            };

            var initialWatchersCount = testTicket.Watchers.Count();

            _ticketServiceMock.Setup(service => service.GetAsync(ticketId)).ReturnsAsync(testTicket);

            await _sut.Unwatch(ticketId, userId);

            Assert.Equal(testTicket.Watchers.Count(), --initialWatchersCount);
        }

        [Fact]
        public async Task Unwatch_ThrowsEntityNotFoundException_IfTicketIdIsInvalid()
        {
            var ticketId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            _ticketServiceMock
                .Setup(service => service.GetAsync(ticketId))
                .ThrowsAsync(new EntityNotFoundException(string.Empty, string.Empty));

            await Assert.ThrowsAsync<EntityNotFoundException>(() => _sut.Unwatch(ticketId, userId));
        }

        [Fact]
        public void Board_CallsGetAllUsers()
        {
            _sut.Board();

            _userServiceMock.Verify(x => x.GetAll(), Times.Once);
        }

        [Fact]
        public void Board_ReturnsViewResult_WhenDataExists()
        {
            _ticketServiceMock.Setup(x => x.GetAll()).Returns(new List<TicketDto>());
            _userServiceMock.Setup(x => x.GetAll()).Returns(new List<UserDto>());

            var result = _sut.Board();

            Assert.IsType(typeof(ViewResult), result);
        }

        [Fact]
        public void Create_ReturnsNotNullListInModel_WhenTagsExist()
        {
            var tag = new TagDto
            {
                Name = "stub"
            };

            var tags = new List<TagDto> { tag };

            _tagServiceMock.Setup(x => x.GetAll()).Returns(tags);

            var result = _sut.Create();
            var viewResult = result as ViewResult;
            var viewModel = viewResult?.Model as TicketViewModel;

            Assert.NotNull(viewModel?.AllTags);
        }

        [Fact]
        public async Task Edit_ReturnsBadReguestView_WhenIdIsNull()
        {
            var result = await _sut.Edit(It.IsAny<Guid?>());

            var viewResult = result as ViewResult;

            Assert.Equal("BadRequest", viewResult?.ViewName);
        }

        [Fact]
        public async Task Edit_CallsGetTicket_WhenIdIsValid()
        {
            var id = Guid.NewGuid();
            var ticket = new TicketDto();

            _ticketServiceMock.Setup(x => x.GetAsync(It.IsAny<Guid>())).ReturnsAsync(ticket);

            await _sut.Edit(id);

            _ticketServiceMock.Verify(x => x.GetAsync(It.IsAny<Guid>()), Times.Once);
        }

        [Fact]
        public async Task Assign_ReturnsBadReguestView_WhenIdIsNull()
        {
            var result = await _sut.Assign(It.IsAny<Guid?>(), It.IsAny<Guid?>());

            var viewResult = result as ViewResult;

            Assert.Equal("BadRequest", viewResult?.ViewName);
        }

        [Fact]
        public async Task Edit_CallsGetTags_WhenIdIsValid()
        {
            var id = Guid.NewGuid();
            var ticket = new TicketDto();

            _ticketServiceMock.Setup(x => x.GetAsync(It.IsAny<Guid>())).ReturnsAsync(ticket);

            await _sut.Edit(id);

            _tagServiceMock.Verify(x => x.GetAll(), Times.Once);
        }
    }
}