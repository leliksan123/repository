﻿using System;
using Microsoft.AspNetCore.Mvc;
using Moq;
using TaskManager.BLL.Infrastructure.Exceptions;
using TaskManager.BLL.Interfaces;
using TaskManager.WEB.Controllers;
using TaskManager.WEB.ViewModels.StatisticViewModels;
using Xunit;

namespace TaskManager.WEB.Tests.Controllers
{
    public class StatisticControllerTest
    {
        private readonly Mock<IUserService> _userServiceMock;
        private readonly Mock<IStatisticService> _statisticServiceMock;
        private readonly StatisticController _sut;

        public StatisticControllerTest()
        {
            _statisticServiceMock = new Mock<IStatisticService>();
            _userServiceMock = new Mock<IUserService>();

            _sut = new StatisticController(_statisticServiceMock.Object, _userServiceMock.Object);
        }

        [Fact]
        public void Dashboard_ReturnsViewWithStartDate()
        {
            const int defaultStatisticDays = 14;
            var date = DateTime.UtcNow.AddDays(-defaultStatisticDays).Date;
            
            var result = _sut.Dashboard() as ViewResult;
            var model = result?.Model as DashboardViewModel;

            Assert.Equal(date, model?.StartDate.Date);
        }

        [Fact]
        public void GetStatistic_ReturnsJsonWithoutData_WhenInputValueNotValid()
        {
            var date = DateTime.MaxValue;

            var result = _sut.GetStatistic(null, date);
         
            Assert.Null(result.ContentType);
        }

        [Fact]
        public void GetStatistic_ReturnsJsonWithoutData_WhenEntityNotFoundException()
        {
            var date = DateTime.MaxValue;
            var userName = "name-stub";

            _statisticServiceMock.Setup(x => x.GetStatisticFiltered(It.IsAny<string>(), It.IsAny<DateTime>()))
                .Throws(new EntityNotFoundException(string.Empty, string.Empty));

            var result = _sut.GetStatistic(userName, date);

            Assert.Null(result.ContentType);
        }

        [Fact]
        public void Dashboard_ReturnsJsonWithData_WhenInputValueIsValid()
        {
            var date = DateTime.MaxValue;
            var userName = "name-stub";

            var result = _sut.GetStatistic(userName, date);

            Assert.NotNull(result.Value);
        }

        [Fact]
        public void AutocompleteSearch_CallsGetEmailsForSearch_WhenInputValueIsValid()
        {
            const string search = "search-stub";

            _sut.AutocompleteSearch(search);

            _userServiceMock.Verify(m => m.GetEmailsForSearch(It.IsAny<string>()), Times.Once);
        }
    }
}