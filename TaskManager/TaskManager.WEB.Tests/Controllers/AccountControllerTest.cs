﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using TaskManager.Auth.Exceptions;
using TaskManager.Auth.Interfaces;
using TaskManager.Auth.Models.AccountModels;
using TaskManager.WEB.Controllers;
using TaskManager.WEB.ViewModels.AccountViewModels;
using Xunit;

namespace TaskManager.WEB.Tests.Controllers
{
    public class AccountControllerTest : TestBase
    {
        private readonly AccountController _sut;
        private readonly Mock<IAccountService> _accountServiceMock;

        public AccountControllerTest()
        {
            SetUp();
            _accountServiceMock = new Mock<IAccountService>();
           _sut = new AccountController(_accountServiceMock.Object, Mapper);  
        }

        [Fact]
        public async Task Login_CallsLoginMethod_WhenModelIsValid()
        {
            var model = new LoginViewModel();

            await _sut.Login(model);

            _accountServiceMock.Verify(x => x.Login(It.IsAny<LoginModel>()), Times.Once);
        }

        [Fact]
        public async Task Login_ReturnsRedirectToAction_WhenModelIsValid()
        {
            var model = new LoginViewModel();

            var result = await _sut.Login(model);

            Assert.IsType(typeof(RedirectToActionResult), result);
        }

        [Fact]
        public async Task Login_AddedModelStateRecord_WhenThrowAuthException()
        {
            var model = new LoginViewModel();

            _accountServiceMock.Setup(x => x.Login(It.IsAny<LoginModel>())).Throws(new AuthException());

            var result = await _sut.Login(model) as ViewResult;

            Assert.True(result?.ViewData.ModelState.ErrorCount > 0);
        }

        [Fact]
        public async Task Login_ReturnsView_WhenModelIsNotValid()
        {
            _sut.ModelState.AddModelError("key", "error message");

            var result = await _sut.Login(null);

            Assert.IsType(typeof(ViewResult), result);
        }

        [Fact]
        public async Task Register_CallsRegisterMethod_WhenModelIsValid()
        {
            var model = new RegisterViewModel();

            await _sut.Register(model);

            _accountServiceMock.Verify(x => x.Register(It.IsAny<RegisterModel>()), Times.Once);
        }

        [Fact]
        public async Task Register_ReturnsRedirectToAction_WhenModelIsValid()
        {
            var model = new RegisterViewModel();

            var result = await _sut.Register(model);

            Assert.IsType(typeof(RedirectToActionResult), result);
        }

        [Fact]
        public async Task Register_AddedModelStateRecord_WhenThrowAuthException()
        {
            var listErrors = new List<string> { "error" };
            var model = new RegisterViewModel();

            _accountServiceMock.Setup(x => x.Register(It.IsAny<RegisterModel>())).Throws(new AuthException { Errors = listErrors });

            var result = await _sut.Register(model) as ViewResult;

            Assert.True(result?.ViewData.ModelState.ErrorCount > 0);
        }

        [Fact]
        public async Task Register_ReturnsView_WhenModelIsNotValid()
        {
            _sut.ModelState.AddModelError("key", "error message");

            var result = await _sut.Register(null);

            Assert.IsType(typeof(ViewResult), result);
        }
    }
}
