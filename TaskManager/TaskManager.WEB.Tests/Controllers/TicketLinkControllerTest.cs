﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using TaskManager.BLL.Interfaces;
using TaskManager.WEB.Controllers;
using Xunit;

namespace TaskManager.WEB.Tests.Controllers
{
    public class TicketLinkControllerTest : TestBase
    {
        private readonly TicketLinkController _sut;
        private readonly Mock<ITicketService> _ticketServiceMock;
        private readonly Mock<ITicketLinkService> _ticketLinkServiceMock;

        public TicketLinkControllerTest()
        {
           SetUp();
           _ticketServiceMock = new Mock<ITicketService>();
           _ticketLinkServiceMock = new Mock<ITicketLinkService>();
            
           _sut = new TicketLinkController(_ticketServiceMock.Object, Mapper, _ticketLinkServiceMock.Object);  
        }

        [Fact]
        public async Task GetLinkedTickets_ReturnsBadRequestView_WhenIdIsEmpty()
        {
            var result = await _sut.GetLinkedTickets(null) as ViewResult;

            Assert.Equal("BadRequest", result?.ViewName);
        }

        [Fact]
        public async Task GetLinkedTickets_CallsGetLinkedTicketsAsync_WhenIdIsValid()
        {
            var stubId = Guid.NewGuid();

            await _sut.GetLinkedTickets(stubId);

            _ticketLinkServiceMock.Verify(m => m.GetLinkedTickets(It.IsAny<Guid>()));
        }

        [Fact]
        public async Task GetLinkedTickets_CallsGetAsync_WhenIdIsValid()
        {
            var stubId = Guid.NewGuid();

            await _sut.GetLinkedTickets(stubId);

            _ticketServiceMock.Verify(m => m.GetAsync(It.IsAny<Guid>()));
        }

        [Fact]
        public async Task GetLinkedTickets_ReturnsLinkedTicketsView_WhenIdIsValid()
        {
            var stubId = Guid.NewGuid();

            var result = await _sut.GetLinkedTickets(stubId) as ViewResult;

            Assert.Equal("LinkedTickets", result?.ViewName);
        }

        [Fact]
        public async Task ManageLinks_ReturnsBadRequestView_WhenIdIsEmpty()
        {
            var result = await _sut.ManageLinks(null) as ViewResult;

            Assert.Equal("BadRequest", result?.ViewName);
        }

        [Fact]
        public async Task ManageLinks_CallsGetAsync_WhenIdIsValid()
        {
            var stubId = Guid.NewGuid();

            await _sut.ManageLinks(stubId);

            _ticketServiceMock.Verify(m => m.GetAsync(It.IsAny<Guid>()));
        }

        [Fact]
        public async Task ManageLinks_CallsGetLinkedTicketsAsync_WhenIdIsValid()
        {
            var stubId = Guid.NewGuid();

            await _sut.ManageLinks(stubId);

            _ticketLinkServiceMock.Verify(m => m.GetLinkedTickets(It.IsAny<Guid>()));
        }

        [Fact]
        public async Task ManageLinks_CallsGetUnlinkedTicketsAsync_WhenIdIsValid()
        {
            var stubId = Guid.NewGuid();

            await _sut.ManageLinks(stubId);

            _ticketLinkServiceMock.Verify(m => m.GetUnlinkedTickets(It.IsAny<Guid>()), Times.Once);
        }

        [Fact]
        public async Task ManageLinks_ReturnsLinkBoardView_WhenIdIsValid()
        {
            var stubId = Guid.NewGuid();

            var result = await _sut.ManageLinks(stubId) as ViewResult;

            Assert.Equal("LinkBoard", result?.ViewName);
        }

        [Fact]
        public async Task LinkTickets_VerifyCallLinkTicketsAsync_WhenIdsValid()
        {
            var stubId = Guid.NewGuid();

            await _sut.LinkTickets(stubId, stubId);

            _ticketLinkServiceMock.Verify(x => x.LinkTicketsAsync(It.IsAny<Guid>(), It.IsAny<Guid>()));
        }

        [Fact]
        public async Task UnLinkTickets_CallsLinkTicketsAsync_WhenIdsValid()
        {
            var stubId = Guid.NewGuid();

            await _sut.UnLinkTickets(stubId, stubId);

            _ticketLinkServiceMock.Verify(x => x.UnlinkTicketsAsync(It.IsAny<Guid>(), It.IsAny<Guid>()));
        }

        [Fact]
        public async Task LinkTickets_ReturnsRedirectToAction_WhenIdsValid()
        {
            var stubId = Guid.NewGuid();

            var result = await _sut.LinkTickets(stubId, stubId);

            Assert.IsType(typeof(RedirectToActionResult), result);
        }

        [Fact]
        public async Task UnLinkTickets_ReturnsRedirectToAction_WhenIdsValid()
        {
            var stubId = Guid.NewGuid();

            var result = await _sut.UnLinkTickets(stubId, stubId);

            Assert.IsType(typeof(RedirectToActionResult), result);
        }

        [Fact]
        public async Task LinkTickets_ReturnsViewResult__WhenIdsNull()
        {
            var result = await _sut.LinkTickets(null, null);

            Assert.IsType(typeof(ViewResult), result);
        }

        [Fact]
        public async Task LinkTickets_ReturnsViewResult__WhenFirstIdNull()
        {
            var stubId = Guid.NewGuid();

            var result = await _sut.LinkTickets(null, stubId);

            Assert.IsType(typeof(ViewResult), result);
        }

        [Fact]
        public async Task LinkTickets_ReturnsViewResult__WhenSecondIdNull()
        {
            var stubId = Guid.NewGuid();

            var result = await _sut.LinkTickets(stubId, null);

            Assert.IsType(typeof(ViewResult), result);
        }

        [Fact]
        public async Task UnLinkTickets_ReturnsViewResult__WhenIdsNull()
        {
            var result = await _sut.UnLinkTickets(It.IsAny<Guid?>(), It.IsAny<Guid?>());

            Assert.IsType(typeof(ViewResult), result);
        }
    }
}
