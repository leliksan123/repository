﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Mvc;
using TaskManager.Auth.Interfaces;
using TaskManager.BLL.Interfaces;
using TaskManager.WEB.Controllers;
using TaskManager.WEB.ViewModels.CommentViewModels;
using Xunit;
using Moq;

namespace TaskManager.WEB.Tests.Controllers
{
    public class CommentControllerTest : TestBase
    {
        private readonly CommentController _sut;
        private readonly Mock<ICommentService> _commentServiceMock;

        public CommentControllerTest()
        {
            SetUp();
            _commentServiceMock = new Mock<ICommentService>();
            var accountManagerMock = new Mock<IAccountManager>();
            _sut = new CommentController(
                _commentServiceMock.Object,
                accountManagerMock.Object,
                Mapper);
        }

        [Fact]
        public async Task Add_ReturnsViewWithValidationError_IfModelStateNotValid()
        {
            _sut.ModelState.AddModelError("test", "test");
            var commentViewModel = new CommentViewModel();

            await _sut.Add(commentViewModel);

            Assert.False(_sut.ModelState.IsValid);
        }

        [Fact]
        public async Task Delete_ReturnsBadRequestView_WhenIdNull()
        {
            var result = await _sut.Delete(null, null);
            var viewResult = result as ViewResult;

            Assert.Equal("BadRequest", viewResult?.ViewName);
        }

        [Fact]
        public async Task Delete_CallsDeleteAsync_WhenIdIsValid()
        {
            var stubId = Guid.NewGuid();

            await _sut.Delete(stubId, stubId);

            _commentServiceMock.Verify(x => x.DeleteAsync(It.IsAny<Guid>()), Times.Once);
        }

        [Fact]
        public async Task Delete_ReturnsRedirectResult_WhenIdIsValid()
        {
            var stubId = Guid.NewGuid();

            var result = await _sut.Delete(stubId, stubId);

            Assert.IsType(typeof(RedirectToActionResult), result);
        }

        [Fact]

        public async Task Add_ChecksRedirectRoute_IfModelStateIsValid()
        {
            var httpContextMock = new Mock<HttpContext>();
            var httpRequestMock = new Mock<HttpRequest>();

            httpRequestMock.Setup(m => m.Headers["Referer"]).Returns("/Test");
            httpContextMock.Setup(m => m.Request).Returns(httpRequestMock.Object);

            var actionContext = new ActionContext(
                    httpContextMock.Object,
                    new Mock<RouteData>().Object,
                    new Mock<ActionDescriptor>().Object);

            _sut.ControllerContext = new ControllerContext(new ActionContext()
            {
                RouteData = new RouteData(),
                HttpContext = actionContext.HttpContext,
                ActionDescriptor = new ControllerActionDescriptor()
            });

            var commentViewModel = new CommentViewModel();
            
            var result = await _sut.Add(commentViewModel);
            var redirectResult = result as RedirectResult;

            Assert.Equal(httpContextMock.Object.Request.Headers["Referer"].ToString(), redirectResult?.Url);
        }
    }
}