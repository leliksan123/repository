﻿using AutoMapper;

namespace LinkManager.BLL.Configuration
{
    public class ServiceAutoMapperConfiguration
    {
        public static void Initialize(IMapperConfigurationExpression cfg)
        {
            cfg.AddProfile(new BLLProfile());
        }
    }
}
