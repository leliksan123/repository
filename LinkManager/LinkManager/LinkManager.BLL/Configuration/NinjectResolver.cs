﻿using LinkManager.DAL.Interfaces;
using LinkManager.DAL.Realization;
using Ninject;

namespace LinkManager.BLL.Configuration
{
    public static class NinjectResolver
    {
        public static void Configure(IKernel kernel)
        {
            kernel.Bind<IUnitOfWork>().To<UnitOfWork>();
        }
    }
}
