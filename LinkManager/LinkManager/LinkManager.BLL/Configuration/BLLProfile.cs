﻿using AutoMapper;
using LinkManager.BLL.DTO;
using LinkManager.DAL.Entities;

namespace LinkManager.BLL.Configuration
{
    public class BLLProfile : Profile
    {
        public BLLProfile()
        {
            CreateMap<EnteredLink, EnteredLinkDto>().ReverseMap();

            CreateMap<ResponseTime, ResponseTimeDto>().ReverseMap();
        }
    }
}
