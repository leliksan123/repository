﻿using System.Collections.Generic;
using LinkManager.BLL.DTO;

namespace LinkManager.BLL.Interfaces
{
    public interface IResponseTimeService
    {
        IEnumerable<ResponseTimeDto> GetResponseTimeList(string testTime, string url);

        void CreateTime(IEnumerable<ResponseTimeDto> timeDto);
    }
}
