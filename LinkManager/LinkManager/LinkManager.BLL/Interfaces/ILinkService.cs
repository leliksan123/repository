﻿using System;
using System.Collections.Generic;
using LinkManager.BLL.DTO;

namespace LinkManager.BLL.Interfaces
{
    public interface ILinkService
    {
        List<EnteredLinkDto> GetLinks();
             
        List<ResponseTimeDto> TakeSitemapOrCreateIfNotExist(string url, EnteredLinkDto model);

        void CreateLink(EnteredLinkDto link);

        List<EnteredLinkDto> GetLinks(string searchString);

        void DeleteLink(int id);

        List<EnteredLinkDto> Sort(DateTime date);

        EnteredLinkDto GetLink(int id);
    }
}
