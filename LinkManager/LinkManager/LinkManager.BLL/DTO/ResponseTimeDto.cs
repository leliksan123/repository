﻿using System;

namespace LinkManager.BLL.DTO
{
    public class ResponseTimeDto
    {
        public string Url { get; set; }

        public long CurrentResponseTime { get; set; }

        public long MaxResponseTime { get; set; }

        public long MinResponseTime { get; set; }

        public DateTime TestTime { get; set; }
    }
}
