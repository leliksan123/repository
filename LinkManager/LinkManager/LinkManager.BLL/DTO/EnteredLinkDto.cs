﻿using System;

namespace LinkManager.BLL.DTO
{
    public class EnteredLinkDto
    {
        public int Id { get; set; }

        public string Url { get; set; }

        public DateTime TestedTime { get; set; }
    }
}
