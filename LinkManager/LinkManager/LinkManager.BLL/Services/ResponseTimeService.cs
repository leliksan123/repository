﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using LinkManager.BLL.DTO;
using LinkManager.BLL.Interfaces;
using LinkManager.DAL.Entities;
using LinkManager.DAL.Interfaces;

namespace LinkManager.BLL.Services
{
    public class ResponseTimeService : IResponseTimeService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ResponseTimeService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public IEnumerable<ResponseTimeDto> GetResponseTimeList(string testTime, string url)
        {
            var responseTimes = _mapper.Map<List<ResponseTimeDto>>(_unitOfWork.ResponseTimes.GetAll()
                .Where(f => f.TestTime.ToString("MM/dd/yyyy HH:mm:ss") == testTime && f.Url.Contains(url)));
            return responseTimes;
        }

        public void CreateTime(IEnumerable<ResponseTimeDto> timeDto)
        {
            foreach (var time in timeDto)
            {
                _unitOfWork.ResponseTimes.Create(_mapper.Map<ResponseTime>(time));
            }
            
            _unitOfWork.Save();
        }
    }
}
