﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using AutoMapper;
using HtmlAgilityPack;
using LinkManager.BLL.DTO;
using LinkManager.BLL.Interfaces;
using LinkManager.DAL.Entities;
using LinkManager.DAL.Interfaces;

namespace LinkManager.BLL.Services
{
    public class LinkService : ILinkService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public LinkService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public List<EnteredLinkDto> GetLinks()
        {
            var links = _mapper.Map<List<EnteredLinkDto>>(_unitOfWork.EnteredLinks.GetAll());

            return links;
        }

        public void CreateLink(EnteredLinkDto linkDto)
        {
            var link = _mapper.Map<EnteredLink>(linkDto);

            _unitOfWork.EnteredLinks.Create(link);
            _unitOfWork.Save();
        }

        public void DeleteLink(int id)
        {
            _unitOfWork.EnteredLinks.Delete(id);

            _unitOfWork.Save();
        }

        public EnteredLinkDto GetLink(int id)
        {
            var link = _mapper.Map<EnteredLinkDto>(_unitOfWork.EnteredLinks.GetById(id));

            return link;
        }

        public List<EnteredLinkDto> GetLinks(string searchString)
        {
            var links = _mapper.Map<List<EnteredLinkDto>>(_unitOfWork.EnteredLinks.GetAll().Where(x => x.Url.Contains(searchString)));

            return links;
        }

        public List<EnteredLinkDto> Sort(DateTime date)
        {
            var links = _mapper.Map<List<EnteredLinkDto>>(_unitOfWork.EnteredLinks.GetAll().Where(x => x.TestedTime.Date == date.Date));

            return links;
        }

        public List<ResponseTimeDto> TakeSitemapOrCreateIfNotExist(string url, EnteredLinkDto model)
        {
            var sitemaps = new List<ResponseTime>();
            var xmlDoc = new XmlDocument();

            try
            {
                xmlDoc.Load(url);

                foreach (XmlNode node in xmlDoc.GetElementsByTagName("url"))
                {
                    var item = new ResponseTime
                    {
                        Url = node["loc"]?.InnerText
                    };

                    sitemaps.Add(item);
                }
            }
            catch (WebException ex) when ((ex.Response as HttpWebResponse)?.StatusCode == HttpStatusCode.NotFound)
            {
                sitemaps = GenerateSitemap(model);
            }
            catch (XmlException)
            {
                sitemaps = GenerateSitemap(model);
            }
            catch (Exception)
            {
                return _mapper.Map<List<ResponseTimeDto>>(sitemaps);
            }

            return _mapper.Map<List<ResponseTimeDto>>(sitemaps);
        }

        private List<ResponseTime> GenerateSitemap(EnteredLinkDto model)
        {
            var sitemaps = new List<ResponseTime>();
            var htmlWeb = new HtmlWeb { OverrideEncoding = Encoding.UTF8 };
            var htmlDocument = htmlWeb.Load(model.Url);
            var rule = "//a[starts-with(@href, '/')] | //a[starts-with(@href, '.')] | //a[starts-with(@href, '" +
                       model.Url + "')]";

            foreach (var link in htmlDocument.DocumentNode.SelectNodes(rule))
            {
                string hrefValue;
                ResponseTime item;
                if (link.OuterHtml.Contains(model.Url))
                {
                    hrefValue = WebUtility.HtmlDecode(link.GetAttributeValue("href", string.Empty));
                    item = new ResponseTime
                    {
                        Url = hrefValue
                    };
                }
                else
                {
                    hrefValue = WebUtility.HtmlDecode(link.GetAttributeValue("href", string.Empty).Substring(1));

                    var index = hrefValue.LastIndexOf("#", StringComparison.Ordinal);

                    if (index > 0)
                    {
                        hrefValue = hrefValue.Substring(0, index);
                    }
                        
                    item = new ResponseTime
                    {
                        Url = model.Url + hrefValue
                    };
                }

                sitemaps.Add(item);
            }

            sitemaps = sitemaps.GroupBy(l => l.Url).Select(group => group.First()).ToList();

            return sitemaps;
        }
    }
}
