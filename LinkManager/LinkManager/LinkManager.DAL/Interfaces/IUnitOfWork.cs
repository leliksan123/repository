﻿using LinkManager.DAL.Entities;

namespace LinkManager.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<EnteredLink> EnteredLinks { get; }

        IRepository<ResponseTime> ResponseTimes { get; }

        void Save();
    }
}
