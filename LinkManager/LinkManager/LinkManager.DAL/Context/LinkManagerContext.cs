﻿using System.Data.Entity;
using LinkManager.DAL.Entities;

namespace LinkManager.DAL.Context
{
    public class LinkManagerContext : DbContext
    {
        public LinkManagerContext()
            : base("name=LinkManager")
        {
        }

        public virtual DbSet<EnteredLink> Links { get; set; }

        public virtual DbSet<ResponseTime> ResponseTimes { get; set; }
    }
}
