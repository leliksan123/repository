﻿using System;

namespace LinkManager.DAL.Entities
{
    public class ResponseTime
    {
        public int Id { get; set; }

        public string Url { get; set; }

        public long CurrentResponseTime { get; set; }

        public long MaxResponseTime { get; set; }

        public long MinResponseTime { get; set; }

        public DateTime TestTime { get; set; }
    }
}
