﻿using System;

namespace LinkManager.DAL.Entities
{
    public class EnteredLink
    {
        public int Id { get; set; }

        public string Url { get; set; }

        public DateTime TestedTime { get; set; }
    }
}
