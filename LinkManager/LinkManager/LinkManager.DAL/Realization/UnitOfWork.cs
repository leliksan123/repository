﻿using System;
using LinkManager.DAL.Context;
using LinkManager.DAL.Entities;
using LinkManager.DAL.Interfaces;

namespace LinkManager.DAL.Realization
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly LinkManagerContext _context;
        private readonly Lazy<IRepository<EnteredLink>> _enteredLinkRepository;

        private readonly Lazy<IRepository<ResponseTime>> _responseTimeRepository;

        public UnitOfWork(LinkManagerContext context)
        {
            _context = context;
            _enteredLinkRepository = new Lazy<IRepository<EnteredLink>>(() => new CommonRepository<EnteredLink>(_context));

            _responseTimeRepository = new Lazy<IRepository<ResponseTime>>(() => new CommonRepository<ResponseTime>(_context));
        }

        public IRepository<EnteredLink> EnteredLinks => _enteredLinkRepository.Value;

        public IRepository<ResponseTime> ResponseTimes => _responseTimeRepository.Value;

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
