﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using LinkManager.DAL.Context;
using LinkManager.DAL.Interfaces;

namespace LinkManager.DAL.Realization
{
    public class CommonRepository<T> : IRepository<T> where T : class
    {
        private readonly LinkManagerContext _context;
        private readonly DbSet<T> _dbSet;

        public CommonRepository(LinkManagerContext context)
        {
            _context = context;
            _dbSet = context.Set<T>();
        }

        public virtual IEnumerable<T> GetAll()
        {
            return _dbSet.ToList();
        }

        public virtual T GetById(int id)
        {
            return _dbSet.Find(id);
        }

        public virtual void Create(T newItem)
        {
            _dbSet.Add(newItem);
        }

        public virtual void Update(T editItem)
        {
            _context.Entry(editItem).State = EntityState.Modified;
        }

        public virtual void Delete(int id)
        {
            var entityToDelete = _dbSet.Find(id);

            Delete(entityToDelete);
        }

        protected virtual void Delete(T deleteItem)
        {
            if (_context.Entry(deleteItem).State == EntityState.Detached)
            {
                _dbSet.Attach(deleteItem);
            }

            _dbSet.Remove(deleteItem);
        }
    }
}
