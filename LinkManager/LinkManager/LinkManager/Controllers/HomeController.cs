﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using AutoMapper;
using LinkManager.BLL.DTO;
using LinkManager.BLL.Interfaces;
using LinkManager.ViewModels;

namespace LinkManager.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILinkService _linkService;
        private readonly IResponseTimeService _responseTimeService;
        private readonly IMapper _mapper;
        private static readonly List<ResponseTimeViewModel> List = new List<ResponseTimeViewModel>();

        public HomeController(ILinkService linkService, IResponseTimeService responseTimeService, IMapper mapper)
        {
            _linkService = linkService;
            _responseTimeService = responseTimeService;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var model = new LinksListViewModel
            {
                EnteredLinks = _mapper.Map<IEnumerable<EnteredLinkViewModel>>(_linkService.GetLinks())
            };

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Index(EnteredLinkViewModel model)
        {
            var dateTime = DateTime.UtcNow;
            var stopwatch = new Stopwatch();

            if (ModelState.IsValid)
            {
                var link = _mapper.Map<EnteredLinkDto>(model);
                link.TestedTime = dateTime;
                _linkService.CreateLink(link);

                var sitemaps = _linkService.TakeSitemapOrCreateIfNotExist(model.Url, _mapper.Map<EnteredLinkDto>(model));

                await IterateLinks(sitemaps, stopwatch, dateTime);

                var res = SortLinks(model, dateTime);

                var resultOrder = res.OrderByDescending(x => x.CurrentResponseTime).ToList();

                _responseTimeService.CreateTime(_mapper.Map<List<ResponseTimeDto>>(resultOrder));

                return PartialView("_PartialResults", resultOrder);
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            _linkService.DeleteLink(id);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Search(string searchString)
        {
            var links = _linkService.GetLinks(searchString);

            var model = new LinksListViewModel {EnteredLinks = _mapper.Map<IEnumerable<EnteredLinkViewModel>>(links)};

            return View("Index", model);
        }

        [HttpGet]
        public ActionResult SortByDate(DateTime date)
        {
            var links = _linkService.Sort(date);

            var model = new LinksListViewModel {EnteredLinks = _mapper.Map<IEnumerable<EnteredLinkViewModel>>(links)};

            return View("Index", model);
        }

        [HttpGet]
        public FileResult GetTestInFile(string testTime, string url)
        {
            var responseTimes = _mapper.Map<IEnumerable<ResponseTimeViewModel>>(_responseTimeService.GetResponseTimeList(testTime, url));

            var ser = new JavaScriptSerializer().Serialize(responseTimes);

            var byteArray = Encoding.ASCII.GetBytes(ser);
            var stream = new MemoryStream(byteArray);

            return File(stream, "text/plain", "Report.txt");
        }

        private async Task IterateLinks(List<ResponseTimeDto> sitemaps, Stopwatch stopwatch, DateTime dateTime)
        {
            for (var i = 0; i < sitemaps.Count; i++)
            {
                using (var client = new HttpClient())
                {
                    stopwatch.Start();

                    try
                    {
                        var response = await client.GetAsync(sitemaps[i].Url);
                        response.EnsureSuccessStatusCode();
                    }
                    catch (Exception e)
                    {
                        ViewBag.Error = "Some problems with testing" + e.StackTrace;
                    }

                    stopwatch.Stop();

                    var item = new ResponseTimeViewModel
                    {
                        CurrentResponseTime = stopwatch.ElapsedMilliseconds,
                        Url = sitemaps[i].Url,
                        TestTime = dateTime
                    };

                    List.Add(item);
                }
            }
        }

        private IEnumerable<ResponseTimeViewModel> SortLinks(EnteredLinkViewModel model, DateTime dateTime)
        {
            var res =
                List.Where(x => x.Url.Contains(model.Url)).GroupBy(y => y.Url).Select(z => new ResponseTimeViewModel
                {
                    Url = z.Key,
                    MaxResponseTime = z.Max(rec => rec.CurrentResponseTime),
                    MinResponseTime = z.Min(rec => rec.CurrentResponseTime),
                    CurrentResponseTime = z.Select(rec => rec.CurrentResponseTime).LastOrDefault(),
                    TestTime = dateTime
                }).ToList();

            return res;
        }
    }
}