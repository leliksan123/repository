﻿using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using LinkManager.BLL.Interfaces;
using LinkManager.ViewModels;

namespace LinkManager.Controllers
{
    public class HistoryController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IResponseTimeService _responseTimeService;

        public HistoryController(IResponseTimeService responseTimeService, IMapper mapper)
        {
            _responseTimeService = responseTimeService;
            _mapper = mapper;
        }

        public PartialViewResult Index(string testTime, string url)
        {
            var responseTimes = _mapper.Map<IEnumerable<ResponseTimeViewModel>>(_responseTimeService.GetResponseTimeList(testTime, url));

            return PartialView("/Views/Home/_PartialResults.cshtml", responseTimes);
        }
    }
}