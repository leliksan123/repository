﻿using System;

namespace LinkManager.ViewModels
{
    public class EnteredLinkViewModel
    {
        public int Id { get; set; }

        public string Url { get; set; }

        public DateTime TestedTime { get; set; }
    }
}