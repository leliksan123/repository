﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LinkManager.ViewModels
{
    public class LinksListViewModel
    {
        [Required]
        [DataType(DataType.Url)]
        public string Url { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime TestedTime { get; set; }

        public IEnumerable<EnteredLinkViewModel> EnteredLinks { get; set; }
    }
}