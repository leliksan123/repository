﻿using System;

namespace LinkManager.ViewModels
{
    public class ResponseTimeViewModel
    {
        public string Url { get; set; }

        public long CurrentResponseTime { get; set; }

        public long MaxResponseTime { get; set; }

        public long MinResponseTime { get; set; }

        public DateTime TestTime { get; set; }
    }
}