﻿using AutoMapper;
using LinkManager.BLL.DTO;
using LinkManager.ViewModels;

namespace LinkManager.Configuration
{
    public class WEBProfile : Profile
    {
        public WEBProfile()
        {
            CreateMap<EnteredLinkViewModel, EnteredLinkDto>().ReverseMap();

            CreateMap<ResponseTimeDto, ResponseTimeViewModel>().ReverseMap();
        }
    }
}