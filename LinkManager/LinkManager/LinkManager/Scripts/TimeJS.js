﻿$(document).ready(function () {
    $("abbr.timeago").timeago();
    $("#hide")
        .click(function () {
            $("#hd").hide();
        });
});

$(function () {
    $('.datetimepicker').datepicker();
});

$(document).ready(function () {
    $("a[data-type='deleteModal']").click(function () {
        var dataAttr = $(this).attr("data-value");
        var link = $(".confirmDelete");
        var href = link.attr("href");
        var path = href.split("?");
        $(".confirmDelete").attr("href", path[0] + '?id=' + dataAttr);
    });
});
