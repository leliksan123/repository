﻿using AutoMapper;
using LinkManager.Configuration;

namespace LinkManager.UnitTests
{
    public class TestBase
    {
        protected IMapper Mapper { get; private set; }

        public virtual void SetUp()
        {
            Mapper = new AutoMapperConfiguration().Configure().CreateMapper();
        }
    }
}
