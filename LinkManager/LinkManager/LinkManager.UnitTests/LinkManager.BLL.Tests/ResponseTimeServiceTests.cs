﻿using System.Collections.Generic;
using LinkManager.BLL.Services;
using LinkManager.DAL.Entities;
using LinkManager.DAL.Interfaces;
using Moq;
using NUnit.Framework;

namespace LinkManager.UnitTests.LinkManager.BLL.Tests
{
    [TestFixture]
    public class ResponseTimeServiceTests : TestBase
    {
        private Mock<IUnitOfWork> _unitOfWork;

        private ResponseTimeService _responseTimeService;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            _unitOfWork = new Mock<IUnitOfWork>();

            _responseTimeService = new ResponseTimeService(_unitOfWork.Object, Mapper);
        }

        [Test]
        public void GetResponseTimeList_VerifyGetAll()
        {
            var list = new List<ResponseTime>
            {
                new ResponseTime()
            };

            _unitOfWork.Setup(x => x.ResponseTimes.GetAll()).Returns(list);

            var result = _responseTimeService.GetResponseTimeList(It.IsAny<string>(), It.IsAny<string>());

            _unitOfWork.Verify(x => x.ResponseTimes.GetAll(), Times.Once);
        }
    }
}
