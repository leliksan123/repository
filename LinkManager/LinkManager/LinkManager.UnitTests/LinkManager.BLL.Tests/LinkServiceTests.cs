﻿using System.Collections.Generic;
using LinkManager.BLL.DTO;
using LinkManager.BLL.Services;
using LinkManager.DAL.Entities;
using LinkManager.DAL.Interfaces;
using Moq;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace LinkManager.UnitTests.LinkManager.BLL.Tests
{
    [TestFixture]
    public class LinkServiceTests : TestBase
    {
        private Mock<IUnitOfWork> _unitOfWork;

        private LinkService _linkService;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            _unitOfWork = new Mock<IUnitOfWork>();

            _linkService = new LinkService(_unitOfWork.Object, Mapper);
        }

        [Test]
        public void GetLinks_ReturnsNotEmptyList()
        {
            var list = new List<EnteredLink>
            {
                new EnteredLink()
            };

            _unitOfWork.Setup(x => x.EnteredLinks.GetAll()).Returns(list);

            var result = _linkService.GetLinks();

            _unitOfWork.Verify(x=>x.EnteredLinks.GetAll(), Times.Once);
            Assert.AreNotEqual(0, result.Count);
        }

        [Test]
        public void CreateLink_VerifySaveMethod()
        {
            _unitOfWork.Setup(x => x.EnteredLinks.Create(It.IsAny<EnteredLink>()));
            var dto = new EnteredLinkDto();

             _linkService.CreateLink(dto);

            _unitOfWork.Verify(x => x.EnteredLinks.Create(It.IsAny<EnteredLink>()), Times.Once);
            _unitOfWork.Verify(x=>x.Save(), Times.Once);
        }

        [Test]
        public void GetLink_ReturnsLinkWithId()
        {
            var link = new EnteredLink
            {
                Id = 1
            };

            _unitOfWork.Setup(x => x.EnteredLinks.GetById(It.IsAny<int>())).Returns(link);

            var result = _linkService.GetLink(It.IsAny<int>());

            _unitOfWork.Verify(x => x.EnteredLinks.GetById(It.IsAny<int>()), Times.Once);

            Assert.AreEqual(link.Id, result.Id);
        }
    }
}
